<?php
namespace BaseTheme;

/**
* Loads the base theme class.  The base_theme_class is extended here.
* Please see wiki documentation for full set of features and helpers available in the base_theme_class.
*/
include_once('includes/base-theme-class.php' );
include_once('includes/cws-theme-class.php');
include_once( 'CW-widgets.php' );
include_once( 'CW.php' );

class Theme extends base_theme_class {

    var $version = '1.0';

    var $theme_name = 'legal-optimized';

    var $include_jquery = true;

    var $load_options_panel = true;

    var $disabled_theme_editor = true;

    var $load_thumbnail_support = true;

    var $excerpt_text = 'Read More';

    var $force_enable_acf_option_panel = true;

    /* Load more custom post types here */
    public function load_custom_post_types()
    {
        if(get_field('enable_testimonials', 'option'))
        {
            $this->custom_post_types['testimonial'] = array(

                'label' => 'Testimonials',
                'description' => 'This is the testimonial custom post type',
                'public' => false,
                'exclude_from_search' => true,
                'show_ui' => true,
                'supports' => array('title', 'thumbnail'),
                'has_archive' => false,
                'rewrite' => false,
                'menu_icon' => 'dashicons-format-quote'
            );

        }


        if(get_field('enable_case_results', 'option'))
        {

            $this->custom_post_types['case-result'] = array(

                'label' => 'Case Results',
                'description' => 'This is the case results custom post type',
                'public' => false,
                'exclude_from_search' => true,
                'show_ui' => true,
                'supports' => array('title', 'editor'),
                'has_archive' => false,
                'rewrite' => false,
                'menu_icon' => 'dashicons-shield'
            );

        }


        if(get_field('enable_faqs', 'option'))
        {
            $this->custom_post_types['faq'] = array(

                'label' => 'FAQs',
                'description' => 'This is the FAQ custom post type',
                'public' => false,
                'exclude_from_search' => true,
                'show_ui' => true,
                'supports' => array('title', 'editor'),
                'has_archive' => false,
                'rewrite' => false,
                'menu_icon' => 'dashicons-clipboard'
            );
        }


        $this->custom_post_types['partner-logo'] = array(

            'label' => 'Partner Logos',
            'description' => 'This is the partner logos custom post type',
            'public' => false,
            'exclude_from_search' => true,
            'show_ui' => true,
            'supports' => array('title', 'thumbnail'),
            'has_archive' => false,
            'rewrite' => false,
            'menu_icon' => 'dashicons-images-alt'
        );



//        if(get_field('enable_community', 'option'))
//        {
//            $this->custom_post_types['community'] = array(
//
//                'label' => 'Communities',
//                'description' => 'This is the communities custom post type',
//                'public' => true,
//                'exclude_from_search' => false,
//                'show_ui' => true,
//                'supports' => array('title', 'editor', 'thumbnail'),
//                'has_archive' => false,
//                'rewrite' => true,
//                'menu_icon' => 'dashicons-universal-access-alt'
//            );
//        }

        if(get_field('enable_events', 'option'))
        {
            $this->custom_post_types['event'] = array(

                'label' => 'Events',
                'description' => 'This is the events custom post type',
                'public' => true,
                'exclude_from_search' => false,
                'show_ui' => true,
                'supports' => array('title', 'editor'),
                'has_archive' => false,
                'rewrite' => true,
                'menu_icon' => 'dashicons-megaphone'

            );
        }

        $this->custom_post_types['cw_video'] = array(

            'label' => 'Videos',
            'description' => 'This is the videos custom post type',
            'public' => true,
            'exclude_from_search' => false,
            'show_ui' => true,
            'supports' => array('title', 'editor'),
            'has_archive' => false,
            'rewrite' => true,
            'menu_icon' => 'dashicons-video-alt2'

        );

        $this->custom_post_types['sidebar'] = array(

            'label' => 'Custom Sidebars',
            'description' => 'This is the custom sidebar custom post type',
            'public' => false,
            'exclude_from_search' => false,
            'show_ui' => true,
            'supports' => array('title'),
            'has_archive' => false,
            'rewrite' => false,
            'menu_icon' => 'dashicons-menu'
        );
    }


    public function load_custom_taxonomies()
    {


        if(get_field('enable_case_results', 'option'))
        {
            $this->custom_taxonomies['case-result-category'] = array(

                'belongs_to_post_type' => 'case-result',
                'label' => 'Case Result Categories',
                'description' => 'These are the categories used to sort case results',
                'public' => true,
                'hierarchical' => true

            );
        }

        if(get_field('enable_faqs', 'option'))
        {
            $this->custom_taxonomies['faq-category'] = array(

                'belongs_to_post_type' => 'faq',
                'label' => 'FAQ Categories',
                'description' => 'These are the categories used to sort faq results',
                'public' => true,
                'hierarchical' => true

            );
        }
    }

public function load_shortcodes()
    {



        if(get_field('enable_case_results', 'option'))
        {
            /**
             * Case results short code that outputs a feed of case results.
             * Usage: [cw_case_results category="{case-result-category-slug}"]
             * @param  category (optional)
             * @return case result items with ajax load more functionality
             */
            add_shortcode( 'cw_case_results', function($atts) {
                ob_start();
                get_template_part('includes/shortcodes/case-results');
                return ob_get_clean();

            });
        }

        /**
             * Video shortcode.  Output a video thumbnail
             * Usage: [cw_video_in_content id="{id-of-video}"]
             * @param  id (required)
             * @return video box
             */
        add_shortcode('cw_video_in_content', function($atts) {
            ob_start();
            get_template_part('includes/shortcodes/video');
            return ob_get_clean();
        });


        if(get_field('enable_faqs', 'option'))
        {
           /**
             * FAQ short code that outputs a feed of FAQs.
             * Usage: [cw_faqs category="1"] OR [cw_faqs category="1,2,3,4"] OR  [cw_faqs category="1,2,3,4" tabbed="true"]
             * @param  category (optional)
             * @param  tabbed (optional)
             * @return faq items with ajax load more functionality, and optional sort by tab if multiple categories are passed into category option.
             */
            add_shortcode( 'cw_faqs', function($atts) {
                ob_start();
                //get_template_part('includes/shortcodes/faqs');
                include(locate_template('includes/shortcodes/faqs.php'));
                return ob_get_clean();
            });
        }

        if(get_field('enable_testimonials', 'option'))
        {
            /**
             * Testimonial short code that outputs a feed of testimonials.
             * Usage: [cw_testimonials] [cw_testimonials exclude="4,5,7"] [cw_testimonials include="1,3,5"]
             * @param  exclude (optional)
             * @param  include (optional)
             * @return testimonial items with ajax load more functionality, and optional exclude or include options.
             */
            add_shortcode( 'cw_testimonials', function($atts) {
                ob_start();
                get_template_part('includes/shortcodes/testimonials');
                return ob_get_clean();
            });
        }

        /**
         * Contact form shortcode to output a contact form in the content area.
         * Usage: [cw_contact_form title="Contact Us Now"] OR [cw_contact_form]
         * @param  title (optional)
         * @return contact form
         */
        add_shortcode( 'cw_contact_form', function($atts) {

            return get_template_part('includes/shortcodes/contact-form');

        });

        add_shortcode( 'cw_social', function($atts) {
            ob_start();
            get_template_part('includes/shortcodes/social');
              return ob_get_clean();

        });

        add_shortcode( 'community-form', function($atts) {
          ob_start();
          include(locate_template('includes/shortcodes/community-form.php'));
          return ob_get_clean();
        });


        if(get_field('enable_community', 'option'))
        {
            /**
             * Community short code that outputs a feed of community posts.
             * Usage: [cw_community] [cw_community exclude="4,5,7"] [cw_community include="1,3,5"]
             * @param  exclude (optional)
             * @param  include (optional)
             * @return community items with ajax load more functionality, and optional exclude or include options.
             */
            add_shortcode( 'cw_community', function($atts) {
                ob_start();
                get_template_part('includes/shortcodes/community');
                return ob_get_clean();
            });
        }
    }




    public function load_sidebars()
    {


        register_sidebar(array(
            'name'          => 'Default Sidebar',
            'id'            => 'default-sidebar',
            'before_widget' => '<div class="block %1$s %2$s">',
            'after_widget'  => '</div>',
            'before_title'  => '<h3>',
            'after_title'   => '</h3>',
        ));

        register_sidebar(array(
            'name'          => 'Blog Sidebar',
            'id'            => 'blog-sidebar',
            'before_widget' => '<div class="block %1$s %2$s">',
            'after_widget'  => '</div>',
            'before_title'  => '<h3>',
            'after_title'   => '</h3>',
        ));



        /* need to add caching to this query */
        $sidebars = new \WP_Query(array(
            'post_type' => 'sidebar',
            'orderby' => 'date',
            'posts_per_page' => -1
        ));
        while( $sidebars->have_posts() ) : $sidebars->the_post();

            register_sidebar(array(
                'name'          => get_the_title(),
                'id'            => 'custom-sidebar-' . get_the_ID(),
                'before_widget' => '<section class="widget %1$s %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h3>',
                'after_title'   => '</h3>',
            ));

        endwhile;
        wp_reset_postdata();


    }

    public function load_options_panel()
    {

        acf_add_options_page(array(
            'page_title'    => 'ConsultWebs Theme Options',
            'menu_title'    => 'CW Options',
            'menu_slug'     => 'cw-theme-options-settings',
            'capability'    => 'edit_posts',
            'redirect'      => true
        ));

        acf_add_options_page(array(
            'page_title'    => 'Widget Options',
            'menu_title'    => 'Widget Options',
            'menu_slug'     => 'widget-options-settings',
            'capability'    => 'edit_posts',
            'redirect'      => true
        ));


        /*acf_add_options_sub_page(array(
            'page_title'    => 'Header & Footer Options',
            'menu_title'    => 'Header / Footer',
            'parent_slug'   => 'theme-options-settings',
        ));

        acf_add_options_sub_page(array(
            'page_title'    => 'JavaScript & CSS Options',
            'menu_title'    => 'Javascript / CSS',
            'parent_slug'   => 'theme-options-settings',
        ));*/



    }

    public function set_menus()
    {

        $this->menus = array(
//            'main_nav' => 'Main Navigation',
//            'footer_nav' => 'Footer Navigation',
//            'drop_nav' => 'Mobile Header Menu'
        );

    }

    /**
    * Set the image size array.
    *
    * $image_sizes[] = array('name' => 'image-size-name', 'width' => 600, 'height' => 400, 'crop' => true)
    * set width/height to 9999 to not force that size.
    * set crop to false to not force the size.
    */
    public function set_image_sizes()
    {

        $this->image_sizes[] = array(
            'name' => 'video-preview',
            'width' => 272,
            'height' => 174,
            'crop' =>true
        );

        $this->image_sizes[] = array(
            'name' => 'testimonial',
            'width' => 170,
            'height' => 170,
            'crop' =>true
        );

        $this->image_sizes[] = array(
            'name' => 'blog-single',
            'width' => 670,
            'height' => 306,
            'crop' =>true
        );

        $this->image_sizes[] = array(
            'name' => 'attorney',
            'width' => 355,
            'height' => 355,
            'crop' =>true
        );
    }

}

$theme = new \BaseTheme\Theme;

add_theme_support( 'post-thumbnails' );
?>
