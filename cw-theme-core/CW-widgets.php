<?php


class CWTeamWidget extends WP_Widget {

    function __construct() {

        parent::__construct( false, 'CW Team Box' );
    }

    function widget( $args, $instance ) {

       get_template_part('includes/widgets/team');

    }

    function update( $new_instance, $old_instance ) {

    }

    function form( $instance ) {

    }
}

class CWpracticeAreas extends WP_Widget {

    function __construct() {

        parent::__construct( false, 'CW Practice Areas' );
    }

    function widget( $args, $instance ) {

       get_template_part('includes/widgets/practice-areas');

    }

    function update( $new_instance, $old_instance ) {

    }

    function form( $instance ) {

    }
}

class CWAttorneyCreds extends WP_Widget {

    function __construct() {

        parent::__construct( false, 'CW Attorney Credentials' );
    }

    function widget( $args, $instance ) {

       get_template_part('includes/widgets/attorney-credentials');

    }

    function update( $new_instance, $old_instance ) {

    }

    function form( $instance ) {

    }
}

class CWBlogWidget extends WP_Widget {

    function __construct() {

        parent::__construct( false, 'CW Blog Post' );
    }

    function widget( $args, $instance ) {

        get_template_part('includes/widgets/blog-post');
    }

    function update( $new_instance, $old_instance ) {

    }

    function form( $instance ) {

    }
}

class CWResultsWidget extends WP_Widget {

    function __construct() {

        parent::__construct( false, 'CW Results' );
    }

    function widget( $args, $instance ) {

        get_template_part('includes/widgets/results');
    }

    function update( $new_instance, $old_instance ) {

    }

    function form( $instance ) {

    }
}

class CWContactFormWidget extends WP_Widget {

    function __construct() {

        parent::__construct( false, 'CW Contact Form' );
    }

    function widget( $args, $instance ) {

        get_template_part('includes/widgets/contact-form');
    }

    function update( $new_instance, $old_instance ) {

    }

    function form( $instance ) {

    }
}

class CWRelatedPagedWidget extends WP_Widget {

    function __construct() {

        parent::__construct( false, 'CW Related Pages' );
    }

    function widget( $args, $instance ) {

        get_template_part('includes/widgets/related-pages');
    }

    function update( $new_instance, $old_instance ) {

    }

    function form( $instance ) {

    }
}

class CWTestimonialWidget extends WP_Widget {

    function __construct() {

        parent::__construct( false, 'CW Testimonial' );
    }

    function widget( $args, $instance ) {

        get_template_part('includes/widgets/testimonial');
    }

    function update( $new_instance, $old_instance ) {

    }

    function form( $instance ) {

    }
}

class CWButtonBox extends WP_Widget {

    function __construct() {

        parent::__construct( false, 'CW Button Box' );
    }

    function widget( $args, $instance ) {

        get_template_part('includes/widgets/button-box');
    }

    function update( $new_instance, $old_instance ) {

    }

    function form( $instance ) {

    }
}

class CWLocation extends WP_Widget {

    function __construct() {

        parent::__construct( false, 'CW Locations' );
    }

    function widget( $args, $instance ) {

        get_template_part('includes/widgets/location');
    }

    function update( $new_instance, $old_instance ) {

    }

    function form( $instance ) {

    }
}
