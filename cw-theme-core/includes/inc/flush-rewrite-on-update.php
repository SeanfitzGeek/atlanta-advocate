<?php

/* Created by Connor Hood */
class CW_Flush_Rewrite {

    protected $option_key;

    public function __construct()
    {

        $this->option_key = 'cw_should_flush_rewrites';

        add_action('acf/save_post', array($this, 'flush_rewrite_rules'),20);

        add_action('wp', array($this, 'flush'));
    }

    public function flush_rewrite_rules()
    {


        if(isset($_POST['acf']['field_5660502941ebd']))
        {   

            update_option($this->option_key, 'true');

        }

    }


    public function flush()
    {
        if(delete_option($this->option_key))
        {
            
            flush_rewrite_rules(true);
 
        }
    }
}


$color_scheme = new CW_Flush_Rewrite();