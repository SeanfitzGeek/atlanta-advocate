<?php

/* Created by Connor Hood */
class CW_Color_Scheme_Updater {
    public $path_to_vars;

    public function __construct()
    {
        $this->path_to_vars = get_stylesheet_directory() . '/css/sass/_dynamic-vars.scss';

        add_action('acf/save_post', array($this, 'update_scss_var_file'), 20);
    }
    public function color_scheme_template()
    {

        $serif = get_field('font_family_serif','option');
        $sans_serif = get_field('font_family_sans_serif','option');

        $colors = array(
            '$source' => '"' . $sans_serif['font'] . '"',
            '$droid' => '"' . $serif['font'] . '"',
            '$col-base' => get_field('primary_theme_color','option'),
            '$col-dark' => get_field('darker_theme_color','option'),
            '$col-light' => get_field('secondary_theme_color','option'),
            '$col-gray' => get_field('gray_theme_color','option'),
            '$col-attention' => get_field('theme_link_color','option'),
            '$col-font-std' => get_field('default_text_color','option'),
        );
        $template  = '';
        foreach($colors as $key => $value)
        {
            $template .= $key . ':' . $value . ';';
        }
        return $template;
    }
    public function update_scss_var_file()
    {
        if(isset($_POST['acf']['field_56603be91da8f']))
        {
            file_put_contents($this->path_to_vars, $this->color_scheme_template());
        }
    }
}


$color_scheme = new CW_Color_Scheme_Updater();
