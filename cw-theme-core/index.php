<?php get_header(); ?>

<section class="content-wrapper">
    <div class="container">
        <div class="row">
            <article>
                <h1><?php echo  Helper::index_title() ; ?></h1>
                <div class="blog-wrapper">

                    <?php if ( have_posts() ) : ?>
                    <div class="post-ajax-wrapper">
                    <?php while ( have_posts() ) : the_post(); ?>


                    <div class="blog-item">
                        <h2><?php echo  get_the_title() ; ?></h2>
                        <div class="date"><i><?php echo  the_time('F') ; ?></i><span><?php echo  the_time('j') ; ?></span></div>
                        <div class="meta">
                            by <?php echo  get_the_author_link() ; ?>
                            <?php if(get_post_type() == 'post'): ?>
                            <span class="sep">|</span><?php echo  the_category(', ') ; ?>
                            <?php endif; ?>
                        </div>
                        <div class="preview">
                            <?php echo  the_excerpt() ; ?>
                        </div>
                        <div class="tools">

                            <?php if(get_field('enable_comments', 'option')): ?>
                                <a href="#">Comments (0)</a><span class="sep">|</span><a href="#">Reply</a>
                            <?php endif; ?>

                            <a href="<?php echo  get_permalink() ; ?>" class="pull-right read-more">Read more</a>
                        </div>
                    </div>

                    <?php endwhile; ?>
                    </div><!--post-ajax-wrapper-->

                    <div class="btn-load-more-ajax" data-load-more=".btn-load-more-ajax a" data-target=".post-ajax-wrapper .blog-item" data-append-to=".post-ajax-wrapper">
                        <?php echo  next_posts_link('Load more posts') ; ?>
                    </div>

                    <?php else: ?>
                    <div class="blog-item" style="padding-left: 0">
                        <h2>No results found.</h2>

                        <div class="preview">
                            <p>Sorry, we could not find what you were looking for.  Try searching for something else in the sidebar.</p>
                        </div>
                        <div class="tools">
                            <a href="<?php echo  get_bloginfo('url') ; ?>" class="pull-right read-more">Back to home page.</a>
                        </div>
                    </div>
                    <?php endif; ?>

                </div>
            </article>

            <?php get_template_part('sidebars/blog'); ?>
        </div>
    </div>
</section>

<?php get_template_part('partials/partner-logos'); ?>

<?php get_footer(); ?>
