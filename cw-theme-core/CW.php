<?php 

/* A class for rapid ConsultWeb WordPress theme development. */
class CW {


    public static function get_first_word($string)
    {

        $parts = explode(" ", $string);

        return $parts[0];
    }



    public static function related_pages()
    {

        global $post;

        $children = wp_list_pages('title_li=&child_of='.$post->ID.'&echo=0&depth=1');

        $custom_menu = get_field('custom_sidebar_menu', $post->ID);


        if($custom_menu)
        {

            $output = '';
            foreach($custom_menu as $menu)
            {

                $permalink = get_permalink($menu->ID);

                $output .= "<li><a href='{$permalink}'>{$menu->post_title}</a></li>";

            }
        
            return $output;
        }

        if($children)
        {

            return $children;

        }
  

        return false;
    }


    public static function sitemap_archives()
    {

        if(is_page('site-map'))
        {

            $output  = '<ul class="site-map list-triangle">';

            $output .= wp_list_pages('title_li&echo=0');


            $output .= '</ul>';

            echo $output;
        }


        if(is_page('archives'))
        {

            $output  = '<h2>Archives by Month:</h2>';
            $output .= '<ul class="site-map">' . wp_get_archives('type=monthly&echo=0') . '</ul>';

            $output .= '<h2>Archives by Subject:</h2>';
            $output .= '<ul class="site-map">' . wp_list_categories('title_li&echo=0') . '</ul>';


            echo $output;
        }  

    }

    /**
     * Returns h1 field from a post or page.
     *
     * @param  $id   - integer
     * @param  $echo - boolean  
     * @return title
     */
    public static function h1_title($id=false, $echo=true){
        global $post;

        $title = "";
        if($id)
        {
            $title = get_field('h1_title', $id)? get_field('h1_title', $id) : get_the_title($id);   
        }
        else
        {
            $title = get_field('h1_title', $post->ID)? get_field('h1_title', $post->ID) : get_the_title($post->ID);
        }

        if($title != "")
        {

            if($echo)
            {
                echo $title;
            }
            else
            {
                return $title;
            }

        }
        else
        {

            return false;

        }
    }

    /*
     $params = array(
          'hide_name' => true, //or remove, defaults to false
          'show_state' => true,
          'show_country' => false,
          'show_phone' => true,
          ’show_phone_2’ => false,
          ’show_fax’ => false,
          'oneline' => false,
          'show_opening_hours' => false
     );
    */
    public static function get_address($params)
    {
        if( function_exists( 'wpseo_local_show_address' ) )
        {   
            return wpseo_local_show_address( $params );
        }
    }


    /*
        $params = array(
            'width' => 390,
            'height' => 150,
            'zoom' => 12,
            'show_route' => false
        );
    */
    public static function get_map($params)
    {
        if( function_exists( 'wpseo_local_show_map' ) )
        {   
            $params['map_style'] = 'roadmap';
            $params['show_route'] = false;
            return wpseo_local_show_map( $params );
        }
    }

    public static function get_opening_hours($params)
    {

        if ( function_exists( 'wpseo_local_show_opening_hours' ) )
        {
            return wpseo_local_show_opening_hours( $params );
        }

    }
}