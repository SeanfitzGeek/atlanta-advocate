<?php


/**
 * Customize the classes added to next/prev post links
 */
add_filter('next_posts_link_attributes', 'posts_link_attributes');
add_filter('previous_posts_link_attributes', 'posts_link_attributes');
function posts_link_attributes() {
    return 'class="next-posts-link"';
}



/**
 * Adds the ability for v-card to be uploaded to media uploader.  Add additional mime types here as needed.
 */
add_filter('upload_mimes', 'custom_upload_mimes');
function custom_upload_mimes ( $existing_mimes=array() ) {
    // add your extension to the array
    $existing_mimes['vcf'] = 'text/x-vcard';
    return $existing_mimes;
}




function cw_register_widgets() {

    unregister_widget('WP_Widget_Archives');
    register_widget( 'CWResultsWidget' );
    register_widget( 'CWPracticeAreas' );
    register_widget( 'CWAttorneyCreds' );
    register_widget( 'CWTeamWidget' );
    register_widget( 'CWContactFormWidget' );
    register_widget( 'CWBlogWidget' );
    register_widget( 'CWRelatedPagedWidget' );
    register_widget( 'CWTestimonialWidget' );
    register_widget( 'CWButtonBox' );
    register_widget( 'CWLocation' );

}

add_action( 'widgets_init', 'cw_register_widgets' );
