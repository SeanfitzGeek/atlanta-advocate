<?php /* Template Name: Practice Areas */ ?>

<?php get_header('dark-blue'); ?>

<section class="content-wrapper wide">

    <div class="container">

        <div class="row">
            <article>
                <h1><?php the_title(); ?></h1>
				<?php the_content(); ?>
                <div id="areas-main">
                    <div class="placeholder">
						<?php foreach ( get_field( 'practice_areas' ) as $area ): ?>
                            <div class="col sameHeight">
                                <div class="item">
                                    <header>
                                        <img src="<?php echo $area['background_image']; ?>"/>
                                        <div class="ico"><img src="<?php echo $area['icon']; ?>"/></div>
                                        <h3><?php echo $area['practice_area']; ?></h3>
                                    </header>
                                    <div class="content">
                                        <ul>
											<?php foreach ( $area['areas'] as $page ): ?>
                                                <li><a href="<?php echo get_the_permalink( $page->ID ); ?>"><?php echo get_the_title( $page->ID ); ?></a></li>
											<?php endforeach; ?>
                                        </ul>
                                        <a href="<?php echo $area['cta_link']; ?>"><?php echo $area['cta_text']; ?></a>
                                    </div>
                                </div>
                            </div>
						<?php endforeach; ?>
                    </div>
                </div>
            </article>

            <aside>
				<?php get_template_part( 'sidebars/default' ); ?>
            </aside>

        </div>

    </div>

</section>

<?php get_footer('dark-blue'); ?>
