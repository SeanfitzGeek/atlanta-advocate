<?php get_header('dark-blue'); ?>
<section class="content-wrapper">
    <div class="container">
        <div class="row">
            <article>
                <h1><?php h1_title(); ?><?php if ( get_field( 'job_title' ) ): ?><span><?php the_field( 'job_title' ); ?></span><?php endif; ?></h1>
				<?php if ( is_page( 'site-map' ) ) :?>
					<div class='sitemap-container'>
                        <?php get_template_part( 'partials/sitemap' ) ?>
                    </div>
				<?php else : ?>
                    <p>
						<?php if ( has_post_thumbnail() ): ?>
                            <img src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>" class="thumbnail image left"/>
						<?php endif; ?>
						<?php the_content(); ?>
                    </p>
				<?php endif ?>
            </article>
            
            <aside>
				<?php
                if (!isset($_GET['lang'])):
                    get_template_part( 'sidebars/multi-custom-related-pages' );
                elseif (isset($_GET['lang'])):
//	                get_template_part( 'sidebars/multi-custom-related-pages-test' );
                endif;
                ?>
				<?php get_template_part( 'sidebars/default' ); ?>
            </aside>
        </div>
    </div>
</section>
<?php get_footer('dark-blue'); ?>