<?php get_header('dark-blue'); ?>

<section class="content-wrapper">
    <div class="container">
        <div class="row">
            <article>

                <h1>Locations</h1>

				<?php
				$the_query = new WP_Query( 'post_type=wpseo_locations&posts_per_page=-1&orderby=menu_order&order=ASC' ); $counter = 0;
				while ( $the_query->have_posts() ) : $the_query->the_post();
					?>

                    <div class="block side-address clearfix">
                        <div class="address-holder">
                            <div class="address">
                                <div class="address-header">
                                    <h3><?php echo  get_the_title(); ?></h3>
                                </div>
                                <address>
                                    <h4><a href="<?php the_permalink(); ?>"><strong>The Millar Law Firm</strong></a></h4>
                                    <p><?php echo  get_field('_wpseo_business_address') ; ?></p>
                                    <?php if(get_field( '_wpseo_business_address_2' )) { ?><p><?php echo get_field( '_wpseo_business_address_2' ); ?></p><?php } ?>
                                    <p><?php echo  get_field('_wpseo_business_city') ; ?>, <?php echo  get_field('_wpseo_business_state') ; ?> <?php echo  get_field('_wpseo_business_zipcode') ; ?></p>
                                    <p class="ctm-no-swap"><?php echo  get_field('_wpseo_business_phone') ; ?></p>
                                    <div class="buttons">
                                        <a href="<?php the_field('directions_link'); ?>" class="but-directions lazyload" target="_blank">Directions</a>
                                        <a href="#" class="but-hours">Hours</a>
                                    </div>
                                </address>
                                <div class="popup-hours">
                                    <h5>Hours of Operation</h5>
                                    <ul>
										<?php for($n = 1; $n < 6; $n++): ?>
											<?php $day = date('l', strtotime('Sunday +' . $n . ' days')); ?>
                                            <li class="clearfix"><span><?php echo $day ?></span>
                                                <b>
													<?php if(date('g:iA', strtotime(get_field('_wpseo_opening_hours_' . strtolower($day) . '_from'))) == '12:00AM' && date('g:iA', strtotime(get_field('_wpseo_opening_hours_' . strtolower($day) . '_to'))) == '12:00AM'):
														echo 'Closed';
													else:
														echo date('g:iA', strtotime(get_field('_wpseo_opening_hours_' . strtolower($day) . '_from'))) . '-' . date('g:iA', strtotime(get_field('_wpseo_opening_hours_' . strtolower($day) . '_to')));
													endif; ?>
                                                </b>
                                            </li>
										<?php endfor; ?>

                                    </ul>
                                    <p>Scheduled appointments after hours</p>
                                    <div class="popup-arrow"></div>
                                </div>
                            </div>
                            <div class="map-holder">
                                <div data-coords="<?php echo get_field('_wpseo_coordinates_lat'); ?>, <?php echo get_field('_wpseo_coordinates_long'); ?>" data-marker="1" id="map-<?php echo $counter; ?>" class="map"></div>
                            </div>
                        </div>
                    </div>
					<?php /*if(get_field('show_team')): ?>
                        <h2><?php the_field('team_title'); ?></h2>
                        <p><?php the_field('team_description'); ?></p>

                        <div id="meet-location">
                            <ul>
								<?php foreach(get_field('team_members') as $team): ?>
                                    <li>
                                        <a href="<?php echo get_the_permalink($team); ?>">
                                            <figure>
												<?php echo get_the_post_thumbnail($team); ?>
                                                <figcaption><span><?php echo get_the_title($team); ?></span> <?php echo get_field('job_title', $team); ?></figcaption>
                                            </figure>
                                        </a>
                                    </li>
								<?php endforeach; ?>
                            </ul>
                        </div>
					<?php endif;*/ ?>
					<?php $counter++; endwhile; wp_reset_postdata(); ?>

	            <?= getPracticeAreasHtml(); ?>
            </article>
            <aside>
				<?php get_template_part('sidebars/default'); ?>
            </aside>
        </div>
    </div>
</section>

<?php get_footer('dark-blue'); ?>
