<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta name="p:domain_verify" content="f26323dcfd98864d56b4360762be67d6"/>
    <meta charset="<?php bloginfo( 'charset' ); ?>"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="format-detection" content="telephone=no">
    <meta name="google-site-verification" content="Eh_s11a_sIMxHqJeFXgm263m8asgyOYBUumsNbbcfEI"/>

    <title><?php wp_title( '|', true, 'right' ); ?></title>

    <!--<link rel="profile" href="//gmpg.org/xfn/11" />
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" /> NOT SURE ABOUT THESE -->
    <!-- enable noydir and noodp in SEO > Titles & Metas -->

    <link href="#" rel="publisher"/>

    <!-- Google Tag Manager -->
    <!--    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':-->
    <!--                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],-->
    <!--            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=-->
    <!--            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);-->
    <!--        })(window,document,'script','dataLayer','GTM-WF4XL8F');</script>-->


    <script>
        document.addEventListener('wpcf7mailsent', function (event) {
            dataLayer.push({
                'event': 'wpcf7successfulsubmit',
                'CF7formID': event.detail.contactFormId
            });
        }, false);
    </script>

	<?php if ( get_field( 'favicon', 'option' ) ): ?>
        <link rel="shortcut icon" href="<?php echo get_field( 'favicon', 'option' ); ?>"/>
	<?php endif; ?>
    <!--<link rel="apple-touch-icon" type="image/png" href="/wp-content/themes/avrek/images/apple-touch-icon.png">
	<link rel="apple-touch-icon" type="image/png" sizes="72x72" href="/wp-content/themes/avrek/images/apple-touch-icon-72.png">
	<link rel="apple-touch-icon" type="image/png" sizes="114x114" href="/wp-content/themes/avrek/images/apple-touch-icon-114.png">-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="//ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
    <script src="//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="//oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

	<?php wp_head(); ?>
<?php
if (function_exists('icl_object_id')) {
$lang = $post->ID !== icl_object_id( $post->ID, 'page', true, 'es' ) ? '?lang=es' : '';
} else {
    $lang = '';
}

?>
    <script>
        if ('scrollRestoration' in history) {
            // Tell browser it can restore scroll automatically (e.g. if we want to use on-page links that jump to one section,
            // and we want to allow the back button to bring us back to the same point we were at, previously)
            history.scrollRestoration = 'auto';
        }
    </script>
</head>

<body <?php body_class(); ?>>
    <!-- Google Tag Manager (noscript) -->
    <!--            <noscript>-->
    <!--                <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WF4XL8F"-->
    <!--                        height="0" width="0" style="display:none;visibility:hidden"></iframe>-->
    <!--            </noscript>-->
    <!-- End Google Tag Manager (noscript) -->
    <!-- Facebook noscript tracking -->
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=320668835423599&ev=PageView&noscript=1"/></noscript>
    <!-- End of Facebook Noscript Tracking -->

    <div class="page-inner-area site-header-fixed">
        <div id="wrapper">
            <header class="site-header-dark-blue fixed-header navbar-fixed-top">
                <div class="container fluid-container-head">
                    <div class="row">
                        <div class="col-md-5">
                            <a href="/">
                                <img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/white_gold_logo.png?b5=5"
                                     alt="Atlanta Advocates Logo" class="img-responsive site-logo center-block">
                            </a>
                            <div class="header-left-buttons hidden-xs hidden-sm">

                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="header-right-buttons">
                                <a href="tel:770-400-0000" class="btn btn-blue">770-400-0000</a>
                                <a href="tel:770-400-0000" class="btn btn-white">
                                    <span class="hidden-xs hidden-sm">CALL NOW</span>
                                    <span class="visible-xs visible-sm hidden-md hidden-lg">TAP TO CALL</span>
                                </a>
                                <a href="/contact-us" class="btn btn-blue">CONTACT US 24/7</a>

                            </div>

                            <button class="navbar-toggle " type="button" data-toggle="collapse"
                                    data-target="#main-navbar"
                                    aria-controls="main-navbar" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="glyphicon glyphicon-menu-hamburger color-dark-blue"
                                      aria-hidden="true"></span>
                            </button>
							<?php
							wp_nav_menu( array(
								"theme_location"  => "header-dark-blue",
								"menu_id"         => "main-navigation",
								'depth'             => 2,
								"container"       => "div",
								'container_id'    => 'main-navbar',
								"container_class" => "site-header-navigation navbar-collapse collapse",
								'menu_class'        => 'nav navbar-nav',
								'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
								'walker'          => new WP_Bootstrap_Navwalker(),
							) );

							?>
                        </div>
                    </div>
                </div>
            </header>