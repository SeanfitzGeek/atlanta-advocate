<?php get_header( 'dark-blue' ); ?>
<?php if ( have_posts() ) : ?>
	<?php while ( have_posts() ) : the_post(); ?>

        <section class="search-results-header">
            <div class="container">
                <div class="search-query-result">
                    <h2>Search Results For: <strong><?php the_search_query(); ?></strong></h2>
                </div>
                <div class="new-search">
                    <form action="/" method="get" class="noauto">
                        <div class="input-group">
                            <input type="text" name="s" id="search" class="form-control form-search"
                                   placeholder="SEARCH" value="<?php the_search_query(); ?>"/>
                            <div class="input-group-btn">
                                <button class="btn btn-primary" type="submit">
                                    <span class="glyphicon glyphicon-search"></span>
                                </button>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </section>

		<?php
		$title    = '';
		$title    = 'Evidence That Strengthens Georgia Motorcycle Accident Cases';
		$category = '';
		$category = 'Legal Guide Category';
		$content  = '';
		$content  = 'When your claim involves a personal injury after a motorcycle accident, it is important to gather evidence as quickly as possible after the crash. The longer evidence goes without being collected, the more likely it will be difficult or even impossible to find later.';

		?>
        <div class="search-result">
            <h3><?= $title; ?></h3>
            <div class="result-category">
				<?= $category; ?>
            </div>

            <div class="result-content">
				<?= $content; ?>
            </div>

            <div class="result-questions-answered">
                <div class="question-title">
                    <h3>Questions Answered in this legal guide:</h3>
                </div>

                <div class="question-sub-title">
                    Click any of the questions below to quickly navigate to that section in the legal guide.
                </div>

                <ul class="questions">
                    <li>What types of evidence can be used in a motorcycle accident case?</li>
                </ul>

                <div class="search-result-footer">
                    <a class="left" href="">View Legal Guide</a>
                    <a class="right" href="">Speak with an Attorney</a>
                </div>
            </div>
        </div>
	<?php endwhile; ?>
<?php endif; ?>
<?php wp_reset_postdata(); ?>

    <div class="btn-load-more-ajax" data-load-more=".btn-load-more-ajax a"
         data-target=".post-ajax-wrapper .blog-item" data-append-to=".post-ajax-wrapper">
		<?php echo next_posts_link( 'Load more posts' ); ?>
    </div>
    <aside>
		<?php get_template_part( 'sidebars/blog' ); ?>
    </aside>
<?php get_footer( 'dark-blue' ); ?>