<?php
get_header( 'dark-blue' );
add_filter( 'the_content', 'add_ids_to_header_tags' );


?>
    <div class="single-article-template">

		<?php while ( have_posts() ) : the_post(); ?>
			<?php
			$content          = apply_filters( 'the_content', get_the_content() );
			$content          = str_replace( ']]>', ']]&gt;', $content );
			$content          = htmlspecialchars_decode( utf8_decode( htmlentities( $content, ENT_COMPAT, 'utf-8', false ) ) );
			$sidebarArray     = generate_navigation( $content );
			$mainContentWidth = '';
			$columns          = 'col-sm-12 col-md-8 col-lg-9';
			$postcat = get_the_category( get_the_ID() );
			$learnMoreBlock = $postcat ? get_field( 'category_learn_more_block', 'category_' . $postcat[0]->cat_ID ) : '';
			ob_start(); ?>
			<?php if ( ! array_filter( $sidebarArray ) > 0 ) : ?>
				<?php
				$mainContentWidth = 'main-full-width';
				$columns          = 'col-sm-12 col-md-12 col-lg-12';
				?>
                <div class="quick-link-block">
                    <ul>
						<?php
						$i = 0;
						foreach ( $sidebarArray as $h2Slug => $h3Array ) {
							$i ++;

							if ( empty( $h2Slug ) ) {
								continue;
							}

							if ( count( $h3Array['children'] ) === 0 ) :
								printf( '<li><a class="init-scroll-to" data-scroll-to="#%1$s" href="#%1$s"><span class="glyphicon glyphicon-chevron-right"></span>%2$s</a></li>', formatSlugForId( $h2Slug ), $h3Array['title'] );
							else :
								printf( '<li><a role="button" data-toggle="collapse"  class="sidebar-toggle collapsed"
                                                href="#position-%s" aria-expanded="false">%s <span class="glyphicon glyphicon-chevron-right"></span></a></li>', $i, $h3Array['title'] );
								?>
                                <div class="collapse" id="position-<?= $i; ?>">
                                    <ul>
										<?php foreach ( $h3Array['children'] as $h3 ) :
											if ( empty( $h3 ) ) {
												continue;
											}

											printf( '<li><a class="init-scroll-to" data-scroll-to="#%1$s" href="#%1$s"><span class="glyphicon glyphicon-chevron-right"></span>%2$s</a></li>', formatSlugForId( $h3['slug'] ), $h3['title'] );
										endforeach; ?>
                                    </ul>
                                </div>
							<?php endif; ?>
						<?php } ?>
                    </ul>
                </div>
			<?php endif; ?>

			<?php $sidebar = ob_get_clean(); ?>
            <section class="content-wrapper">
                <div class="container">

                    <div class="row">
                        <div class="col-xs-12 col-sm-3">
                            <div style="padding-top:25px;padding-bottom:50px; text-align: center">
                                <img data-src="<?= get_stylesheet_directory_uri(); ?>/assets/images/award-2.png"
                                     class="lazyload img-responsive" style="display: block;margin: 0 auto 20px;">
                                <img data-src="<?= get_stylesheet_directory_uri(); ?>/assets/images/badge-highest-rating.jpg"
                                     class="lazyload img-responsive"
                                     style="display: block;margin: 0 auto;max-width: 200px;">
                            </div>
                        </div>
                        <div class="top-title col-md-6">

                            <h1><?php the_title(); ?></h1>
                            <div id="share"></div>

                            <div class="row">
                                <div class="col-sm-5">
                                </div>

                                <div class="col-sm-7">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div style="padding-top:30px;padding-bottom:50px; text-align: center">
                                <img data-src="<?= get_stylesheet_directory_uri(); ?>/assets/images/million-dollar-advocates-lg.png"
                                     class="lazyload img-responsive"
                                     style="display: block; max-width: 125px;margin:0 auto 15px">
                                <img data-src="<?= get_stylesheet_directory_uri(); ?>/assets/images/super_lawyer.png"
                                     class="lazyload img-responsive"
                                     style="display:block;max-width: 225px;margin:0 auto">
                            </div>
                        </div>
                    </div>
                    <div class="link-sidebar">
						<?php if ( ! empty( $sidebarArray ) ) : ?>

                            <div class="quick-links-sidebar add-affix-left">
                                <div class="article-quick-links ">
                                    <div class="sidebar-opener visible-sm visible-xs" data-toggle="collapse"
                                         data-target="#blog-sidebar" aria-expanded="false" aria-controls="blog-sidebar">
                                        Article Outline
                                    </div>
                                    <div id="blog-sidebar" class="collapse" aria-expanded="true">
                                        <div class="quick-link-block-select">
                                            <h3 class="hidden-xs hidden-sm">Quickly Navigate</h3>

                                            <ul>
												<?php
												$i = 0;
												foreach ( $sidebarArray as $h2Slug => $h3Array ) {
													$i ++;


													if ( empty( $h2Slug ) ) {
														continue;
													}

													if ( $h2Slug === 'no-h2' ) :
														foreach ( $h3Array['children'] as $h3 ) :
															if ( empty( $h3 ) ) {
																continue;
															}

															printf( '<li><a class="init-scroll-to" data-scroll-to="#%1$s" href="#%1$s"><span class="glyphicon glyphicon-chevron-right"></span>%2$s</a></li>', formatSlugForId( $h3['slug'] ), $h3['title'] );
														endforeach;

                                                    elseif ( count( $h3Array['children'] ) === 0 ) :
														printf( '<li><a class="init-scroll-to" data-scroll-to="#%1$s" href="#%1$s"><span class="glyphicon glyphicon-chevron-right"></span>%2$s</a></li>', formatSlugForId( $h2Slug ), $h3Array['title'] );
													else :
														printf( '<li><a role="button" data-toggle="collapse"  class="sidebar-toggle collapsed"
                                                href="#position-%s" aria-expanded="false">%s <span class="glyphicon glyphicon-chevron-right"></span></a></li>', $i, $h3Array['title'] );
														?>
                                                        <div class="collapse" id="position-<?= $i; ?>">
                                                            <ul>
																<?php foreach ( $h3Array['children'] as $h3 ) :
																	if ( empty( $h3 ) ) {
																		continue;
																	}

																	printf( '<li><a class="init-scroll-to" data-scroll-to="#%1$s" href="#%1$s"><span class="glyphicon glyphicon-chevron-right"></span>%2$s</a></li>', formatSlugForId( $h3['slug'] ), $h3['title'] );
																endforeach; ?>
                                                            </ul>
                                                        </div>
													<?php endif; ?>
												<?php } ?>
                                            </ul>
                                        </div>
                                    </div>

                                </div>
                            </div>
						<?php endif; ?>
                    </div>

                    <div class="article-main-content <?= $mainContentWidth; ?>">
                        <article>

                            <div class="article-container">
								<?php the_content(); ?>

                            </div>
                        </article>
                    </div>


                    <div class="right-sidebar">
                        <div class="quick-link-block">
							<?php if ( trim(strlen( $learnMoreBlock )) > 0) : ?>
                                <?php printf('<div class="learn-more-buttons">%s</div>', $learnMoreBlock); ?>
							<?php endif; ?>
                            <div class="contact-form" id="main-contact-form">
                                <div class="form-box-container">
                                    <h2 class="">Have a Personal Injury Question?</h2>
                                    <p class="bold" style="font-size: 14px;font-weight: 700 !important;">One of our
                                        attorneys will be happy to respond at no cost or
                                        obligation.</p>

									<?= do_shortcode( '[contact-form-7 id="115831" title="Single Page Question Form_copy"]' ); ?>
                                    <small style="color: #FFF; font-size: 12px;">Your submission and information is
                                        private. We will follow up with you, but
                                        <span class="color-yellow"> you will not be added to any email or postal mailing lists.</span></small>
                                </div>
                            </div>

							<?php if ( have_rows( 'other_info_links' ) ) : ?>
                                <div class="link-blocks other-info">
                                    <h3><?= $otherInfoTitle; ?></h3>
                                    <div class="other-information">
                                        <ul>
											<?php
											while ( have_rows( 'other_info_links' ) ): the_row();
												$title  = get_sub_field( 'title' );
												$urlObj = get_sub_field( 'other_link' );
												?>
                                                <li>
                                                    <a href="<?= $urlObj['url']; ?>">
                                                        <span class="glyphicon glyphicon-chevron-right"></span>
														<?= $title; ?>
                                                    </a>
                                                </li>
											<?php endwhile; ?>
                                        </ul>
                                    </div>
                                </div>
							<?php endif; ?>

                            <div class="get-in-touch">
                                <h3>Get in Touch</h3>
                                <ul>
                                    <li>
                                        <a href="/contact-us">
                                            <img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/contact-us/Icon-awesome-envelope-open-text.png"
                                                 alt="Mail Icon" class="img-responsive center-block img-icon">
                                            Use our Contact Form</a>
                                    </li>

                                    <li>
                                        <a href="tel:770-400-0000" class="">
                                            <img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/contact-us/Icon-awesome-phone-alt.png"
                                                 alt="Chat Icon"
                                                 class="img-responsive center-block enter-block img-icon">
                                            Call Us (We Answer 24/7)</a>
                                    </li>

                                    <li>
                                        <a href="#" role="button" class="btn-chat">
                                            <img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/contact-us/Icon-ionic-ios-chatboxes.png"
                                                 alt="Chat Icon"
                                                 class="img-responsive center-block enter-block img-icon">
                                            Chat With Us</a>
                                    </li>
                                </ul>
                            </div>


							<?php if ( have_rows( 'related_topics' ) ): ?>
                                <div class="related-topics">
                                    <h3>Related Topics</h3>

                                    <ul>
										<?php
										while ( have_rows( 'related_topics' ) ): the_row();
											$title  = get_sub_field( 'related_topics_title' );
											$urlObj = get_sub_field( 'related_topics_other_link' );
											?>
                                            <li><a href="<?= $urlObj['url']; ?>"><span
                                                            class="glyphicon glyphicon-chevron-right"></span><?= $title; ?>
                                                </a>
                                            </li>
										<?php endwhile; ?>
                                    </ul>
                                </div>
							<?php endif; ?>
                        </div>
                    </div>
                </div>
            </section>
		<?php endwhile; ?>
    </div>

    <section class="ask-a-question">
        <div class="container">
            <h2>Have a Question?</h2>
            <p>Contact us with any questions you have and we’ll get back to you!</p>
            <div class="question-input">
                <div class="input-group">
                    <input type="text" class="form-control user-question-response"
                           name="user_question_response"
                           placeholder="Ask A Question">
                    <span class="input-group-btn">
            <button type="button" class="btn btn-red btn-user-response">Submit</button>
                </span>
                </div>
            </div>
            <div class="question-form-fields" style="display: none;">
				<?= do_shortcode( '[contact-form-7 id="104668" title="Ask A Question"]' ); ?>
            </div>

        </div>
    </section>

<?php get_footer( 'dark-blue' ); ?>