<?php
/*
  * Template Name: FAQ Template
  */
?>

<?php get_header('dark-blue'); ?>
<section class="content-wrapper">
    <div class="container">
        <div class="row">
            <article>
                <h1><?php h1_title(); ?><?php if ( get_field( 'job_title' ) ): ?>
                        <span><?php the_field( 'job_title' ); ?></span><?php endif; ?></h1>


                    <div class="col-md-5">
                        <p style="font-size:16px">We've curated a list of hundreds of our clients' most frequently asked questions. Search for
                            topics related to your case and see what others in your situation wanted to know.</p>
                    </div>

                    <div class="col-md-7">
                        <div class="faq-search header-search search-box">
                            <form method="get" class="noauto" action="/">
                                <div class="input-group">
                                    <input type="text" name="search" class="form-control form-search"
                                           placeholder="SEARCH"
                                           value="<?= isset( $_REQUEST['search'] ) ? $_REQUEST['search'] : ''; ?>"/>
                                    <div class="input-group-btn">
                                        <button class="btn btn-primary" type="submit">
                                            <span class="glyphicon glyphicon-search"></span>
                                        </button>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </form>
                        </div>
                    </div>

                    <div class="clearfix"></div>

				<?php if ( is_page( 'site-map' ) ) : ?>
                    <div class='sitemap-container'>
						<?php get_template_part( 'partials/sitemap' ) ?>
                    </div>
				<?php else : ?>
                    <p>
						<?php if ( has_post_thumbnail() ): ?>
                            <img src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>"
                                 class="thumbnail image left"/>
						<?php endif; ?>
						<?php the_content(); ?>
                    </p>
				<?php endif ?>
            </article>
            <aside>
				<?php get_template_part( 'sidebars/default' ); ?>
            </aside>
        </div>
    </div>
</section>
<?php get_footer('dark-blue'); ?>
