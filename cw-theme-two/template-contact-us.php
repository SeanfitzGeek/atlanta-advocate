<?php
/*
  * Template Name: Contact Us Version 2 Template
  */

get_header( 'dark-blue' );

$locations = get_posts(
	[
		'post_type'      => 'wpseo_locations',
		'post_status'    => 'publish',
		'posts_per_page' => 10,
		'orderby'        => 'post_title',
		'order'          => 'ASC'

	] );
?>

    <section class="contact-us-main-banner lazyload"
             data-bg="<?= get_stylesheet_directory_uri(); ?>/assets/images/contact-us/scales-of-justice.jpg">

    </section>

    <section class="how-contact">
        <h2>How would you like to contact us?</h2>
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <div class="box-container">
                        <a href="#main-contact-form">
                            <img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/contact-us/Icon-awesome-envelope-open-text.png"
                                 alt="Mail Icon" class="img-responsive center-block">
                        </a>
                        <h3>Use Our Contact Form</h3>
                        <p>Contact us today. Hear from an attorney TODAY.</p>

                        <a href="#" class="btn btn-red init-scroll-to" data-scroll-to="#main-contact-form">Contact Us</a>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="box-container">
                        <a href="#" class="btn-chat">
                            <img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/contact-us/Icon-ionic-ios-chatboxes.png"
                                 alt="Chat Icon" class="img-responsive center-block">
                        </a>
                        <h3>Chat With Us</h3>
                        <p>Live chat online with us and we will respond to you instantly.</p>

                        <a href="#" class="btn btn-red btn-chat">START CHATTING</a>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="box-container">
                        <a href="tel:770-400-0000">
                            <img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/contact-us/Icon-awesome-phone-alt.png"
                                 alt="Phone Icon" class="img-responsive center-block">
                        </a>
                        <h3>Call Us</h3>
                        <p>We answer 24/7. Call Today. Speak to an attorney TODAY.</p>

                        <a href="tel:770-400-0000" class="btn btn-red init-call">Click To Call</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="our-locations">
        <h2>Visit our locations</h2>
        <div class="container-lg">
            <div class="locations-container">
				<?php $counter = 0;
				foreach ( $locations

				as $location ) :
				$counter ++;
				$alignmentClass = $counter % 2 === 0 ? 'left' : 'right';
				$mapBoxHtml     = sprintf( '
                    <div class="box-holder %s"><div class="map-box" data-lat="%s" data-long="%s"></div></div>', $alignmentClass,
					get_field( '_wpseo_coordinates_lat', $location->ID ), get_field( '_wpseo_coordinates_long', $location->ID ) );

				if ( $counter % 2 !== 0 ) :
					echo $mapBoxHtml;
				endif;
				?>

                <div class="locations-info-box <?= $alignmentClass; ?>">
                    <div class="location-header">
                        <a href="<?= get_permalink( $location->ID ); ?>">
                            <h3 class="text-center">
								<?= $location->post_title; ?>
                            </h3>
                        </a>
                    </div>

                    <div class="content">
                        <div class="row marg-top-15">
                            <div class="col-xs-4">
                                <div class="office-info-title">Address</div>
                            </div>
                            <div class="col-xs-8">
                                <div class="office-info">
                                    <address>
                                        <div itemprop="address">
                                            <span itemprop="streetAddress"><?php echo get_field( '_wpseo_business_address', $location->ID ); ?></span><br/>
											<?php if ( get_field( '_wpseo_business_address_2', $location->ID ) ) { ?>
                                                <span itemprop="streetAddress"><?php echo get_field( '_wpseo_business_address_2', $location->ID ); ?></span>
                                                <br/>
											<?php } ?>

                                            <span itemprop="addressLocality"><?php echo get_field( '_wpseo_business_city', $location->ID ); ?></span>,
                                            <span itemprop="addressRegion"><?php echo get_field( '_wpseo_business_state', $location->ID ); ?></span>
                                            <span itemprop="postalCode"><?php echo get_field( '_wpseo_business_zipcode', $location->ID ); ?></span><br/>
                                        </div>
                                    </address>
                                </div>
                            </div>
                        </div>

                        <div class="row marg-top-15">
                            <div class="col-xs-4">
                                <div class="office-info-title">Phone</div>
                            </div>

                            <div class="col-xs-8">
                                <div class="office-info">
                                    <span itemprop="telephone"><?php echo get_field( '_wpseo_business_phone', $location->ID ); ?></span>
                                </div>
                            </div>
                        </div>


						<?php if ( get_field( '_wpseo_business_notes_1', $location->ID ) ): ?>
                            <div class="row marg-top-15">
                                <div class="col-xs-4">
                                    <div class="office-info-title">MAILING ADDRESS</div>
                                </div>
                                <div class="col-xs-8">
                                    <div class="office-info">
                                <span>
                                    <?php echo get_field( '_wpseo_business_notes_1', $location->ID ); ?>
                                    <br/>
                                    <?php echo get_field( '_wpseo_business_notes_2', $location->ID ); ?>
                                </span>
                                    </div>
                                </div>
                            </div>
						<?php endif; ?>

                        <div class="get-directions">
                            <a href="<?php echo get_field( 'directions_link', $location->ID ); ?>" target="_blank">Get
                                Directions</a>
                        </div>

                    </div>
                </div>

				<?php
				if ( $counter % 2 === 0 ) :
					echo $mapBoxHtml;
				endif;
				?>
            </div>

            <div class="clear"></div>
			<?php endforeach; ?>
        </div>
    </section>

    <section class="contact-form lazyload" id="main-contact-form"
             data-bg="<?= get_stylesheet_directory_uri(); ?>/assets/images/contact-us/over-city-skyline-atlanta-ga-downtown-dusk-georgia-PF48CFM.jpg">
        <div class="container">
            <div class="form-box-container">
                <h2 class="">Receive a FREE Case Evaluation</h2>
                <p>*All information submitted is secure & confidential.
                    All submissions will be sent immediately to our attorneys and legal staff, who will reach out to
                    you, free of charge, and advise you on the next steps to take.</p>

				<?= do_shortcode( '[contact-form-7 id="104671" title="Contact Us Form"]' ); ?>

            </div>

        </div>
    </section>

    <section class="ask-a-question">
        <h2>Have a Question?</h2>
        <p>Contact us with any questions you have and we’ll get back to you!</p>
        <div class="question-input">
            <div class="input-group">
                <input type="text" class="form-control user-question-response" name="user_question_response"
                       placeholder="Ask A Question">
                <span class="input-group-btn">
            <button type="button" class="btn btn-red btn-user-response">Submit</button>
                </span>
            </div>
        </div>
        <div class="question-form-fields" style="display: none;">
			<?= do_shortcode( '[contact-form-7 id="104668" title="Ask A Question"]' ); ?>
        </div>
    </section>

<?php get_footer( 'dark-blue' ); ?>