<?php
remove_action( 'wp_head', 'sccssWriteToHead' );
add_action( 'wp_enqueue_scripts', 'unloadercssmain', 100 );

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta name="p:domain_verify" content="f26323dcfd98864d56b4360762be67d6"/>
    <meta charset="<?php bloginfo( 'charset' ); ?>"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="format-detection" content="telephone=no">

    <title><?php wp_title( '|', true, 'right' ); ?></title>

    <!--<link rel="profile" href="//gmpg.org/xfn/11" />
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" /> NOT SURE ABOUT THESE -->
    <!-- enable noydir and noodp in SEO > Titles & Metas -->

    <link href="#" rel="publisher"/>
    <style>
        <?php readfile(get_stylesheet_directory() . '/dist/mobile-home.min.css');?>
    </style>

    <script>
        // timeout to assist with speed and performance
        setTimeout(function () {

            // GTM
            (function (w, d, s, l, i) {
                w[l] = w[l] || [];
                w[l].push({
                    'gtm.start':
                        new Date().getTime(), event: 'gtm.js'
                });
                var f = d.getElementsByTagName(s)[0],
                    j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
                j.async = true;
                j.src =
                    'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', 'GTM-WF4XL8F');
            // End GTM

            // Clixtell Tracking Code
            var script = document.createElement('script');
            var prefix = document.location.protocol;
            script.async = true;
            script.type = 'text/javascript';
            var target = prefix + '//app.clixtell.com/scripts/latest.js';
            script.src = target;
            var elem = document.head;
            elem.appendChild(script);

            //  Twitter universal website tag code
            !function (e, t, n, s, u, a) {
                e.twq || (s = e.twq = function () {
                    s.exe ? s.exe.apply(s, arguments) : s.queue.push(arguments);
                }, s.version = '1.1', s.queue = [], u = t.createElement(n), u.async = !0, u.src = '//static.ads-twitter.com/uwt.js',
                    a = t.getElementsByTagName(n)[0], a.parentNode.insertBefore(u, a))
            }(window, document, 'script');
            // Insert Twitter Pixel ID and Standard Event data below
            twq('init', 'o1axe');
            twq('track', 'PageView');

            // FB Tracking Code
            !function (f, b, e, v, n, t, s) {
                if (f.fbq) return;
                n = f.fbq = function () {
                    n.callMethod ?
                        n.callMethod.apply(n, arguments) : n.queue.push(arguments)
                };
                if (!f._fbq) f._fbq = n;
                n.push = n;
                n.loaded = !0;
                n.version = '2.0';
                n.queue = [];
                t = b.createElement(e);
                t.async = !0;
                t.src = v;
                s = b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t, s)
            }(window,
                document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');

            fbq('init', '320668835423599');
            fbq('set', 'agent', 'tmgoogletagmanager', '320668835423599');
            fbq('track', "PageView");
            //End Facebook Pixel Code

            // chimptastic
            !function (c, h, i, m, p) {
                m = c.createElement(h), p = c.getElementsByTagName(h)[0], m.async = 1, m.src = i, p.parentNode.insertBefore(m, p)
            }(document, "script", "https://chimpstatic.com/mcjs-connected/js/users/8571009818cd0a863463f19af/4bc74922c5c86008c2cad3f18.js");

            // lazy load home iframe video via timer delay instead of viewport
            // var homeVideo = document.getElementById('iframe-video');
            // if (homeVideo) {
            //     homeVideo.setAttribute('src', homeVideo.getAttribute('lazy-src'));
            // }

            //function loadNonCriticalJs() {
            //    var script = document.createElement("script");
            //    script.src = "<?//= get_stylesheet_directory_uri() . '/dist/non-critical.min.js?v=' . JS_VERS;?>//";
            //    script.type = "text/javascript";
            //    document.getElementsByTagName("head")[0].appendChild(script);
            //}
            //    loadNonCriticalJs();

        }, 3500);

        document.addEventListener('wpcf7mailsent', function (event) {
            dataLayer.push({
                'event': 'wpcf7successfulsubmit',
                'CF7formID': event.detail.contactFormId
            });
        }, false);

        if ('scrollRestoration' in history) {
            // Tell browser it can restore scroll automatically (e.g. if we want to use on-page links that jump to one section,
            // and we want to allow the back button to bring us back to the same point we were at, previously)
            history.scrollRestoration = 'auto';
        }
    </script>
	<?php if ( get_field( 'favicon', 'option' ) ): ?>
        <link rel="shortcut icon" href="<?php echo get_field( 'favicon', 'option' ); ?>"/>
	<?php endif; ?>
    <!--<link rel="apple-touch-icon" type="image/png" href="/wp-content/themes/avrek/images/apple-touch-icon.png">
	<link rel="apple-touch-icon" type="image/png" sizes="72x72" href="/wp-content/themes/avrek/images/apple-touch-icon-72.png">
	<link rel="apple-touch-icon" type="image/png" sizes="114x114" href="/wp-content/themes/avrek/images/apple-touch-icon-114.png">-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="//ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
    <script src="//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="//oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<?php

	//    if (isset($_GET['test'])){
	//	    $active_plugins = get_option( 'active_plugins' );
	//	    $plugins = "";
	//	    if( count( $active_plugins ) > 0 ){
	//		    $plugins = "<ul>";
	//		    foreach ( $active_plugins as $plugin ) {
	//			    $plugins .= "<li>" . $plugin . "</li>";
	//		    }
	//		    $plugins .= "</ul>";
	//	    }
	//
	//	    echo $plugins;
	//    }
	?>
    <div class="page-inner-area">
        <div id="mobile-wrapper">
            <!-- Google Tag Manager (noscript) -->
            <noscript>
                <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WF4XL8F"
                        height="0" width="0" style="display:none;visibility:hidden"></iframe>
            </noscript>
            <!-- End Google Tag Manager (noscript) -->

            <!-- Facebook noscript tracking -->
            <noscript><img height="1" width="1" style="display:none"
                           src="https://www.facebook.com/tr?id=320668835423599&ev=PageView&noscript=1"/></noscript>
            <!-- End of Facebook Noscript Tracking -->

            <!-- Header Section -->
            <header class="mobile-header lazyload"
                    data-bg="<?= get_stylesheet_directory_uri(); ?>/assets/images/billboard-banner-min.jpg">
                <div class="container">
                    <a href="/" class="home-icon"><span class="glyphicon glyphicon-home color-dark-blue"></span></a>

                    <!-- Brand and toggle get grouped for better mobile display -->
                    <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#main-navbar"
                            aria-controls="main-navbar" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="glyphicon glyphicon-option-vertical color-dark-blue" aria-hidden="true"></span>
                    </button>

					<?php /* Primary navigation */
					wp_nav_menu( array(
						'theme_location'  => 'header-dark-blue',
						'container'       => 'div',
						'container_class' => 'site-header-navigation collapse navbar-collapse',
						'menu_class'      => 'navbar-nav',
						'container_id'    => 'main-navbar',
						'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
						'walker'          => new WP_Bootstrap_Navwalker(),
					) );
					?>
                    <div class="logo-container text-center">
                        <img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/logo_mobile_small.png"
                             alt="Atlanta Advocates"
                             class="img-responsive">
                    </div>
                    <div class="header-slogan">
                        <p class="text-center color-dark-blue">SPECIALIZING IN<br>
                            <strong>GEORGIA PERSONAL INJURY LAW</strong><br>
                            SINCE 1993</p>
                    </div>
                </div>
            </header>

            <section class="mobile-header-cta dark-blue-bg">
                <div class="call-cta text-center">
                    <a href="tel:770-400-0000" class="cta-btn btn-red">
                        <strong>CALL NOW</strong>
                        <br>
                        FOR A FREE CASE EVALUATION
                    </a>
                </div>
                <div class="header-phone text-center">
                    <a href="tel:770-400-0000" class="color-light-blue">
                        <span class="phone-container">
                            <img data-src="<?= get_stylesheet_directory_uri(); ?>/assets/images/icons/phone_icon.png"
                                 class="lazyload"
                                 alt="phone icon">(770) 400-0000</span>
                        <span class="phone-cta">Call Anytime - 24/7 | "770-4-Million" | Se Habla Español</span>
                    </a>
                </div>
            </section>
            <!-- End Header Mobile -->
