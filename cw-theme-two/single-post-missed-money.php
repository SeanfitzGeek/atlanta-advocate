<?php

/*

* Template Name: Missed Money

*/

?>
<!DOCTYPE html>

<html lang="en">



<head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title><?php wp_title(); ?></title>



    <!-- Bootstrap -->

    <link href="/wp-content/themes/cw-theme-two/missed/missed-css/bootstrap.min.css" rel="stylesheet">

    <link href="/wp-content/themes/cw-theme-two/missed/missed-css/style.css" rel="stylesheet">

    <link href="/wp-content/themes/cw-theme-two/missed/missed-css/lawsuit-style.css" rel="stylesheet">

    <link href="/wp-content/themes/cw-theme-two/missed/missed-css/mediaqueries.css" rel="stylesheet">

    <link href="/wp-content/themes/cw-theme-two/missed/missed-css/checkbox.css" rel="stylesheet">

    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">

    <link href="/wp-content/themes/cw-theme-two/missed/missed-css/animate.css" rel="stylesheet">

    <!-- Modernizr -->

    

    <!-- Page-specific styles -->

    <link href="/wp-content/themes/cw-theme-two/missed/leaflet/leaflet.css" rel="stylesheet">

	<link href='/wp-content/themes/cw-theme-two/missed/missed-css/chartist.css' rel='stylesheet' type='text/css'>

	<link href='/wp-content/themes/cw-theme-two/missed/missed-css/chartist.tooltip.css' rel='stylesheet' type='text/css'>



    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->

    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

    <!--[if lt IE 9]>

      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>

      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

    <![endif]-->

    <?php wp_head(); ?>

    

</head>



<body>

    <header>

        <div class="container-fluid">

            <div class="row">

                <div class="col-sm-12 col-md-8 col-md-offset-2 header-text text-center">

                    <h3 class="first-h3">You may have missed a lot of money</h3>

                    <h3 class="second-h3">by not hiring a</h3>

                    <h1><span class="header-red">CAR ACCIDENT</span> LAWYER</h1>

                    <br/>

                    <div class="border"></div>

                    <p><strong>The truth is: You may be entitled to much more than you think.</strong> A study of over 87,000 car accident injury claims by the Insurance Research Council found that the average insurance company payout when a person hires a lawyer was over 3.5 times higher than without a lawyer.</p>

                </div>

            </div>

            <div class="row">

                <div class="col-md-12">

                    <div class="wc-arrow wc-arrow--floating text-center">

                        <div class="wc-section__container container">

                            <a class="wc-arrow__link" href="#map-intro"><i class="glyphicon glyphicon-menu-down"></i></a>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </header>

    

    

    <div class="highlighted-stat" id="map-intro">

        <div class="container">

            <div class="row">

                <div class="col-md-10">

                    <h3><strong>YOU ARE NOT ALONE</strong></h3>

                    <p>Every driver in Georgia knows that the daily commute can be hazardous, and it’s not uncommon to pass by an accident, particularly when driving on I-285 or I-85. In fact, Georgia is one of the deadliest states to travel in the Southeast, according to the National Highway Traffic Safety Administration (NHTSA). The map below illustrates where most crashes occur throughout the state.</p>

                    <h5>Select a year:</h5>

			        <ul class="year-nav"> 

			            <li><a href="javascript:void(0)" class="year selected">2014</a></li>

			            <li><a href="javascript:void(0)" class="year">2013</a></li> 

			            <li><a href="javascript:void(0)" class="year">2012</a></li> 

			            <li><a href="javascript:void(0)" class="year">2011</a></li> 

			            <li><a href="javascript:void(0)" class="year">2010</a></li> 

			            <li><a href="javascript:void(0)" class="year">2009</a></li> 

			            <li><a href="javascript:void(0)" class="year">2008</a></li> 

			            <li><a href="javascript:void(0)" class="year">2007</a></li> 

			            <li><a href="javascript:void(0)" class="year">2006</a></li> 

			            <li><a href="javascript:void(0)" class="year">2005</a></li> 

			        </ul>                     

                    

                </div>

            </div>

        </div>

    </div>    

    



    <div class="story-map" id="story-map">       

        

        <div id="map" class="story-map-content"></div>

        

        <div class="leaflet-attribution">

	        <a title="A JS library for interactive maps" href="http://leafletjs.com">Leaflet</a> | Imagery from <a href="http://mapbox.com/about/maps/">MapBox</a> &mdash; Map data &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>

	    </div>

    

    </div>



    <div class="lawsuit-gauge">

        <div class="container-fluid">

            <div class="row">

                <div class="col-xs-12 col-md-10 col-md-offset-1">

                    <h3>Is It Worth Talking to a Lawyer About Your Car Accident?</h3>

                    <p>Although every auto accident case is different, there are several factors that can affect how much a case is worth. In many instances, an insurance company will pressure an accident victim to accept a quick and easy settlement offer before it’s really clear what the damages are. Talking to a lawyer about your case does not necessarily mean you are filing a lawsuit. Rather, it means you are enlisting the help of a skilled and aggressive negotiator to fight for your right to full and fair compensation.</p>

                    <p>Use the slider buttons below to see what factors in your case may make it worth contacting a knowledgeable Georgia car accident lawyer.</p>

                    <div class="border--gray"></div>

                    <div class="progressbar-container">

                        <div class="progressbar-bar"></div>

                        <div class="progressbar-label"></div>

                    </div>

                    <div class="ready"></div>

                    <div class="panel panel-default">



                        <!-- List group -->

                        <ul class="list-group">

                            <li class="list-group-item">

                                <div class="row">

                                    <div class="col-md-9"><strong class="question-label">Question 1:</strong> Did you need to have your vehicle repaired or replaced?</div>

                                    <div class="col-md-3">

                                        <div class="material-switch pull-right">

                                            <input id="someSwitchOptionDefault" name="someSwitchOption001" type="checkbox" />

                                            <label for="someSwitchOptionDefault" class="label-danger"></label>

                                        </div>

                                    </div>

                                </div>

                            </li>

                            <li class="list-group-item">

                                <div class="row">

                                    <div class="col-md-9"><strong class="question-label">Question 2:</strong> If your car was repaired, will the accident affect its resale value eventually?</div>

                                    <div class="col-md-3">

                                        <div class="material-switch pull-right">

                                            <input id="someSwitchOptionPrimary" name="someSwitchOption001" type="checkbox" />

                                            <label for="someSwitchOptionPrimary" class="label-danger"></label>

                                        </div>

                                    </div>

                                </div>

                            </li>

                            <li class="list-group-item">

                                <div class="row">

                                    <div class="col-md-9"><strong class="question-label">Question 3:</strong> Did you need to get a rental car?</div>

                                    <div class="col-md-3">

                                        <div class="material-switch pull-right">

                                            <input id="someSwitchOptionSuccess" name="someSwitchOption001" type="checkbox" />

                                            <label for="someSwitchOptionSuccess" class="label-danger"></label>

                                        </div>

                                    </div>

                                </div>

                            </li>

                            <li class="list-group-item">

                                <div class="row">

                                    <div class="col-md-9"><strong class="question-label">Question 4:</strong> Were you hurt in the accident?</div>

                                    <div class="col-md-3">

                                        <div class="material-switch pull-right">

                                            <input id="someSwitchOptionInfo" name="someSwitchOption001" type="checkbox" />

                                            <label for="someSwitchOptionInfo" class="label-danger"></label>

                                        </div>

                                    </div>

                                </div>

                            </li>

                            <li class="list-group-item">

                                <div class="row">

                                    <div class="col-md-9"><strong class="question-label">Question 5:</strong> Did you go to the hospital or see a doctor afterward?</div>

                                    <div class="col-md-3">

                                        <div class="material-switch pull-right">

                                            <input id="someSwitchOptionWarning" name="someSwitchOption001" type="checkbox" />

                                            <label for="someSwitchOptionWarning" class="label-danger"></label>

                                        </div>

                                    </div>

                                </div>

                            </li>

                            <li class="list-group-item">

                                <div class="row">

                                    <div class="col-md-9"><strong class="question-label">Question 6:</strong> Do you expect to be treated in follow-up visits or with therapy or medications?</div>

                                    <div class="col-md-3">

                                        <div class="material-switch pull-right">

                                            <input id="someSwitchOptionDanger" name="someSwitchOption001" type="checkbox" />

                                            <label for="someSwitchOptionDanger" class="label-danger"></label>

                                        </div>

                                    </div>

                                </div>

                            </li>

                            <li class="list-group-item">

                                <div class="row">

                                    <div class="col-md-9"><strong class="question-label">Question 7:</strong> Do you need any special medical equipment, such as crutches or a wheelchair?</div>

                                    <div class="col-md-3">

                                        <div class="material-switch pull-right">

                                            <input id="someSwitchOption7" name="someSwitchOption001" type="checkbox" />

                                            <label for="someSwitchOption7" class="label-danger"></label>

                                        </div>

                                    </div>

                                </div>

                            </li>

                            <li class="list-group-item">

                                <div class="row">

                                    <div class="col-md-9"><strong class="question-label">Question 8:</strong> Did you take time off work (even if you used sick time or vacation days)?</div>

                                    <div class="col-md-3">

                                        <div class="material-switch pull-right">

                                            <input id="someSwitchOption8" name="someSwitchOption001" type="checkbox" />

                                            <label for="someSwitchOption8" class="label-danger"></label>

                                        </div>

                                    </div>

                                </div>

                            </li>

                            <li class="list-group-item">

                                <div class="row">

                                    <div class="col-md-9"><strong class="question-label">Question 9:</strong> Are you able to perform the same work you did before the accident?</div>

                                    <div class="col-md-3">

                                        <div class="material-switch pull-right">

                                            <input id="someSwitchOption9" name="someSwitchOption001" type="checkbox" />

                                            <label for="someSwitchOption9" class="label-danger"></label>

                                        </div>

                                    </div>

                                </div>

                            </li>

                            <li class="list-group-item">

                                <div class="row">

                                    <div class="col-md-9"><strong class="question-label">Question 10:</strong> Did you have to hire someone to do the household chores you normally do (such as yardwork and babysitting)?</div>

                                    <div class="col-md-3">

                                        <div class="material-switch pull-right">

                                            <input id="someSwitchOption10" name="someSwitchOption001" type="checkbox" />

                                            <label for="someSwitchOption10" class="label-danger"></label>

                                        </div>

                                    </div>

                                </div>

                            </li>

                            <li class="list-group-item">

                                <div class="row">

                                    <div class="col-md-9"><strong class="question-label">Question 11:</strong> Has the accident and resulting injury affected your relationship with your spouse?</div>

                                    <div class="col-md-3">

                                        <div class="material-switch pull-right">

                                            <input id="someSwitchOption11" name="someSwitchOption001" type="checkbox" />

                                            <label for="someSwitchOption11" class="label-danger"></label>

                                        </div>

                                    </div>

                                </div>

                            </li>

                            <li class="list-group-item">

                                <div class="row">

                                    <div class="col-md-9"><strong class="question-label">Question 12:</strong> Has the accident and injury caused you to miss out on parts of life that you normally enjoy?</div>

                                    <div class="col-md-3">

                                        <div class="material-switch pull-right">

                                            <input id="someSwitchOption12" name="someSwitchOption001" type="checkbox" />

                                            <label for="someSwitchOption12" class="label-danger"></label>

                                        </div>

                                    </div>

                                </div>

                            </li>

                            <li class="list-group-item">

                                <div class="row">

                                    <div class="col-md-9"><strong class="question-label">Question 13:</strong> Have you experienced psychological trauma as a result of the accident?</div>

                                    <div class="col-md-3">

                                        <div class="material-switch pull-right">

                                            <input id="someSwitchOption13" name="someSwitchOption001" type="checkbox" />

                                            <label for="someSwitchOption13" class="label-danger"></label>

                                        </div>

                                    </div>

                                </div>

                            </li>

                            <li class="list-group-item">

                                <div class="row">

                                    <div class="col-md-9"><strong class="question-label">Question 14:</strong> Was the at-fault party behaving in a way that was extremely dangerous?</div>

                                    <div class="col-md-3">

                                        <div class="material-switch pull-right">

                                            <input id="someSwitchOption14" name="someSwitchOption001" type="checkbox" />

                                            <label for="someSwitchOption14" class="label-danger"></label>

                                        </div>

                                    </div>

                                </div>

                            </li>                                                                                                                                                                                                                                

                        </ul>

                    </div>

                    <!-- Panel -->

                </div>

            </div>

        </div>

    </div>


    <div class="stat-vs-claim">

        <div class="container-fluid">

            <div class="row">

                <div class="col-sm-10 col-sm-offset-1 col-md-4 statclaim1">

                    <h3>Georgia Car Accident Statistics</h3>

                    <div class="border--gray"></div>

                    <ul>

                        <li>More than <strong>112,000 injuries</strong> are expected in Georgia auto accidents in 2016.</li>

                        <li>Traffic accident victims in Georgia faced <strong>$754 million in hospital charges</strong> in 2014.</li>

                        <li>In 2015, <strong>1,430 fatalities</strong> were reported in Georgia car crashes.</li>

                        <li>About <strong>60 percent of fatalities occurred in cities</strong>, rather than rural areas.</li>

                        <li><strong>Fulton County</strong> tops the list for fatal accidents in Georgia.</li>

                    </ul>

                </div>



                <div class="col-sm-6 col-sm-offset-3 col-md-4 statclaim2">

                    <img src="/wp-content/themes/cw-theme-two/missed/images/claims-cases-bg.jpg" alt="Statistics Vs Claims Image" class="img-responsive" />

                </div>

                <div class="col-sm-10 col-sm-offset-1 col-md-4 statclaim3">

                    <h3>What Types of Compensation Can a Lawyer Help You Pursue?</h3>

                    <div class="border--gray"></div>

                    <ul>

	                    <li>Payment for past and future medical bills related to the accident</li>

	                    <li>Lost wages if you took time off work (even if you took sick or vacation days) or if you can’t do your normal work</li>

	                    <li>Payment to repair or replace your vehicle</li>

	                    <li>Compensation for the value of household services that you paid someone else to do because you couldn’t</li>

	                    <li>Payment for the physical and emotional pain and suffering you experienced, such as missing out on the things you normally enjoy in life</li>

	                    <li>Punitive damages to punish the at-fault party for extreme misconduct</li>

                    </ul>

                </div>



            </div>

        </div>

    </div>

    

    

    <div class="intro" id="intro">

        <div class="container">

            <div class="row">

                <div class="col-md-12 second-col-intro">

                    <h3>How Are Pain and Suffering Damages Calculated?</h3>

                    <p>Some of the damages that you suffer after an accident are easily tracked: medical bills, vehicle repairs, lost wages, etc. But trying to put a monetary amount on the physical and emotional pain and suffering you endure can be difficult. There is no set formula for how these damages may be determined, but there are two common tactics that your attorney may consider:</p>

                </div>

            </div>

            <div class="row">

                <div class="col-md-5 calc-box">

                    <h5>1 to 5 Scale</h5>

                    <p>Depending on how severely you were hurt, your pain and suffering could be calculated by multiplying your economic damages (such as medical bills and lost income) by a number from 1 to 5. So, for example, if your economic damages were $10,000, and your injury level was a 3, your attorney may pursue additional pain and suffering damages of $30,000.</p>

                </div>

                <div class="col-md-5 col-md-offset-1 calc-box">

                    <h5>Per Diem</h5>

                    <p>In some cases, your attorney may calculate pain and suffering damages based on a set amount for each day between the time you were injured and the time you reached maximum medical improvement. So, for example, if your determined per diem amount was $150, and it took you 100 days to recover, your attorney may demand pain and suffering damages of $15,000.</p>

                </div>

            </div>

        </div>

    </div>  

    

    

    <div class="resources">

        <div class="container-fluid">

            <div class="row">

                <div class="col-md-8 col-md-offset-2">

                    <h2>It All Adds Up</h2>

                </div>

            </div>

            <div class="row">

                <div class="col-lg-4 col-md-12 col-sm-4 col-xs-12 resource-box">

                    <img src="/wp-content/themes/cw-theme-two/missed/images/-stat-box-img1.png" alt="Statistics Box" class="resource-icon img-responsive">

                    <div class="resource-text">

                        <h5>$3,300</h5>

                        <p>Average cost of a crash-related emergency room visit</p>

                    </div>

                </div>

                <div class="col-lg-4 col-md-12 col-sm-4 col-xs-12 resource-box">

                    <img src="/wp-content/themes/cw-theme-two/missed/images/-stat-box-img1.png" alt="Statistics Box" class="resource-icon img-responsive">

                    <div class="resource-text">

                        <h5>$57,000</h5>

                        <p>Average cost for a crash-related hospitalization</p>

                    </div>

                </div>

                <div class="col-lg-4 col-md-12    col-sm-4 col-xs-12 resource-box">

                    <img src="/wp-content/themes/cw-theme-two/missed/images/-stat-box-img1.png" alt="Statistics Box" class="resource-icon img-responsive">

                    <div class="resource-text">

                        <h5>$500+</h5>

                        <p>Amount of damage to your vehicle that makes it necessary for you to report the accident to Georgia law enforcement</p>

                    </div>

                </div>



            </div>

        </div>

    </div>    

    



    <div class="additional-stat">

        <div class="container-fluid">

            <div class="row">

                <div class="col-md-3 col-md-offset-1">

                    <h3>What If I Filed an Insurance Claim Before I Realized I Was Injured?</h3>

                </div>

                <div class="col-md-7 col-md-offset-1">

                    <p>Car accident injuries can be tricky, as symptoms from injuries such as whiplash may not appear immediately. If you reported your accident to the insurance company immediately but didn’t mention injuries, you may still be able to pursue compensation for those injuries. If you sought treatment for your injuries soon after the accident, you should speak with a skilled Georgia car accident attorney as soon as possible to learn about your legal rights.</p>

                </div>

            </div>

            <div class="row">

                <div class="col-md-3 col-md-offset-1">

                    <h3>How Long Do I Have to File an Accident Claim?</h3>

                </div>

                <div class="col-md-7 col-md-offset-1">

                    <p>After an accident, Georgia law allows you two years to file a personal injury lawsuit. However, insurance companies have much shorter time limits within their policies for reporting the accident. The sooner you talk to an attorney, the better. A skilled attorney will start collecting evidence in your case right away, before physical proof disappears or memories of the accident fade.</p>

                </div>

            </div>

            <div class="row">

                <div class="col-md-3 col-md-offset-1">

                    <h3>How Much Does It Cost to Hire an Attorney After a Car Accident?</h3>

                </div>

                <div class="col-md-7 col-md-offset-1">

                    <p>Many people worry that they can’t afford to hire an attorney after a car accident, so they try to handle the settlement negotiations themselves. It’s important to know that at Millar &amp; Mixon, our dedicated Atlanta car accident attorneys offer free, no obligation case consultations. And we do not charge any attorney fees upfront. Instead, we only collect legal fees if and when we recover compensation for you.</p>

                </div>

            </div>

        </div>

    </div>

    



    <div class="links">

        <div class="container">

            <div class="row">

                <div class="col-md-12 link-text-block">

                    <h4>Sources</h4>

                    <ul>

	                    <li><a href="http://www.cdc.gov/media/releases/2014/p1007-crash-injuries.html" target="_blank">Centers for Disease Control and Prevention</a></li>

	                    <li><a href="http://dph.georgia.gov/crash-outcome-data-evaluation-survey-codes" target="_blank">Georgia Department of Public Health Crash Outcome Data Evaluation Survey (CODES)</a></li>

	                    <li><a href="http://www.gahighwaysafety.org/research/ga-crashes/injuries/fatalities/" target="_blank">Georgia Governor’s Office of Highway Safety</a></li>

	                    <li><a href="https://cdan.nhtsa.gov/STSI.htm" target="_blank">National Highway Traffic Safety Administration (NHTSA)</a></li>

	                    <li><a href="http://www.nhtsa.gov" target="_blank">NHTSA Fatality Analysis Reporting System (FARS)</a></li>

                    </ul>

                </div>

            </div>

        </div>

    </div>



    <footer>

        <div class="container">

            <div class="row">

                <div class="col-md-12">

                    <p><small>Millar &amp; Mixon Law Firm, LLC. | Copyright <?php echo date('Y'); ?> | All Rights Reserved.</small></p>

                </div>

            </div>

        </div>

    </footer>





    <?php wp_footer(); ?>



    

    <!-- Page-specific scripts -->

    <script type="text/javascript" src="//www.cw-apps.com/wp-content/js-repository/8ba548dfdf168047cfe07f092f613a4d.js"></script>

    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>

    <script src="/wp-content/themes/cw-theme-two/missed/missed-js/bootstrap.viewportchecker.min.js"></script>

    <script src="/wp-content/themes/cw-theme-two/missed/missed-js/leaflet.chartist.min.js"></script>

    <script src="/wp-content/themes/cw-theme-two/missed/missed-js/scripts.js"></script>

    



</body>

</html>