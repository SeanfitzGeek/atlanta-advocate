<?php
global $wp;
remove_action('wp_head', 'wp_print_styles', 8);
remove_action('wp_head', 'wp_print_head_scripts', 9);
?>
<!doctype html>
<html ⚡ lang="en">
<head>
    <meta charset="utf-8"/>

    <link rel="icon" href="https://c9j7q2u7.stackpathcdn.com/wp-content/uploads/2018/02/AtlantaAdvocate-Favicon.png" sizes="192x192">
    <link rel="canonical" href="<?= home_url( $wp->request ); ?>">
    <meta name="description" content="<?= get_post_meta(get_the_ID(), '_yoast_wpseo_metadesc', true);?>">
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">

    <script async src="https://cdn.ampproject.org/v0.js"></script>
    <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>
    <script async custom-element="amp-iframe" src="https://cdn.ampproject.org/v0/amp-iframe-0.1.js"></script>
    <script async custom-element="amp-analytics" src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js"></script>
	<?php
	//TODO Removed wp_head() because scripts are loading in here that I have been unable to completely remove as of yet using convential WP means of DE queuing..
//	wp_head();

	?>
    <?php
    // To prevent mutations since amp is so picky and validates this with regex....
    include __DIR__ . '/includes/amp/style-boilerplate.php';?>

    <style amp-custom>
        html {
            margin-top: 8px;
        }

        body {
            width: auto;
            margin: 0;
            padding: 0;
        }


        h1, h2, h3, h4, h5 {
            text-align: center;
        }

        header {
            color: #000;
            font-size: 2em;
            text-align: center;
            position: relative;
        }

        h1 {
            margin: 0;
            padding: 0.5em;
            /*box-shadow: 0px 3px 5px grey;*/
            color: #0a3c67;
            font-size: 1.2em;
        }

        h2 {
            font-size: 1.1em;
        }

        p {
            padding: 0.5em;
            margin: 0.5em;
        }

        .home-button {
            margin-top: 8px;
        }

        .headerbar {
            /*height: 181px;*/
            position: relative;
            z-index: 999;
            top: 0;
            width: 100%;
            /*display: flex;*/
            align-items: center;
            background: #FFF;
        }

        .headerbar .info {

            font-size: 18px;
        }

        .headerbar .phone_number {
            margin: 0 auto;
        }

        .headerbar .phone_number a {
            text-decoration: none;
            border-bottom: 2px solid #c1272d;
        }

        .color-dark-blue {
            color: #0a3f61;
        }

        .color-red {
            color: #C1272D;
        }

        .headerbar .container {
            padding-top: 0;
        }

        .site-name {
            flex: 1;
            /*margin-left: -36px;*/
        }

        main {
            /*margin-top: 183px;*/
            max-width: 700px;
            margin: 0 auto;
            /*padding-top: 183px;*/
        }

        .hamburger {
            display: flex;
            float: right;
            margin-top: 30px;
        }

        .sidebar {
            padding: 10px;
            margin: 0;
        }

        .sidebar > li {
            list-style: none;
            margin-bottom: 10px;
        }

        .sidebar a {
            text-decoration: none;
            color: #FFF;
        }

        .sidebar a:focus {
            text-decoration: underline;
        }

        .close-sidebar {
            font-size: 1.5em;
            text-align: left;
            padding: 5px;
            margin-bottom: 15px;
        }

        .container {
            padding: 25px;
        }

        #sidebar1 {
            padding: 25px;
        }

        .sidebar a {
            font-weight: bold;
            font-size: 22px;
        }

        .sidebar .submenu li a {
            text-transform: capitalize;
        }


        .absolute {
            position: absolute;
        }

        .relative {
            position: relative;
        }

        /** Practice Areas Block **/
        .content {
            font-size: 20px;
            padding: 10px 3px;
        }

        .practice-area-listing-title {
            text-align: center;
            font-weight: bold;
            margin-top: 15px;
        }

        .practice-listing-container {
            height: 310px;
            padding: 10px;
        }

        .additional-practice-areas h3 {
            margin: 60px 0 30px;
        }

        .practice-area-lists {
            border-left: 2px solid #771F26;
        }

        .practice-area-category {
            margin: 20px 0;
        }

        .page-main-content h1 {
            text-align: center;
        }

        .practice-area-main-title {
            text-align: center;
        }

        .practice-listing-container .img-container {
            background: #112e50;
            border-radius: 120px;
            width: 150px;
            height: 150px;
            margin: 0 auto;
            padding: 15px;
            transition: all .3s;
        }

        .practice-listing-container .img-container:hover {
            box-shadow: 0 0 20px #112e50;
            transform: rotate(-5deg);
        }

        .practice-listing-container img {
            margin: 0 auto;
        }

        /** End Practice Areas Block **/

        .dark-blue-bg {
            background: #132f51;
        }

        .color-white {
            color: #fff;
        }

        .text-center {
            text-align: center;
        }

        /** Footer Styles **/
        .footer-copyright {
            padding: 50px 0 40px;
        }

        /** End Footer Styles **/

        .practice-area-single-amp header h1 {
            background: #0a3c67;
            color: #FFF;
        }

        /** Sidebar Styles **/
        #sidebar1 {
            background: #0a3c67;
            color: #FFF;
        }

        .sidebar > li {
            margin: 20px 0;
        }

        /** End Sidebar Styles **/

        /** BreadCrumbs **/
        .breadcrumb {
            background: #CCC;
        }

        .breadcrumb a {
            float: left;
            display: inline-block;
            vertical-align: middle;
            padding: 0 17px 0 0;
            font-size: 16px;
            line-height: 1;
            margin-right: 11px;
            text-decoration: none;
            background: url(<?= get_stylesheet_directory_uri();?>/assets/images/ico-breadcrumbs.png) no-repeat scroll right 50%;
            color: #0c4a7e;
        }

        .breadcrumb span {
            display: inline-block;
            padding: 0;
            font-size: 16px;
            line-height: 1;
            color: #000;
            margin-right: 5px;
            text-decoration: none;
        }

        .breadcrumb ul {
            padding: 10px 0 12px 10px;
            margin: 0;
            list-style: none;
        }

        .breadcrumb ul li {
            display: inline-block;
            line-height: 1;
            vertical-align: middle;
        }
        /** End BreadCrumbs **/

        /** Helping with Aspect Ratio of Images **/
        .fixed-height-container {
            position: relative;
            width: 100%;
            height: 300px;
        }

        amp-img.contain img {
            object-fit: contain;
        }
        /** End Aspect Ratio Tweaks **/

    </style>
</head>
<body>
    <amp-analytics type="gtag" data-credentials="include">
        <script type="application/json">
            {
                "vars" : {
                    "gtag_id": "UA-69607206-1",
                    "config" : {
                        "UA-69607206-1": { "groups": "default" }
                    }
                }
            }
        </script>
    </amp-analytics>
    <header class="headerbar">
        <div class="container">
            <!--        <a href="/">-->
            <!--            <amp-img class="home-button" src="-->
			<? //= get_stylesheet_directory_uri(); ?><!--/assets/images/ico-home-alt.png"-->
            <!--                     width="20" height="17"></amp-img>-->
            <!--        </a>-->
            <a href="/">
                <amp-img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/logo_amp.png"
                         alt="Atlanta Advocates" width="150" height="92" layout="fixed"></amp-img>
            </a>

            <div role="button" on="tap:sidebar1.toggle" tabindex="0" class="hamburger">☰</div>


            <p class="text-center color-dark-blue info">SPECIALIZING IN<br>
                <strong>GEORGIA PERSONAL INJURY LAW</strong><br>
                SINCE 1993</p>

            <h4 class="phone_number">
                <a class="color-red" href="tel:<?php the_field( 'header_phone_number_text', 'option' ); ?>">
					<?= get_field( 'header_phone_number_text', 'option' ); ?>
                </a>
            </h4>

        </div>
    </header>

    <amp-sidebar id="sidebar1" layout="nodisplay" side="left">
        <div role="button" aria-label="close sidebar" on="tap:sidebar1.toggle" tabindex="0" class="close-sidebar">x
        </div>
		<?php
		wp_nav_menu( array(
			'theme_location'  => 'header-dark-blue',
			'container_class' => '',
			'menu_class'      => 'sidebar',


		) );
		?>
    </amp-sidebar>