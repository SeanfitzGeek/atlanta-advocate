<?php
/*
  * Template Name: Wide Homepage Template
  */

if ( get_query_var( 'lang' ) ) {
	header( 'Location: https://atlantaadvocate.com/' );
	exit;
}

get_header( 'dark-blue' );
$ctaBg       = get_field( 'cta_section_image' );
$ctaAwardImg = get_field( 'cta_awards_image' );
$linkArray   = [];
ob_start();
require_once __DIR__ . '/includes/block/repeatable-sections-home-wide.php'; // $LinkArray set in here
$repeatSectionHtml = ob_get_clean();
?>

    <section class="main-cta lazyload" data-bg="<?= $ctaBg['url']; ?>">
        <div class="cta-main-left">
            <div class="award-icons">
                <img src="" data-src="<?= $ctaAwardImg['url']; ?>" alt="<?= $ctaAwardImg['alt']; ?>"
                     class="img-responsive lazyload center-block"></div>
        </div>
        <div class="cta-main-right">
            <div class="inner-container">
                <div class="content">
                    <h1><?= get_field( 'cta_h1_text' ); ?></h1>
                </div>
                <div class="btn-set">
                    <a href="/about-us/" class="btn btn-red">Learn More</a>
                    <a href="#" class="btn btn-transparent" data-toggle="modal" data-target="#videomodal">
                        <span class="glyphicon glyphicon-play" aria-hidden="true"
                              style="border: 2px solid #FFF;border-radius: 25px;padding: 6px;"></span>
                        Watch Our Video</a>
                </div>
                <div class="cta-sub"><p>With Offices in <a
                                href="https://atlantaadvocate.com/georgia/atlanta/">Atlanta</a> &amp; <a
                                href="https://atlantaadvocate.com/georgia/jonesboro/">Jonesboro</a>, Georgia</p></div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="videomodal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
						<?= get_field( 'cta_video' ); ?>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="link-directory">

        <ul>
			<?php $linkCount = 0;

			foreach ( $linkArray as $title => $link ) :?>
                <li><a href="#<?= $link; ?>" data-bar-size="<?= ++ $linkCount; ?>"><?= $title; ?></a></li>
			<?php endforeach; ?>
        </ul>

        <div class="dynamic-border-bar"></div>


    </section>

    <section class="what-we-handle">
        <h3 class="text-center">Personal Injury Cases We Handle</h3>

        <div class="practice-area-container hidden-xs hidden-sm">
			<?php
			while ( have_rows( 'practice_areas_repeater_fields' ) ): the_row();
				$title  = get_sub_field( 'practice_title' );
				$urlObj = get_sub_field( 'practice_link' );
				$imgBg  = get_sub_field( 'practice_bg' );
				$imgAlt = get_post_meta( $imgBg->ID, '_wp_attachment_image_alt', true );

				?>
                <div class="practice-listing-container trigger-on-hover add-delayed-lazyload"
                     data-bg="<?= $imgBg['url']; ?>">
                    <a href="<?= $urlObj['url']; ?>">
                        <div class="what-we-handle-info-container">
                            <div class="practice-area-listing-title"><?= $title; ?></div>
                            <div class="practice-area-listing-learn-more">
                                <button href="<?= $urlObj['url']; ?>" class="btn btn-red">Learn More</button>
                            </div>
                        </div>
                    </a>
                </div>
			<?php endwhile; ?>
            <div class="clear"></div>
        </div>

        <div class="glide-practice-areas visible-sm visible-xs">
            <div class="glide__track" data-glide-el="track">
                <ul class="glide__slides">
					<?php
					while ( have_rows( 'practice_areas_repeater_fields' ) ): the_row();
						$title  = get_sub_field( 'practice_title' );
						$urlObj = get_sub_field( 'practice_link' );
						$imgBg  = get_sub_field( 'practice_bg' );
						?>
                        <li class="glide__slide add-delayed-lazyload" data-bg="<?= $imgBg['url']; ?>">
                            <div class="glide-slide-container">
                                <div class="glide-title">
                                    <h2><?= $title; ?></h2>
                                </div>
                                <div class="glide-content">
                                    <a href="<?= $urlObj['url']; ?>" class="btn btn-red">Learn More</a>
                                </div>


                            </div>

                            <div class="glide-slide-overlay"></div>
                        </li>
					<?php endwhile; ?>
                </ul>
            </div>
            <div class="glide__arrows" data-glide-el="controls">
                <button class="glide__arrow prev" data-glide-dir="<"><span
                            class="glyphicon glyphicon-menu-left"></span>
                </button>
                <button class="glide__arrow next" data-glide-dir=">"><span
                            class="glyphicon glyphicon-menu-right"></span></button>
            </div>

        </div>
    </section>

    <section class="client-stories">
        <div class="glide">
            <div class="glide__track" data-glide-el="track">
                <ul class="glide__slides">
					<?php
					while ( have_rows( 'client_stories_repeater_fields' ) ): the_row();
						$title   = get_sub_field( 'client_story_title' );
						$content = get_sub_field( 'client_story_content' );
						$imgBg   = get_sub_field( 'client_story_background_image' );
						?>
                        <li class="glide__slide lazyload" data-bg="<?= $imgBg['url']; ?>">
                            <div class="glide-slide-container">
                                <div class="glide-title">
                                    <h2><?= $title; ?></h2>
                                </div>
                                <div class="glide-content">
									<?= $content; ?>
                                </div>
                            </div>

                            <div class="glide-slide-overlay"></div>
                        </li>
					<?php endwhile; ?>
                </ul>
            </div>
            <div class="glide__arrows" data-glide-el="controls">
                <button class="glide__arrow prev" data-glide-dir="<"><span class="glyphicon glyphicon-menu-left"></span>
                </button>
                <button class="glide__arrow next" data-glide-dir=">"><span
                            class="glyphicon glyphicon-menu-right"></span></button>
            </div>

        </div>
    </section>

    <section class="legal-team">
        <h3 class="text-center">Meet Your Legal Team</h3>
        <h5 class="text-center">Award Winning Georgia Personal Injury Lawyers</h5>
        <div class="row hidden-xs hidden-sm">

            <div class="col-xs-6 col-sm-3 trigger-on-hover">
                <a href="/about-us/justin-oliverio/">
                    <div class="legal-team-member lazyload img-responsive"
                         data-bg="<?= get_stylesheet_directory_uri(); ?>/assets/images/homepage-design-assets/Lawyer-Bio-Color-copy-(1).jpg">

                        <div class="team-info-container">
                            <div class="team-name">Justin Oliverio, Esq.</div>
                        </div>

                    </div>
                </a>
            </div>

            <div class="col-xs-6 col-sm-3 trigger-on-hover">
                <a href="/about-us/ivory-roberson/">
                    <div class="legal-team-member lazyload img-responsive"
                         data-bg="<?= get_stylesheet_directory_uri(); ?>/assets/images/homepage-design-assets/Meet-Our-Lawyers-Ivory.jpg">

                        <div class="team-info-container">
                            <div class="team-name">Ivory Roberson, Esq.</div>
                        </div>

                    </div>
                </a>
            </div>

            <div class="col-xs-6 col-sm-3 trigger-on-hover">
                <a href="/about-us/bruce-millar/">

                    <div class="legal-team-member lazyload img-responsive"
                         data-bg="<?= get_stylesheet_directory_uri(); ?>/assets/images/homepage-design-assets/Our-Lawyers-Bruce.jpg">
                        <div class="team-info-container">
                            <div class="team-name">Bruce R. Millar, Esq.</div>
                        </div>

                    </div>
                </a>
            </div>

            <div class="col-xs-6 col-sm-3 trigger-on-hover">
                <a href="/about-us/anthony-jones/">

                    <div class="legal-team-member lazyload img-responsive"
                         data-bg="<?= get_stylesheet_directory_uri(); ?>/assets/images/homepage-design-assets/Our-Lawyers-Anthony.jpg">
                        <div class="team-info-container">
                            <div class="team-name">Anthony R. Jones, Esq.</div>
                        </div>

                    </div>
                </a>
            </div>
        </div>

        <div class="glide-legal-team visible-xs visible-sm">
            <div class="glide__track" data-glide-el="track">
                <ul class="glide__slides" style="max-height: 300px;">

                    <li class="glide__slide lazyload"
                        data-bg="<?= get_stylesheet_directory_uri(); ?>/assets/images/homepage-design-assets/Our-Lawyers-Anthony.jpg">
                        <a href="/about-us/anthony-jones/">
                            <div class="glide-slide-container">
                                <h2>Anthony Jones, II</h2>
                                <div class="glide-content text-center">Attorney</div>

                            </div>

                            <div class="glide-slide-overlay"></div>
                        </a>
                    </li>

                    <li class="glide__slide lazyload"
                        data-bg="<?= get_stylesheet_directory_uri(); ?>/assets/images/homepage-design-assets/Lawyer-Bio-Color-copy-(1).jpg">
                        <a href="/about-us/justin-oliverio/">
                            <div class="glide-slide-container">
                                <h2>Justin Oliverio</h2>
                                <div class="glide-content text-center">Attorney</div>

                            </div>

                            <div class="glide-slide-overlay"></div>
                        </a>
                    </li>

                    <li class="glide__slide lazyload"
                        data-bg="<?= get_stylesheet_directory_uri(); ?>/assets/images/homepage-design-assets/Meet-Our-Lawyers-Ivory.jpg">
                        <a href="/about-us/ivory-roberson/">
                            <div class="glide-slide-container">
                                <h2>Ivory Roberson</h2>
                                <div class="glide-content text-center">Attorney</div>
                            </div>

                            <div class="glide-slide-overlay"></div>
                        </a>
                    </li>

                    <li class="glide__slide lazyload"
                        data-bg="<?= get_stylesheet_directory_uri(); ?>/assets/images/homepage-design-assets/Our-Lawyers-Bruce.jpg">
                        <a href="/about-us/bruce-millar/">
                            <div class="glide-slide-container">
                                <h2>Bruce Millar</h2>
                                <div class="glide-content text-center">Attorney</div>

                            </div>

                            <div class="glide-slide-overlay"></div>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="glide__arrows" data-glide-el="controls">
                <button class="glide__arrow prev" data-glide-dir="<"><span
                            class="glyphicon glyphicon-menu-left"></span>
                </button>
                <button class="glide__arrow next" data-glide-dir=">"><span
                            class="glyphicon glyphicon-menu-right"></span></button>
            </div>

        </div>
    </section>

<?php echo $repeatSectionHtml; ?>

    <section class="testimonial">
        <div class="left-block lazyload"
             data-bg="<?= get_stylesheet_directory_uri(); ?>/assets/images/testimonial_image.jpg"></div>
        <div class="right-block">
            <h2>What Clients Say About Our

                Law Firm</h2>
            <div class="content">
                <p>We are proud that the majority of our cases are referred by satisfied clients. Here’s what they’ve
                    been saying:</p>
            </div>

            <div class="testimonial-box">
                <div class="stars">
                    <img data-src="<?= get_stylesheet_directory_uri(); ?>/assets/images/homepage-design-assets/ratings.png"
                         class="lazyload img-responsive">
                </div>
                <div class="content">
                    <p>"I am very appreciative of the help I received from Millar Law Firm. The staff was so
                        professional and kind from beginning to end. Everyone I encountered was very kind and attentive
                        to my case...."</p>

                    <p>-Kathie Neyman</p>

                    <a href="https://www.google.com/search?tbm=lcl&sxsrf=ACYBGNSAeM9UEl-eV6WR1kfY0Keog7kYVg%3A1580586473142&ei=6dU1XoSpCOiyytMP292e6Ao&q=atlanta+advocate+miller+law+firm&oq=atlanta+advocate+miller+law+firm&gs_l=psy-ab.3..33i299k1.9169.15196.0.15371.32.30.0.2.2.0.193.2717.18j10.28.0....0...1c.1.64.psy-ab..2.30.2726...0j0i67k1j0i273k1j0i131i273k1j0i131k1j0i131i67k1j0i22i30k1j0i22i10i30k1j33i22i29i30k1j33i160k1.0.aJ-kzkb2QzI#lrd=0x88f505a605ff480f:0x70200a614adaeb66,1,,,&rlfi=hd:;si:8079469144486636390,l,CiBhdGxhbnRhIGFkdm9jYXRlIG1pbGxlciBsYXcgZmlybVo8ChhhZHZvY2F0ZSBtaWxsZXIgbGF3IGZpcm0iIGF0bGFudGEgYWR2b2NhdGUgbWlsbGVyIGxhdyBmaXJt;mv:[[33.9315145,-84.342726],[33.7493069,-84.4694196]]"
                       target="_blank">
                        Read More Reviews
                    </a>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </section>

<?php get_footer( 'dark-blue' ); ?>