<?php get_header('dark-blue'); ?>
<section class="content-wrapper">
    <div class="container">
        <div class="row">
            <article>
                <h1><?php h1_title(); ?></h1>				
                    <p>
						<?php if ( has_post_thumbnail() ): ?>
                            <img src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>" class="thumbnail image left"/>
						<?php endif; ?>
						<?php the_content(); ?>
                    </p>
            </article>
            <aside>
				<?php get_template_part( 'sidebars/default' ); ?>
            </aside>
        </div>
    </div>
</section>
<?php get_footer('dark-blue'); ?>