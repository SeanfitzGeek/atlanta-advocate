<?php
get_header('dark-blue');

// Id of Education page for use of fields for images/backgrounds
$educationPageId = 90536;

$args       = [ 'post_type' => 'legal_terms', 'limit' => 500 ];
$legalTerms = get_posts( $args );
?>
    <div class="education-page-content">
        <div id="main-content">
            <section class="main-education-banner-section main-banner-section lazyload"
                     data-bg="<?= get_field( 'education_banner_background', $educationPageId ); ?>">
                <div class="container">
                    <h1>Legal Terms</h1>

                    <div class="education-form-search">
						<?= getEducationSearch(); ?>
                        <div class="faq-search header-search search-box">
                            <form method="get" action="/">
                                <div class="input-group">
                                    <input type="text" name="search" class="form-control form-search"
                                           placeholder="Search legal terms, topics, or questions."
                                           value="<?= isset( $_REQUEST['search'] ) ? $_REQUEST['search'] : ''; ?>"/>
                                    <div class="input-group-btn">
                                        <button class="btn btn-primary" type="submit">
                                            <span class="search-btn">Search</span>
                                            <span class="glyphicon glyphicon-search"></span>
                                        </button>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </form>
                        </div>

                    </div>
                </div>
            </section>
            <section class="legal-terms">
                <div class="container">

					<?php foreach ( $legalTerms as $legalTermPost ) : ?>
                        <h2 class="bold"><?= $legalTermPost->post_title;?></h2>
						<?php while ( have_rows( 'legal_terms', $legalTermPost ) ): the_row(); ?>

                            <div class="term">
								<?php printf( '<p><span class="bold">%s</span> - %s</p>', get_sub_field( 'legal_term_title' ), get_sub_field( 'legal_term_description' ) ); ?>
                            </div>
						<?php endwhile; ?>
					<?php endforeach; ?>
                </div>
            </section>
        </div>
    </div>
<?php get_footer('dark-blue'); ?>