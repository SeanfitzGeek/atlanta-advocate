<?php
/*
  * Template Name: RSVP Large Template
  */

get_header( 'rsvp' );
?>

<div class="logo-container text-center">
    <a href="/">
        <img data-src="<?= get_stylesheet_directory_uri(); ?>/assets/images/logo_mobile_small.png"
             alt="Atlanta Advocates"
             class="img-responsive lazyload center-block">
    </a>
</div>
<!-- Navigation -->
<!--<nav class="navbar navbar-expand-lg navbar-light bg-light static-top mb-5 shadow">-->
<!--    <div class="container">-->
<!--        <a class="navbar-brand" href="#">Start Bootstrap</a>-->
<!--        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">-->
<!--            <span class="navbar-toggler-icon"></span>-->
<!--        </button>-->
<!--        <div class="collapse navbar-collapse" id="navbarResponsive">-->
<!--            <ul class="navbar-nav ml-auto">-->
<!--                <li class="nav-item active">-->
<!--                    <a class="nav-link" href="#">Home-->
<!--                        <span class="sr-only">(current)</span>-->
<!--                    </a>-->
<!--                </li>-->
<!--                <li class="nav-item">-->
<!--                    <a class="nav-link" href="#">About</a>-->
<!--                </li>-->
<!--                <li class="nav-item">-->
<!--                    <a class="nav-link" href="#">Services</a>-->
<!--                </li>-->
<!--                <li class="nav-item">-->
<!--                    <a class="nav-link" href="#">Contact</a>-->
<!--                </li>-->
<!--            </ul>-->
<!--        </div>-->
<!--    </div>-->
<!--</nav>-->

<!-- Page Content -->
<div class="container">
    <div class="form-container">
	<?= do_shortcode( '[contact-form-7 id="87592" title="RSVP Form"]' ); ?>
    </div>

</div>

<?php get_footer( 'rsvp' ); ?>
