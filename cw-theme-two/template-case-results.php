<?php
/*
  * Template Name: Case Results Template
  */

get_header( 'dark-blue' );
$now = new DateTime();

$selectedYear          = 0;
$practiceArea          = 0;
$compAmount            = 0;
$metaQueries           = 0;
$settlementAmountArray = [
	[
		'label'       => '$100 - 250k',
		'amount'      => [ 100000, 250000 ],
		'filter_type' => 'BETWEEN'
	],
	[
		'label'       => '$250 - 500k',
		'amount'      => [ 250000, 500000 ],
		'filter_type' => 'BETWEEN'
	],
	[
		'label'       => '$500k+',
		'amount'      => 500000,
		'filter_type' => '>='
	],

];

$conditionalArray[] = [
];

$selectedConditionsArray = [];

if ( isset( $_GET['settlement-year'] ) && strlen( trim( $_GET['settlement-year'] ) ) === 4 ) {
	$conditionalArray[] = [
		'key'     => 'settlement_year',
		'compare' => 'EXISTS',
	];

	$selectedYear            = $_GET['settlement-year'];
	$selectedConditionsArray = [
		'key'     => 'settlement_year',
		'value'   => $selectedYear,
		'compare' => '=',
	];

	array_push( $conditionalArray, $selectedConditionsArray );

}

if ( isset( $_GET['practice-area'] ) ) {
	$practiceArea            = $_GET['practice-area'];
	$selectedConditionsArray = array(
		'key'     => 'settlement_year',
		'value'   => $selectedYear,
		'compare' => '=',
	);

	array_push( $conditionalArray, $selectedConditionsArray );
}

if ( isset( $_GET['settlement-amount'] ) && ( $_GET['settlement-amount'] !== '' ) ) {
	$compAmount              = $_GET['settlement-amount'];
	$selectedConditionsArray = array(
		'key'     => 'settlement_amount',
		'value'   => $settlementAmountArray[ $compAmount ]['amount'],
		'compare' => $settlementAmountArray[ $compAmount ]['filter_type'],
		'type'    => 'numeric',
	);

	array_push( $conditionalArray, $selectedConditionsArray );
}

$args = array(
	'post_type'      => 'case-result',
	'posts_per_page' => 30,
	'meta_query'     => array(
		'relation' => 'AND',
		$conditionalArray,
	)
);

$settlementsQuery = new WP_Query( $args );

$args = array(
	'post_type'      => 'case-result',
	'posts_per_page' => 3,
	'meta_query'     => array(
		'relation' => 'AND',
		[
			'key'     => 'settlement_featured',
			'compare' => '!=',
			'value'   => '',
		],
		$conditionalArray,
	)
);


$featuredSettlementsQuery = new WP_Query( $args );

$headerBgUrl              = get_field( 'case_results_header_background' );
$caseResultsHeaderContent = get_field( 'case_results_header_content' );

$yearArray = [ 2014,2015,2016, 2017, 2018, 2019 ];
?>

    <section class="case-results-header lazyload" data-bg="<?= $headerBgUrl['url']; ?>">
        <div class="header-left">
			<?= $caseResultsHeaderContent; ?>
        </div>
    </section>

    <div class="case-results-main">
		<?php if ( $featuredSettlementsQuery->have_posts() ) : ?>
            <h2 class="featured-settlement-section-title">Featured Settlements</h2>
		<?php endif; ?>

        <div class="row">
            <div class="col-md-7">
				<?php if ( $featuredSettlementsQuery->have_posts() ) :
					while ( $featuredSettlementsQuery->have_posts() ) :
						$featuredSettlementsQuery->the_post();
						?>
						<?php
						$amount      = get_field( 'amount' );
						$year        = get_field( 'settlement_year' );
						$yearArray[] = $year;
						?>
                        <section class="featured-settlements">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="settlement-title">
                                        <h3><?= the_title(); ?></h3>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="settlement-amount">
										<?= $amount; ?></h3>
                                    </div>
                                </div>
                            </div>


                            <div class="settlement-year">
                                <h5><?= $year; ?></h5>
                            </div>

                            <div class="content">
								<?= the_content(); ?>
                            </div>
                        </section>
					<?php endwhile; ?>
				<?php endif; ?>
				<?php wp_reset_postdata(); ?>

                <div class="settlements-main">
					<?php if ( $settlementsQuery->have_posts() ) :
						while ( $settlementsQuery->have_posts() ) :
							$settlementsQuery->the_post();

							$amount      = get_field( 'settlement_amount' );
							$year        = get_field( 'settlement_year' );
							$yearArray[] = $year;
							?>


                            <div class="settlements">
                                <h3><?php the_title(); ?></h3>

                                <div class="settlement-year">
                                    <h5><?= $year; ?></h5>
                                </div>

                                <div class="content">
									<?php the_content(); ?>
                                </div>

                            </div>


						<?php endwhile;

					else :?>
                        <h2>No Case Results Found!</h2>
					<?php endif; ?>
					<?php wp_reset_postdata(); ?>

                </div>
            </div>

            <div class="col-md-5">
                <div class="sidebar-forms">
                    <div class="filter-results">
                        <h3>Filter Results</h3>
                        <div class="filter-fields">
                            <form method="get">
								<?php
								$yearArray

                                    = array_unique( $yearArray );
								sort( $yearArray );
								?>
                                <div class="field">
                                    <select name="settlement-year">
                                        <option value="">Year</option>
										<?php foreach ( $yearArray as $year ): ?>
											<?php
											$selected = (isset($_GET['settlement-year'])) && ((int) $_GET['settlement-year'] === (int) $year) ? 'selected' : '';
											if ( strlen( $year ) === 4 ) {
												printf( '<option %s value="%s">%s</option>', $selected, $year, $year );
											} ?>
										<?php endforeach; ?>
                                    </select>
                                </div>


                                <div class="field">
                                    <select name="settlement-amount">
                                        <option value="">Compensation Amount</option>

										<?php foreach ( $settlementAmountArray as $settlementAmountIndex => $arr ): ?>
											<?php $selected = ( isset($_GET['settlement-amount'])) && ($_GET['settlement-amount'] !== '' ) && (int) $_GET['settlement-amount'] === (int) $settlementAmountIndex ? 'selected' : ''; ?>
											<?php printf( '<option %s value="%s">%s</option>', $selected, $settlementAmountIndex, $arr['label'] ); ?>
										<?php endforeach; ?>
                                    </select>
                                </div>

                                <div class="field">
                                    <button type="submit" class="btn">FILTER RESULT</button>
                                </div>
                            </form>

                        </div>
                    </div>

                    <div class="site-search-form">
                        <h3>Ask a Personal Injury Legal Question</h3>


                        <form action="/" method="get" class="noauto">
                            <div class="">
                                <input type="text" name="s" id="search" class="form-control"
                                       placeholder="Type keywords to find answers" value=""/>
                            </div>

                            <button class="btn btn-default" type="submit">
                                <span class="glyphicon glyphicon-search"></span> Search
                            </button>
                            <div class="clearfix"></div>
                        </form>
                    </div>

                    <div class="contact-dark-blue">
                        <h3>Have a Personal Injury Question?</h3>
                        <p>One of our attorneys will be happy to respond at no cost or obligation.</p>

						<?= do_shortcode( '[contact-form-7 id="96254" title="Contact Us Form"]' ); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php get_footer( 'dark-blue' ); ?>