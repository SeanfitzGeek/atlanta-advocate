<?php
// Register Custom Post Type
function customPostLegalTerms() {

	$labels = array(
		'name'                  => 'Legal Terms',
		'singular_name'         => 'Term',
		'menu_name'             => 'Legal Terms',
		'name_admin_bar'        => 'Legal Terms',
		'archives'              => 'Legal Terms Archives',
		'attributes'            => 'Item Attributes',
		'parent_item_colon'     => 'Parent Legal Term:',
		'all_items'             => 'All Legal Terms',
		'add_new_item'          => 'Add New Legal Term',
		'add_new'               => 'Add New Legal Term',
		'new_item'              => 'New Legal Term',
		'edit_item'             => 'Edit Legal Term',
		'update_item'           => 'Update Legal Term',
		'view_item'             => 'View Legal Term',
		'view_items'            => 'View Legal Terms',
		'search_items'          => 'Search Legal Terms',
		'not_found'             => 'Not found',
		'not_found_in_trash'    => 'Not found in Trash',
		'featured_image'        => 'Featured Image',
		'set_featured_image'    => 'Set featured image',
		'remove_featured_image' => 'Remove featured image',
		'use_featured_image'    => 'Use as featured image',
		'insert_into_item'      => 'Insert into item',
		'uploaded_to_this_item' => 'Uploaded to this Legal Term',
		'items_list'            => 'Legal Term list',
		'items_list_navigation' => 'Items list navigation',
		'filter_items_list'     => 'Filter items list',
	);
	$args = array(
		'label'                 => 'term',
		'description'           => 'List of Legal terms',
		'labels'                => $labels,
		'supports'              => array( 'title' ),
		'taxonomies'            => array(),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'rewrite' => array('slug' => 'legal-terms', 'with_front' => false)

	);
	register_post_type( 'legal_terms', $args );

}
add_action( 'init', 'customPostLegalTerms', 0 );