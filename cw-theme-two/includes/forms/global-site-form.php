<?php global $caption, $title, $notice; ?>
<div class="main-form">
    <div class="main-form-container">
        <div class="clearfix header-form noauto">
            <h3 class="text-center"><?= $title;?></h3>
            <small><?= $caption;?></small>
			<?= do_shortcode( '[contact-form-7 id="59271" title="New Main Contact Form" html_class="noauto"]' ); ?>

            <p class="form-info"><?= $notice; ?></p>
        </div>
    </div>
</div>
