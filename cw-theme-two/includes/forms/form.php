<form class="clearfix header-form">
    <h3>GET A FREE CASE EVALUATION</h3>
    <input name="name" type="text" placeholder="Name"/>
    <div class="required">
        <input name="phone_or_email" type="text" placeholder="Phone or Email"/>
        <span>required</span>
    </div>
    <textarea name="notes" placeholder="How can we help you today?"></textarea>
    <button class="but-form">Send to our Attorneys</button>
</form>
