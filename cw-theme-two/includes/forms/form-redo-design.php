<div class="main-form">
    <div class="main-form-container">
        <div class="clearfix header-form noauto">
            <h3 class="text-center"><?= get_field('main_form_header', 'option');?></h3>
            <small><?= get_field('main_form_privacy_top', 'option');?></small>
			<?= do_shortcode( '[contact-form-7 id="59271" title="New Main Contact Form" html_class="noauto"]' ); ?>

            <p class="form-info"><?= get_field('main_form_privacy_bottom', 'option');?></p>
        </div>
    </div>
</div>
