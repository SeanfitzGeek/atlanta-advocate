<div class="glide-legal-team visible-xs visible-sm" id="our-staff">
    <div class="glide__track" data-glide-el="track">
        <ul class="glide__slides">
            <?php
            while ( have_rows( 'meet_the_lawyers_repeater_fields' ) ): the_row();
            $name = get_sub_field( 'lawyer_title' );
            $link = get_sub_field( 'lawyer_link' );
            $image = get_sub_field( 'lawyer_image' );
				?>
            <li class="glide__slide lazyload"
                data-bg="<?= $image['url'];?>">
                <a href="<?= $link;?>">
                    <div class="glide-slide-container">
                        <h2><?= $name;?></h2>

                    </div>

                    <div class="glide-slide-overlay"></div>
                </a>
            </li>
            <?php endwhile; ?>
        </ul>
    </div>
    <div class="glide__arrows" data-glide-el="controls">
        <button class="glide__arrow prev" data-glide-dir="<"><span
                    class="glyphicon glyphicon-menu-left"></span>
        </button>
        <button class="glide__arrow next" data-glide-dir=">"><span
                    class="glyphicon glyphicon-menu-right"></span></button>
    </div>
</div>