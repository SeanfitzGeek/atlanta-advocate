<?php
global $linkArray;
$totalCount   = sizeof( get_field( 'repeater_section_block' ) );
$shadeCounter = 0;
while ( have_rows( 'repeater_section_block' ) ): the_row();
	$title     = get_sub_field( 'title' );
	$content   = get_sub_field( 'content' );
	$alignment = get_sub_field( 'alignment' );
	$moreLink  = get_sub_field( 'more_link' );
	$bg        = get_sub_field( 'background_image' );
	$idLink    = str_replace( ' ', '-', trim($title));

	if ( !ctype_alpha( $idLink[0])) {
		$idLink = 'a-' . $idLink;
	}

	$idLink = preg_replace( "/[^A-Za-z0-9\-]/", '', $idLink );

	$linkArray[ $title ] = $idLink;

	?>
    <section class="repeat-section-block lazyload" data-bg="<?= $bg['url']; ?>" id="<?= $idLink; ?>">
        <div class="<?= $alignment; ?> side-block <?= $shadeCounter % 2 === 0 ? 'shade' : ''; ?>">

            <div class="inner-container">
                <h3 class="title"><?= $title; ?></h3>

                <div class="content">
					<?= $content; ?>
                </div>
				<?php if ( strlen( $moreLink['url'] ) ): ?>
                    <div class="more-link">
                        <a href="<?= $moreLink['url']; ?>">Learn More</a>
                    </div>
				<?php endif; ?>
            </div>
        </div>

		<?php if ( get_row_index() !== $totalCount ) : ?>
            <div class="next-section">
                <a href="#"><span class="glyphicon glyphicon-menu-down"></span></a>
            </div>
		<?php endif; ?>


        <div class="back-to-top">
            <a href="#">Back to Top <span class="glyphicon glyphicon-menu-up"></span></a>
        </div>
    </section>
	<?php $shadeCounter ++; ?>
<?php endwhile; ?>