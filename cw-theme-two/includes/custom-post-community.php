<?php
// Register Custom Post Type
function registerCommunityPostType() {

	$labels = array(
		'name'                  => 'Community',
		'singular_name'         => 'community',
		'menu_name'             => 'Community Posts',
		'name_admin_bar'        => 'Community Posts',
		'archives'              => 'Community Archives',
		'attributes'            => 'Community Attributes',
		'parent_item_colon'     => 'Parent Community',
		'all_items'             => 'All Communities',
		'add_new_item'          => 'Add New Community',
		'add_new'               => 'Add New Community',
		'new_item'              => 'New Community',
		'edit_item'             => 'Edit Community',
		'update_item'           => 'Update Community',
		'view_item'             => 'View Community',
		'view_items'            => 'View Communities',
		'search_items'          => 'Search Community',
		'not_found'             => 'Not found',
		'not_found_in_trash'    => 'Not found in Trash',
		'featured_image'        => 'Featured Image',
		'set_featured_image'    => 'Set featured image',
		'remove_featured_image' => 'Remove featured image',
		'use_featured_image'    => 'Use as featured image',
		'insert_into_item'      => 'Insert into Community',
		'uploaded_to_this_item' => 'Uploaded to this Community',
		'items_list'            => 'Communities list',
		'items_list_navigation' => 'Items list navigation',
		'filter_items_list'     => 'Filter items list',
	);
	$args = array(
		'label'                 => 'community',
		'description'           => 'List of Communities',
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'taxonomies'            => array(),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 0,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'rewrite' => array('slug' => 'community', 'with_front' => false),
		'capability_type'       => 'page',
	);
	register_post_type( 'Community', $args );

}
add_action( 'init', 'registerCommunityPostType', 10 );