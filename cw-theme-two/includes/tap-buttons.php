<div class="actions visible-xs" id="tap-buttons">
    <div class="container">
        <div class="row">
            <div class="col-xs-6">
                <a href="tel:4046204301" class="tap-but">Tap to Call</a>
            </div>
            <div class="col-xs-6">
                <a href="<?php the_field( 'directions_link', get_field( 'contact_location', 'option' ) ); ?>"
                   target="_blank" class="tap-but">Directions</a>
            </div>
        </div>
    </div>
</div>