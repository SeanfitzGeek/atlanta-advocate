<?php
acf_add_local_field_group( array(
		'key'                   => 'amp_page_settings_fields',
		'title'                 => 'AMP Page Settings',
		'fields'                => array(
			array(
				'key'                 => 'show_amp_template',
				'label'               => 'Enable AMP',
				'name'                => 'show_amp_template',
				'type'                => 'checkbox',
				'instructions'        => 'Enables AMP on this page template for mobile devices.',
				'choices' => array(
					1 => 'Enable',
				),
				'allow_custom' => 0,
				'default_value' => array(
				),
			),
		),
		'location'              => array(
			array(
				array(
					'param'    => 'post_type',
					'operator' => '==',
					'value'    => 'page',
				),
			),
		),
		'menu_order'            => 15,
		'position'              => 'normal',
		'style'                 => 'default',
		'label_placement'       => 'top',
		'instruction_placement' => 'label',
		'active'                => true,
		'description'           => '',
	)
);