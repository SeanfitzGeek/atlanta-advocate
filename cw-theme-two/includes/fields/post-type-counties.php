<?php

acf_add_local_field_group( array(
		'key'                   => 'county_content_blocks',
		'title'                 => 'Enter information about County',
		'fields'                => array(
			array(
				'key'                 => 'county_information_tab',
				'label'               => 'Main Community Information',
				'name'                => 'county_information_tab',
				'type'                => 'tab',
				'instructions'        => '',
				'required'            => 0,
				'conditional_logic'   => 0,
				'placement'           => 'top',
				'endpoint'            => 0,
				'wpml_cf_preferences' => 0,
			),

			array(
				'key'          => 'counties_main_content',
				'label'        => 'Main Counties Information',
				'name'         => 'counties_main_content',
				'type'         => 'wysiwyg',
				'instructions' => '',
			),

			array(
				'key'           => 'county_dropdown_background',
				'label'         => 'County Dropdown Section Background Image',
				'name'          => 'county_dropdown_background',
				'type'          => 'image',
				'instructions'  => 'Add a background image for this sections dropdown area',
				'return_format' => 'url',
				'preview_size'  => 'thumbnail',
				'library'       => 'all',
			),

			array(
				'key'           => 'county_background',
				'label'         => 'County Section Background',
				'name'          => 'county_background',
				'type'          => 'image',
				'instructions'  => 'background that appears on the counties information after section opened',
				'return_format' => 'url',
				'preview_size'  => 'thumbnail',
				'library'       => 'all',
			),

			array(
				'key'           => 'county_main_image',
				'label'         => 'County Main Image',
				'name'          => 'county_main_image',
				'type'          => 'image',
				'instructions'  => 'Appears to the left of the main content for the county if not empty',
				'return_format' => 'object',
				'preview_size'  => 'thumbnail',
				'library'       => 'all',
			),

			array(
				'key'          => 'county_location_assocation',
				'label'        => 'Association to Location to pull certain data from (practice areas)',
				'name'         => 'county_location_assocation',
				'type'         => 'relationship',
				'instructions' => '',
			),

			array(
				'key'                 => 'count_cities_tab',
				'label'               => 'County Cities Tab',
				'name'                => 'count_cities_tab',
				'type'                => 'tab',
				'instructions'        => '',
				'required'            => 0,
				'conditional_logic'   => 0,
				'placement'           => 'top',
				'endpoint'            => 0,
				'wpml_cf_preferences' => 0,
			),

			array(
				'key'          => 'repeating_cities',
				'label'        => 'Add City',
				'name'         => 'repeating_cities',
				'type'         => 'repeater',
				'layout'       => 'block',
				'button_label' => 'Add New City',
				'sub_fields'   => array(
					array(
						'key'   => 'city_name',
						'label' => 'Name of city',
						'name'  => 'city_name',
						'type'  => 'text',
					),

					array(
						'key'          => 'repeating_practice_areas',
						'label'        => 'Add New Practice Area to City',
						'name'         => 'repeating_practice_areas',
						'type'         => 'repeater',
						'layout'       => 'block',
						'button_label' => 'Add New Practice Area',
						'sub_fields'   => array(
							array(
								'key'   => 'practice_area_name',
								'label' => 'Practice Area Name',
								'name'  => 'practice_area_name',
								'type'  => 'text',
							),

							array(
								'key'           => 'practice_area_link',
								'label'         => 'Practice Area Link',
								'name'          => 'practice_area_link',
								'type'          => 'link',
								'return_format' => 'object',

							),
						),
					),
				),
			)
		),
		'location'              => array(
			array(
				array(
					'param'    => 'post_type',
					'operator' => '==',
					'value'    => 'counties',
				),
			),
		),
		'menu_order'            => 0,
		'position'              => 'acf_after_title',
		'style'                 => 'default',
		'label_placement'       => 'top',
		'hide_on_screen'        => '',
		'instruction_placement' => 'label',
		'active'                => true,
		'description'           => '',
	)
);

