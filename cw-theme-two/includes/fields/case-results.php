<?php
acf_add_local_field_group( array(
		'key'                   => 'case_results_section',
		'title'                 => 'Case Results Information',
		'fields'                => array(
//			array(
//				'key'          => 'case_results_info_tab',
//				'label'        => 'Case Results Information',
//				'name'         => 'community_involvement_heading_section',
//				'type'         => 'tab',
//				'instructions' => '',
//				'placement'    => 'top',
//			),

			array(
				'key'   => 'case_results_header_background',
				'name'  => 'case_results_header_background',
				'label' => 'Case Result Banner Image',
				'type'  => 'image',
				'return_format' => 'object',
			),

			array(
				'key'   => 'case_results_header_content',
				'name'  => 'case_results_header_content',
				'label' => 'Case Result Banner Content',
				'type'  => 'wysiwyg',
			),
		),
		'location'              => array(
			array(
				array(
					'param'    => 'post_template',
					'operator' => '==',
					'value'    => 'template-case-results.php',
				),
			),
		),
		'menu_order'            => 0,
		'position'              => 'acf_after_title',
		'style'                 => 'default',
		'label_placement'       => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen'        => '',
		'active'                => true,
		'description'           => '',
	)
);