<?php
acf_add_local_field_group(array(
	'key' => 'post_sub_category_relationship_block',
	'title' => 'Article topic Relationship Settings',
	'fields' => array(
		array(
			'key'                 => 'article_settings',
			'label'               => 'Article Settings',
			'name'                => 'article_settings',
			'type'                => 'tab',
			'placement'           => 'top',
		),

		array(
			'key' => 'sub_category_relationship',
			'label' => 'Relationship to Article topic',
			'name' => 'sub_category_relationship',
			'type' => 'taxonomy',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'taxonomy' => 'sub_category',
			'field_type' => 'select',
			'allow_null' => 1,
			'add_term' => 0,
			'save_terms' => 1,
			'load_terms' => 0,
			'return_format' => 'id',
			'multiple' => 0,
		),

		array(
			'key'                 => 'post_faq_questions',
			'label'               => 'List of FAQ',
			'name'                => 'post_faq_questions',
			'type'                => 'repeater',
			'instructions'        => 'Add an additional FAQ Item',
			'layout'              => 'row',
			'button_label'        => 'Add FAQ',
			'sub_fields'          => array(
				array(
					'key'          => 'faq_title',
					'label'        => 'FAQ Title',
					'name'         => 'faq_title',
					'type'         => 'text',
					'instructions' => 'Enter the FAQ Title',
					'required'     => 1,
				),

				array(
					'key'           => 'faq_link',
					'label'         => 'Link to FAQ',
					'name'          => 'faq_link',
					'type'          => 'url',
					'return_format' => 'url',
				),
			),
		),

		array(
			'key'                 => 'event_settings',
			'label'               => 'Event Settings',
			'name'                => 'event_settings',
			'type'                => 'tab',
			'placement'           => 'top',
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'post',
			),
		),
	),
	'menu_order'            => 0,
	'position'              => 'acf_after_title',
	'style'                 => 'default',
	'label_placement'       => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));