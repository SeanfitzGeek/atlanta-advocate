<?php

acf_add_local_field_group( array(
		'key'                   => 'locations_communities_content_blocks',
		'title'                 => 'Enter information about Office Location',
		'fields'                => array(
			array(
				'key'                 => 'locations_communities_main_content',
				'label'               => 'Main Community Information',
				'name'                => 'locations_communities_information_tab',
				'type'                => 'tab',
				'instructions'        => '',
				'required'            => 0,
				'conditional_logic'   => 0,
				'placement'           => 'top',
				'endpoint'            => 0,
				'wpml_cf_preferences' => 0,
			),

			array(
				'key'                 => 'locations_communities_main_content',
				'label'               => 'Main Community Information',
				'name'                => 'locations_communities_main_content',
				'type'                => 'wysiwyg',
				'instructions'        => '',
			),
		),
		'location'              => array(
			array(
				array(
					'param'    => 'post_template',
					'operator' => '==',
					'value'    => 'template-communities-we-serve.php',
				),
			),
		),
		'menu_order'            => 0,
		'position'              => 'acf_after_title',
		'style'                 => 'default',
		'label_placement'       => 'top',
		'hide_on_screen'        => '',
		'instruction_placement' => 'label',
		'active'                => true,
		'description'           => '',
	)
);