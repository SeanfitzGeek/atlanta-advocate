<?php

acf_add_local_field_group( array(
		'key'                   => 'practice_areas_content_blocks',
		'title'                 => 'Enter information about Practice Areas',
		'fields'                => array(
			array(
				'key'                 => 'practice_areas_main_content',
				'label'               => 'Main Call To Action',
				'name'                => 'practice_areas_main_content',
				'type'                => 'tab',
				'instructions'        => '',
				'required'            => 0,
				'conditional_logic'   => 0,
				'placement'           => 'top',
				'endpoint'            => 0,
				'wpml_cf_preferences' => 0,
			),

			array(
				'key'                 => 'main_call_to_action',
				'label'               => 'Main red practice area content block',
				'name'                => 'main_call_to_action',
				'type'                => 'wysiwyg',
				'instructions'        => '',
				'required'            => 0,
				'conditional_logic'   => 0,
				'default_value'       => '',
				'placeholder'         => '',
				'maxlength'           => '',
				'rows'                => '',
				'new_lines'           => '',
				'wpml_cf_preferences' => 0,
			),

			array(
				'key'           => 'main_practice_area_background',
				'label'         => 'Main practice area background',
				'name'          => 'main_practice_area_background',
				'type'          => 'image',
				'instructions'  => 'Add a background image for this Main Area (Best if larger than 900px x 900px)',
				'return_format' => 'url',
				'preview_size'  => 'thumbnail',
				'library'       => 'all',
			),



			array(
				'key'                 => 'practice_areas_categories_tab',
				'label'               => 'List of Categories',
				'name'                => 'practice_areas_categories_tab',
				'type'                => 'tab',
				'instructions'        => '',
				'required'            => 0,
				'conditional_logic'   => 0,
				'placement'           => 'top',
				'endpoint'            => 0,
				'wpml_cf_preferences' => 0,
			),

			array(
				'key'                 => 'practice_areas_categories',
				'label'               => 'List of Practice Area Categories',
				'name'                => 'practice_areas_categories',
				'type'                => 'repeater',
				'instructions'        => 'Add an additional Practice Areas Category',
				'required'            => 0,
				'conditional_logic'   => 0,
				'collapsed'           => '',
				'min'                 => 0,
				'max'                 => 0,
				'layout'              => 'row',
				'button_label'        => 'Add Practice Area Category',
				'wpml_cf_preferences' => 0,
				'sub_fields'          => array(
					array(
						'key'          => 'practice_area_title',
						'label'        => 'Practice Area Title',
						'name'         => 'practice_area_title',
						'type'         => 'text',
						'instructions' => 'Enter the Practice Area Title',
						'required'     => 1,
					),

					array(
						'key'           => 'practice_area_title_background',
						'label'         => 'Practice Area Background Image',
						'name'          => 'practice_area_title_background',
						'type'          => 'image',
						'instructions'  => 'Add a background image for this sections dropdown area',
						'return_format' => 'url',
						'preview_size'  => 'thumbnail',
						'library'       => 'all',
					),

					array(
						'key'          => 'practice_area_category_description',
						'label'        => 'Practice Area Category Description',
						'name'         => 'practice_area_category_description',
						'type'         => 'wysiwyg',
						'instructions' => 'Enter a short description of the category (this appears first, before the video and main category description content_',
					),

					array(
						'key'          => 'practice_area_category_video',
						'label'        => 'Practice Area Category Video',
						'name'         => 'practice_area_category_video',
						'type'         => 'textarea',
						'instructions' => 'Enter an embedded video code into this block to display a video',
					),

					array(
						'key'                 => 'practice_areas_categories_sub_information',
						'label'               => 'Add Sub Information for each section in a dropdown content block',
						'name'                => 'practice_areas_categories_sub_information',
						'type'                => 'repeater',
						'instructions'        => 'Add additional information tabs inside the category (appears after a short category description',
						'required'            => 0,
						'conditional_logic'   => 0,
						'collapsed'           => '',
						'min'                 => 0,
						'max'                 => 0,
						'layout'              => 'block',
						'button_label'        => 'Add Sub Information Dropdowns',
						'wpml_cf_preferences' => 0,
						'sub_fields'          => array(
							array(
								'key'          => 'sub_info_title',
								'label'        => 'Sub Info Title',
								'name'         => 'sub_info_title',
								'type'         => 'text',
								'instructions' => 'Enter the Sub Info Title',
							),

							array(
								'key'          => 'sub_field_link',
								'label'        => 'Optional Sub Info Link',
								'name'         => 'sub_field_link',
								'type'         => 'url',
								'instructions' => 'Instead of dropping down with content - this will directly link the user to a page specified',
							),

							array(
								'key'          => 'sub_info_content',
								'label'        => 'Sub Info Content',
								'name'         => 'sub_info_content',
								'type'         => 'wysiwyg',
								'instructions' => 'Enter the Sub Info Content',
							),
						),
					),

					array(
						'key'          => 'practice_area_category_full_information',
						'label'        => 'Practice Area Category Full Information',
						'name'         => 'practice_area_category_full_information',
						'type'         => 'wysiwyg',
						'instructions' => 'Enter information that will display inside the dropdown for this category (appears after main video if video is present)',
					),

					array(
						'key'                 => 'practice_areas_categories_image_gallery',
						'label'               => 'Add Multiple Images at end of content',
						'name'                => 'practice_areas_categories_image_gallery',
						'type'                => 'repeater',
						'instructions'        => 'This will add multiple thumbnail sized images at the end of the categories content',
						'required'            => 0,
						'conditional_logic'   => 0,
						'collapsed'           => '',
						'min'                 => 0,
						'max'                 => 0,
						'layout'              => 'block',
						'button_label'        => 'Add Additional Thumbnail Images',
						'wpml_cf_preferences' => 0,
						'sub_fields'          => array(
							array(
								'key'           => 'practice_areas_categories_image_gallery_image',
								'label'         => 'Practice Area Gallery Image',
								'name'          => 'practice_areas_categories_image_gallery_image',
								'type'          => 'image',
								'instructions'  => 'Add a Gallery image for this Category at the end of the content',
								'return_format' => 'object',
								'preview_size'  => 'thumbnail',
								'library'       => 'all',
							),
						),
					),
				),

			),

		),
		'location'              => array(
			array(
				array(
					'param'    => 'post_template',
					'operator' => '==',
					'value'    => 'template-practice-areas.php',
				),
			),
		),
		'menu_order'            => 0,
		'position'              => 'acf_after_title',
		'style'                 => 'default',
		'label_placement'       => 'top',
		'hide_on_screen'        => '',
		'instruction_placement' => 'label',
		'active'                => true,
		'description'           => '',
	)
);