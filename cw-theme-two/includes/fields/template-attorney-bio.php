<?php

acf_add_local_field_group( array(
		'key'                   => 'attorney_bio_tabs',
		'title'                 => 'Attorney Bio Information',
		'fields'                => array(
			array(
				'key'          => 'attorney_bio_cta_action_tab',
				'label'        => 'Call To Action Section',
				'name'         => 'attorney_bio_cta_action_tab',
				'type'         => 'tab',
				'instructions' => '',
				'placement'    => 'top',
			),
			array(
				'key'           => 'cta_attorney_bio_section_image',
				'label'         => 'CTA Background',
				'name'          => 'cta_attorney_bio_section_image',
				'type'          => 'image',
				'return_format' => 'array',
				'preview_size'  => 'thumbnail',
				'library'       => 'all',
			),

			array(
				'key'   => 'cta_content',
				'name'  => 'cta_content',
				'label' => 'Content for CTA',
				'type'  => 'wysiwyg'
			),

			array(
				'key'          => 'meet_attorney',
				'label'        => 'Meet the Attorney Section',
				'name'         => 'meet_attorney',
				'type'         => 'tab',
				'instructions' => '',
				'placement'    => 'top',
			),

			array(
				'key'           => 'meet_image',
				'name'          => 'meet_image',
				'label'         => '',
				'type'          => 'image',
				'return_format' => 'array',
				'preview_size'  => 'thumbnail',
			),
			array(
				'key'   => 'meet_content',
				'name'  => 'meet_content',
				'label' => 'Attorney Bio',
				'type'  => 'wysiwyg',
			),


			array(
				'key'       => 'lawyer_trust_section',
				'label'     => 'Lawyer Trust Section',
				'name'      => 'lawyer_trust_section',
				'type'      => 'tab',
				'placement' => 'top',
			),

			array(
				'name'  => 'lawyer_trust_content',
				'key'   => 'lawyer_trust_content',
				'label' => 'Lawyer Trust Content',
				'type'  => 'wysiwyg'
			),

			array(
				'key'          => 'trust_badges',
				'name'         => 'trust_badges',
				'label'        => 'Image of Awards/Badges',
				'type'         => 'repeater',
				'layout'       => 'table',
				'button_label' => 'Add additional Badge',
				'sub_fields'   => array(
					array(
						'key'           => 'trust_badge',
						'label'         => 'Badge Image',
						'name'          => 'trust_badge',
						'type'          => 'image',
						'return_format' => 'object',
					),

					array(
						'key'           => 'badge_title',
						'label'         => 'Badge Title',
						'name'          => 'badge_title',
						'type'          => 'text',
					),
				),
			),


			array(
				'key'       => 'lawyer_creds',
				'label'     => 'Lawyer Credientials',
				'name'      => 'lawyer_creds',
				'type'      => 'tab',
				'placement' => 'top',
			),

			array(
				'key'           => 'lawyer_creds_bg',
				'name'          => 'lawyer_creds_bg',
				'label'         => 'Credientials Background',
				'type'          => 'image',
				'return_format' => 'object',
			),

			array(
				'key'   => 'lawyer_creds_title',
				'name'  => 'lawyer_creds_title',
				'label' => 'Credientials Title',
				'type'  => 'text',
			),
			array(
				'key'   => 'lawyer_creds_content',
				'name'  => 'lawyer_creds_content',
				'label' => 'Lawyer Credientials Content',
				'type'  => 'wysiwyg',
			),

			array(
				'key'          => 'video_section',
				'label'        => 'Video Section',
				'name'         => 'video_section',
				'type'         => 'tab',
				'instructions' => '',
				'placement'    => 'top',
			),

			array(
				'key'   => 'video_section_media',
				'name'  => 'video_section_media',
				'label' => 'Video for Video Section if available',
				'type'  => 'wysiwyg',
			),


		),
		'location'              => array(
			array(
				array(
					'param'    => 'post_template',
					'operator' => '==',
					'value'    => 'template-attorney-bio.php',
				),
			),
		),
		'menu_order'            => 0,
		'position'              => 'acf_after_title',
		'style'                 => 'default',
		'label_placement'       => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen'        => '',
		'active'                => true,
		'description'           => '',
	)
);
