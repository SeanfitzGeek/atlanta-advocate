<?php
acf_add_local_field_group( array(
	'key'                   => 'locations_single_practice_area_template',
	'title'                 => 'Content for Locations and Sidebar',
	'fields'                => array(
		array(
			'key'                 => 'location_practice_area_main_cta_section',
			'label'               => 'Call To Action Section',
			'name'                => 'location_practice_area_main_cta_section',
			'type'                => 'tab',
			'instructions'        => '',
			'required'            => 0,
			'conditional_logic'   => 0,
			'placement'           => 'top',
			'endpoint'            => 0,
			'wpml_cf_preferences' => 0,
		),
		array(
			'key'    => 'location_practice_area_main_banner_image',
			'label'  => 'Main Banner Image',
			'name'   => 'location_practice_area_main_banner_image',
			'type'   => 'image',
			'return' => 'object'
		),

		array(
			'key'   => 'page_information',
			'name'  => 'page_information',
			'label' => 'Short Description about this practice area (Below H1 Page Title Tag)',
			'type'  => 'text',
		),

		array(
			'key'   => 'location_practice_page_h2',
			'name'  => 'location_practice_page_h2',
			'label' => 'Page h2 Heading',
			'type'  => 'text',
		),

		array(
			'key'   => 'cta_video',
			'name'  => 'cta_video',
			'label' => 'CTA Video Popup (Embedded Video Code)',
			'type'  => 'textarea',
		),

		array(
			'key'   => 'cta_learn_more',
			'name'  => 'cta_learn_more',
			'label' => 'CTA Learn More Link',
			'type'  => 'link',
			'return' => 'url',
		),

		array(
			'key'                 => 'why_chose_practice_area',
			'label'               => 'Why Choose',
			'name'                => 'why_chose_practice_area',
			'type'                => 'tab',
			'instructions'        => '',
			'required'            => 0,
			'conditional_logic'   => 0,
			'placement'           => 'top',
			'endpoint'            => 0,
			'wpml_cf_preferences' => 0,
		),
		array(
			'key'    => 'why_choose_banner_image',
			'label'  => 'Banner Image',
			'name'   => 'why_choose_banner_image',
			'type'   => 'image',
			'return' => 'object'
		),

		array(
			'key'   => 'why_choose_content',
			'name'  => 'why_choose_content',
			'label' => 'Why Choose Content',
			'type'  => 'wysiwyg',
		),

		array(
			'key'                 => 'sidebar_information',
			'label'               => 'Sidebar Information',
			'name'                => 'sidebar_information',
			'type'                => 'tab',
			'instructions'        => '',
			'required'            => 0,
			'conditional_logic'   => 0,
			'placement'           => 'top',
			'endpoint'            => 0,
			'wpml_cf_preferences' => 0,
		),
		array(
			'key'   => 'other_info_title',
			'name'  => 'other_info_title',
			'label' => 'Other Information Title',
			'type'  => 'text',
		),


		array(
			'key'          => 'other_info_links',
			'name'         => 'other_info_links',
			'label'        => 'Other Information Links',
			'type'         => 'repeater',
			'layout'       => 'table',
			'button_label' => 'Add Other Information Link',
			'sub_fields'   => array(
				array(
					'key'                 => 'title',
					'label'               => 'Link Title',
					'name'                => 'title',
					'type'                => 'text',
				),

				array(
					'key'                 => 'other_link',
					'label'               => 'Link',
					'name'                => 'other_link',
					'type'                => 'link',
				),
			),
		),


		array(
			'key'          => 'related_topics',
			'name'         => 'related_topics',
			'label'        => 'Related Topics',
			'type'         => 'repeater',
			'layout'       => 'table',
			'button_label' => 'Add Other Related Topics',
			'sub_fields'   => array(
				array(
					'key'                 => 'related_topics_title',
					'label'               => 'Link Title',
					'name'                => 'related_topics_title',
					'type'                => 'text',
				),

				array(
					'key'                 => 'related_topics_other_link',
					'label'               => 'Link',
					'name'                => 'related_topics_other_link',
					'type'                => 'link',
				),
			),
		),


		array(
			'key'          => 'faq_title',
			'name'         => 'faq_title',
			'label'        => 'FAQ Title',
			'type'         => 'text',
		),

			array(
			'key'          => 'faq_questions',
			'name'         => 'faq_questions',
			'label'        => 'FAQ Questions',
			'type'         => 'repeater',
			'layout'       => 'row',
			'button_label' => 'Add FAQ Question',
			'sub_fields'   => array(
				array(
					'key'                 => 'faq_question_title',
					'label'               => 'FAQ Title/Question',
					'name'                => 'faq_question_title',
					'type'                => 'text',
				),

				array(
					'key'                 => 'faq_question_content',
					'label'               => 'FAQ Content/Answer',
					'name'                => 'faq_question_content',
					'type'                => 'wysiwyg',
				),
			),
		),


	),
	'location'              => array(
		array(
			array(
				'param'    => 'post_template',
				'operator' => '==',
				'value'    => 'template-location-practice-area.php',
			),
		),
	),
	'menu_order'            => 0,
	'position'              => 'acf_after_title',
	'style'                 => 'default',
	'label_placement'       => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen'        => '',
	'active'                => true,
	'description'           => '',
) );
