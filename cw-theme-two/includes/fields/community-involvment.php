<?php
acf_add_local_field_group( array(
		'key'                   => 'community_involvement_section',
		'title'                 => 'Community Involvement',
		'fields'                => array(
			array(
				'key'          => 'community_involvement_heading_section',
				'label'        => 'Main Heading Sections Content',
				'name'         => 'community_involvement_heading_section',
				'type'         => 'tab',
				'instructions' => '',
				'placement'    => 'top',
			),

			array(
				'key'   => 'community_involvement_header_content',
				'name'  => 'community_involvement_header_content',
				'label' => 'Community Involvement Heading Content',
				'type'  => 'wysiwyg',
			),

			array(
				'key'           => 'community_involvement_header_background',
				'name'          => 'community_involvement_header_background',
				'label'         => 'Community Involvement Heading Background Image',
				'type'          => 'image',
				'return_format' => 'object'
			),

			array(
				'key'           => 'community_involvement_header_background_mobile',
				'name'          => 'community_involvement_header_background_mobile',
				'label'         => 'Community Involvement Heading Background Image on mobile devices',
				'type'          => 'image',
				'return_format' => 'object'
			),

			array(
				'key'          => 'miller_community',
				'label'        => 'Miller in the Community',
				'name'         => 'miller_community',
				'type'         => 'tab',
				'instructions' => '',
				'placement'    => 'top',
			),

			array(
				'key'   => 'miller_community_content',
				'name'  => 'miller_community_content',
				'label' => 'Miller in the Community Content',
				'type'  => 'wysiwyg',
			),

			array(
				'key'        => 'community_relationship',
				'name'       => 'community_relationship',
				'label'      => 'Post category to display for Miller Community Sectionx',
				'type' => 'taxonomy',
				'instructions' => '',
				'required' => 0,
				'taxonomy' => 'category',
				'field_type' => 'select',
				'allow_null' => 0,
				'add_term' => 1,
				'save_terms' => 0,
				'load_terms' => 0,
				'return_format' => 'id',
				'multiple' => 0,
			),

			array(
				'key'        => 'events_relationship',
				'name'       => 'events_relationship',
				'label'      => 'Post category to display for Events',
				'type' => 'taxonomy',
				'instructions' => '',
				'required' => 0,
				'taxonomy' => 'category',
				'field_type' => 'select',
				'allow_null' => 0,
				'add_term' => 1,
				'save_terms' => 0,
				'load_terms' => 0,
				'return_format' => 'id',
				'multiple' => 0,
			),


			array(
				'key'          => 'upcoming_events',
				'label'        => 'Upcoming Events',
				'name'         => 'upcoming_events',
				'type'         => 'tab',
				'instructions' => '',
				'placement'    => 'top',
			),

			array(
				'key'   => 'upcoming_event_content',
				'name'  => 'upcoming_event_content',
				'label' => 'Upcoming Event Description/Content',
				'type'  => 'wysiwyg',
			),


			array(
				'key'          => 'recent_events',
				'label'        => 'Recent Events',
				'name'         => 'recent_events',
				'type'         => 'tab',
				'instructions' => '',
				'placement'    => 'top',
			),

			array(
				'key'   => 'recent_event_content',
				'name'  => 'recent_event_content',
				'label' => 'Recent Event Description/Content',
				'type'  => 'wysiwyg',
			),


			array(
				'key'          => 'organizations_we_support_tab',
				'label'        => 'Organizations We Support',
				'name'         => 'organizations_we_support_tab',
				'type'         => 'tab',
				'instructions' => '',
				'placement'    => 'top',
			),

			array(
				'key'          => 'organization_support_title',
				'name'         => 'organization_support_title',
				'label'        => 'Organizations We Support Title',
				'instructions' => 'Insert Heading for Section',
				'type'         => 'text',
			),

			array(
				'key'          => 'organization_support_content',
				'name'         => 'organization_support_content',
				'label'        => 'Organizations We Support Content',
				'instructions' => 'Insert short description that appears after the organizations',
				'type'         => 'wysiwyg',
			),

			array(
				'key'          => 'organization_list',
				'label'        => 'Organization List',
				'name'         => 'organization_list',
				'type'         => 'repeater',
				'layout'       => 'block',
				'button_label' => 'Add New Organization',
				'sub_fields'   => array(
					array(
						'key'   => 'organization_title',
						'name'  => 'organization_title',
						'label' => 'Organization Title',
						'type'  => 'text',
					),

					array(
						'key'           => 'organization_logo',
						'name'          => 'organization_logo',
						'label'         => 'Organization Logo',
						'type'          => 'image',
						'return_format' => 'object'
					),

					array(
						'key'   => 'organization_content',
						'name'  => 'organization_content',
						'label' => 'Organization Content',
						'type'  => 'wysiwyg',
					),

					array(
						'key'   => 'organization_link',
						'name'  => 'organization_link',
						'label' => 'Organization Link',
						'type'  => 'link',
					),
				),
			),

			array(
				'key'          => 'community_involvement_tab',
				'label'        => 'Community Involvement Section',
				'name'         => 'community_involvement_tab',
				'type'         => 'tab',
				'instructions' => '',
				'placement'    => 'top',
			),

			array(
				'key'   => 'community_involvement_content',
				'name'  => 'community_involvement_content',
				'label' => 'Community Involvement Content',
				'type'  => 'wysiwyg',
			),

			array(
				'key'           => 'community_involvement_form_bg',
				'name'          => 'community_involvement_form_bg',
				'label'         => 'Community Involvement Form Background',
				'type'          => 'image',
				'return_format' => 'object'
			),
		),
		'location'              => array(
			array(
				array(
					'param'    => 'post_template',
					'operator' => '==',
					'value'    => 'template-community-involvement.php',
				),
			),
		),
		'menu_order'            => 0,
		'position'              => 'acf_after_title',
		'style'                 => 'default',
		'label_placement'       => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen'        => '',
		'active'                => true,
		'description'           => '',
	)
);