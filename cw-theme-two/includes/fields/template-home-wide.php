<?php

acf_add_local_field_group( array(
	'key'                   => 'wide_home_template_tabs',
	'title'                 => 'Home Template Content',
	'fields'                => array(
		array(
			'key'                 => 'cta_action_tab',
			'label'               => 'Call To Action Section',
			'name'                => '',
			'type'                => 'tab',
			'instructions'        => '',
			'required'            => 0,
			'conditional_logic'   => 0,
			'wrapper'             => array(
				'width' => '',
				'class' => '',
				'id'    => '',
			),
			'placement'           => 'top',
			'endpoint'            => 0,
			'wpml_cf_preferences' => 0,
		),

		array(
			'key'           => 'cta_section_image',
			'label'         => 'CTA Background',
			'name'          => 'cta_section_image',
			'type'          => 'image',
			'return_format' => 'array',
			'preview_size'  => 'thumbnail',
			'library'       => 'all',
		),

		array(
			'key'           => 'cta_awards_image',
			'label'         => 'CTA Lower Left Image Icon/Affiliation Image',
			'name'          => 'cta_awards_image',
			'type'          => 'image',
			'return_format' => 'array',
			'preview_size'  => 'thumbnail',
			'library'       => 'all',
		),


		array(
			'key'          => 'cta_h1_text',
			'label'        => 'CTA H1 Text',
			'name'         => 'cta_h1_text',
			'type'         => 'textarea',
			'instructions' => 'Enter text for H1 Block',
		),


		array(
			'key'          => 'cta_video',
			'label'        => 'Call To Action Video Iframe',
			'name'         => 'cta_video',
			'type'         => 'textarea',
			'instructions' => 'Enter the iframe code for a video to show in this initial CTA section',
		),


		array(
			'key'                 => 'field_5db85e67b1d60',
			'label'               => 'Cta Content',
			'name'                => 'cta_content',
			'type'                => 'wysiwyg',
			'instructions'        => '',
			'required'            => 0,
			'conditional_logic'   => 0,
			'wrapper'             => array(
				'width' => '',
				'class' => '',
				'id'    => '',
			),
			'default_value'       => '',
			'tabs'                => 'all',
			'toolbar'             => 'full',
			'media_upload'        => 1,
			'delay'               => 0,
			'wpml_cf_preferences' => 0,
		),
		array(
			'key'               => 'practice_areas_block',
			'label'             => 'Practice Areas',
			'name'              => '',
			'type'              => 'tab',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => 0,

			'wpml_cf_preferences' => 0,
			'placement'           => 'top',
			'endpoint'            => 0,
		),

		array(
			'key'          => 'practice_areas_repeater_fields',
			'label'        => 'Home Practice Areas',
			'name'         => 'practice_areas_repeater_fields',
			'type'         => 'repeater',
			'layout'       => 'block',
			'button_label' => '',
			'sub_fields'   => array(
				array(
					'key'          => 'practice_title',
					'label'        => 'Practice Title',
					'name'         => 'practice_title',
					'type'         => 'text',
					'instructions' => '',
					'required'     => 0,

				),
				array(
					'key'               => 'practice_link',
					'label'             => 'Practice Areas URL',
					'name'              => 'practice_link',
					'type'              => 'link',
					'instructions'      => '',
					'required'          => 0,
					'conditional_logic' => 0,
					'default_value'     => '',
					'placeholder'       => '',
				),
				array(
					'key'           => 'practice_bg',
					'label'         => 'Practice Area background',
					'name'          => 'practice_bg',
					'type'          => 'image',
					'return_format' => 'array',
					'preview_size'  => 'thumbnail',
					'library'       => 'all',
					'max_width'     => 400,
					'max_height'    => 390,
				),
			),
		),

		array(
			'key'                 => 'field_5db22e79f7ab5',
			'label'               => 'Meet Our Team',
			'name'                => '',
			'type'                => 'tab',
			'instructions'        => '',
			'required'            => 0,
			'conditional_logic'   => 0,
			'wrapper'             => array(
				'width' => '',
				'class' => '',
				'id'    => '',
			),
			'wpml_cf_preferences' => 0,
			'placement'           => 'top',
			'endpoint'            => 0,
		),

		array(
			'key'                 => 'repeater_sections',
			'label'               => 'Repeatable Sections',
			'name'                => '',
			'type'                => 'tab',
			'instructions'        => '',
			'required'            => 0,
			'conditional_logic'   => 0,
			'wrapper'             => array(
				'width' => '',
				'class' => '',
				'id'    => '',
			),
			'wpml_cf_preferences' => 0,
			'placement'           => 'top',
			'endpoint'            => 0,
		),

		array(
			'key'          => 'repeater_section_block',
			'label'        => 'Repeatable Section',
			'name'         => 'repeater_section_block',
			'type'         => 'repeater',
			'layout'       => 'block',
			'button_label' => '',
			'sub_fields'   => array(
				array(
					'key'          => 'title',
					'label'        => 'Section Title',
					'name'         => 'title',
					'type'         => 'text',
					'instructions' => '',
				),

				array(
					'key'          => 'content',
					'label'        => 'Section Content',
					'name'         => 'content',
					'type'         => 'textarea',
					'instructions' => '',
				),
				array(
					'key'          => 'more_link',
					'label'        => 'Read More Link',
					'name'         => 'more_link',
					'type'         => 'link',
					'instructions' => '',
				),

				array(
					'key'          => 'alignment',
					'label'        => 'Content Alignment',
					'name'         => 'alignment',
					'type'         => 'radio',
					'instructions' => '',
					'choices'      => array(
						'left'  => 'left',
						'right' => 'right',
					),
					'required'     => true,
				),
				array(
					'key'           => 'background_image',
					'label'         => 'Background Image',
					'name'          => 'background_image',
					'type'          => 'image',
					'return_format' => 'array',
					'preview_size'  => 'thumbnail',
					'library'       => 'all',
					'max_width'     => 2000,
					'max_height'    => 2000,
				),
			),
		),

		array(
			'key'               => 'client_stories_block',
			'label'             => 'Client Stories',
			'name'              => 'client_stories_block',
			'type'              => 'tab',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => 0,

			'wpml_cf_preferences' => 0,
			'placement'           => 'top',
			'endpoint'            => 0,
		),
		array(
			'key'          => 'client_stories_repeater_fields',
			'label'        => 'Add Client Stories',
			'name'         => 'client_stories_repeater_fields',
			'type'         => 'repeater',
			'layout'       => 'block',
			'button_label' => '',
			'sub_fields'   => array(
				array(
					'key'          => 'client_story_title',
					'label'        => 'Client Story Title',
					'name'         => 'client_story_title',
					'type'         => 'text',
					'instructions' => '',
					'required'     => 0,
				),

				array(
					'key'          => 'client_story_content',
					'label'        => 'Client Story Content',
					'name'         => 'client_story_content',
					'type'         => 'wysiwyg',
					'instructions' => '',
					'required'     => 0,
				),
				array(
					'key'           => 'client_story_background_image',
					'label'         => 'Background Image',
					'name'          => 'client_story_background_image',
					'type'          => 'image',
					'return_format' => 'array',
					'preview_size'  => 'thumbnail',
					'library'       => 'all',
					'max_width'     => 2000,
					'max_height'    => 2000,
				),
			),
		),


	),
	'location'              => array(
		array(
			array(
				'param'    => 'post_template',
				'operator' => '==',
				'value'    => 'template-wide-home.php',
			),
		),
	),
	'menu_order'            => 0,
	'position'              => 'acf_after_title',
	'style'                 => 'default',
	'label_placement'       => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen'        => '',
	'active'                => true,
	'description'           => '',
) );
