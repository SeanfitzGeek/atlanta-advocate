<?php


acf_add_local_field_group( array(
	'key'                   => 'locations_single_template_group',
	'title'                 => 'Content for Locations and Sidebar',
	'fields'                => array(

		array(
			'key'                 => 'main_content_tab',
			'label'               => 'Main Content',
			'name'                => 'main_content_tab',
			'type'                => 'tab',
			'placement'           => 'top',
			'endpoint'            => 0,
			'wpml_cf_preferences' => 0,
		),

		array(
			'key'   => 'sub_title',
			'label' => 'Sub Title',
			'name'  => 'sub_title',
			'type'  => 'text',
		),

		array(
			'key'   => 'sub_address_road',
			'label' => 'Sub Address Used for better location accuracy/description. (use a road)',
			'name'  => 'sub_address_road',
			'type'  => 'text',
		),

		array(
			'key'               => 'field_57f59952c1a9b',
			'label'             => 'Directions Link',
			'name'              => 'directions_link',
			'type'              => 'text',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => 0,
			'wrapper'           => array(
				'width' => '',
				'class' => '',
				'id'    => '',
			),
			'default_value'     => '',
			'placeholder'       => '',
			'prepend'           => '',
			'append'            => '',
			'maxlength'         => '',
		),
		array(
			'key'               => 'field_57f5977c5b688',
			'label'             => 'Show Team',
			'name'              => 'show_team',
			'type'              => 'true_false',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => 0,
			'wrapper'           => array(
				'width' => '',
				'class' => '',
				'id'    => '',
			),
			'message'           => '',
			'default_value'     => 0,
		),
		array(
			'key'               => 'field_57f597905b689',
			'label'             => 'Team Title',
			'name'              => 'team_title',
			'type'              => 'text',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => array(
				array(
					array(
						'field'    => 'field_57f5977c5b688',
						'operator' => '==',
						'value'    => '1',
					),
				),
			),
			'wrapper'           => array(
				'width' => '',
				'class' => '',
				'id'    => '',
			),
			'default_value'     => '',
			'placeholder'       => '',
			'prepend'           => '',
			'append'            => '',
			'maxlength'         => '',
		),
		array(
			'key'               => 'field_57f597b35b68a',
			'label'             => 'Team Description',
			'name'              => 'team_description',
			'type'              => 'wysiwyg',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => array(
				array(
					array(
						'field'    => 'field_57f5977c5b688',
						'operator' => '==',
						'value'    => '1',
					),
				),
			),
			'wrapper'           => array(
				'width' => '',
				'class' => '',
				'id'    => '',
			),
			'default_value'     => '',
			'tabs'              => 'all',
			'toolbar'           => 'full',
			'media_upload'      => 1,
		),
		array(
			'key'               => 'field_57f597bd5b68b',
			'label'             => 'Team Members',
			'name'              => 'team_members',


			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => array(
				array(
					array(
						'field'    => 'field_57f5977c5b688',
						'operator' => '==',
						'value'    => '1',
					),
				),
			),
			'wrapper'           => array(
				'width' => '',
				'class' => '',
				'id'    => '',
			),
			'post_type'         => array(
				0 => 'page',
			),

			'taxonomy'          => array(),
			'filters'           => '',
			'elements'          => '',
			'min'               => '',
			'max'               => '',
			'return_format'     => 'object',
		),
		array(
			'key'               => 'field_5a94b755cae5d',
			'label'             => 'Location Bottom Content',
			'name'              => 'location_bottom_content',
			'type'              => 'wysiwyg',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => 0,
			'wrapper'           => array(
				'width' => '',
				'class' => '',
				'id'    => '',
			),
			'default_value'     => '',
			'tabs'              => 'all',
			'toolbar'           => 'full',
			'media_upload'      => 1,
		),

		array(
			'key'   => 'location_schema_meta_data',
			'label' => 'Location Schema Meta Data',
			'name'  => 'location_schema_meta_data',
			'type'  => 'textarea',
		),
		array(
			'key'   => 'office_location_image',
			'label' => 'Office Location Image',
			'name'  => 'office_location_image',
			'type'  => 'image',
			'return_format' => 'array',
			'preview_size'  => 'thumbnail',
		),


		array(
			'key'                 => 'sidebar_content_tab',
			'label'               => 'Sidebar Content',
			'name'                => 'sidebar_content_tab',
			'type'                => 'tab',
			'placement'           => 'top',
			'endpoint'            => 0,
			'wpml_cf_preferences' => 0,
		),

		array(
			'key'           => 'sidebar_map',
			'label'         => 'Sidebar Map',
			'name'          => 'sidebar_map',
			'type'          => 'image',
			'return_format' => 'array',
			'preview_size'  => 'thumbnail',
		),


		array(
			'key'   => 'sidebar_video_title',
			'label' => 'Sidebar Video Title',
			'name'  => 'sidebar_video_title',
			'type'  => 'text',
		),
		array(
			'key'          => 'sidebar_video',
			'label'        => 'Sidebar Video',
			'name'         => 'sidebar_video',
			'type'         => 'textarea',
			'instructions' => 'Video Iframe for sidebar',
		),
		array(
			'key'          => 'sidebar_video_content',
			'label'        => 'Description of Video',
			'name'         => 'sidebar_video_content',
			'type'         => 'wysiwyg',
			'instructions' => 'Short description of video',
		),
		array(
			'key'       => 'meet_attorney_tab',
			'label'     => 'Meet Attorneys Tab',
			'name'      => 'meet_attorney_tab',
			'type'      => 'tab',
			'placement' => 'top',
		),
		array(
			'key'     => 'meet_attorney_title',
			'label'   => 'Meet Attorney Title',
			'name'    => 'meet_attorney_title',
			'type'    => 'text',
			'default' => 'Meet Our Atlanta Injury Lawers',
		),


		array(
			'key'       => 'practice_areas_tab',
			'label'     => 'Practice Areas',
			'name'      => 'practice_areas_tab',
			'type'      => 'tab',
			'placement' => 'top',
		),

		array(
			'key'     => 'practice_areas_title',
			'label'   => 'Practice Areas Title',
			'name'    => 'practice_areas_title',
			'type'    => 'text',
			'default' => 'Atlanta Practice Areas',
		),

		array(
			'key'   => 'page_main_section_title',
			'label' => 'Main Practice Areas Section Title',
			'name'  => 'page_main_section_title',
			'type'  => 'text'
		),
		array(
			'key'          => 'main_practice_areas',
			'label'        => 'Main Practice Areas',
			'name'         => 'main_practice_areas',
			'type'         => 'repeater',
			'layout'       => 'row',
			'button_label' => 'Add Practice Area',
			'sub_fields'   => array(
//				array(
//					'key'                 => 'practice_title',
//					'label'               => 'Practice Title',
//					'name'                => 'practice_title',
//					'type'                => 'text',
//
//				),
//				array(
//					'key'                 => 'url',
//					'label'               => 'url',
//					'name'                => 'url',
//					'type'                => 'link',
//					'instructions'        => '',
//					'required'            => 0,
//					'conditional_logic'   => 0,
//					'default_value'       => '',
//					'placeholder'         => '',
//					'wpml_cf_preferences' => 0,
//				),
			),
		),
		array(
			'key'   => 'additional_practice_area_title',
			'label' => 'Additional Practice Areas Title',
			'name'  => 'additional_practice_area_title',
			'type'  => 'text',
		),

		array(
			'key'                 => 'additional_practice_areas_categories',
			'label'               => 'Additional Practice Areas Categories',
			'name'                => 'additional_practice_areas_categories',
			'type'                => 'repeater',
			'instructions'        => '',
			'required'            => 0,
			'conditional_logic'   => 0,
			'collapsed'           => '',
			'min'                 => 0,
			'max'                 => 0,
			'layout'              => 'row',
			'wpml_cf_preferences' => 0,
			'button_label'        => 'Add Category',
			'sub_fields'          => array(
				array(
					'key'   => 'url',
					'label' => 'Additional Practice Areas Category URL',
					'name'  => 'url',
					'type'  => 'link',
				),
				array(
					'key'   => 'title',
					'label' => 'Title',
					'name'  => 'title',
					'type'  => 'text',
				),
				array(
					'key'           => 'icon',
					'label'         => 'Icon',
					'name'          => 'icon',
					'type'          => 'image',
					'return_format' => 'array',
					'preview_size'  => 'thumbnail',
					'library'       => 'all',

				),
				array(
					'key'                 => 'additional_practice_areas_links',
					'label'               => 'Additional Practice Areas Links',
					'name'                => 'additional_practice_areas_links',
					'type'                => 'repeater',
					'layout'              => 'table',
					'button_label'        => 'Add URL',
					'wpml_cf_preferences' => 0,
					'sub_fields'          => array(
						array(
							'key'                 => 'title',
							'label'               => 'Title',
							'name'                => 'title',
							'type'                => 'text',
							'instructions'        => '',
							'required'            => 0,
							'conditional_logic'   => 0,
							'wpml_cf_preferences' => 0,
							'default_value'       => '',
							'placeholder'         => '',
							'prepend'             => '',
							'append'              => '',
							'maxlength'           => '',
						),
						array(
							'key'                 => 'url',
							'label'               => 'url',
							'name'                => 'url',
							'type'                => 'link',
							'instructions'        => '',
							'required'            => 0,
							'conditional_logic'   => 0,
							'default_value'       => '',
							'placeholder'         => '',
							'wpml_cf_preferences' => 0,
						),
						array(
							'key'                 => 'left_or_right_column',
							'label'               => 'Left or Right Column',
							'name'                => 'left_or_right_column',
							'type'                => 'radio',
							'choices'             => array(
								'Left'  => 'Left',
								'Right' => 'Right',
								'Full'  => 'Full Width',
							),
							'allow_null'          => 0,
							'other_choice'        => 0,
							'default_value'       => 'Left',
							'layout'              => 'vertical',
							'return_format'       => 'value',
							'wpml_cf_preferences' => 0,
							'save_other_choice'   => 0,
						),
					),
				),
			),
		),
	),
	'location'              => array(
		array(
			array(
				'param'    => 'post_type',
				'operator' => '==',
				'value'    => 'wpseo_locations',
			),
		),
	),
	'menu_order'            => 0,
	'position'              => 'acf_after_title',
	'style'                 => 'default',
	'label_placement'       => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen'        => '',
	'active'                => true,
	'description'           => '',
) );

acf_add_local_field_group( array(
		'key'                   => 'pull_content_from_wpseo_locations_post_type',
		'title'                 => 'Choose a Location to pull content from Locations Post Type',
		'fields'                => array(
			array(
				'key'       => 'location_page_id',
				'label'     => 'Location page to pull content from',
				'name'      => 'location_page_id',
				'type'      => 'relationship',
				'single'    => true,
				'post_type' => array(
					0 => 'wpseo_locations',
				),
			),
		),
		'location'              => array(
			array(
				array(
					'param'    => 'post_template',
					'operator' => '==',
					'value'    => 'template-locations-single-redo.php',
				),
			),
		),
		'menu_order'            => 0,
		'position'              => 'acf_after_title',
		'style'                 => 'default',
		'label_placement'       => 'top',
		'hide_on_screen'        => '',
		'instruction_placement' => 'label',
		'active'                => true,
		'description'           => '',
	)
);