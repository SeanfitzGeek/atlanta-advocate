<?php

acf_add_local_field_group( array(
	'key'                   => 'about_us_template_tabs',
	'title'                 => 'About Us Content',
	'fields'                => array(
		array(
			'key'          => 'cta_action_tab',
			'label'        => 'Call To Action Section',
			'name'         => '',
			'type'         => 'tab',
			'instructions' => '',
			'wrapper'      => array(
				'width' => '',
				'class' => '',
				'id'    => '',
			),
			'placement'    => 'top',
		),
		array(
			'key'           => 'cta_section_image',
			'label'         => 'CTA Background',
			'name'          => 'cta_section_image',
			'type'          => 'image',
			'return_format' => 'array',
			'preview_size'  => 'thumbnail',
			'library'       => 'all',
		),

		array(
			'key'               => 'meet_the_lawyers',
			'label'             => 'Meet The Lawyers',
			'name'              => '',
			'type'              => 'tab',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => 0,
			'placement'         => 'top',
			'endpoint'          => 0,
		),
		array(
			'key'   => 'meet_the_lawyers_title',
			'label' => 'Section Title',
			'name'  => 'meet_the_lawyers_title',
			'type'  => 'text',
			'default' => 'Meet Our Legal Staff',
		),

		array(
			'key'          => 'meet_the_lawyers_repeater_fields',
			'label'        => 'Meet The Lawyers',
			'name'         => 'meet_the_lawyers_repeater_fields',
			'type'         => 'repeater',
			'layout'       => 'block',
			'button_label' => 'Add Lawyer',
			'sub_fields'   => array(
				array(
					'key'          => 'lawyer_title',
					'label'        => 'Lawyer Title',
					'name'         => 'lawyer_title',
					'type'         => 'text',
					'required'     => 1,
				),
				array(
					'key'               => 'lawyer_link',
					'label'             => 'Lawyer Page URL',
					'name'              => 'lawyer_link',
					'type'              => 'link',
					'instructions'      => '',
					'required'          => 0,
					'conditional_logic' => 0,
					'default_value'     => '',
					'placeholder'       => '',
				),
				array(
					'key'           => 'lawyer_image',
					'label'         => 'Lawyer Image',
					'name'          => 'lawyer_image',
					'type'          => 'image',
					'return_format' => 'array',
					'preview_size'  => 'thumbnail',
					'library'       => 'all',
					'max_width'     => 400,
					'max_height'    => 390,
				),
			),
		),




		array(
			'key'               => 'meet_our_legal_staff',
			'label'             => 'Meet Our Legal Staff',
			'name'              => '',
			'type'              => 'tab',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => 0,
			'placement'         => 'top',
			'endpoint'          => 0,
		),
		array(
			'key'   => 'meet_our_legal_staff_title',
			'label' => 'Section Title',
			'name'  => 'meet_our_legal_staff_title',
			'type'  => 'text',
			'default' => 'Meet Our Legal Staff',
		),



		array(
			'key'          => 'meet_the_staff_repeater_fields',
			'label'        => 'Meet The Staff',
			'name'         => 'meet_the_staff_repeater_fields',
			'type'         => 'repeater',
			'layout'       => 'block',
			'button_label' => 'Add Staff',
			'sub_fields'   => array(
				array(
					'key'          => 'staff_title',
					'label'        => 'Staff Title',
					'name'         => 'staff_title',
					'type'         => 'text',
					'required'     => 1,
				),
				array(
					'key'               => 'staff_link',
					'label'             => 'Staff Page URL',
					'name'              => 'staff_link',
					'type'              => 'link',
					'instructions'      => '',
					'required'          => 0,
					'conditional_logic' => 0,
					'default_value'     => '',
					'placeholder'       => '',
				),
				array(
					'key'           => 'staff_image',
					'label'         => 'Staff Image',
					'name'          => 'staff_image',
					'type'          => 'image',
					'return_format' => 'array',
					'preview_size'  => 'thumbnail',
					'library'       => 'all',
					'max_width'     => 400,
					'max_height'    => 390,
				),
			),
		),





		array(
			'key'               => 'about_us_section',
			'label'             => 'About Us',
			'name'              => '',
			'type'              => 'tab',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => 0,
			'placement'         => 'top',
			'endpoint'          => 0,
		),

		array(
			'key'   => 'about_us_title',
			'label' => 'Section Title',
			'name'  => 'about_us_title',
			'type'  => 'text',
			'default' => 'Meet Our Legal Staff',
		),

		array(
			'key'   => 'about_us_background',
			'label' => 'Left Image if no video is present',
			'name'  => 'about_us_background',
			'type'  => 'image',
			'default' => 'Add an image that will show on the left if no video is present',
		),

		array(
			'key'   => 'about_us_embed_video',
			'label' => 'External Video Code',
			'name'  => 'about_us_embed_video',
			'type'  => 'textarea',
		),

		array(
			'key'          => 'about_repeater_section_block',
			'label'        => 'Repeatable Content Section',
			'name'         => 'about_repeater_section_block',
			'type'         => 'repeater',
			'layout'       => 'block',
			'button_label' => 'Add New About Content Block',
			'sub_fields'   => array(
				array(
					'key'   => 'about_repeater_section_block_title',
					'label' => 'Section Title',
					'name'  => 'about_repeater_section_block_title',
					'type'  => 'text',
				),

				array(
					'key'          => 'about_repeater_section_block_content',
					'label'        => 'Section Content',
					'name'         => 'about_repeater_section_block_content',
					'type'         => 'wysiwyg',
					'instructions' => '',
				),
				array(
					'key'          => 'more_link',
					'label'        => 'Read More Link',
					'name'         => 'more_link',
					'type'         => 'link',
					'instructions' => '',
				),
			),
		),

		array(
			'key'          => 'case_results',
			'label'        => 'Our Case Results',
			'name'         => 'case_results',
			'type'         => 'tab',
			'instructions' => '',
			'placement'    => 'top',
			'endpoint'     => 0,
		),

		array(
			'key'   => 'case_results_title',
			'label' => 'Section Title',
			'name'  => 'case_results_title',
			'type'  => 'text',
		),

		array(
			'key'          => 'case_results_repeater_section_block',
			'label'        => 'Add Case Results Boxes',
			'name'         => 'case_results_repeater_section_block',
			'type'         => 'repeater',
			'layout'       => 'block',
			'button_label' => 'Add New Case Result',
			'sub_fields'   => array(
				array(
					'key'   => 'title',
					'label' => 'Type Of Case Result',
					'name'  => 'title',
					'type'  => 'text',
				),

				array(
					'key'          => 'results_value',
					'label'        => 'Results Value',
					'name'         => 'results_value',
					'type'         => 'text',
					'instructions' => '',
				),
				array(
					'key'          => 'results_image',
					'label'        => 'Results Image',
					'name'         => 'results_image',
					'type'         => 'image',
				),
				array(
					'key'          => 'more_link',
					'label'        => 'Link to Learn More',
					'name'         => 'more_link',
					'type'         => 'link',
					'instructions' => '',
				),
			),
		),


		array(
			'key'          => 'testimonials_tab',
			'label'        => 'Client Stories',
			'name'         => 'testimonials_tab',
			'type'         => 'tab',
			'instructions' => '',
			'placement'    => 'top',
			'endpoint'     => 0,
		),

		array(
			'key'   => 'client_testimonials_title',
			'label' => 'Client Testimonials Section Title',
			'name'  => 'client_testimonials_title',
			'type'  => 'text',
		),

		array(
			'key'   => 'all_reviews_link',
			'label' => 'Link To all Reviews',
			'name'  => 'all_reviews_link',
			'type'  => 'textarea',
		),
		array(
			'key'          => 'client_testimonials',
			'label'        => 'Add Client Testimonial',
			'name'         => 'client_testimonials',
			'type'         => 'repeater',
			'layout'       => 'block',
			'button_label' => '',
			'sub_fields'   => array(
				array(
					'key'          => 'client_testimonial_name',
					'label'        => 'Client Name',
					'name'         => 'client_testimonial_name',
					'type'         => 'text',
					'instructions' => '',
					'required'     => 0,
				),

				array(
					'key'          => 'client_story_content',
					'label'        => 'Client Story Content',
					'name'         => 'client_story_content',
					'type'         => 'wysiwyg',
					'instructions' => '',
					'required'     => 0,
				),
			),
		),

		array(
			'key'          => 'in_community',
			'label'        => 'In The Community',
			'name'         => 'in_community',
			'type'         => 'tab',
			'instructions' => '',
			'placement'    => 'top',
			'endpoint'     => 0,
		),

		array(
			'key'   => 'in_community_title',
			'label' => 'In Community Title',
			'name'  => 'in_community_title',
			'type'  => 'text',
		),

		array(
			'key'   => 'in_community_content',
			'label' => 'In Community Content',
			'name'  => 'in_community_content',
			'type'  => 'wysiwyg',
		),

		array(
			'key'           => 'in_community_content_image',
			'label'         => 'In The Community Background Image',
			'name'          => 'in_community_content_image',
			'type'          => 'image',
			'return_format' => 'array',
			'preview_size'  => 'thumbnail',
			'library'       => 'all',
		),


	),
	'location'              => array(
		array(
			array(
				'param'    => 'post_template',
				'operator' => '==',
				'value'    => 'template-about-us.php',
			),
		),
	),
	'menu_order'            => 0,
	'position'              => 'acf_after_title',
	'style'                 => 'default',
	'label_placement'       => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen'        => '',
	'active'                => true,
	'description'           => '',
) );
