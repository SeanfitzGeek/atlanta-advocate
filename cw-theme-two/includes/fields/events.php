<?php
acf_add_local_field_group( array(
		'key'                   => 'events_section',
		'title'                 => 'Events Data',
		'fields'                => array(
			array(
				'key'          => 'events_data_tab',
				'label'        => 'Event Information',
				'name'         => 'events_data_tab',
				'type'         => 'tab',
				'instructions' => '',
				'placement'    => 'top',
			),

			array(
				'key'   => 'event_date',
				'name'  => 'event_date',
				'label' => 'Date of Event',
				'type'  => 'date_picker',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'display_format' => 'm/d/Y',
				'return_format' => 'd/m/Y',
				'first_day' => 1,
			),

			array(
				'key' => 'event_date_text',
				'name' => 'event_date_text',
				'label' => 'Text if no Date available',
				'type' => 'text',
			),

			array(
				'key' => 'event_link',
				'name' => 'event_link',
				'label' => 'Event Link',
				'type' => 'link',
				'return_format' => 'string'
			),

		),
		'location'              => array(
			array(
				array(
					'param'    => 'post_type',
					'operator' => '==',
					'value'    => 'events',
				),
			),
		),
		'menu_order'            => 0,
		'position'              => 'acf_after_title',
		'style'                 => 'default',
		'label_placement'       => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen'        => '',
		'active'                => true,
		'description'           => '',
	)
);
