<section id="breadcrumbs">
	<div class="container">
		<div class="row">
			<div class="col-sx-12">
				<div class="breadcrumb">
					<?php if ( function_exists('yoast_breadcrumb') ):
						$breadcrumbs = yoast_breadcrumb( '<ul><li>', '</li></ul>', false );
						echo str_replace( '', '</li><li>', $breadcrumbs );
					endif; ?>
				</div>
			</div>
		</div>
	</div>
</section>
