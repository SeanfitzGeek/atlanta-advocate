<?php
// lang match
$lang = $post->ID !== icl_object_id( $post->ID, 'page', true, 'es' ) ? '?lang=es' : '';
?>
<section class="lazyload main-header" data-bg="<?= get_stylesheet_directory_uri(); ?>/images/gb-wave-too.png"
         style="background-repeat: repeat-x;background-position:center; background-color:#f4f4f4;">

    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <a href="/">
                    <img class="lazyload logo img-responsive"
                         data-src="<?= get_stylesheet_directory_uri(); ?>/assets/images/logo-long-optimized.png"
                         alt="Atlanta Advocate">
                    <!--                    <picture>-->
                    <!--                        <source type="image/png" srcset="-->
					<? //= get_stylesheet_directory_uri();?><!--/assets/images/logo-long.webp">-->
                    <!--                        <img class="lazyload logo img-responsive" data-src="-->
					<? //= get_stylesheet_directory_uri();?><!--/assets/images/logo-long.png" alt="Atlanta Advocate">-->
                    <!--                    </picture>-->
                </a>
            </div>

            <div class="col-sm-6">
                <div class="header-right-container">
                        <span class="phone-number color-red">
                            <?php the_field( 'header_phone_number_text', 'option' ); ?>
                        </span>

                    <div class="call-cta">
						<?php the_field( 'header_phone_number_sub_text', 'option' ); ?>
                        <a href="<?= get_permalink( $post->ID ) . $lang; ?>"
                           title="Español traducción si está disponible">Se Habla Español</a>
                    </div>

                    <div class="header-search">
                        <form action="/" method="get" class="noauto">
                            <div class="input-group">
                                <input type="text" name="s" id="search" class="form-control form-search"
                                       aria-label="Search the Site"
                                       placeholder="SEARCH" value="<?php the_search_query(); ?>"/>
                                <div class="input-group-btn">
                                    <button class="btn btn-primary" type="submit" aria-label="Search Button">
                                        <span class="glyphicon glyphicon-search"></span>
                                    </button>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="navigator">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <nav class="menu-main">
					<?php wp_nav_menu( array(
                        "theme_location" => "header-updated",
						"menu_id"         => "nav",
						"container"       => "ul",
						"container_class" => "clearfix"
					) ); ?>
                </nav>
            </div>
        </div>
    </div>
</div>