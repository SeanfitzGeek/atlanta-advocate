<?php

$page_id = get_id_by_slug('blog');

?>

<header class="lazyload deactivated-blog-hero" style="background-image: url('<?php the_field( 'banner_image', $page_id ); ?>');" data-bg="<?php echo get_field('banner_image',$page_id); ?>">
        <section id="welcome">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="content">
                            <div class="inner">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-top">
		                    <?php
		                    if ( isset( $_GET['test'] ) ) {
			                    get_template_part( '/includes/forms/form-redo-design' );
		                    }
		                    ?>
                        </div>
                    </div>
                </div>


            </div>
        </section>
	</header>