<header class="lazyload" style="background-image: url('<?php the_field( 'banner_image' ); ?>');" data-bg="<?php the_field( 'banner_image' ); ?>">

	<?php get_template_part( 'includes/tap-buttons' ); ?>
    <section id="welcome" <?php echo is_page_template( 'page-practice-areas.php' ) ? 'class="type-3"' : ''; ?> >

        <div class="container">

            <div class="row">

				<?php if ( get_field( show_form ) && ! is_page_template( 'page-about.php' ) && ! is_page_template( 'page-attorney.php' ) && ! is_page_template( 'page-practice-area-landing.php' ) ): ?>

                <div class="left">

					<?php endif; ?>

                    <div class="content">

                        <div class="inner">

							<?php if ( get_field( 'supporting_logo' ) ): ?>
                                <p><img src="<?php the_field( 'supporting_logo' ); ?>" class="hidden-xs"/></p>
							<?php endif; ?>

							<h1><span><?php the_field( 'title' ); ?></span></h1>

                            <h3><?php the_field( 'subtitle' ); ?></h3>

                        </div>

                    </div>

				<?php if ( get_field( show_form ) && ! is_page_template( 'page-about.php' ) && ! is_page_template( 'page-attorney.php' ) && ! is_page_template( 'page-practice-area-landing.php' ) ): ?>

                </div>

			<?php endif; ?>

				<?php if ( get_field( show_form ) && ! is_page_template( 'page-about.php' ) && ! is_page_template( 'page-attorney.php' ) && ! is_page_template( 'page-practice-area-landing.php' ) ): ?>
                    <div class="right">
	                    <?= getPreviousText(); ?>
                        <div id="form" class="form">
							<?php get_template_part( '/includes/forms/form' ); ?>
                        </div>
                    </div>
				<?php endif; ?>

            </div>

        </div>

        <?php if(get_field('tagline')): ?>
        <div class="welcome-bar">
            <div class="container">
                <p><?php the_field( 'tagline' ); ?></p>
            </div>
        </div>
        <?php endif ?>

    </section>

	<?php if ( get_field( 'show_description' ) ): ?>
        <section id="quote-top">
            <div class="container">
                <div class="row">
					<?php if ( get_field( 'text_or_description' ) == 'testimonial' ): ?>
						<?php $testimonial = get_field( 'hero_testimonial' ); ?>
                        <p><?php the_field( 'teaser_text', $testimonial ); ?>
                            <i>– <?php echo get_the_title( $testimonial ); ?></i></p>
					<?php elseif ( get_field( 'text_or_description' ) == 'description' ): ?>
                    <p><?php the_field( 'hero_description' ); ?>
						<?php endif; ?>
                </div>
            </div>
        </section>
	<?php endif; ?>

</header>