<section id="welcome-home" class="type-2" style="
    background-image: url(<?php the_field( 'banner_image' ); ?>);
    background-position: 50% 0;
    background-size: cover;
">

    <picture>
        <source type="image/png" srcset="<?= get_stylesheet_directory_uri();?>/assets/images/logo-long.webp">
        <img class="lazyload logo img-responsive" data-src="<?= get_stylesheet_directory_uri();?>/assets/images/logo-long.png" alt="Atlanta Advocate">
    </picture>
	<div class="container">
		<div class="row">
			<div class="left">
				<div class="content">
					<div class="inner">
						<?php if ( get_field( 'supporting_logo' ) ): ?>
							<img src="<?php echo get_field( 'supporting_logo' ); ?>" alt="" class="hidden-xs"/>
						<?php endif; ?>
						<h2><?php echo get_field( 'title' ); ?></h2>
					</div>
				</div>
			</div>
		</div>

		<?php get_template_part( 'includes/forms/form-redo-design' ); ?>

	</div>
</section>