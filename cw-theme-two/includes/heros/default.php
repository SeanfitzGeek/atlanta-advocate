<?php
$showForm = get_field( show_form );
?>
<header class="lazyload default-hero <?= $showForm ? 'form-visible' : ''; ?>"
        style="background-image: url('<?php the_field( 'banner_image' ); ?>');"
        data-bg="<?php the_field( 'banner_image' ); ?>">

    <section id="welcome" <?php echo is_page_template( 'page-practice-areas.php' ) ? 'class="type-3"' : ''; ?> style="display: none;">
        <div class="container">
            <div class="row">
                <div class="col-sm-7 full-height">
					<?php if ( $showForm ): ?>
                    <div class="<?= $showForm ? 'deactivated-' : ''; ?>left">
						<?php endif; ?>
                        <div class="content">
                            <div class="inner">
								<?php if ( get_field( 'supporting_logo' ) ): ?>
                                    <img src="<?php the_field( 'supporting_logo' ); ?>" class="hidden-xs"/>
								<?php endif; ?>

                                <h2><span><?php the_field( 'title' ); ?></span></h2>

                                <h3><?php the_field( 'subtitle' ); ?></h3>

                                <p><?php the_field( 'tagline' ); ?></p>
                            </div>
                        </div>
						<?php if ( $showForm ): ?>
                    </div>
				<?php endif; ?>
                    <!--				--><?php //if ( get_field( show_form ) && ! is_page_template( 'page-about.php' ) && ! is_page_template( 'page-attorney.php' ) && ! is_page_template( 'page-practice-area-landing.php' ) ): ?>
                    <!--                    <div class="right">-->
                    <!--                        <div id="form" class="form">-->
                    <!--							--><?php //get_template_part( '/includes/forms/form' ); ?>
                    <!--                        </div>-->
                    <!--                    </div>-->
                    <!--				--><?php //endif; ?>
                </div>
                <div class="col-sm-5">
					<?php if ( $showForm ): ?>
                        <div class="form-top">
							<?php get_template_part( '/includes/forms/form-redo-design' ); ?>
                        </div>
					<?php endif; ?>
                </div>
            </div>


        </div>
    </section>
	<?php if ( get_field( 'show_description' ) ): ?>
        <section id="quote-top">
            <div class="container">
                <div class="row">
					<?php if ( get_field( 'text_or_description' ) == 'testimonial' ): ?>
						<?php $testimonial = get_field( 'hero_testimonial' ); ?>
                        <p><?php the_field( 'teaser_text', $testimonial ); ?>
                            <i>– <?php echo get_the_title( $testimonial ); ?></i></p>
					<?php elseif ( get_field( 'text_or_description' ) == 'description' ): ?>
                    <p><?php the_field( 'hero_description' ); ?>
						<?php endif; ?>
                </div>
            </div>
        </section>
	<?php endif; ?>
</header>