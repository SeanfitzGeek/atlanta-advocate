<header class="lazyload deactivated-testimonial-hero" style="background-image: url('<?php the_field( 'banner_image' ); ?>');" data-bg="<?php echo get_field('banner_image'); ?>">
        <div class="navigator">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <nav class="menu-main">
                        	<?php wp_nav_menu(array("menu" => "Main Navigation", "menu_id" => "nav", "container" => "ul", "container_class" => "clearfix")); ?>
                    	</nav>
                    </div>
                </div>
            </div>
        </div>
        <section id="welcome" class="hero-testimonial" >
            <div class="container">
                <div class="row">
                    <?php if(get_field(show_form)): ?>
                      <div class="right">
                        <div id="form" class="form">
                          <?php get_template_part('/includes/forms/form'); ?>
                        </div>
                      </div>
                    <?php endif; ?>
                </div>
            </div>
        </section>
        <section id="quote-top">
            <div class="container">
                <div class="row">
                  <?php if(get_field('text_or_description') == 'testimonial'): ?>
                  <?php $testimonial = get_field('hero_testimonial'); ?>
                    <p><?php the_field('teaser_text', $testimonial); ?>
                        <i>– <?php echo get_the_title($testimonial); ?></i></p>
                  <?php elseif(get_field('text_or_description') == 'description'): ?>
                    <p><?php the_field('hero_description'); ?>
                  <?php endif; ?>
                </div>
            </div>
        </section>
	</header>
