<header class="lazyload" style="background-image: url('<?php the_field( 'banner_image' ); ?>');" data-bg="<?php echo get_field('banner_image'); ?>">
        <section id="welcome" class="type-2">
            <div class="container">
                <div class="row">
                    <div class="content">
                        <div class="inner">
                          <?php if(get_field('supporting_logo')): ?>
                            <img src="<?php echo get_field('supporting_logo'); ?>" class="hidden-xs" />
                          <?php endif; ?>
                            <h2><?php echo get_field('title'); ?></h2>
                        </div>
                    </div>
                </div>
            </div>
        </section>
	</header>
