<?php
global $post;
$post_id  = $post->ID;
$ri_args  = array(
	'post_parent'    => $post_id,
	'post_type'      => array( 'page' ),
	'posts_per_page' => - 1,
	'order'          => "ASC",
);
$children = new WP_Query( $ri_args );

if ( $post_objects || $children->have_posts() ) : ?>

    <div class="block useful-links">

        <h3>Related Information</h3>

        <div class="block-body">

            <ul class="list">

				<?php while ( $children->have_posts() ): $children->the_post(); ?>
                    <li><a href="<?php the_permalink() ?>"><?php the_title() ?></a></li>
				<?php endwhile ?>
				<?php foreach ( $post_objects as $post_object ) : ?>
                    <li><a href="<?php echo get_permalink( $post_object->ID ) ?>"><?php echo get_the_title( $post_object->ID ) ?></a></li>
				<?php endforeach ?>

            </ul>

        </div>

    </div>

<?php endif; ?>




