<div class="block block-where-begin">
    <h3><?php the_field('bb_heading', 'option'); ?></h3>
    <p><?php the_field('bb_description', 'option'); ?></p>
    <div class="buttons">
        <a class="but" href="<?php the_field('bb1_link', 'option'); ?>"><?php the_field('bb1_text', 'option'); ?></a>
        <a class="but" href="javascript:void(0);"><?php the_field('bb2_text', 'option'); ?></a>
    </div>
</div>
