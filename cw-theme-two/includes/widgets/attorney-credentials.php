<?php if(get_field('attorney_credentials')): ?>
  <div class="block accordion-generic">
      <h3><?php the_field('attorney_credentials_title'); ?></h3>
      <dl class="accordion style-2">
          <?php $counter = 0; foreach(get_field('attorney_credentials') as $cred): ?>
          <dt class="<?php echo 'accordion-opened'; ?>"><a href="#accord-<?php echo $counter; ?>"><?php echo $cred['title']; ?><span></span></a></dt>
          <dd id="accord-<?php echo $counter; ?>" class="<?php echo 'accordion-opened'; ?>" style="display: <?php echo $counter == 0 ? 'block' : 'none'; ?>">
              <?php echo $cred['content']; ?>
          </dd>
        <?php $counter++; endforeach; ?>
      </dl>
  </div>
<?php endif; ?>
