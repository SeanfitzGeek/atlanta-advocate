<?php
  $args = array(
    'post_type' => 'wpseo_locations',
    'post_status' => 'publish',
    'posts_per_page' => -1,
  );

  if(get_field('which_locations' == 'specific')):
    $args[] = array('p' => get_field('location', 'option'));
  endif;
$query = new WP_Query($args);
if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>
<div class="block side-address">
    <h3><?php echo  get_the_title(); ?></h3>
    <div class="map" data-coords="<?php echo get_field('_wpseo_coordinates_lat'); ?>, <?php echo get_field('_wpseo_coordinates_long'); ?>" id="map-<?php echo get_the_ID(); ?>"></div>
    <div class="address">
        <address>
            <h4><strong>The Millar Law Firm</strong></h4>
            <p><?php echo  get_field('_wpseo_business_address') ; ?></p>
            <?php if(get_field( '_wpseo_business_address_2' )) { ?><p><?php echo get_field( '_wpseo_business_address_2' ); ?></p><?php } ?>
            <p><?php echo  get_field('_wpseo_business_city') ; ?>, <?php echo  get_field('_wpseo_business_state') ; ?> <?php echo  get_field('_wpseo_business_zipcode') ; ?></p>
            <p><?php echo  get_field('_wpseo_business_phone') ; ?></p>
        </address>
        <a target="_blank" href="<?php the_field('directions_link'); ?>" class="but-directions lazyload">Directions</a>
        <a href="#" class="but-hours">Hours</a>
    </div>
    <div class="popup-hours">
        <h5>Hours of Operation</h5>
        <ul>
          <?php for($n = 1; $n < 6; $n++): ?>
            <?php $day = date('l', strtotime('Sunday +' . $n . ' days')); ?>
            <li class="clearfix"><span><?php echo $day ?></span> <b><?php echo date('g:iA', strtotime(get_field('_wpseo_opening_hours_' . strtolower($day) . '_from'))); ?> - <?php echo date('g:iA', strtotime(get_field('_wpseo_opening_hours_' . strtolower($day) . '_to'))); ?></b></li>
          <?php endfor; ?>
        </ul>
        <p>Scheduled appointments after hours</p>
        <div class="popup-arrow"></div>
    </div>
</div>
<?php endwhile;
endif; ?>
