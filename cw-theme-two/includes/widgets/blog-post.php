<div class="block blog-item">
    <h3><?php the_field('blog_title', 'option'); ?></h3>
    <div class="blog-inner">
        <?php
        if(get_field('blog_category')):
          $args = array(
            'post_type' => 'post',
            'post_status' => 'publish',
            'posts_per_page' => 1,
            'orderby' => 'date',
            'order' => 'DESC',
            'tax_query' => array(
              array(
                'taxonomy' => 'category',
                'field' => 'term_id',
                'terms' => get_field('blog_category')
              ),
            ),
          );
        else:
          if(get_field('post_type', 'option') == 'random'):
            $args = array(
              'post_type' => 'post',
              'post_status' => 'publish',
              'posts_per_page' => 1,
              'orderby' => 'rand'
            );
          elseif(get_field('post_type', 'option') == 'latest'):
            $args = array(
              'post_type' => 'post',
              'post_status' => 'publish',
              'posts_per_page' => 1,
              'orderby' => 'date',
              'order' => 'DESC'
            );
          elseif(get_field('post_type', 'option') == 'specific'):
            $args = array(
              'post_type' => 'post',
              'post_status' => 'publish',
              'posts_per_page' => 1,
              'p' => get_field('post', 'option')
            );
          endif;
        endif;

        $query = new WP_Query($args);
        if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>
          <h4><?php the_title(); ?></h4>
          <div class="meta clearfix">
              <?php the_date(); ?> by <?php the_author_posts_link(); ?></a>
          </div>

          <div class="preview">
              <p><?php the_excerpt(); ?></p>
          </div>
          <div class="tools">
              <a href="<?php the_permalink(); ?>" class="but"><?php the_field('read_more_button_text', 'option'); ?></a>
          </div>
        <?php endwhile; wp_reset_postdata(); endif; ?>
    </div>
</div>
