<div class="block meet-attoneys">
    <h3><?php the_field( 'team_title_widget', 'option' ); ?></h3>
    <div class="block-body">
		<?php global $post;
		$count = count( get_field( 'team_members', 'option' ) );
		$team  = get_field( 'team_members', 'option' );

		if ( is_array( $team ) ):
			foreach ( get_field( 'team_members', 'option' ) as $member ): ?>
				<?php if ( $post->ID != $member ) : ?>
					<?php if ( strlen( trim( get_the_title( $member ) ) ) === 0 ) {
						continue;
					}
					?>
                    <div class=" <?php echo $count == 1 ? 'single-attorney' : 'attorney-item'; ?>">
                        <a href="<?php echo get_the_permalink( $member ); ?>">
							<?php echo get_the_post_thumbnail( $member ); ?>
                            <span><?php echo get_the_title( $member ); ?></span>
                        </a>
                    </div>
				<?php endif; ?>
			<?php endforeach; ?>
		<?php endif; ?>
    </div>
</div>
