<?php /**<div class="block results">
 * <ul>
 * <li><?php the_field('cr_title', 'option'); ?></li>
 * <?php foreach(get_field('cr_results', 'option') as $result): ?>
 * <li><h3>$<?php echo get_field('amount', $result); ?></h3> <p class="r_title"><?php echo get_the_title($result); ?></p>
 * <p>
 * <?php if(get_field('subtitle')):
 * echo get_field('subtitle');
 * else:
 * $term = get_the_terms($result, 'case-result-category');
 * echo $term[0]->name;
 * endif; ?>
 * </p>
 * </li>
 * <?php endforeach; ?>
 * <li><a href="<?php echo get_field('cr_link', 'option'); ?>"><div><?php echo get_field('cta_text', 'option'); ?> <img src="<?php echo get_stylesheet_directory_uri() . '/images/button-arrow-right.png'; ?>" /></div></a></li>
 * </ul>
 * </div> **/ ?>

<div class="block verdicts-slider">
    <h3><?php the_field( 'cr_title', 'option' ); ?></h3>
    <div class="block-body">
        <h4><?php the_field( 'case_results_description' ); ?></h4>

		<?php
		$posts = get_field( 'case_results' );

		if ( $posts ): ?>

            <ul id="slider-verdicts" class="slider clearfix">
				<?php foreach ( $posts as $post ): // variable must be called $post (IMPORTANT) ?>
					<?php setup_postdata( $post ); ?>
                    <li>
                        <a>
                            <span class="categories">
                                <?php $term = get_the_terms(get_the_id(), 'case-result-category');
                                echo $term[0]->name; ?>
                            </span>
							<p class="amount"><?php the_field( 'amount', get_the_id() ); ?></p>
                        </a>
                    </li>
				<?php endforeach; ?>
            </ul>

			<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>

		<?php else: ?>

            <ul id="slider-verdicts" class="slider clearfix">
				<?php foreach ( get_field( 'cr_results', 'option' ) as $result ): ?>
                    <li>
                        <a>
                            <span class="categories">
                                <?php echo get_the_title( $result, get_the_id() ); ?>
                            </span>
                            <p class="amount"><?php the_field( 'amount', get_the_id() ); ?></p>
                        </a>
                    </li>
				<?php endforeach; ?>
            </ul>

		<?php endif; ?>

    </div>

</div>
