<?php if(get_field('practice_areas', 'option')): global $post;?>
  <div class="block block-other-areas mb-40">
      <div class="inner">
          <h3><?php the_field('pa_title', 'options'); ?></h3>
          <ul>
             <?php foreach(get_field('practice_areas', 'option') as $area): ?>
                <?php if($area['practice_area'] != $post->ID): ?>
                  <li><a href="<?php echo get_the_permalink($area['practice_area']); ?>" style="background-image: url('<?php echo $area['practice_area_icon']; ?>')"><?php echo get_the_title($area['practice_area']); ?></a></li>
                <?php endif; ?>
              <?php endforeach;?>
          </ul>
      </div>
  </div>
<?php endif;?>
