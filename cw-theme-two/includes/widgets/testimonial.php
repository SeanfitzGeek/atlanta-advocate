<?php if ( get_field( 'which_testimonial', 'option' ) == 'random' ):

	$args = array(
		'post_type'      => 'testimonial',
		'post_status'    => 'publish',
		'posts_per_page' => 1,
		'orderby'        => 'rand',
	);

elseif ( get_field( 'which_testimonial', 'option' ) == 'specific' ):

	$args = array(
		'post_type'      => 'testimonial',
		'post_status'    => 'publish',
		'posts_per_page' => 1,
		'p'              => get_field( 'testimonial', 'option' ),
	);

endif;

$query = new WP_Query( $args );

if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>
    <div class="block block-quotes mb-20">
        <div class="inner">
            <p>“<?php the_field('teaser_text'); ?>”</p>
            <i>– <?php the_title(); ?></i>
        </div>
    </div>
<?php endwhile;

endif; ?>
