<?php
$a = shortcode_atts( array(
        'count' => '3',
    ), $atts );

if(get_field('faq_category')):
  $args = array(
      'post_type' => 'faq',
      'posts_per_page' => $a['count'],
      'tax_query' => array(
        array(
          'taxonomy' => 'faq-category',
          'field' => 'term_id',
          'terms' => get_field('faq_category')
        ),
      ),
    );
    $query = new WP_Query( $args );
    if($query->have_posts()) : $count = 0; while ($query->have_posts()) : $query->the_post(); ?>
    <dl class="accordion style-3">
        <dt><a href="#accord-<?php echo $count; ?>"><?php the_title(); ?><span></span></a></dt>
        <dd id="accord-<?php echo $count; ?>" style="display: none">
            <p><?php the_content(); ?></p>
        </dd>
  </dl>
  <?php $count++; endwhile; endif;
else:
  $terms = get_terms(
    array(
      'taxonomy' => 'faq-category',
      'hide_empty' => true,
      'order_by' => 'name',
      'order' => 'ASC'
    )
  ); ?>
  <ul class="faq-tabs nav nav-tabs" role="tablist">
  <?php $counter = 0; foreach($terms as $term): ?>
    <li role="presentation" class="<?php echo $counter == 0? 'active' : ''; ?>"><a href="#faq-cat-<?php echo $counter; ?>" aria-controls="faq-cat-<?php echo $counter; ?>" role="tab" data-toggle="tab"><?php echo $term->name; ?></a></li>
  <?php $counter++; endforeach; ?>
  </ul>

  <div class="faq-tabs-content tab-content">
  <?php $counter = 0;
  foreach($terms as $term): ?>
    <div role="tabpanel" class="tab-pane <?php echo $counter == 0 ? 'active' : ''; ?>" id="faq-cat-<?php echo $counter; ?>">
      <div id="faq-accord-<?php echo $counter; ?>" class="faq-accordion" aria-multiselectable="true" role="tablist">
      <?php $args = array(
        'post_type' => 'faq',
        'tax_query' => array(
          array(
            'taxonomy' => 'faq-category',
            'field' => 'name',
            'terms' => $term
          ),
        ),
      );
      $query = new WP_Query( $args );
      if($query->have_posts()) : $count = 0; while ($query->have_posts()) : $query->the_post(); ?>
        <div class="panel">
          <dt role="tab">
            <a class="<?php echo $count == 0 ? '' : 'collapsed'; ?>" href="#faq-<?php echo $counter; ?>-item-<?php echo $count; ?>" aria-controls="faq-<?php echo $counter; ?>-item-<?php echo $count; ?>" role="button" data-toggle="collapse" data-parent="#faq-accord-<?php echo $counter; ?>" aria-expanded="<?php echo $count == 0 ? 'true' : 'false'; ?>">
              <?php the_title(); ?>
            </a>
          </dt>
          <dd id="faq-<?php echo $counter; ?>-item-<?php echo $count; ?>" class="collapse <?php echo $count == 0 ? 'in' : ''; ?>" role="tabpanel">
            <p><?php the_content(); ?></p>
          </dd>
        </div>
      <?php $count++; endwhile; endif; wp_reset_postdata(); ?>
      </div>
    </div>
  <?php $counter++; endforeach; ?>
  </div>

  <div class="btn-load-more-ajax" data-target="" data-append-to="" data-load-more="">
      <?php next_posts_link('Load more faqs', $faqs->max_num_pages); ?>
  </div>
<?php endif; ?>
