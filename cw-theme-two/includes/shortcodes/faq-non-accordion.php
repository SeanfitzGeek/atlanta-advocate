<?php
function faqNonAccordion( $atts ) {
	$a = shortcode_atts( array(
		'count' => '50',
		'cat'   => 'general'
	), $atts );

	$category = get_term_by( 'slug', $a['cat'], 'faq_category' );
//	if ( get_field( 'faq_category' ) ):
	$args = array(
		'order'          => 'ASC',
		'orderby'        => 'menu_order',
		's'              => isset( $_GET['search'] ) ? $_GET['search'] : '',
		'post_type'      => 'qa_faqs',
		'post_status'    => 'publish',
		'posts_per_page' => $a['count'],
		'is_search' => false,
		'faq_category'   => $category->slug
//		'tax_query'      => array(
//			array(
//				'taxonomy' => 'faq-category',
//				'field'    => 'term_id',
//				'terms'    => get_field( 'faq_category' )
//			),
//		),
	);

	$query = new WP_Query( $args );
	if ( $query->have_posts() ) {
		while ( $query->have_posts() ) : $query->the_post();
			$faqPosts[] = [ 'title' => get_the_title(), 'content' => get_the_content() ];
		endwhile;
	}

	wp_reset_postdata();

	ob_start();
	$current     = 0;
	$titlesHtml  = '';
	$contentHtml = '';
	$total       = count( $faqPosts );
	foreach ( $faqPosts as $key => $faqPayload ) {
		$titlesHtml .= sprintf( '<li><a href="#icon-%s">%s</a></li>', $key, $faqPayload['title'] );

		$contentHtml .= sprintf( '<div id="icon-%s"></div>
        <div class="fourheadersect">
            <a href="#faqtop">
            <img class="fourlefticon lazyload" alt="homeicon" data-src="https://atlantaadvocate.com/wp-content/uploads/2018/02/home-button-small.png" /></a>
            <h3><b>%s</b></h3>
        </div>
        <span style="font-weight: 400;">%s</span>
        <div class="borderline"></div>', $current, $faqPayload['title'], $faqPayload['content'] );
		$current ++;
	}
	$titlesHtml = '<ul>' . $titlesHtml . '</ul>';
	?>
	<?php if ( $total === 0 ): ?>
        <h3>No Results Found! Try a different search.</h3>
	<?php else : ?>

        <h2>FAQs: You have real concerns and questions, we have answers.</h2>
        <div id="faqtop"></div>
        <div class="faq-content-full">
			<?= $titlesHtml; ?>
        </div>

		<?= $contentHtml; ?>
	<?php endif; ?>


	<?php return ob_get_clean();

//	endif;

}

add_shortcode( 'faq_full', 'faqNonAccordion' );

// Adds a custom serarch query to blog page for searching keywords inside of blog articles/titles
add_action( 'pre_get_posts', 'archiveSearch', 999 );

if ( isset( $_GET['search'] ) ) {
	add_filter( 'searchwp_remove_pre_get_posts', '__return_true', 9999 );
}

function archiveSearch( $query ) {

	if ( ! isset( $_GET['search'] ) || !$query->is_main_query()) {
		return $query;
	}

	$query->is_search = false;
	$query->set( 's', $_GET['search'] );

	return $query;

}