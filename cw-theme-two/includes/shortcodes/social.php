<div class="socials-big">
  <?php foreach(get_field('social_media_icons','option') as $icon): ?>
  <a href="<?php echo  $icon['link'] ; ?>" class="social"><span><?php echo  $icon['icon'] ; ?></span></a>
  <?php endforeach; ?>
</div>
