<?php
$args = array(
  'post_type' => 'community',
  'post_status' => 'publish',
  'posts_per_page' => 10,
  'orderby' => 'title',
  'order' => 'DESC'
);
$query = new WP_Query($args);
if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>
<div class="comm-block">
    <?php if(has_post_thumbnail()): ?>
        <?php echo  Helper::image(get_post_thumbnail_id(), 'attorney', array('class' => 'image left img-shadow')) ; ?>
    <?php endif; ?>
    <h3><?php the_title(); ?></h3>
    <?php the_content(); ?>
</div>
<?php endwhile; endif; wp_reset_postdata();?>

<div class="btn-load-more-ajax" data-target="" data-append-to="" data-load-more="">

    <?php echo  next_posts_link('Load more testimonials', $community->max_num_pages) ; ?>

</div>
