<?php

$args  = array(
	'post_type'      => 'testimonial',
	'post_status'    => 'publish',
	'posts_per_page' => -1,
	'orderby'        => 'date',
	'order'          => 'DESC',
);

$query = new WP_Query( $args );

while ( $query->have_posts() ) : $query->the_post();
	$thumb_alt = get_post_meta(get_post_thumbnail_id(get_the_ID()), '_wp_attachment_image_alt', true);
?>
	<div class="quote-block <?php echo has_post_thumbnail() ? 'quote-image' : 'no-image'; ?>">
		<img alt="<?php echo $thumb_alt; ?>" src="<?php echo has_post_thumbnail() ? the_post_thumbnail_url() : get_stylesheet_directory_uri() . '/images/quote.png'; ?>" />
		<div class="quote-text">
			<p><?php the_content(); ?></p>
			<p>“<?php the_field( 'teaser_text' ); ?>”</p>
			<?php the_field( 'testimonial_bottom_content' ); ?>
			<span class="client">- <?php the_title(); ?></span>
		</div>
		<div class="clearfix"></div>
	</div>
<?php 
endwhile;
?>