
<?php
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$args = array(
  'post_type' => 'case-result',
  'post_status' => 'publish',
  'posts_per_page' => 10,
  'paged' => $paged,
  'meta_key' => 'amount',
  'orderby' => 'date',
  'order' => 'DESC'
);
$query = new WP_Query($args);
if ( $query->have_posts() ) :?>
  <div class="post-ajax-wrapper">
    <?php while ( $query->have_posts() ) : $query->the_post(); ?>
    <div class="result-block">
        <h3><?php the_field('amount'); ?> - <span><?php $term = get_the_terms(get_the_id(), 'case-result-category');
                                echo $term[0]->name; ?></span></h3>
        <?php the_content(); ?>
    </div>
    <?php endwhile; ?>
  </div>
<?php endif; wp_reset_postdata();?>

<div class="btn-load-more-ajax text-center" data-load-more=".btn-load-more-ajax a" data-target=".post-ajax-wrapper .result-block" data-append-to=".post-ajax-wrapper">
    <?php next_posts_link('Load more case results', $query->max_num_pages) ; ?>
</div>
