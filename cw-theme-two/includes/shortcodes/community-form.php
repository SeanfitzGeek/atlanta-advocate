<div class="block form form-wide">
	<form class="clearfix header-form">
	    <input name="email" type="text" placeholder="Email Address"/>
	    <input name="organization_or_charity" type="text" placeholder="Non-profit organization or charity"/>
	    <input type="text" name="contact_organization_or_charity"  placeholder="Contact at Non-profit organization or charity">
	    <input type="text" name="phone_organization_or_charity" placeholder="Phone of Non-profit or charity">
	    <input type="text" name="city_organization_or_charity"  placeholder="City of Non-profit or charity">
	    <input type="text" name="your_name" placeholder="Your Name">
	    <input type="text" name="phone" placeholder="Phone">
	    <textarea name="notes" placeholder="Please tell us about this organization and why you want them to be recognized."></textarea>
	    <button class="but-form">Send</button>
	</form>
</div>