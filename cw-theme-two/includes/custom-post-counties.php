<?php
// Register Custom Post Type
function registerCountiesPostType() {

	$labels = array(
		'name'                  => 'Counties',
		'singular_name'         => 'county',
		'menu_name'             => 'Counties',
		'name_admin_bar'        => 'Counties',
		'archives'              => 'Counties Served Archives',
		'attributes'            => 'County Attributes',
		'parent_item_colon'     => 'Parent County',
		'all_items'             => 'All Counties',
		'add_new_item'          => 'Add New County',
		'add_new'               => 'Add New County',
		'new_item'              => 'New County',
		'edit_item'             => 'Edit County',
		'update_item'           => 'Update County',
		'view_item'             => 'View County',
		'view_items'            => 'View Counties',
		'search_items'          => 'Search Counties',
		'not_found'             => 'Not found',
		'not_found_in_trash'    => 'Not found in Trash',
		'featured_image'        => 'Featured Image',
		'set_featured_image'    => 'Set featured image',
		'remove_featured_image' => 'Remove featured image',
		'use_featured_image'    => 'Use as featured image',
		'insert_into_item'      => 'Insert into Counties',
		'uploaded_to_this_item' => 'Uploaded to this County',
		'items_list'            => 'Counties list',
		'items_list_navigation' => 'Items list navigation',
		'filter_items_list'     => 'Filter items list',
	);
	$args = array(
		'label'                 => 'county',
		'description'           => 'List of Counties',
		'labels'                => $labels,
		'supports'              => array( 'title' ),
		'taxonomies'            => array(),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => true,
		'publicly_queryable'    => false,
		'capability_type'       => 'page',
	);
	register_post_type( 'counties', $args );

}
add_action( 'init', 'registerCountiesPostType', 0 );