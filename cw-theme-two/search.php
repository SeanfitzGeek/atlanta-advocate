<?php
get_header( 'dark-blue' );
wp_enqueue_style( 'non-critical-min', get_stylesheet_directory_uri() . '/dist/non-critical.min.css', false, CSS_VERS );
global $wp_query;
?>
    <section class="search-results-header">
        <div class="container bottom-border">
            <div class="col-sm-8">
                <div class="search-query-result">
                    <h1>Search Results For: <strong><?php the_search_query(); ?></strong></h1>

                    <div class="search-query-result-count">
						<?php printf( '%s Search Results', $wp_query->found_posts ); ?>
                    </div>
                </div>
            </div>

            <div class="col-sm-4">
                <div id="new-search">
                    <button class="btn" id="btn-new-search" role="button"><span class="glyphicon glyphicon-search"></span>Start A New Search</button>
                </div>
            </div>

            <div class="col-sm-8">
                <div id="search-form" class="form-hidden col-sm-7">

                    <form action="/" method="get" class="noauto">
                        <div class="input-group">
                            <input type="text" name="s" id="search" class="form-control"
                                   placeholder="SEARCH" value="<?php the_search_query(); ?>"/>

                            <span class="input-group-btn">
                            <button class="btn btn-default" type="submit">
                                <span class="glyphicon glyphicon-search"></span> Search
                            </button>
                        </span>
                        </div>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <div class="container">

		<?php if ( have_posts() ) : ?>
			<?php while ( have_posts() ) : the_post();
				$catObj = get_post_primary_category( get_the_ID() );
				$title        = get_the_title();
				$categoryName     = isset($catObj['primary_category']->name) ? $catObj['primary_category']->name . ' Legal Guides' : 'Legal Guides';

				$categoryLink = isset($catObj['primary_category']->term_id) ? get_category_link($catObj['primary_category']->term_id) : '';
				$content      = get_the_excerpt();
				?>

                <div class="search-result">
                    <div class="search-results-container">
                        <h3 class="search-result-title"><a href="<?= get_the_permalink(); ?>"><?= $title; ?></a></h3>

						<?php if ( strlen( $categoryLink ) > 0 ): ?>
                            <div class="result-category legal-guides-arrow-block">
                                <a href="<?= $categoryLink; ?>"><?= $categoryName; ?></a>

                                <div class="pointer"></div>
                            </div>
						<?php endif; ?>
                        <div class="result-content">
							<?= $content; ?>
                        </div>

						<?php if ( have_rows( 'post_faq_questions' ) ): ?>
                            <div class="result-questions-answered">
                                <div class="question-title">
                                    <h3>Questions Answered in this legal guide:</h3>
                                </div>

                                <div class="question-sub-title">
                                    Click any of the questions below to quickly navigate to that section in the legal
                                    guide.
                                </div>

                                <ul class="questions">
									<?php while ( have_rows( 'post_faq_questions' ) ): the_row();
										$question = get_sub_field( 'faq_title' );
										$link     = get_sub_field( 'faq_link' );

										printf( '<li><a href="%s">%s</a></li>', $link, $question );

									endwhile; ?>
                                </ul>
                            </div>
						<?php endif; ?>
                    </div>

                    <div class="search-result-footer">
                        <a class="left" href="<?= get_the_permalink(); ?>">View Legal Guide</a>
                        <a class="right" href="tel:770-400-0000">Speak with an Attorney</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
			<?php endwhile; ?>
		<?php endif; ?>
		<?php wp_reset_postdata(); ?>

    </div>
    <div class="pagination">
		<?= paginate_links( [ 'prev_next' => false ] ); ?>
    </div>

    <section class="ask-a-question">
        <div class="container">
            <h2>Have a Question?</h2>
            <p>Contact us with any questions you have and we’ll get back to you!</p>
            <div class="question-input">
                <div class="input-group">
                    <input type="text" class="form-control user-question-response"
                           name="user_question_response"
                           placeholder="Ask A Question">
                    <span class="input-group-btn">
            <button type="button" class="btn btn-red btn-user-response">Submit</button>
                </span>
                </div>
            </div>
            <div class="question-form-fields" style="display: none;">
				<?= do_shortcode( '[contact-form-7 id="104668" title="Ask A Question"]' ); ?>
            </div>

        </div>
    </section>

    <section class="contact-form lazyload" id="main-contact-form"
             data-bg="<?= get_stylesheet_directory_uri(); ?>/assets/images/contact-us/over-city-skyline-atlanta-ga-downtown-dusk-georgia-PF48CFM.jpg">
        <div class="container">
            <div class="form-box-container">
                <h2 class="">Receive a FREE Case Evaluation</h2>
                <p>*All information submitted is secure & confidential.
                    All submissions will be sent immediately to our attorneys and legal staff, who will reach out to
                    you, free of charge, and advise you on the next steps to take.</p>

				<?= do_shortcode( '[contact-form-7 id="104671" title="Contact Us Form"]' ); ?>

            </div>
        </div>
    </section>

<?php get_footer( 'dark-blue' ); ?>