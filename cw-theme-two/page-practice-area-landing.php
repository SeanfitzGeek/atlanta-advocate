<?php /* Template Name: Practice Area Landing */ ?>
<?php get_header('dark-blue'); ?>

    <section class="content-wrapper"  itemscope itemtype="http://schema.org/Service" >
        <div class="customimgcaracident">
            <div class="container">
                <div class="row">
                    <article>
                        <h1><?php h1_title(); ?></h1>
                        <p>
							<?php if ( has_post_thumbnail() ) : ?>
                                <img class="lazyload" data-src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>" class="thumbnail image left"/>
							<?php endif; ?>
							<?php if ( get_field( 'section_1_image' ) ): ?><img class="image left lazyload" data-="<?php the_field( 'section_1_image' ); ?>" alt="Image"/><?php endif; ?>
							<?php the_field( 'section_1_content' ); ?>
                        </p>

						<?php if ( get_field( 'show_testimonial' ) ): ?>
                            <article>
                                <div class="client-say-block">
                                    <h3>Client Testimonials</h3>
                                    <div class="inner">
										<?php $test = get_field( 'testimonial' ); ?>
                                        <p>“<?php echo $test->teaser_text; ?>”</p>
                                        <i>– <?php echo $test->post_title; ?></i>
                                    </div>
                                </div>
                            </article>
						<?php endif; ?>



                    </article>

                    <aside>
                        <!--				--><?php //if ( get_field( 'show_form' ) ): ?>
                        <!---->
                        <!--                    <div class="block form">-->
                        <!--						--><?php //get_template_part( '/includes/forms/form' ); ?>
                        <!--                    </div>-->
                        <!--				--><?php //endif; ?>

						<?php if ( get_field( 'case_results' ) ): ?>

							<?php get_template_part( 'sidebars/default' ); ?>

						<?php endif; ?>

                    </aside>
                </div>
            </div>
    </section>

<?php if ( get_field( 'section_2_title' ) ): ?>

    <section id="prepared-resources" data-bg="<?= get_stylesheet_directory_uri();?>/images/bg-wave-2.png">
        <div class="container">
            <div class="row">
                <h2><?php the_field( 'section_2_title' ); ?></h2>
                <div class="placeholder">
					<?php $counter = 1;
					foreach ( get_field( 'section_2_columns' ) as $column ): ?>
                        <div class="item <?php echo $counter > 1 ? 'col-' . $counter . '"' : ''; ?>">
                            <div class="inner">
                                <h3><?php echo $column['title']; ?></h3>
								<?php echo $column['content']; ?>
                            </div>
                            <div class="button">
                                <a href="<?php echo $column['cta_link']; ?>"><?php echo $column['cta_text']; ?></a>
                            </div>
                        </div>
						<?php $counter ++; endforeach; ?>
                </div>
                <div class="have-question">
					<?php the_field( 'section_2_bottom_text' ); ?>
                </div>
            </div>
        </div>
    </section>

<?php endif; ?>
    <div class="lowercontainer">
        <section class='content-wrapper'>
            <div class='container'>
                <article>
					<?php the_field( 'section_3_content' ); ?>
                </article>
                <div class="asidelower">
                    <aside>
						<?php  dynamic_sidebar('custom-sidebar-11493'); ?>
                    </aside>
                </div>
            </div>
        </section>
    </div>

<?php get_footer('dark-blue'); ?>