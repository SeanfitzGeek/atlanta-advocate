<?php


get_header( 'dark-blue' );

$args       = [ 'post_type' => 'legal_terms'];
//$legalTerms = get_posts( $args );
$taxonomy   = 'category';
//$taxonomy   = 'sub_category';
$catId      = get_queried_object()->term_id;

$cPosts = get_posts( [
	'category'       => $taxonomy . '_' . $catId,
	'posts_per_page' => 1
] );

$categories = get_field( 'categories_listing_relationship', $taxonomy . '_' . $catId );

// load default categories set in options area if none are defined for this category
if ( ! is_array( $categories ) || count( $categories ) === 0 ) {
	$categories = get_field( 'categories_listing_relationship', 'option' );
}

$image            = get_field( 'image', $term );
$bannerBackground = get_field( 'blog_categories_banner_background', $catId );

// adds a default background img
if ( empty( $bannerBackground ) || ! isset( $bannerBackground['url'] ) ) {
	$bannerBackground['url'] = '/wp-content/uploads/2018/02/category_banner_car_accidents.jpg';
}

$counter              = 0;
$categoryDropdownHtml = '';
$catTitle             = get_queried_object()->name;
$featuredImg          = has_post_thumbnail( $cPosts[0]->ID ) ? get_the_post_thumbnail_url( $cPosts[0]->ID, 'post-thumbnail' ) : '/wp-content/uploads/2014/12/Rear-end-white-car-into-truck.jpg';
$rows                 = '';
$catLength            = count( $categories );
$catDesc = get_field( 'category_call_to_action_description', $catId );
if ( strlen( $catDesc ) === 0 ) {
	$catDesc = 'Join our mailing list to receive all information that you might need during your trial process.';
}
?>
<div class="post-categories-page">


    <section class="main-banner-duel-section lazyload">
        <div class="left-banner-block lazyload" data-bg="<?= $bannerBackground['url']; ?>">
            <div class="block-container">
                <h1><?= $catTitle; ?></h1>
                <p><?= $catDesc; ?></p>

                <div class="education-form-search subscribe-box">
                    <div class="faq-search header-search search-box">
						<?= do_shortcode( '[contact-form-7 id="95194" title="Subscribe Form"]' ); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="right-banner-block">
            <div class="block-container">
                <div class="featured-post-block">
                    <h4><?php printf( 'Featured %s Post', $catTitle ); ?></h4>
                    <h3><?= $cPosts[0]->post_title; ?></h3>
                    <div class="category-content">
						<?= substr( trim( strip_tags( apply_filters( 'the_content', $cPosts[0]->post_content ) ) ), 0, 120 ); ?>
                        ...
                        <br>
                        <a class="bold" href="<?= get_permalink( $cPosts[0]->ID ); ?>">View Content</a>
                    </div>
                </div>
                <a href="<?= get_permalink( $cPosts[0]->ID ); ?>">
                    <div class="overlay-container"></div>
                    <div class="featured-media-container">

                        <div class="category-featured-media lazyload" data-bg="<?= $featuredImg; ?>"></div>

                    </div>
                </a>
            </div>
        </div>
        <div class="clear clearfix"></div>
    </section>


    <section class="post-categories">
        <div id="post-category-dropdowns" role="tablist" aria-multiselectable="false">
            <div class="container">

                <div class="row">
					<?php foreach ( $categories as $category ) : ?>
						<?php
						$counter ++;
						$icon                = get_field( 'category_icon', $taxonomy . '_' . $category->term_id );
						$title               = $category->name;
						$desc                = get_field( 'category_call_to_action_description', $taxonomy . '_' . $category->term_id );
						$categoryPostContent = '';
						$catCounter          = 0;
//						$categoryPosts       = get_posts( [
//							'category'       => $taxonomy . '_' . $category->term_id,
//							'posts_per_page' => 6,
//
//						] );

						// $categorySubTaxRelationshipId is the id of the custom tax sub_category. This relates to the categories so we can loosely associate them without tying them into multiple categories

//						$args = array(
//							'post_type' => 'post',
//							'posts_per_page' => 3,
//							'orderby' => 'date',
//							'cat' => $taxonomy . '_' . $catId,
//							'tax_query' => array(
//								'relation' => 'AND',
//								array(
//									'taxonomy' => 'sub_category',
//									'field' => 'slug',
//                                    'terms' => $category->slug,
//								),
//							),
//						);

						// This query will get all posts in the current post category that the page is showing that is also in the custom taxonomy sub_category
						$args = array(
							'post_type'      => 'post',
							'posts_per_page' => -1,
							'orderby'        => 'date',
							'cat'            => $taxonomy . '_' . $catId,
							'meta_query'     => array(
								'relation' => 'AND',
								array(
									'key'     => 'sub_category_relationship',
									'value'   => $category->term_id,
									'compare' => '=',
								),
							),
						);

						$categoryPosts = new WP_Query( $args );

						if ( $categoryPosts->have_posts() ) {
							while ( $categoryPosts->have_posts() ) {
								$categoryPosts->the_post();

								$catCounter ++;

								$categoryPostContent .= sprintf( '<div class="col-sm-6"><div class="category-post-block"><h4>%s</h4><div class="content">%s...</div><a class="read-more" href="%s">Read More</a></div></div>', get_the_title(), trim( substr( strip_tags( apply_filters( 'the_content', get_the_content() ) ), 0, 120 ) ), get_permalink( get_the_ID() ) );
								if ( $catCounter === 2 ) {
									$categoryPostContent .= '</div>';
									$categoryPostContent .= '<div class="row">';

									$catCounter = 0;
								}

							}
						} else {
							$categoryPostContent .= '<div class="category-post-block"><h4 class="text-center">No Posts Found!</h4></div>';
						}
						wp_reset_postdata();

						ob_start(); ?>
                        <div class="category-information-dropdown panel-collapse collapse" role="tabpanel"
                             id="<?= $taxonomy . '_' . $category->term_id; ?>">
                            <div class="inner-information">
                                <div class="container">
                                    <div class="row"><?= $categoryPostContent; ?></div>
                                </div>
                            </div>

                        </div>
						<?php $rows .= ob_get_clean(); ?>


                        <div class="col-sm-3">
                            <div class="category-information-action">


                                <a class="" role="button" data-toggle="collapse"
                                   data-parent="#post-category-dropdowns"
                                   href="#<?= $taxonomy . '_' . $category->term_id; ?>"
                                   aria-expanded="false" aria-controls="<?= $taxonomy . '_' . $category->term_id; ?>">
                                    <div class="image-background">

                                        <img src="<?= $icon['url']; ?>" class="img-responsive">

                                    </div>


                                    <h5><?= $title; ?></h5>
                                </a>

                            </div>


                            <div class="category-information-dropdown mobile hidden" role="tabpanel"
                                 data-id="<?= $taxonomy . '_' . $category->term_id; ?>">
                                <div class="inner-information">
                                    <div class="container">
                                        <div class="row"><?= $categoryPostContent; ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>

						<?php
						if ( $counter === 4 ) :
							$counter = 0;
							printf( '</div><div class="row"><div class="col-sm-12">%s</div>', $rows );
							$rows = '';
						endif;
						?>
					<?php endforeach; ?>
                </div>

				<?php
				if ( strlen( $rows ) !== 0 ) :
					echo $rows;
				endif; ?>
            </div>
        </div>
    </section>


<!--    <section class="legal-terms-block">-->
<!--        <div class="container">-->
<!--            <h2>Helpful Terms</h2>-->
<!--			--><?php //foreach ( $legalTerms as $legalTermPost ) : ?>
<!--				--><?php //while ( have_rows( 'legal_terms', $legalTermPost ) ): the_row(); ?>
<!---->
<!--                    <div class="term">-->
<!--						--><?php //printf( '<p><span class="bold">%s</span> - %s</p>', get_sub_field( 'legal_term_title' ), get_sub_field( 'legal_term_description' ) ); ?>
<!--                    </div>-->
<!--				--><?php //endwhile; ?>
<!--			--><?php //endforeach; ?>
<!---->
<!--            <a href="/legal-terms/" class="btn btn-white btn-full-glossary">FULL GLOSSARY</a>-->
<!--        </div>-->
<!--    </section>-->
</div>
<?php get_footer( 'dark-blue' ); ?>
