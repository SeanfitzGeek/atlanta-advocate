<?php
/* 280 W */
/*
  * Template Name: Community Involvement Template
  */

get_header( 'dark-blue' );

$now         = new DateTime();
$communities = get_posts( [
	'numberposts' => 3,
] );

$args = array(
	'post_type'      => 'community',
	'posts_per_page' => 3,
);

$communitiesQuery = new WP_Query( $args );

$args              = array(
	'post_type'      => 'events',
	'posts_per_page' => 3,
	'meta_query'     => array(
		array(
			'key'     => 'event_date',
			'value'   => $now->format( 'Ymd' ),
			'compare' => '>=',
		)
	)
);
$futureEventsQuery = new WP_Query( $args );

$args = array(
	'post_type'      => 'events',
	'posts_per_page' => 3,
	'meta_query'     => array(
		array(
			'key'     => 'event_date',
			'value'   => $now->format( 'Ymd' ),
			'compare' => '<',
		)
	)
);

$pastEventsQuery = new WP_Query( $args );

$communityFormBg                   = get_field( 'community_involvement_form_bg' );
$headerBgUrl                       = get_field( 'community_involvement_header_background' );
$communityInvolvementHeaderContent = get_field( 'community_involvement_header_content' );
$millerCommunityContent            = get_field( 'miller_community_content' );
$upcomingEventsContent             = get_field( 'upcoming_event_content' );
$recentEventsContent               = get_field( 'recent_event_content' );
$communityInvolvementContent       = get_field( 'community_involvement_content' );
$organizationSupportContent        = get_field( 'organization_support_content' );
?>

    <section class="community-involvement-header lazyload" data-bg="<?= $headerBgUrl['url']; ?>">
        <div class="header-left">
			<?= $communityInvolvementHeaderContent; ?>
        </div>
    </section>

    <section class="grey-nav-thick">
        <ul>
            <li><a class="xinit-scroll-to" data-scroll-to="#upcoming-events" href="#upcoming-events">Upcoming Events</a>
            </li>
            <li><a class="xinit-scroll-to" data-scroll-to="#recent-events" href="#recent-events">Recent Events</a></li>
            <li><a class="xinit-scroll-to" data-scroll-to="#organizations-we-support" href="#organizations-we-support">Organizations
                    We Support</a></li>
            <li><a class="xinit-scroll-to" data-scroll-to="#recommend-nonprofit" href="#recommend-nonprofit">Recommend A
                    Nonprofit</a></li>
        </ul>
    </section>

    <section class="miller-community">
        <div class="container">
            <div class="community-content">
				<?= $millerCommunityContent; ?>
            </div>
            <div class="row">
				<?php
				if ( $communitiesQuery->have_posts() ) :
					while ( $communitiesQuery->have_posts() ) :
						$communitiesQuery->the_post();
						$comId = get_the_ID();
						?>
                        <div class="col-sm-4">
                            <div class="community-container">
                                <div class="post-image">
                                    <img data-src="<?= get_the_post_thumbnail_url( $comId, 'post-rectangle' ); ?>"
                                         class="lazyload img-responsive center-block">
                                </div>

                                <div class="community-box">
                                    <h4>
                                        <a href="<?= get_permalink( $comId ); ?>">
											<?= get_the_title( $comId ); ?>
                                        </a>
                                    </h4>
									<?= substr( trim( strip_tags( get_the_content() ) ), 0, 120 ); ?>...


                                    <div class="read-more">
                                        <a href="<?= get_permalink( $comId ); ?>">Click here to read full
                                            article</a>
                                    </div>
                                </div>
                            </div>
                        </div>
					<?php endwhile; endif;
				wp_reset_postdata();
				?>
            </div>
        </div>

    </section>

    <section class="upcoming-events" id="upcoming-events">
        <div class="container">
            <div class="community-content">
				<?= $upcomingEventsContent; ?>
            </div>
        </div>
        <div class="inner-container">
            <div class="row">
				<?php
				if ( $futureEventsQuery->have_posts() ) :
					while ( $futureEventsQuery->have_posts() ) :
						$futureEventsQuery->the_post();
						$eventId   = get_the_ID();
						$eventLink = get_field( 'event_link', $eventId );

						?>
                        <div class="col-sm-4">
                            <div class="community-container">
                                <div class="post-image">
                                    <img data-src="<?= get_the_post_thumbnail_url( $eventId, 'post-rectangle' ); ?>"
                                         class="lazyload img-responsive center-block">
                                </div>

                                <div class="community-box">
                                    <h4>
										<?= get_the_title( $eventId ); ?>
                                    </h4>

									<?= get_the_content(); ?>
									<?php if ( strlen( trim( $eventLink ) ) ): ?>
                                        <div class="read-more">
                                            <a href="<?= $eventLink; ?>">
                                                Click here to read full article
                                            </a>
                                        </div>
									<?php endif; ?>
                                </div>
                            </div>
                        </div>
					<?php
					endwhile;
				endif;
				wp_reset_postdata();
				?>
            </div>
        </div>

    </section>


    <section class="recent-events" id="recent-events">
        <div class="container">
            <div class="community-content"><?= $recentEventsContent; ?></div>
        </div>
        <div class="inner-container">
            <div class="row">
				<?php if ( $pastEventsQuery->have_posts() ) :
					while ( $pastEventsQuery->have_posts() ) :
						$pastEventsQuery->the_post();
						$eventId   = get_the_ID();
						$eventLink = get_field( 'event_link', $eventId );
						?>
                        <div class="col-sm-4">
                            <div class="community-container">
                                <div class="post-image">
                                    <img data-src="<?= get_the_post_thumbnail_url( $eventId, 'post-rectangle' ); ?>"
                                         class="lazyload img-responsive center-block">
                                </div>

                                <div class="community-box">
                                    <h4>
										<?= get_the_title( $eventId ); ?>
                                    </h4>
									<?= get_the_content(); ?>
									<?php if ( strlen( trim( $eventLink ) ) ): ?>
                                        <div class="read-more">
                                            <a href="<?= $eventLink; ?>">
                                                Click here to read full article
                                            </a>
                                        </div>
									<?php endif; ?>
                                </div>
                            </div>
                        </div>
					<?php
					endwhile;
				endif;
				wp_reset_postdata();
				?>
            </div>
        </div>
    </section>

    <section class="organizations-we-support" id="organizations-we-support">
        <h2 class="text-center"><?= get_field('organization_support_title');?></h2>
        <div class="container">
			<?php
			while ( have_rows( 'organization_list' ) ): the_row();
				$orgTitle   = get_sub_field( 'organization_title' );
				$orgContent = get_sub_field( 'organization_content' );
				$orgLogo    = get_sub_field( 'organization_logo' );
				$orgLink    = get_sub_field( 'organization_link' );
				$imageAlt   = get_post_meta( $orgLogo->ID, '_wp_attachment_image_alt', true );

				?>
                <div class="organization-block">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="organization-logo">
                                <img data-src="<?= wp_get_attachment_image_url( $orgLogo['ID'], 'post-square' ); ?>"
                                     class="lazyload img-responsive center-block" alt="<?= $imageAlt; ?>">
                            </div>
                        </div>
                        <div class="col-sm-9">
                            <div class="organization-info">
                                <h4>
                                    <a href="<?= $orgLink['url']; ?>" target="_blank" rel="nofollow">
										<?= $orgTitle; ?>
                                    </a>
                                </h4>
                                <div class="org-content">
									<?= $orgContent; ?>
                                </div>

                                <div class="read-more">
                                    <a href="<?= $orgLink['url']; ?>" target="_blank" rel="nofollow">
                                        Visit Organization's Website
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
			<?php endwhile; ?>
            <div class="community-content">
		        <?= $organizationSupportContent; ?>
            </div>
        </div>
    </section>

    <section class="community-involvement-form lazyload" id="recommend-nonprofit"
             data-bg="<?= $communityFormBg['url']; ?>">
        <div class="container">
            <div class="community-content"><?= $communityInvolvementContent; ?></div>
        </div>
    </section>
<?php get_footer( 'dark-blue' ); ?>