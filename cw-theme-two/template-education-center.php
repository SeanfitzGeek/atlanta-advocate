<?php
/* 280 W */
/*
  * Template Name: Education Template
  */

get_header( 'dark-blue' );

$featuredPosts = get_posts();
$legalTerms    = get_field( 'terms_category_relationship_link' ); //page ID for legal Terms

$legalCounter = 0;
$termsHtml    = '';
$terms        = '';
$termsHtml    = '<div class="term"><p><span class="bold">%s</span> - %s</p></div>';
$termsArray   = [];
$termsCurrentIndex = 0;

while ( have_rows( 'legal_terms', $legalTerms[0]->ID ) ): the_row();
	$termsArray[] = [
		'title'       => get_sub_field( 'legal_term_title' ),
		'description' => get_sub_field( 'legal_term_description' )
	];

	$terms .= sprintf( $termsHtml, get_sub_field( 'legal_term_title' ), get_sub_field( 'legal_term_description' ) );
	$legalCounter ++;

endwhile;
reset_rows();

$totalSections = get_field( 'education_categories' );

//$termMax = round( count( $termsArray ) / count( $totalSections ) );
//if ( $termMax < 1 ) {
//	$termMax = 1;
//}
$termMax = 6;


?>
    <script>
        function openNav() {
            // document.body.classList.add("noscroll");
            document.getElementById("mySidenav").style.width = "100%";
            // document.getElementById("main-content").style.marginRight = "250px";
        }

        function closeNav() {
            // document.body.classList.remove("noscroll");

            document.getElementById("mySidenav").style.width = "0";
            // document.getElementById("main-content").style.marginRight = "0";
        }
    </script>

    <div class="education-page-content">
        <div class="education-quick-links rotate">
            <button type="button" onclick="openNav();" href="javascript:void()">Quick Links</button>
        </div>

        <div id="mySidenav" class="sidenav">
            <div class="sidebar-container">
                <div class="sidebar-categories">
                    <div class="close-tab rotate">
                        <button onclick="closeNav()">Close</button>
                    </div>
                    <div class="category-container">

						<?php while ( have_rows( 'education_categories' ) ): the_row(); ?>
							<?php
							$categoryTitle = get_sub_field( 'education_title' );
							?>
                            <!--                            <div class="sidebar-nav-category lazyload" onclick="closeNav()" href="#--><? //= str_replace( ' ', '-', $categoryTitle ); ?><!--"-->
                            <!--                                 data-bg="--><? //= get_sub_field( 'education_sidebar_background' ); ?><!--">-->
                            <!--                                <a onclick="closeNav()" href="#--><? //= str_replace( ' ', '-', $categoryTitle ); ?><!--">-->
                            <!--                                    <h4 onclick="closeNav()" href="#--><? //= str_replace( ' ', '-', $categoryTitle ); ?><!--">--><? //= $categoryTitle; ?><!--</h4>-->
                            <!--                                </a>-->

                            <a class="sidebar-nav-category lazyload"
                               data-bg="<?= get_sub_field( 'education_sidebar_background' ); ?>"
                               onclick="closeNav()" href="#<?= str_replace( ' ', '-', $categoryTitle ); ?>">
                                <h4 onclick="closeNav()"
                                    href="#<?= str_replace( ' ', '-', $categoryTitle ); ?>"><?= $categoryTitle; ?></h4>
                            </a>
                            <!--                            </div>-->
						<?php endwhile; ?>
						<?php reset_rows(); ?>
                    </div>
                    <div class="clear clearfix"></div>
                </div>
            </div>
        </div>


        <div id="main-content">
            <section class="main-education-banner-section main-banner-section lazyload"
                     data-bg="<?= get_field( 'education_banner_background' ); ?>">
                <div class="container">
                    <h1>The Georgia Legal<br>Knowledge Base</h1>

                    <div class="education-form-search">
						<?= getEducationSearch(); ?>
                        <div class="faq-search header-search search-box">
                            <form method="get" action="/">
                                <div class="input-group">
                                    <input type="text" name="search" class="form-control form-search"
                                           placeholder="Search legal terms, topics, or questions."
                                           value="<?= isset( $_REQUEST['search'] ) ? $_REQUEST['search'] : ''; ?>"/>

                                    <span class="input-group-btn">
                                        <button class="btn btn-primary" type="submit">
                                             <span class="search-btn">Search</span>
                                            <span class="glyphicon glyphicon-search"></span>
                                        </button>
                                    </span>
                                    <p style="font-weight: bold; color: #fff; line-height: 3.0; margin: 0 10px; text-align: left;">
                                        AtlantaAdvocate.com</p>
                                </div>
                                <div class="clearfix"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>

            <section class="featured">
                <div class="container">
                    <div class="featured-posts">
                        <div class="row">
                            <div class="col-sm-4">
                                <h4 class="sub-title">FEATURED POSTS</h4>

                                <div id="featured-posts" class="owl-carousel">
									<?php foreach ( $featuredPosts as $featuredPost ): ?>
                                        <div class="item">
                                            <div class="row">
                                                <div class="col-sm-3 text-center bold color-dark-blue">
                                            <span class="post-timestamp">
                                                <?= ( new DateTime( $featuredPost->post_date ) )->format( 'M' ); ?>
                                                <br>
                                                <?= ( new DateTime( $featuredPost->post_date ) )->format( 'd' ); ?></span>
                                                    -
                                                </div>
                                                <div class="col-sm-9">
                                                    <h5 class="post-title">
                                                        <a href="<?= get_permalink( $featuredPost->ID ); ?>"><?= $featuredPost->post_title; ?></a>
                                                    </h5>
                                                    <p class="post-description"><?= substr( trim( strip_tags( $featuredPost->post_content ) ), 0, 120 ); ?>
                                                        ...
                                                        <br>
                                                        <a class="bold"
                                                           href="<?= get_permalink( $featuredPost->ID ); ?>">View
                                                            Content</a>
                                                    </p>
                                                </div>
                                            </div>

                                        </div>
									<?php endforeach; ?>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="owl-nav">
                                    <div class="prev">
                                        <img src="" data-src="<?= get_stylesheet_directory_uri(); ?>/images/prev.png"
                                             alt="Back"
                                             class="lazyload">
                                    </div>
                                    <div class="next">
                                        <img src="" data-src="<?= get_stylesheet_directory_uri(); ?>/images/next.png"
                                             alt="Next"
                                             class="lazyload">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="featured-video">
									<?= get_field( 'main_cta_video' ); ?>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>


            </section>

			<?php while ( have_rows( 'education_categories' ) ): the_row(); ?>
				<?php
				$categoryRelationShipObj = get_sub_field( 'category_relationship_link' );
				$categoryTitle           = get_sub_field( 'education_title' );
//				$legalTerms              = $legalTerms[0];
				$categoryPosts      = get_posts( [
					'category'       => $categoryRelationShipObj,
					'posts_per_page' => 3
				] );
				$defaultCategoryImg = get_sub_field( 'category_default_img' );
				?>
                <section class="education-section lazyload" data-bg="<?= get_sub_field( 'education_background' ); ?>"
                         id="<?= str_replace( ' ', '-', $categoryTitle ); ?>">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-8">
                                <h4 class="sub-title">WHAT YOU NEED TO KNOW ABOUT</h4>

                                <h2 class="title"><?= $categoryTitle; ?></h2>
                                <div class="education_info"><?= get_sub_field( 'education_category_description' ); ?></div>

								<?php foreach ( $categoryPosts as $categoryPost ):
									$imgUrl = get_the_post_thumbnail_url( $categoryPost->ID, 'category-thumb' ) ?: wp_get_attachment_image_url( $defaultCategoryImg['ID'], 'category-thumb' );
									$imgId = get_post_thumbnail_id( $categoryPost->ID );
									?>
                                    <div class="category-posts">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <a href="<?= get_permalink( $categoryPost->ID ); ?>">
                                                    <img data-src="<?= $imgUrl; ?>"
                                                         alt="<?= get_post_meta( $imgId, '_wp_attachment_image_alt', true ); ?>"
                                                         class="img-responsive post-thumbnail lazyload">
                                                </a>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="post-container">
                                                    <h5 class="post-title">
                                                        <a href="<?= get_permalink( $categoryPost->ID ); ?>"><?= $categoryPost->post_title; ?></a>
                                                    </h5>
                                                    <p class="post-description">
														<?php printf( '%s...',
															substr( trim( strip_tags( apply_filters( 'the_content', $categoryPost->post_content ) ) ), 0, 120 ) ); ?>

                                                        <br>
                                                        <a class="bold"
                                                           href="<?= get_permalink( $categoryPost->ID ); ?>">View
                                                            Content</a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
								<?php endforeach; ?>

                            </div>
                            <div class="col-sm-4">
                                <div class="helpful-terms">
                                    <h3>Helpful Terms</h3>
									<?php
                                    $counter = 0;
									//										print_r( $termsArray );
									foreach ( $termsArray as $termData ) {
										printf( '<div class="term"><p><span class="bold">%s</span> - %s</p></div>', $termData['title'], $termData['description'] );
										unset( $termsArray[ $termsCurrentIndex ] );
										$termsCurrentIndex ++;
										$counter ++;
										if ( $counter >= $termMax ) {
											break;
										}
									}

//									array_values( $termsArray );
									?>
									<?php //echo $terms; ?>
                                    <a class="glossary-link" href="/legal-terms/">Full Glossary</a>
                                </div>

                                <div class="have-a-question">
                                    <h3>Have Questions?</h3>
                                    <p>Submit a legal question in our public forum. Someone from our office will get
                                        back to you within 48 hours with an answer.</p>
                                    <a href="/contact-us" class="bold">ASK A QUESTION</a>
                                    <div class="more-posts">
										<?= sprintf( '<a href="%s">MORE ON %s</a>', get_category_link( $categoryRelationShipObj ), $categoryTitle ); ?>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </section>
			<?php endwhile; ?>
			<?php reset_rows(); ?>
        </div>
    </div>
<?php get_footer( 'dark-blue' ); ?>

<?php

