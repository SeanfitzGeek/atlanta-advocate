<?php get_header(); ?>
<?php if ( get_field( 'section_1' ) ): ?>
    <section id="we-believe" class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-md-7 left">
                    <div class="inner">
                        <!-- Temp until I awake! -->
                        <h1 style="color: #FFF;font-weight: bold;text-align: center;font-size: 52px;    text-shadow: 4px 3px 1px #000;"><?php the_field( 'field_57f2c9f1b23dd' ); ?></h1>

                        <h2><?php echo get_field( 'left_column_title' ); ?></h2>
                        <div class="sectwocontainer left">
                            <p><?php echo get_field( 'left_column_content' ); ?></p>
							<?= get_field( 'left_column_video' ); ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-5 right">
                    <div class="inner lazyload">
                        <h2><?php echo get_field( 'right_column_title' ); ?></h2>
						<?php echo get_field( 'right_column_content' ); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>
<?php if ( get_field( 'section_2' ) ): ?>
    <section id="how-help" class="lazyload" data-bg="/wp-content/themes/cw-theme-two/images/bg-wave-2.png">
        <div class="container">
            <div class="row">
                <div class="h-line"><h2><?php echo get_field( 'section_2_title' ); ?></h2></div>
            </div>
            <div class="row row-1">
				<?php echo get_field( 'section_2_content' ); ?>

                <div class='hidden-xs text-center'>

                    <img class="lazyload" data-src='/wp-content/themes/cw-theme-two/images/mm-infographic-desktop.png'/>

                </div>

                <div class='visible-xs text-center'>
                    <img class="lazyload" data-src='/wp-content/themes/cw-theme-two/images/mm-infographic-mobile.jpg'/>
                </div>

                <!--<div class="col right">
                  <label><?php echo get_field( 'injury_title' ); ?></label>
                  <div class="block-go">
                      <div class="select-style-1">
                          <select>
                              <option value="">Please choose...</option>
                              <?php foreach ( get_field( 'injuries' ) as $injury ): ?>
                                <option value="<?php echo $injury['page']; ?>"><?php echo $injury['injury']; ?></option>
                              <?php endforeach; ?>
                          </select>
                      </div>
                      <a href="#" class="btn-go-url">Go</a>
                  </div>
                  <?php if ( get_field( 'accident_title' ) ): ?>
                  <label><?php echo get_field( 'accident_title' ); ?></label>
                  <div class="block-go">
                      <div class="select-style-1">
                          <select>
                              <option value="">Please choose...</option>
                              <?php foreach ( get_field( 'accidents' ) as $accident ): ?>
                                <option value="<?php echo $accident['page']; ?>"><?php echo $accident['accident']; ?></option>
                              <?php endforeach; ?>
                          </select>
                      </div>
                      <a href="#" class="btn-go-url">Go</a>
                  </div>
                  <?php endif ?>
              </div>
               <div class="col left">
                    <?php echo get_field( 'section_2_content' ); ?>
                </div>-->
            </div>
            <div class="row row-2">
				<?php foreach ( get_field( 'practice_areas' ) as $practice_area ): ?>
                    <div class="col">
                        <div class="item">
                            <header class="lazyload" data-bg="<?php echo $practice_area['background_image']; ?>">
                                <!--                            <img data-src="-->
								<?php //echo $practice_area['background_image']; ?><!--"/>-->
                                <div class="ico"><img class="lazyload" data-src="<?php echo $practice_area['icon']; ?>"
                                                      alt="<?= $practice_area['practice_area']; ?>"></div>
                                <h3><?php echo $practice_area['practice_area']; ?></h3>
                            </header>
                            <div class="content">
                                <p><?php echo $practice_area['description']; ?></p>
                                <a href="<?php echo $practice_area['link_url']; ?>" class="lazyload"><?php echo $practice_area['link_text']; ?></a>
                            </div>
                        </div>
                    </div>
				<?php endforeach; ?>
            </div>
			<?php if ( get_field( 'show_practice_area_cta' ) ): ?>
                <div class="row row-3">
                    <a href="<?php echo get_field( 'practice_area_cta_link' ); ?>"
                       class="but-see-all lazyload"><?php echo get_field( 'practice_area_cta_text' ); ?></a>
                </div>
			<?php endif; ?>
            <div class="row row-4">
				<?php echo get_field( 'section_2_bottom_text' ); ?>
            </div>
        </div>
    </section>
<?php endif; ?>
<?php get_footer(); ?>