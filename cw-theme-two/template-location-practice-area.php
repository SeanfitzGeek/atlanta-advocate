<?php
/*
  * Template Name: Location Practice Area
  */

get_header( 'dark-blue' );
add_filter( 'the_content', 'add_ids_to_header_tags' );

$bannerImage     = get_field( 'location_practice_area_main_banner_image' );
$mainTitle       = get_the_title();
$subtitle        = get_field( 'location_practice_page_h2' ) ?: get_the_title();
$ctaVideo        = get_field( 'cta_video' ) ?: '<iframe width="520" height="350" src="https://www.youtube.com/embed/URAb6ddH2TE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
$ctaLearnMore    = get_field( 'cta_learn_more' ) ?: 'https://atlantaadvocate.com/about-us/';
$otherInfoTitle  = get_field( 'other_info_title' ) ?: 'Other Information';
$pageInformation = get_field( 'page_information' );
$faqCounter      = 0;

$mainContent      = get_the_content();
$mainContent      = generateSidebarQuickLinks( $mainContent );
$quickLinks       = generate_navigation( $mainContent );
$whyChooseBanner  = get_field( 'why_choose_banner_image' );
$whyChooseContent = get_field( 'why_choose_content' );

if ( ! isset( $whyChooseBanner['url'] ) ):
	$whyChooseBanner['url'] = '/wp-content/uploads/2020/05/Millar-Law-Firm-Attorneys-768x512-1.jpg';
endif;

if ( strlen( $whyChooseContent ) === 0 ):
	$whyChooseContent = '<h2><span style="color: #952a32;">Why Choose The Millar Law Firm?</span></h2>';
endif;
?>


    <section class="main-banner lazyload" data-bg="<?= $bannerImage['url']; ?>">
        <div class="content-box">
            <h1><?= $mainTitle; ?></h1>
            <p><?= $pageInformation; ?></p>
            <div class="col-md-6">
                <a class="btn btn-red" href="<?= $ctaLearnMore; ?>">Learn More</a>
            </div>
            <div class="col-md-6">
                <a href="#" class="btn btn-transparent" data-toggle="modal" data-target="#videomodal">
                    <span class="glyphicon glyphicon-play" aria-hidden="true"
                          style="border: 2px solid #FFF;border-radius: 25px;padding: 6px;"></span>
                    Watch Our Video</a>

            </div>
        </div>
        <div class="bg-shader"></div>
        <div class="clearfix"></div>
    </section>


    <!-- Modal -->
    <div class="modal fade" id="videomodal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
					<?= $ctaVideo; ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <h2 class="text-center sub-title"><?= $subtitle; ?></h2>
    <div class="bottom-border"></div>

    <section class="why-choose">
        <div class="row">
            <div class="col-sm-5">
                <div class="why-choose-banner lazyload" data-bg="<?= $whyChooseBanner['url']; ?>"></div>
            </div>
            <div class="col-sm-7">
                <div class="content">
					<?= $whyChooseContent; ?>
                </div>
            </div>
        </div>
    </section>

    <section class="container-sidebars">
        <div class="row">
            <div class="col-xs-12 col-md-2">
                <div class="hidden-xs hidden-sm quick-link-block link-blocks add-affix-left">
                    <h4>Quick Navigate</h4>
                    <div class="nav-content">
                        <ul class="sidebar-navigation">
							<?php
							foreach ( $quickLinks as $key => $value ) {
							    if ($key === 'no-h2') {
							        continue;
                                }
								printf( '<li><a href="#%1$s" class="init-scroll-to" data-scroll-to="#%1$s"><span class="glyphicon glyphicon-chevron-right"></span> %2$s</a></li>', $key, $value['title'] );
							}
							?>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                </div>


                <div class="quick-links-sidebar visible-sm visible-xs">
                    <div class="article-quick-links ">
                        <div class="sidebar-opener" data-toggle="collapse"
                             data-target="#blog-sidebar" aria-expanded="false" aria-controls="blog-sidebar">
                            Quickly Navigate
                        </div>
                        <div id="blog-sidebar" class="collapse" aria-expanded="true">
                            <div class="quick-link-block-select">
                                <ul>
									<?php
									foreach ( $quickLinks as $key => $value ) {
										if ($key === 'no-h2') {
											continue;
										}
										printf( '<li><a href="#%1$s" class="init-scroll-to" data-scroll-to="#%1$s">%2$s</a></li>', $key, $value['title'] );
									}
									?>
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-sm-12 col-md-7">
                <div class="main-content">
					<?php echo $mainContent; ?>
                </div>

                <section class="faq-section">
                    <div class="container">
                        <h2 class="color-red"><?= get_field( 'faq_title' ); ?></h2>

                        <div class="panel-group" id="faq-list" role="tablist" aria-multiselectable="true">
							<?php
							while ( have_rows( 'faq_questions' ) ): the_row();
								$title   = get_sub_field( 'faq_question_title' );
								$content = get_sub_field( 'faq_question_content' );
								$faqCounter ++;
								?>
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab">
                                        <div class="panel-title">
                                            <a role="button" data-toggle="collapse" data-parent="#faq-list"
                                               href="#faq-opener-<?= $faqCounter; ?>"
                                               aria-expanded="true" aria-controls="faq-opener-<?= $faqCounter; ?>">
                                                <i class="more-less glyphicon glyphicon-plus"></i>
												<?= $title; ?>
                                            </a>
                                        </div>
                                    </div>
                                    <div id="faq-opener-<?= $faqCounter; ?>" class="panel-collapse collapse"
                                         role="tabpanel">
                                        <div class="panel-body">
											<?= $content; ?>
                                        </div>
                                    </div>
                                </div>

							<?php endwhile; ?>
                        </div>
                    </div>
                </section>

                <section class="ask-a-question">
                    <div class="container">
                        <h2>Have a Question?</h2>
                        <p>Contact us with any questions you have and we’ll get back to you!</p>
                        <div class="question-input">
                            <div class="input-group">
                                <input type="text" class="form-control user-question-response"
                                       name="user_question_response"
                                       placeholder="Ask A Question">
                                <span class="input-group-btn">
            <button type="button" class="btn btn-red btn-user-response">Submit</button>
                </span>
                            </div>
                        </div>
                        <div class="question-form-fields" style="display: none;">
							<?= do_shortcode( '[contact-form-7 id="104668" title="Ask A Question"]' ); ?>
                        </div>

                    </div>
                </section>
            </div>

            <div class="col-sm-12 col-md-3">
                <div class="quick-link-block">
                    <div class="contact-form" id="main-contact-form">
                        <div class="form-box-container">
                            <h2 class="">Receive a FREE Case Evaluation</h2>
                            <p class="small">*All information submitted is secure & confidential.
                                All submissions will be sent immediately to our attorneys and legal staff, who will
                                reach out to you, free of charge, and advise you on the next steps to take.</p>

							<?= do_shortcode( '[contact-form-7 id="104671" title="Contact Us Form"]' ); ?>

                        </div>
                    </div>

					<?php if ( have_rows( 'other_info_links' ) ) : ?>
                        <div class="link-blocks other-info">
                            <h3><?= $otherInfoTitle; ?></h3>
                            <div class="other-information">
                                <ul>
									<?php
									while ( have_rows( 'other_info_links' ) ): the_row();
										$title  = get_sub_field( 'title' );
										$urlObj = get_sub_field( 'other_link' );
										?>
                                        <li><a href="<?= $urlObj['url']; ?>"><span
                                                        class="glyphicon glyphicon-chevron-right"></span><?= $title; ?>
                                            </a>
                                        </li>
									<?php endwhile; ?>
                                </ul>
                            </div>
                        </div>
					<?php endif; ?>

                    <div class="get-in-touch">
                        <h3>Get in Touch</h3>
                        <ul>
                            <li>
                                <a href="/contact-us">
                                    <img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/contact-us/Icon-awesome-envelope-open-text.png"
                                         alt="Mail Icon" class="img-responsive center-block img-icon">
                                    Use our Contact Form</a>
                            </li>

                            <li>
                                <a href="tel:770-400-0000" class="">
                                    <img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/contact-us/Icon-awesome-phone-alt.png"
                                         alt="Chat Icon" class="img-responsive center-block enter-block img-icon">
                                    Call Us (We Answer 24/7)</a>
                            </li>

                            <li>
                                <a href="#" role="button" class="btn-chat">
                                    <img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/contact-us/Icon-ionic-ios-chatboxes.png"
                                         alt="Chat Icon" class="img-responsive center-block enter-block img-icon">
                                    Chat With Us</a>
                            </li>
                        </ul>
                    </div>


					<?php if ( have_rows( 'related_topics' ) ): ?>
                        <div class="related-topics">
                            <h3>Related Topics</h3>

                            <ul>
								<?php
								while ( have_rows( 'related_topics' ) ): the_row();
									$title  = get_sub_field( 'related_topics_title' );
									$urlObj = get_sub_field( 'related_topics_other_link' );
									?>
                                    <li><a href="<?= $urlObj['url']; ?>"><span
                                                    class="glyphicon glyphicon-chevron-right"></span><?= $title; ?></a>
                                    </li>
								<?php endwhile; ?>
                            </ul>
                        </div>
					<?php endif; ?>
                </div>
            </div>
        </div>
    </section>


<?php get_footer( 'dark-blue' ); ?>