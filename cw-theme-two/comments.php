	        <?php if(comments_open()) { ?>
	        <div id="comments" class="comment-box">
				<div class="comment-form-holder">
				  <h5><?php comments_number( '0 Comments', '1 Comment', '% Comments' ); ?></h5>
				  <div class="com-info clearfix">
					<div class="user-img left">
						<?php echo get_avatar( $comment, 61 ); ?>
					</div><!-- /user-img -->
					<div id="respond-form" class="user-fill left">
						<?php 
						$comments_args = array(
						  'id_form'           => 'commentform',
						  'id_submit'         => '',
						  'title_reply'       => '',
						  'title_reply_to'    => __( 'Leave a Reply to %s' ),
						  'cancel_reply_link' => __( 'Cancel Reply' ),
						  'label_submit'      => __( 'Submit' ),
						
						  'comment_field' =>  '<textarea id="comment" name="comment" placeholder="Leave a Comment"></textarea>',
						
						  'comment_notes_before' => '',
						
						  'comment_notes_after' => '',
						
						  'fields' => apply_filters( 'comment_form_default_fields', array(
						
						    'author' =>
						      '<input type="text" placeholder="Full Name *" id="author" name="author">',
						
						    'email' =>
						      '<input type="text" placeholder="Email *" id="email" name="email">'
						      
						    )
						  ),
						);
						comment_form($comments_args); ?>
					</div><!-- user-fill -->
				  </div><!-- /com-info -->
				</div><!-- /comment-form-holder -->		
				<?php if(have_comments()) { ?>
				<div class="comment-list">
					<ul class="comment-list-cont">
						<?php wp_list_comments('type=comment&avatar_size=61'); ?> 
					</ul>
				</div>
				<?php } ?>
	          </div>
	          <?php } ?>