module.exports = function (grunt) {
    const sass = require('node-sass');

    grunt.initConfig({
        sass: {
            options: {
                implementation: sass,
            },
            dist: {
                files: {
                    'css/sass-compiled.css': 'assets/sass/main.scss',
                    // 'css/education-page.css': ['assets/sass/pages/education-center.scss', 'assets/sass/pages/legal-terms.scss'],
                    'css/non-critical.css': 'assets/sass/non-critical.scss',
                    'css/home-wide.css': 'assets/sass/home-wide.scss',
                }
            }
        },

        uglify: {
            my_target: {
                files: {

                    'dist/home-wide.min.js': [

                        'assets/js/vendor/jquery-3.4.1.js',
                        'assets/js/vendor/modernizr-2.8.3.min.js',
                        'assets/js/vendor/lazysizes.js',
                        'assets/js/vendor/unveilhooks-lazy-sizes.js',
                        'assets/js/vendor/bootstrap-3.4.1.js',
                        'assets/js/vendor/sly-compiled.js',
                        'assets/js/vendor/glide-slider.js',
                        'assets/js/custom/home.js'
                    ],

                    'dist/built.min.js': [
                        'assets/js/vendor/jquery-3.4.1.js',
                        'assets/js/custom/app.js',
                        'assets/js/vendor/lazysizes.js',
                        'assets/js/vendor/unveilhooks-lazy-sizes.js',
                        '../../plugins/contact-form-7/includes/js/scripts.js',
                        'assets/js/vendor/bootstrap-3.4.1.js',
                        'assets/js/custom/vendor.js',
                        'assets/js/vendor/enquire.min.js',
                        'assets/js/custom/non-critical.js',
                        '../../plugins/cw-video/colorbox/jquery.colorbox-min.js',
                        '../../plugins/cw-video/cw-video.js',
                        // 'assets/js/vendor/form-processor.js',
                        '../../plugins/q-and-a/js/q-a-plus.js',
                        'assets/js/vendor/jssocials.min.js',
                    ],

                    'dist/mobile-home.min.js': [
                        'assets/js/vendor/jquery-3.4.1.js',
                        'assets/js/custom/app.js',
                        'assets/js/vendor/lazysizes.js',
                        'assets/js/vendor/unveilhooks-lazy-sizes.js',
                        'assets/js/vendor/bootstrap-3.4.1.js',
                        'assets/js/vendor/modernizr-2.8.3.min.js',
                    ],
                },


               // ## use this config to de-minify
                // options : {
                //     output: { beautify: true },
                //     mangle : false,
                //     compress : false
                // }
            }
        },

        cssmin: {

            options: {
                mergeIntoShorthands: false,
                roundingPrecision: -1,
                level: {
                    1: {
                        specialComments: 0
                    }
                }
            },
            target: {
                files: {
                    'dist/home-wide.min.css': [
                        'css/library/bootstrap-3.4.0.css',
                        // 'css/google-fonts.css',
                        'css/glyphicons.css',
                        'css/home-wide.css',
                        'css/library/glide.core.min.css',
                    ],

                    'dist/built.min.css': [
                        'css/library/bootstrap.css',
                        'css/theme.css',
                        'css/atf.css',
                        'css/header.css',
                        'css/sass-compiled.css',
                        'css/general.css',
                        'css/offloaded.css',
                        'css/forms/main-form.css',
                        'css/footer.css',
                        'css/google-fonts.css',
                        'css/glyphicons.css',
                    ],
                    'dist/built-bs-3-4.min.css': [
                        'css/library/bootstrap-3.4.0.css',
                        'css/theme.css',
                        'css/atf.css',
                        'css/header.css',
                        'css/sass-compiled.css',
                        'css/general.css',
                        'css/offloaded.css',
                        'css/forms/main-form.css',
                        'css/footer.css',
                        'css/google-fonts.css',
                        'css/glyphicons.css',
                    ],
                    'dist/non-critical.min.css': [
                        'css/cw-form-process.css',
                        'css/page.css',
                        'css/page-sidebar.css',
                        'css/breadcrumbs.css',
                        'css/non-critical.css',
                    ],
                    'dist/mobile-home.min.css': [
                        'css/library/bootstrap-3.4.0.css',
                        'css/glyphicons.css',
                        'css/footer.css',
                        'css/page.css',
                        'css/forms/main-form.css',
                        'css/pages/mobile-home.css',
                    ]
                }
            }
        },
    });
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-sass');

    grunt.registerTask('default', ['sass','uglify', 'cssmin']);
    grunt.registerTask('css', ['sass', 'cssmin']);
};