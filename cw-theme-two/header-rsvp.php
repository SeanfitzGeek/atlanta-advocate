<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta name="p:domain_verify" content="f26323dcfd98864d56b4360762be67d6"/>
    <meta charset="<?php bloginfo( 'charset' ); ?>"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="format-detection" content="telephone=no">

    <title><?php wp_title( '|', true, 'right' ); ?></title>

    <link href="#" rel="publisher"/>

    <script>


        document.addEventListener('wpcf7mailsent', function (event) {
            dataLayer.push({
                'event': 'wpcf7successfulsubmit',
                'CF7formID': event.detail.contactFormId
            });
        }, false);
    </script>

	<?php if ( get_field( 'favicon', 'option' ) ): ?>
        <link rel="shortcut icon" href="<?php echo get_field( 'favicon', 'option' ); ?>"/>
	<?php endif; ?>

	<?php wp_head(); ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.slim.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
    <link type="text/css" rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">


</head>

<body <?php body_class(); ?>>
    <style>
        body {
            background: url('<?= get_the_post_thumbnail_url();?>') no-repeat center center fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            background-size: cover;
            -o-background-size: cover;
        }

        .form-container {
            border-radius: 20px;
            padding: 15px;
            background: rgba(255,255,255,.7);
            max-width: 700px;
            margin: 20px auto;
            display: block;
        }

        .form-container .form-control {
            padding: 15px 10px;
            font-size: 16px;
            border-radius: 5px;
            transition: all .3s;
        }

        .form-container .form-control:focus {
            border: 2px solid #00113c;
            border-radius: 10px;
        }


        .form-container label{
            font-size: 20px;
        }

        .form-container .form-group {
            margin-bottom: 20px;
        }

        .form-container .wpcf7-list-item {
            display: block;
        }

        .form-container .wpcf7-list-item-label {
            font-size: 17px;
        }

        .form-container .wpcf7-list-item input {
            margin: 0 15px;
        }

        .form-container .form-number {
            max-width: 100px;
        }

        .wpcf7-submit.btn.btn-primary {
            display: block;
            margin: 0 auto;
            padding: 15px;
            font-size: 26px;
            border-radius: 5px;
            background: #173354;
            border: none;
        }

        .wpcf7-response-output {
            font-size: 25px;
            text-align: center;
        }

        .wpcf7-mail-sent-ok {
            color: #781d26;
            font-size: 28px;
            display: none !important;
        }

        .wpcf7-validation-errors {
            color: red;
        }

        .wpcf7-form.sent .form-group, .wpcf7-form.sent .wpcf7-submit.btn.btn-primary {
            display: none !important;
        }

        .wpcf7-form.sent .success-msg {
            display: block !important;
            color: #781d26;
            font-size: 28px;
            text-align: center;
        }

    </style>