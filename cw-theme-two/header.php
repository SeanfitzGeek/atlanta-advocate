<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta name="p:domain_verify" content="f26323dcfd98864d56b4360762be67d6"/>
    <meta charset="<?php bloginfo( 'charset' ); ?>"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="format-detection" content="telephone=no">
    <meta name="google-site-verification" content="Eh_s11a_sIMxHqJeFXgm263m8asgyOYBUumsNbbcfEI" />

    <title><?php wp_title( '|', true, 'right' ); ?></title>

    <!--<link rel="profile" href="//gmpg.org/xfn/11" />
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" /> NOT SURE ABOUT THESE -->
    <!-- enable noydir and noodp in SEO > Titles & Metas -->

    <link href="#" rel="publisher"/>

    <!-- Google Tag Manager -->
    <!--    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':-->
    <!--                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],-->
    <!--            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=-->
    <!--            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);-->
    <!--        })(window,document,'script','dataLayer','GTM-WF4XL8F');</script>-->


    <script>(function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start':
                    new Date().getTime(), event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-WF4XL8F');

        // timeout to assist with speed and performance
        // setTimeout(function () {
        // Clixtell Tracking Code
        var script = document.createElement('script');
        var prefix = document.location.protocol;
        script.async = true;
        script.type = 'text/javascript';
        var target = prefix + '//app.clixtell.com/scripts/latest.js';
        script.src = target;
        var elem = document.head;
        elem.appendChild(script);

        //  Twitter universal website tag code
        !function (e, t, n, s, u, a) {
            e.twq || (s = e.twq = function () {
                s.exe ? s.exe.apply(s, arguments) : s.queue.push(arguments);
            }, s.version = '1.1', s.queue = [], u = t.createElement(n), u.async = !0, u.src = '//static.ads-twitter.com/uwt.js',
                a = t.getElementsByTagName(n)[0], a.parentNode.insertBefore(u, a))
        }(window, document, 'script');
        // Insert Twitter Pixel ID and Standard Event data below
        twq('init', 'o1axe');
        twq('track', 'PageView');

        // FB Tracking Code
        !function (f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function () {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window,
            document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');

        fbq('init', '320668835423599');
        fbq('set', 'agent', 'tmgoogletagmanager', '320668835423599');
        fbq('track', "PageView");
        //End Facebook Pixel Code

        // chimptastic
        !function (c, h, i, m, p) {
            m = c.createElement(h), p = c.getElementsByTagName(h)[0], m.async = 1, m.src = i, p.parentNode.insertBefore(m, p)
        }(document, "script", "https://chimpstatic.com/mcjs-connected/js/users/8571009818cd0a863463f19af/4bc74922c5c86008c2cad3f18.js");

        // lazy load home iframe video via timer delay instead of viewport
        var homeVideo = document.getElementById('iframe-video');
        if (homeVideo) {
            homeVideo.setAttribute('src', homeVideo.getAttribute('lazy-src'));
        }

        //function loadNonCriticalJs() {
        //    var script = document.createElement("script");
        //    script.src = "<?//= get_stylesheet_directory_uri() . '/dist/non-critical.min.js?v=' . JS_VERS;?>//";
        //    script.type = "text/javascript";
        //    document.getElementsByTagName("head")[0].appendChild(script);
        //}
        //    loadNonCriticalJs();

        // }, 3500);

        document.addEventListener('wpcf7mailsent', function (event) {
            dataLayer.push({
                'event': 'wpcf7successfulsubmit',
                'CF7formID': event.detail.contactFormId
            });
        }, false);
    </script>

	<?php if ( get_field( 'favicon', 'option' ) ): ?>
        <link rel="shortcut icon" href="<?php echo get_field( 'favicon', 'option' ); ?>"/>
	<?php endif; ?>
    <!--<link rel="apple-touch-icon" type="image/png" href="/wp-content/themes/avrek/images/apple-touch-icon.png">
	<link rel="apple-touch-icon" type="image/png" sizes="72x72" href="/wp-content/themes/avrek/images/apple-touch-icon-72.png">
	<link rel="apple-touch-icon" type="image/png" sizes="114x114" href="/wp-content/themes/avrek/images/apple-touch-icon-114.png">-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="//ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
    <script src="//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="//oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

	<?php wp_head(); ?>

	<?php echo get_field( 'additional_css_or_javascriptheader_css_js_custom_in_header', 'option' ); ?>

	<script>
		if ('scrollRestoration' in history) {
			// Tell browser it can restore scroll automatically (e.g. if we want to use on-page links that jump to one section,
			// and we want to allow the back button to bring us back to the same point we were at, previously)
			history.scrollRestoration = 'auto';
		}
	</script>
</head>

<body <?php body_class(); ?>>
    <!-- Google Tag Manager (noscript) -->
    <!--            <noscript>-->
    <!--                <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WF4XL8F"-->
    <!--                        height="0" width="0" style="display:none;visibility:hidden"></iframe>-->
    <!--            </noscript>-->
    <!-- End Google Tag Manager (noscript) -->
    <!-- Facebook noscript tracking -->
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=320668835423599&ev=PageView&noscript=1"/></noscript>
    <!-- End of Facebook Noscript Tracking -->

    <div class="page-inner-area">
        <div id="wrapper">
            <a class="sr-only" href="#welcome">Skip to content</a>
            <div class="fixed-menu visible-xs">
                <ul class="clearfix">
                    <li><a href="/about-us/">About Us</a></li>
                    <li><a href="/contact-us/">Contact Us</a></li>
                </ul>
                <a href="#" class="but-mobile"
                   aria-label="Navigation menu for mobile"><span>&nbsp;</span><span>&nbsp;</span><span>&nbsp;</span></a>
            </div>
            <div class="topbar"
                 style="display: none; background-color: #183251; color: white; padding: 10px 0; text-transform: uppercase; font-size: 18px; font-weight: 600;">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <span>Atlanta's Accident and Injury Lawyers</span>
                        </div>
                    </div>
                </div>
            </div>

			<?php // Header?>
            <header class="site-header">
                <div class="container">
                    <a href="/">
                        <img class="lazyload logo img-responsive center-block"
                             data-src="<?= get_stylesheet_directory_uri(); ?>/assets/images/logo-long-optimized.png"
                             alt="Atlanta Advocate">
                    </a>
                    <div class="row">
                        <div class="col-md-7">

                            <div class="contact-now-cta">
                                <h4 class="text-center">Serving Atlanta and the Entire State of Georgia</h4>
                                <span class="phone-number color-red">
                                    <a class="color-red"
                                       href="tel:<?php the_field( 'header_phone_number_text', 'option' ); ?>"><?php the_field( 'header_phone_number_text', 'option' ); ?></a>
                                </span>

                                <div class="call-cta">
									<?php the_field( 'header_phone_number_sub_text', 'option' ); ?>
                                    <a href="<?= get_permalink( $post->ID ) . $lang; ?>"
                                       title="Español traducción si está disponible">Se Habla Español</a>
                                </div>
                            </div>

                        </div>

                        <div class="col-md-5">
                            <div class="header-img-container">
                                <img data-src="<?= get_stylesheet_directory_uri(); ?>/assets/images/header-attorney-group.png"
                                     class="lazyload img-responsive center-block" alt="Attorneys at Millar Law Firm">
                            </div>
                        </div>
                    </div>
                </div>
            </header>

            <div class="navigator">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <nav class="menu-main">
								<?php wp_nav_menu( array(
									"theme_location"  => "header-dark-blue",
									"menu_id"         => "nav",
									"container"       => "ul",
									"container_class" => "clearfix"
								) ); ?>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>

<?php
//get_template_part( 'includes/heros/header-nav' );

switch ( get_field( 'hero' ) ) {
	case( 'default' ):

		if ( ! is_404() && ! is_front_page() ) {
			get_template_part( 'includes/heros/default' );
		}

		break;
	case( 'heading' ):
		get_template_part( 'includes/heros/heading' );
		break;
	case( 'tagline' ):
		get_template_part( 'includes/heros/tagline' );
		break;
	case( 'testimonial' ):
		get_template_part( 'includes/heros/testimonial' );
		break;
	default:
		if ( is_category() ) {
			get_template_part( 'includes/heros/default' );
		}
		if ( is_home() || is_single() || is_404() || is_search() || is_author() ) {
//			get_template_part( 'includes/heros/blog' );
		} else {
//			get_template_part( 'includes/heros/none' );
		}
		break;
}

if ( ! is_front_page() ) {
	get_template_part( 'includes/tap-buttons' );
	get_template_part( 'includes/breadcrumbs' );

	if ( get_field( 'results_position' ) == 'high' ) {
		get_template_part( '/partials/results' );
	}
}

?>