<?php if ( get_field( 'results_position' ) == 'low' && ! is_front_page() && ! is_page_template( 'page-about.php' ) ):
	get_template_part( '/partials/results' );
endif; ?>

<?php
/* Loads awards section */
if (! is_front_page()) {
	get_template_part( '/partials/partners' );
}
?>

<?php if ( ! is_page_template( 'page-about.php' ) && !is_front_page()) :?>
	<?php get_template_part( '/partials/team' ); ?>
	<?php get_template_part( '/partials/community' ); ?>
<?php endif; ?>
<section id="sub-contacts" class="lazyload" data-bg="<?= get_stylesheet_directory_uri();?>/assets/images/bg-wave-footer.png">
    <div class="container">
        <div class="row">
            <div class="col text-center">
                <a href="<?php echo get_bloginfo( 'url' ); ?>" class="logo">
                    <img class="lazyload"
                         data-src="https://atlantaadvocate.com/wp-content/uploads/2018/02/footerlogo.png"
                         alt="Atlanta Advocate">
                    <!--<img src="<?php //the_field('site_logo', 'option') ?>" >--></a>
            </div>
            <div class="col socials">
                <div class="footersubtext">
                    <img class="lazyload"
                         data-src="https://atlantaadvocate.com/wp-content/uploads/2018/02/title-footer-1.png"
                         alt="Your Atlanta Advocate">
                </div>
				<?php if ( get_field( 'social_media_icons', 'option' ) ): foreach ( get_field( 'social_media_icons', 'option' ) as $icon ): ?>
                    <a href="<?php echo $icon['link']; ?>" class="social"
                       aria-label="Atlanta Advocate Social media icon"
                       target="blank"><span><?php echo $icon['icon']; ?></span></a>
				<?php endforeach; endif; ?>
            </div>
            <div class="col">
                <div class="contacts">
                    <span><?php echo get_field( 'above_phone_number_text', 'option' ); ?></span>
                    <b><?php echo get_field( 'contact_number', 'option' ); ?></b>
					<?php if ( get_field( 'below_phone_number_text', 'option' ) && get_field( 'below_phone_number_link', 'option' ) ): ?>
                        <div class="callback">
                            <a href="<?php echo get_field( 'below_phone_number_link', 'option' ); ?>"><?php echo get_field( 'below_phone_number_text', 'option' ); ?>
                                <img class="lazyload"
                                     data-src="<?php echo get_stylesheet_directory_uri() . '/images/right-arrow-alt.png'; ?>"/></a>
                        </div>
					<?php endif; ?>
                    <div class="button">
                        <a class="but" href="#"><?php echo get_field( 'chat_button_text', 'option' ); ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $args = array(
	'post_type'      => 'wpseo_locations',
	'post_status'    => 'publish',
	'posts_per_page' => - 1,
	'orderby'        => 'ID',
	'order'          => 'ASC'
);
$query      = new WP_Query( $args );
if ( $query->have_posts() ) : ?>
    <section id="contacts" class="map-section">
		<?php $coords = array();
		$count        = 0;
		while ( $query->have_posts() ) : $query->the_post();
			$coords[] = array( get_field( '_wpseo_coordinates_lat' ) => get_field( '_wpseo_coordinates_long' ) );
			$count ++; endwhile;

		$output = '';
		foreach ( $coords as $coord ):
			foreach ( $coord as $lat => $long ):
				$output .= '[' . $lat . ',' . $long . '],';
			endforeach;
		endforeach;
		?>
        <div class="map-home" data-coords="[<?php echo rtrim( $output, ',' ); ?>]" id="map-home"></div>
        <div class="container">
            <h2>Two Locations to meet you</h2>
            <div class="row">
                <div class="col-sm-6">
                    <div class="accordion-contacts">
                        <dl class="accordion style-blue-1">
							<?php $counter = 0;
							while ( $query->have_posts() ) : $query->the_post(); ?>
                                <dt class="<?= $counter === 0 ? 'accordion-opened' : ''; ?>">
                                    <a href="#footer-contacts-<?php echo $counter; ?>"
                                       data-coords="<?php echo get_field( '_wpseo_coordinates_lat' ); ?>, <?php echo get_field( '_wpseo_coordinates_long' ); ?>">
										<?php echo get_the_title(); ?><span class="lazyload"></span>
                                    </a>
                                </dt>
                                <dd id="footer-contacts-<?php echo $counter; ?>"
                                    class='footer-accordion <?php if ( $counter == 0 ): echo ' accordion-opened'; endif; ?>'
                                    style="display: <?php echo $counter == 0 ? 'block' : 'none'; ?>" itemscope=""
                                    itemtype="http://schema.org/Attorney">

                                    <address>
                                        <div itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
                                            <span itemprop="streetAddress"><?php echo get_field( '_wpseo_business_address' ); ?></span><br/>
											<?php if ( get_field( '_wpseo_business_address_2' ) ) { ?>
                                                <span itemprop="streetAddress"><?php echo get_field( '_wpseo_business_address_2' ); ?></span>
                                                <br/>
											<?php } ?>
                                            <span itemprop="addressLocality"><?php echo get_field( '_wpseo_business_city' ); ?></span>,
                                            <span itemprop="addressRegion"><?php echo get_field( '_wpseo_business_state' ); ?></span>
                                            <span itemprop="postalCode"><?php echo get_field( '_wpseo_business_zipcode' ); ?></span><br/>
                                        </div>
                                        <strong>Phone: <span
                                                    itemprop="telephone"><?php echo get_field( '_wpseo_business_phone' ); ?></span></strong>

										<?php if ( get_field( '_wpseo_business_notes_1' ) ): ?><br/><br/>
                                            <span>Mailing Address:<br/><?php echo get_field( '_wpseo_business_notes_1' ); ?><br/><?php echo get_field( '_wpseo_business_notes_2' ); ?></span>
										<?php endif; ?>


                                        </p>
                                        <div class="text-center">
                                            <a target="_blank" href="<?php echo get_field( 'directions_link' ); ?>"
                                               class="but but-direction lazyload" rel="noopener">Directions</a>
                                        </div>
                                    </address>
									<?= get_field( 'location_schema_meta_data', get_the_id() ); ?>

                                </dd>
								<?php $counter ++; endwhile;
								wp_reset_query();
								?>
                        </dl>
                    </div>
                </div>
                <div class="col-sm-6">
					<?= get_template_part( 'includes/forms/form-redo-design' ); ?>

                </div>
            </div>
        </div>
    </section>
<?php endif; ?>

<section class="common-areas-served">
    <div class="container-small">
        <h2 class="text-center">Common Areas we Serve:</h2>
        <ul>
			<?php
			$_count = 0;
			$break  = false;
			if ( $_count === 6 ) {
				$break  = true;
				$_count = 0;
			}

			while ( have_rows( 'common_areas_served', 'option' ) ): the_row();

				$title = get_sub_field( 'area' );
				$url   = get_sub_field( 'link' );
				if ( isset( $url['url'] ) && strlen( $url['url'] ) > 0 ) {
					$title = sprintf( '<a target="_blank" href="%s">%s</a>', $url['url'], $title );
				}

				printf( '<li>%s</li>', $title );
				$_count ++;
			endwhile;
			?>
        </ul>

    </div>
</section>
<footer>
    <div class="navigation">
        <div class="container">
			<?php wp_nav_menu( array(
				"theme_location" => "footer-menu",
                    "container" => "ul"
            ) ); ?>
        </div>
    </div>
    <div class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <p class="pull-left">
                        &copy; <?php echo date( 'Y' ); ?> <?php echo get_field( 'bottom_copyright_text', 'option' ); ?></p>
                </div>
            </div>
        </div>
    </div>
</footer>
<?php echo get_field( 'cw_apps_form_js', 'option' ); ?>

<span class="fader" style="opacity: 0;"></span>
</div> <!-- End Wrapper -->
</div> <!-- End page-inner-area -->
<script type="text/javascript">
    //<![CDATA[
    var wpcf7 = {
        "apiSettings": {
            "root": "https:\/\/atlantaadvocate.com\/wp-json\/contact-form-7\/v1",
            "namespace": "contact-form-7\/v1"
        }
    };
    //]]>
</script>
<?php wp_footer(); ?>

<script>

    //	Jump to a FAQ and Open the Related Answer
    function jumpToAndOpenAnswer(aHash){
        //	assigning `#topFaq` here will bring us back to the index list of questions, when we click back after visiting `aHash`
        location.hash = '#topFaq';
        //	`aHash` needs to include the leading `#` symbol and is the valid value of an id attribute on the page.
        location.hash = aHash;

        var anchor = aHash + ' > .qa-faq-title > .qa-faq-anchor';
        var anchorActState = aHash + ' > .qa-faq-title > .active';
        if( ! document.querySelector( anchorActState ) ){
            document.querySelector( anchor ).click();
        }
    }
</script>

<script type="text/javascript">
    _linkedin_partner_id = "302513";
    window._linkedin_data_partner_ids = window._linkedin_data_partner_ids || [];
    window._linkedin_data_partner_ids.push(_linkedin_partner_id);
</script><script type="text/javascript">
    (function(){var s = document.getElementsByTagName("script")[0];
        var b = document.createElement("script");
        b.type = "text/javascript";b.async = true;
        b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";
        s.parentNode.insertBefore(b, s);})();
</script>
<noscript>
    <img height="1" width="1" style="display:none;" alt="" src="https://px.ads.linkedin.com/collect/?pid=302513&fmt=gif" />
</noscript>
<script type="application/ld+json">
{
  "@context" : "http://schema.org",
  "@type" : "LegalService",
  "name" : "The Millar Law Firm",
  "areaServed": {
    "@type" : "state",
    "name" : "Georgia",
    "containedInPlace": "United States"
  },
  "description": "Personal Injury Lawyers",
  "logo" : "https://c9j7q2u7.stackpathcdn.com/wp-content/themes/cw-theme-two/assets/images/logo-long-optimized.png",
  "image": "https://atlantaadvocate.com/wp-content/uploads/2019/03/Billboard-BANNER_031719-100.jpg",
  "url" : "https://atlantaadvocate.com/",
  "priceRange" : "$0-$100000",
  "telephone" : "(770) 400-0000",
  "address" : [
    {
      "@type" : "PostalAddress",
      "streetAddress" : "2221 Peachtree Road NE Suite P32",
      "addressLocality" : "Atlanta",
      "addressRegion" : "GA",
      "addressCountry" : "United States",
      "postalCode" : "30309"
    },
    {
      "@type" : "PostalAddress",
      "streetAddress" : "151 N Main Street",
      "addressLocality" : "Jonesboro",
      "addressRegion" : "GA",
      "addressCountry" : "United States",
      "postalCode" : "30236"
    }
  ],
  "aggregateRating" : {
    "@type" : "AggregateRating",
    "ratingValue" : "4.8",
    "bestRating" : "5",
    "worstRating" : "1",
    "ratingCount" : "69"
  },
  "reviews" : [
    {
      "@type" : "Review",
      "author" : {
        "@type" : "Person",
        "name" : "Kathie Neyman"
      },
      "datePublished" : "2019-04-01",
      "reviewRating" : {
        "@type" : "Rating",
        "ratingValue" : "5"
      },
      "reviewBody" : "I am very appreciative of the help I received from Millar Law Firm. The staff was so professional and kind from beginning to end. Everyone I encountered was very kind and attentive to my case. I dealt primarily with Joe Baker who always answered any questions in a timely manner and took the time to explain and address any concerns I had. I highly recommend this firm!"
    },
    {
      "@type" : "Review",
      "author" : {
        "@type" : "Person",
        "name" : "Pamela jJean Bourassa"
      },
      "datePublished" : "2019-02-25",
      "reviewRating" : {
        "@type" : "Rating",
        "ratingValue" : "5"
      },
      "reviewBody" : "These are AWESOME Attorneys! This firm has helped me and my family for years! They helped my mom (RIP MAMA), my son and then me! They are kind, compassionate, understanding and do a great job! THEY WIN!! Joe Baker, you ROCK!!"
    }
  ]
}
</script>
<!-- Juvo Lead Tracking tag for atlantaadvocate.com -->
<script type="text/javascript">
    (function() {
        var fd = document.createElement('script'); fd.type = 'text/javascript'; fd.async = true;
        fd.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.juvoleads.com/tag/926453398.js?v='+Math.floor(Math.random()*9999999999);
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(fd, s);
    })();
</script>
</body>
</html>