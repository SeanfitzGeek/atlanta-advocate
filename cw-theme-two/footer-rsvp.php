
<script type="text/javascript">
    //<![CDATA[
    var wpcf7 = {
        "apiSettings": {
            "root": "https:\/\/atlantaadvocate.com\/wp-json\/contact-form-7\/v1",
            "namespace": "contact-form-7\/v1"
        }
    };
    //]]>
</script>
<?php wp_footer(); ?>

<script language="javascript" type="text/javascript">
    // Hide the favorite-color text field by default
    document.getElementById("number-guests").style.display = 'none';
    // On every 'Change' of the drop down with the ID "FavoriteColorDropDown" call the displayTextField function
    // document.getElementById("attending-field").addEventListener("change", displayTextField);
    document.getElementById( 'attending-field' ).getElementsByTagName( 'input' )[0].addEventListener("click", displayTextField);
    document.getElementById( 'attending-field' ).getElementsByTagName( 'input' )[1].addEventListener("click", hideTextField);

    function displayTextField() {
        console.log('..dsplay fired');
        // Get the value of the selected drop down
        var dropDownText = document.getElementById("attending-field").getElementsByTagName('input')[0].value;
        // If selected text matches 'Other', display the text field.
        if (dropDownText == "Attending") {
            document.getElementById("number-guests").style.display = 'block';
        } else {
            document.getElementById("number-guests").style.display = 'none';
        }
    }

        function hideTextField() {
            // Get the value of the selected drop down
        var dropDownText = document.getElementById("attending-field").getElementsByTagName( 'input' )[1].value;
        // If selected text matches 'Other', display the text field.
            document.getElementById("number-guests").style.display = 'none';

    }
</script>

<script type='application/ld+json'>
    {
        "@context": "http://www.schema.org",
        "@type": "LegalService",
        "name": "The Millar Law Firm",
        "url": "https://atlantaadvocate.com/",
        "logo": "https://atlantaadvocate.com/wp-content/uploads/2018/02/MP-Header_LOGO_090718-300-cut.png",
        "image": "https://atlantaadvocate.com/wp-content/uploads/2019/03/Billboard-BANNER_031719-100.jpg",
        "description": "Personal Injury Law Firm located in the Atlanta, Georgia. ",
        "telephone": "+1-770-212-3795",
        "priceRange": "$000 - $10000",
        "address": {
            "@type": "PostalAddress",
            "streetAddress": "2221 Peachtree Road NE Suite P32",
            "addressLocality": "Atlanta ",
            "addressRegion": "Georgia",
            "postalCode": "30309",
            "addressCountry": "United States "
        },
        "contactPoint": {
            "@type": "ContactPoint",
            "telephone": "+1-(770) 212-3795",
            "contactType": "Customer support"
        }
    }
</script>

</body>
</html>