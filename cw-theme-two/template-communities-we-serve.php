<?php
/*
  * Template Name: Communities We Serve
  */

get_header( 'dark-blue' );

$communities = get_field( 'locations_communities_counties' );
$locations   = get_posts(
	[
		'post_type'      => 'wpseo_locations',
		'post_status'    => 'publish',
		'posts_per_page' => 10,
		'orderby'        => 'post_title',
		'order'          => 'ASC'

	] );

$counties        = get_posts(
	[
		'post_type'      => 'counties',
		'post_status'    => 'publish',
		'posts_per_page' => - 1,
		'orderby'        => 'post_title',
		'order'          => 'ASC'

	] );
$countiesCount   = count( $counties );
$countiesCounter = 0;
$counter         = 0;
?>


    <section class="main-communities-served">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <h2 class="bold center-mobile">Convenient Office Locations</h2>
                    <div class="office-locations">
						<?php foreach ( $locations as $location ) : ?>
							<?php $officeImg = get_field( 'office_location_image', $location->ID ); ?>
                            <div class="location-box lazyload" data-bg="<?= $officeImg['url']; ?>">
                                <a href="<?= get_permalink( $location->ID ); ?>">
                                    <h4 class="text-center">
										<?= $location->post_title; ?>
                                    </h4>

                                    <h5><?= get_field('sub_address_road', $location->ID);?></h5>
                                </a>
                                <div class="location-overlay"></div>
                            </div>

						<?php endforeach; ?>
                    </div>
                </div>
                <div class="col-sm-6">
					<?= get_field( 'locations_communities_main_content' ); ?>

                    <div class="counties-key data-dropdown-opener-list">
                        <div class="row">
                            <div class="col-xs-6">
								<?php

								$len = count( $counties );

								$firsthalf  = array_slice( $counties, 0, ceil( $len / 2 ) );
								$secondhalf = array_slice( $counties, ceil( $len / 2 ) );

								foreach ( $firsthalf as $county ) :
									$countiesCounter ++;
									$countHash = trim( str_replace( '&', 'and', str_replace( ' ', '-', strip_tags( strtolower( $county->post_title ) ) ) ) );
									?>
                                    <a data-opener="#<?= $countHash; ?>" href="#<?= $countHash; ?>"
                                       class="dropdown-opener"><?= $county->post_title; ?></a>

								<?php endforeach; ?>
                            </div>
                            <div class="col-sm-6">

								<?php foreach ( $secondhalf as $county ) :
									$countHash = trim( str_replace( '&', 'and', str_replace( ' ', '-', strip_tags( strtolower( $county->post_title ) ) ) ) );

									?>
                                    <a data-opener="#<?= $countHash; ?>" href="#<?= $countHash; ?>"
                                       class="dropdown-opener"><?= $county->post_title; ?></a>

								<?php endforeach; ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="counties-repeatable-sections block-dropdowns non-auto">

        <div class="panel-group" id="counties-dropdown" role="tablist" aria-multiselectable="true">
			<?php foreach ( $counties as $county ): ?>
				<?php
				$id_title      = trim( str_replace( '&', 'and', str_replace( ' ', '-', strip_tags( strtolower( $county->post_title ) ) ) ) );
				$content       = get_field( 'counties_main_content', $county->ID );
				$leftCountyImg = get_field( 'county_main_image', $county->ID );
				$background    = get_field( 'county_background', $county->ID );
				$colSize       = isset( $leftCountyImg['url'] ) && strlen( $leftCountyImg['url'] ) !== 0 ? '6' : '12';
				$subCatCount   = 0;
				?>
                <div class="panel panel-default panel-main" id="<?= $id_title; ?>-opener">

                    <div class="panel-heading panel-heading-main lazyload" role="tab"
                         data-bg="<?= get_field( 'county_dropdown_background', $county->ID ); ?>">


                        <a role="button" data-toggle="collapse" data-parent="#counties-dropdown"
                           href="#<?= $id_title; ?>" id="heading-<?= $counter; ?>-opener" aria-expanded="true"
                           class="panel-button collapsed"
                           aria-controls="heading-<?= $counter; ?>">
                            <h2 class="panel-title"><?= $county->post_title; ?> </h2>

                            <div class="panel-click">Click To Collapse</div>

<!--                            <a href="#" class="panel-top"><span class="glyphicon glyphicon-menu-up"></span></a>-->
                            <a role="button" data-toggle="collapse" data-parent="#counties-dropdown" href="#<?= $id_title; ?>" class="panel-activate"><span class="glyphicon glyphicon-menu-down"></span></a>
                        </a>

                        <div class="panel-background-shader"></div>

                    </div>
                    <div id="<?= $id_title; ?>" class="panel-collapse collapse lazyload county-listing" role="tabpanel"
                         data-bg="<?= $background; ?>">
                        <div class="container">
                            <div class="panel-body">
                                <div class="row">
									<?php if ( $colSize === '6' ): ?>
                                        <div class="col-sm-<?= $colSize; ?>">
                                            <img src="<?= $leftCountyImg['url']; ?>" alt="<?= $leftCountyImg['alt']; ?>"
                                                 class="img-responsive county-main-image">
                                        </div>
									<?php endif; ?>
                                    <div class="col-sm-<?= $colSize; ?>">
                                        <h2><?= $county->post_title; ?></h2>
										<?= $content; ?>
                                    </div>
                                </div>

                                <div class="cities">
                                    <h3 class="text-center">View Practice Areas By City</h3>
                                    <div class="city-practice-areas">

										<?php while ( have_rows( 'repeating_cities', $county->ID ) ): the_row();
											$cityTitle = get_sub_field( 'city_name' );
											$subCatCount ++;
											$cityUniqueId = trim( str_replace( '&', 'and', str_replace( ' ', '-', strip_tags( strtolower( $id_title . '-' . $cityTitle ) ) ) ) );

											?>

                                            <div class="panel-group panel_sub_cat_dropdown" role="tablist"
                                                 aria-multiselectable="true">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading" role="tab">
                                                        <h4 class="panel-title sub_cat_dropdown">


                                                            <a href="#<?= $cityUniqueId; ?>"
                                                               class="non-auto-panel" role="button"
                                                               data-toggle="collapse"
                                                               data-parent="#sub_category_dropdown">

																<?= $cityTitle; ?>
                                                                <span class="glyphicon glyphicon-menu-down"
                                                                      aria-hidden="true"></span>
                                                                <!--                                                                <span class="glyphicon glyphicon-menu-up" aria-hidden="true"></span>-->
                                                                <div class="clear"></div>
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="<?= $cityUniqueId; ?>"
                                                         class="panel-collapse collapse practice-listing"
                                                         role="tabpanel">
                                                        <div class="panel-body">

                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <ul class="practice-areas">

																		<?php $practiceAreaCounter = 0;
																		$locationId = get_field('county_location_assocation', $county->ID);
																		while ( have_rows( 'main_practice_areas',  $locationId[0]->ID) ): the_row(); //dynamic practice areas
																			$practiceAreaName = get_sub_field( 'practice_title' );
																			$practiceAreaLink = get_sub_field( 'url' );
																			if ( $practiceAreaCounter++ > 6 ) :
																				echo '</ul></div><div class="col-sm-6"><ul class="practice-areas">';
																				$practiceAreaCounter = 0;
																			endif;
																			?>
                                                                            <li>
                                                                                <a href="<?= $practiceAreaLink['url']; ?>">
																					<?= str_replace('<br>',' ',ucfirst(strtolower($practiceAreaName))); ?>
                                                                                </a>
                                                                            </li>
																		<?php endwhile; ?>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
										<?php endwhile; ?>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
			<?php endforeach; ?>
        </div>

    </section>

<?php get_footer( 'dark-blue' ); ?>