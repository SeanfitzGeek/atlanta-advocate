<?php
/*
  * Template Name: No Sidebar
  */
?>

<?php get_header('dark-blue'); ?>
<section class="content-wrapper">
    <div class="container">
        <article>
            <h1><?php h1_title(); ?><?php if ( get_field( 'job_title' ) ): ?>
                    <span><?php the_field( 'job_title' ); ?></span><?php endif; ?></h1>
			<?php if ( is_page( 'site-map' ) ) : ?>
                <div class='sitemap-container'>
					<?php get_template_part( 'partials/sitemap' ) ?>
                </div>
			<?php else : ?>
                <p>
					<?php if ( has_post_thumbnail() ): ?>
                        <img src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>"
                             class="thumbnail image left"/>
					<?php endif; ?>
					<?php the_content(); ?>
                </p>
			<?php endif ?>
        </article>
    </div>
</section>
<?php get_footer('dark-blue'); ?>
