<?php get_header('dark-blue'); ?>
<section class="content-wrapper">
    <div class="container">
        <div class="row">
            <article>
                <h1><?php echo Helper::index_title(); ?></h1>
                <div class="blog-wrapper">
					<?php if ( have_posts() ) : ?>
                        <div class="post-ajax-wrapper">
							<?php while ( have_posts() ) : the_post(); ?>
                                <div class="block-article blog-item">
                                    <div class="blog-inner">
                                        <h4><?php h1_title(); ?></h4>
                                        <div class="meta">
                                            Published <?php the_date(); ?>
                                        </div>
                                        <div class="preview">
											<?php if ( has_post_thumbnail() ): ?>
                                                <img src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>" class="thumbnail image left"/>
											<?php endif; ?>
                                            <p><?php the_excerpt(); ?></p>
                                        </div>
                                        <div class="tools">
                                            <a href="<?php the_permalink(); ?>" class="pull-right but but-flat">Read more</a>
                                        </div>
                                    </div>
                                </div>
							<?php endwhile; ?>
                        </div>
					<?php endif; ?>
                    <div class="btn-load-more-ajax" data-load-more=".btn-load-more-ajax a" data-target=".post-ajax-wrapper .blog-item" data-append-to=".post-ajax-wrapper">
						<?php echo next_posts_link( 'Load more posts' ); ?>
                    </div>
                </div>
            </article>
            <aside>
				<?php get_template_part( 'sidebars/blog' ); ?>
            </aside>
        </div>
    </div>
</section>
<?php get_footer('dark-blue'); ?>
