<?php
/*
  * Template Name: Dave Test File
  */

get_header( 'dark-blue' );
$ctaBg       = get_field( 'cta_section_image' );
$communityBg = get_field( 'in_community_content_image' );
$ctaAwardImg = get_field( 'cta_awards_image' );

$sectionIds = [
	'Our Lawyers'           => 'meet-the-law',
	'Our Staff'             => 'our-staff',
	'Our Mission'           => 'about-us',
	'Our History'           => 'about-us',
	'Our Success'           => 'our-success',
	'Our Reviews'           => 'our-reviews',
	'Our Community Service' => 'our-community',

];
?>
    <div class="about-us">
        <section class="main-banner-section lazyload " data-bg="<?= $ctaBg['url']; ?>">

            <div class="cta-main-left">
                <div class="inner-container">
                    <div class="content">
                        <h1><?= get_the_title(); ?></h1>
                        <div class="page-section-links">
                            <h5>Select Your Interest:</h5>
							<?php
							$i   = 0;
							$len = count( $sectionIds );
							?>
							<?php foreach ( $sectionIds as $section_title => $section_id ) : ?>
                                <a href="#<?= $section_id; ?>"><?= $section_title; ?></a>
								<?php
								if ( $i !== $len - 1 ) {
									echo '<span> | </span>';
								}
								$i ++;
								?>
							<?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <section class="meet-the-law legal-team" id="meet-the-law">
            <h2><?= get_field( 'meet_the_lawyers_title' ); ?></h2>

            <div class="glide-legal-team">
                <div class="glide__track" data-glide-el="track">
                    <ul class="glide__slides">
						<?php
						while ( have_rows( 'meet_the_lawyers_repeater_fields' ) ): the_row();
							$name  = get_sub_field( 'lawyer_title' );
							$link  = get_sub_field( 'lawyer_link' );
							$image = get_sub_field( 'lawyer_image' );
							?>
                            <li class="glide__slide">

                                <a href="<?= $link['url']; ?>">
                                    <div class="glide-slide-container lazyload"
                                         data-bg="<?= $image['url']; ?>">
                                        <div class="team-info-container">
                                            <div class="team-name"><?= $name; ?></div>
                                        </div>


                                    </div>

                                    <div class="glide-slide-overlay"></div>
                                </a>
                            </li>
						<?php endwhile; ?>
                    </ul>
                </div>
                <div class="glide__arrows" data-glide-el="controls">
                    <button class="glide__arrow prev" data-glide-dir="<"><span
                                class="glyphicon glyphicon-menu-left"></span>
                    </button>
                    <button class="glide__arrow next" data-glide-dir=">"><span
                                class="glyphicon glyphicon-menu-right"></span></button>
                </div>
            </div>

        </section>
        <section class="meet-our-legal-staff legal-team" id="our-staff">

            <h2><?= get_field( 'meet_our_legal_staff_title' ); ?></h2>

            <div class="glide-staff">
                <div class="glide__track" data-glide-el="track">
                    <ul class="glide__slides">
						<?php
						while ( have_rows( 'meet_the_staff_repeater_fields' ) ): the_row();
							$name  = get_sub_field( 'staff_title' );
							$link  = get_sub_field( 'staff_link' );
							$image = get_sub_field( 'staff_image' );
							?>
                            <li class="glide__slide">

								<?php if ( strlen( $link['url'] ) ): ?><a href="<?= $link['url']; ?>"><?php endif; ?>
                                    <div class="glide-slide-container lazyload"
                                         data-bg="<?= $image['url']; ?>">
                                        <div class="team-info-container">
                                            <div class="team-name"><?= $name; ?></div>
                                        </div>


                                    </div>

                                    <div class="glide-slide-overlay"></div>
									<?php if ( strlen( $link['url'] ) ): ?></a><?php endif; ?>
                            </li>
						<?php endwhile; ?>
                    </ul>
                </div>
                <div class="glide__arrows" data-glide-el="controls">
                    <button class="glide__arrow prev" data-glide-dir="<"><span
                                class="glyphicon glyphicon-menu-left"></span>
                    </button>
                    <button class="glide__arrow next" data-glide-dir=">"><span
                                class="glyphicon glyphicon-menu-right"></span></button>
                </div>
            </div>


        </section>
        <section class="about-us-content-blocks" id="about-us">
			<?php if ( strlen( $title = get_field( 'about_us_title' ) ) ): ?>
                <h2><?= $title; ?></h2>
			<?php endif; ?>
			<?php
			$video  = get_field( 'embed_video' );
			$leftBg = get_field( 'about_us_background' );
			if ( strlen( $video ) ):?>
                <div class="left-video" data-bg="">
					<?= $video; ?>
                </div>

			<?php elseif ( strlen( $leftBg['url'] ) ): ?>
                <div class="left-image lazyload" data-bg="<?= $leftBg['url']; ?>"></div>
			<?php endif; ?>

            <div class="right-content">
				<?php while ( have_rows( 'about_repeater_section_block' ) ): the_row();
					$title   = get_sub_field( 'about_repeater_section_block_title' );
					$content = get_sub_field( 'about_repeater_section_block_content' );
					$link    = get_sub_field( 'more_link' );
					?>
					<?php if ( strlen( $title ) ): ?>
                        <h2><?= $title; ?></h2>
					<?php endif; ?>
                    <div class="content">
						<?= $content; ?>

						<?php if ( strlen( $link['url'] ) ): ?>
                            <br>
                            <a href="<?= $link['url']; ?>">Learn More</a>
						<?php endif; ?>
                    </div>

				<?php endwhile; ?>
            </div>
            <div class="clear"></div>

        </section>

        <section class="our-case-results" id="our-success">
            <h2><?= get_field( 'case_results_title' ); ?></h2>
            <div class="results-boxes">
				<?php
				while ( have_rows( 'case_results_repeater_section_block' ) ): the_row();
					$title        = get_sub_field( 'title' );
					$resultsValue = get_sub_field( 'results_value' );
					$link         = get_sub_field( 'more_link' );
					$image        = get_sub_field( 'results_image' );

					?>
                    <div class="results-box">
                        <div class="results-top lazyload" data-bg="<?= $image['url']; ?>">
                            <div class="results-shader">
                                <h3><?= $resultsValue; ?></h3>
                            </div>

                        </div>
                        <div class="results-bottom">
                            <h3><?= $title; ?></h3>
                            <a href="<?= $link['url']; ?>">Learn More</a>
                        </div>
                    </div>

				<?php endwhile; ?>
            </div>
            <div class="clear"></div>

        </section>
        <section class="what-clients-say" id="our-reviews">
            <h2><?= get_field( 'client_testimonials_title' ); ?></h2>


            <div class="glide-testimonials">
                <div class="glide__track" data-glide-el="track">
                    <ul class="glide__slides">
						<?php
						$link = get_field( 'all_reviews_link' );

						while ( have_rows( 'client_testimonials' ) ): the_row();
							$name    = get_sub_field( 'client_testimonial_name' );
							$content = get_sub_field( 'client_story_content' );
							?>
                            <li class="glide__slide">
                                <div class="glide-slide-container">
                                    <div class="client-info-container">
                                        <div class="content">
											<?php //echo substr($content, 0, 200) . '...'; ?>
											<?= $content; ?>
                                        </div>
                                        <div class="client-name"><?= $name; ?></div>
                                    </div>

                                </div>

                                <div class="glide-slide-overlay"></div>
                            </li>
						<?php endwhile; ?>
                    </ul>
                </div>
                <div class="glide__arrows" data-glide-el="controls">
                    <button class="glide__arrow prev" data-glide-dir="<"><span
                                class="glyphicon glyphicon-menu-left"></span>
                    </button>
                    <button class="glide__arrow next" data-glide-dir=">"><span
                                class="glyphicon glyphicon-menu-right"></span></button>

                </div>
                <div class="clear"></div>

            </div>

            <a href="<?= $link; ?>" class="read-all-reviews">Read All Google Reviews</a>
        </section>


        <section class="in-community lazyload" id="our-community" data-bg="<?= $communityBg['url']; ?>">
            <div class="content-right">
                <div class="inner-container">

                    <h2><?= get_field( 'in_community_title' ); ?></h2>
                    <div class="content">
						<?= get_field( 'in_community_content' ); ?>
                    </div>
                </div>
            </div>

        </section>
    </div>

<?php get_footer( 'dark-blue' ); ?>