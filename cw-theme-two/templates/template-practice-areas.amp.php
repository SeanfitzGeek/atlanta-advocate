<?php
/*
  * Template Name: Amp Index Practice Area
  */

?>
<?php get_header( 'amp' ); ?>

    <main>
        <article>
            <div class="travel-angles max-width-3 mx-auto">
                <div class="travel-angle-left">
                    <div class="travel-angle-1 absolute"></div>
                </div>
                <div class="travel-angle-left">
                    <div class="travel-angle-2 absolute"></div>
                </div>

                <div class="travel-angle-right">
                    <div class="travel-angle-3 absolute"></div>
                </div>
            </div>

            <header>
                <h1><?php h1_title(); ?></h1>
		        <?php
		        $image_attributes = wp_get_attachment_image_src( get_the_ID() );
		        if ( $image_attributes ) :?>
                    <amp-img src="<?php echo $image_attributes[0]; ?>"
                             alt="<?php the_title(); ?>" width="<?php echo $image_attributes[1]; ?>"
                             height="<?php echo $image_attributes[2]; ?>" layout="fixed"></amp-img>
		        <?php endif; ?>
            </header>

            <div class="practice-area-block">
		        <?php require __DIR__ . '/../partials/practice-areas-amp.php';?>
            </div>

            <div class="content">
	            <?php the_content(); ?>
            </div>

            <div class="section-3">
                <?= get_field('section_3_content', 4611);?>
            </div>


        </article>

    </main>

<?php get_footer( 'amp' ); ?>