<?php
/*
 * Template Name: Amp Single Practice Area
  */

?>
<?php get_header( 'amp' ); ?>
    <main class="practice-area-single-amp">
        <article>
            <header>
                <h1><?php h1_title(); ?></h1>
                <div class="breadcrumb">
		            <?php if ( function_exists('yoast_breadcrumb') ):
			            $breadcrumbs = yoast_breadcrumb( '<ul><li>', '</li></ul>', false );
			            echo str_replace( '', '</li><li>', $breadcrumbs );
		            endif; ?>
                </div>
				<?php
				$image_attributes = wp_get_attachment_image_src( get_the_ID() );
				if ( $image_attributes ) :?>
                    <amp-img src="<?php echo $image_attributes[0]; ?>"
                             alt="<?php the_title(); ?>" width="<?php echo $image_attributes[1]; ?>"
                             height="<?php echo $image_attributes[2]; ?>" layout="fixed"></amp-img>
				<?php endif; ?>
            </header>

            <div class="content">

                <?= _ampify_img(apply_filters('the_content', get_post_field('post_content', $post->ID)));?>
            </div>

            <div class="section-1">
				<?= _ampify_img(get_field( 'section_1_content' )); ?>
            </div>


            <div class="section-2">
		        <?= _ampify_img(get_field( 'section_2_content' )); ?>
            </div>

            <div class="section-3">
		        <?= _ampify_img(get_field( 'section_3_content' )); ?>
            </div>

        </article>

    </main>

<?php get_footer( 'amp' ); ?>