<?php if ( get_field( 'default_team_content' ) == 'yes' ):
	$testimonial = get_field( 'team_testimonial', 'option' );
else:
	$testimonial = get_field( 'team_testimonial' );
endif; ?>
<div class="block-quote">
    <p><?php the_field( 'teaser_text', $testimonial ); ?></p>
    <i>– <?php echo $testimonial->post_title; ?></i>
</div>
