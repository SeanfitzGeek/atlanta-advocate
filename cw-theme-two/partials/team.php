<?php if ( get_field( 'enable_team' ) ):
	$background = get_field( 'default_team_content' ) == 'yes' ? get_field( 'team_background_image', 'option' ) : get_field( 'team_background_image' );
	$title = get_field( 'default_team_content' ) == 'yes' ? get_field( 'team_title', 'option' ) : get_field( 'team_title' );
	$subtitle = get_field( 'default_team_content' ) == 'yes' ? get_field( 'team_subtitle', 'option' ) : get_field( 'team_subtitle' );
	$left_column = get_field( 'default_team_content' ) == 'yes' ? get_field( 'team_left_column', 'option' ) : get_field( 'team_left_column' );
	$right_column = get_field( 'default_team_content' ) == 'yes' ? get_field( 'team_right_column', 'option' ) : get_field( 'team_right_column' );
	$testimonial = get_field( 'default_team_content' ) == 'yes' ? get_field( 'display_testimonial', 'option' ) : get_field( 'display_testimonial' );
	$side = get_field( 'default_team_content' ) == 'yes' ? get_field( 'side', 'option' ) : get_field( 'side' );
	$show_attorneys = get_field( 'default_team_content' ) == 'yes' ? get_field( 'display_attorneys', 'option' ) : get_field( 'display_attorneys' );
	$attorney_title = get_field( 'default_team_content' ) == 'yes' ? get_field( 'attorney_section_title', 'option' ) : get_field( 'attorney_section_title' );
	$attorney_count = get_field( 'default_team_content' ) == 'yes' ? get_field( 'how_many_attorneys', 'option' ) : get_field( 'how_many_attorneys' );
	$attorney_description = get_field( 'default_team_content' ) == 'yes' ? get_field( 'attorney_description', 'option' ) : get_field( 'attorney_description' );
	$team_footer = get_field( 'default_team_content' ) == 'yes' ? get_field( 'team_footer', 'option' ) : get_field( 'team_footer' );
	$cta_link = get_field( 'default_team_content' ) == 'yes' ? get_field( 'team_cta_link', 'option' ) : get_field( 'team_cta_link' );
	$cta_text = get_field( 'default_team_content' ) == 'yes' ? get_field( 'team_cta_text', 'option' ) : get_field( 'team_cta_text' );
	$footer_text = get_field( 'default_team_content' ) == 'yes' ? get_field( 'team_footer_text', 'option' ) : get_field( 'team_footer_text' );
	$single = $attorney_count == 'one' ? true : false;

	?>
    <section id="home-team" class="with-bg lazyload" data-bg="<?= $background;?>">
<!--        <img src="" data-src="--><?php //echo $background; ?><!--"/>-->
        <div class="container">
            <div class="row">
                <h2><?php echo $title; ?></h2>
				<?php if ( $subtitle ): ?>
                    <div class="text-center">
                        <h3><?php echo $subtitle; ?></h3>
                    </div>
				<?php endif; ?>
            </div>
            <div class="row lazyload">
                <div class="col left">
					<?php if ( $testimonial && $side == 'left' ): get_template_part( '/partials/testimonial' ); endif; ?>
					<?php echo $left_column; ?>
                </div>
                <div class="col right">
					<?php if ( $testimonial && $side == 'right' ): get_template_part( '/partials/testimonial' ); endif; ?>
					<?php echo $right_column; ?>
                </div>
            </div>
			<?php if ( $show_attorneys ): ?>
                <div class="slider-wrapper">
                    <div class="row row-meet">
                        <h2><?php echo $attorney_title; ?></h2>
						<?php if ( $attorney_count == 'one' ): ?>
                        <div class="col-sm-3">
							<?php endif; ?>
                            <div id="<?php echo $single ? '' : 'slider-team'; ?>" class="<?php echo $single ? '' : 'slider'; ?> clearfix">
								<?php $attorneys = $single ? array( get_field( 'attorney_single' ) ) : get_field( 'team_attorneys' );
								foreach ( $attorneys as $team ): ?>
<!--                                    <li>-->
                                        <a href="<?php echo get_permalink( $team->ID ); ?>">
                                            <figure>
												<?php echo get_the_post_thumbnail( $team->ID, 'full', array( 'class' => 'img-responsive center-block' ) ); ?>
                                                <figcaption><?php echo $team->post_title; ?><span>Attorney</span></figcaption>
                                            </figure>
                                        </a>
<!--                                    </li>-->
								<?php endforeach; ?>
                            </div>
							<?php if ( $single ): ?>
                        </div>
                        <div class="col-sm-9 attorney-description">
							<?php echo $attorney_description; ?>
                        </div>
					<?php endif; ?>

                    </div>
                </div>
			<?php endif; ?>
			<?php if ( $team_footer ): ?>
                <div class="row text-center">
                    <a href="<?php echo $cta_link ?>" class="but-see-all lazyload"><?php echo $cta_text ?></a>
                </div>
                <div class="row row-5">
					<?php echo $footer_text; ?>
                </div>
			<?php endif; ?>
        </div>
    </section>
<?php endif; ?>