<?php $args = array(
	'post_type'      => 'partner-logo',
	'post_status'    => 'publish',
	'posts_per_page' => - 1,
	'order' => 'ASC'
);
$query = new WP_Query( $args );
if ( $query->have_posts() ): ?>

    <section id="awards">
        <div class="slider-wrapper">

            <div class="container">

                <div class="row">

                    <div class="col-xs-12">
                        <h3><?php the_field( 'partner_title', 'option' ); ?></h3>
                        <div class="partners">
                            <div id="slider-awards" class="slider clearfix">
								<?php while ( $query->have_posts() ) : $query->the_post();
									if( get_field('award_link') ):
										?>
<!--                                        <li>-->
                                            <a href="<?php the_field('award_link'); ?>" target="_blank" rel="noopener"><?php echo the_post_thumbnail(); ?></a>
<!--                                        </li>-->
										<?php
									else: ?>
<!--                                        <li>-->
                                            <?php the_post_thumbnail(); ?>
<!--                                        </li>-->
									<?php endif;
								endwhile; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--<section id="awards" class="hidden-xs">

        <h3><?php the_field( 'partner_title', 'option' ); ?></h3>
        <div class="partners container">
            <ul id="awards-list">
			    <?php while ( $query->have_posts() ) : $query->the_post();
				    if( get_field('award_link') ):
					    ?>
                        <li><a href="<?php the_field('award_link'); ?>" target="_blank"><?php echo the_post_thumbnail('full', ['class' => 'img-responsive', 'title' => 'awards']); ?></a></li>
					    <?php
				    else: ?>
                        <li><?php echo the_post_thumbnail('full', ['class' => 'img-responsive', 'title' => 'awards']); ?></li>
				    <?php endif;
			    endwhile; ?>
            </ul>
        </div>

    </section>-->

<?php endif;
wp_reset_postdata(); ?>