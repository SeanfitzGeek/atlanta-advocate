<?php if ( get_field( 'page_main_section_title', $locationId ) ): ?>
    <h2 class="text-center practice-area-main-title"><?= get_field( 'page_main_section_title', $locationId ); ?></h2>
<?php endif; ?>
    <div class="practice-area-listing">

        <div class="row">
			<?php
			while ( have_rows( 'main_practice_areas', $locationId ) ): the_row();
				$title   = get_sub_field( 'practice_title' );
				$urlObj  = get_sub_field( 'url' );
				$iconObj = get_sub_field( 'practice_icon' );
				// TODO grab alt text for image icons from this img obj
//									$imgAlt = get_post_meta($iconObj->ID, '_wp_attachment_image_alt', true );

            if (strlen(trim($iconObj['url'])) === 0) {
                continue;
            }
				?>
                <div class="<?= $colsize; ?>">
                    <div class="practice-listing-container">
                        <a href="<?= $urlObj['url']; ?>">
                            <div class="img-container">
                                <img data-src="<?= $iconObj['url']; ?>" alt="<?= $iconObj['alt']; ?>"
                                     class="lazyload img-responsive text-center">
                            </div>
                            <div class="practice-area-listing-title"><?= $title; ?></div>
                        </a>
                    </div>
                </div>
			<?php endwhile; ?>
        </div>
    </div>

    <div class="clearfix"></div>
<?php if ( $showExtended ): ?>
    <div class="additional-practice-areas">
        <h3><?= get_field( 'additional_practice_area_title', $locationId ); ?></h3>

		<?php while ( have_rows( 'additional_practice_areas_categories', $locationId ) ):
			the_row();
			$title       = get_sub_field( 'title' );
			$url         = get_sub_field( 'url' );
			$icon        = get_sub_field( 'icon' );
			$sideofGroup = [];

			?>
            <div class="practice-area-category">
                <div class="row">

                    <div class="col-sm-3">
                        <div class="practice-area-list-icon">
                            <a href="<?= $url['url']; ?>">
                                <img data-src="<?= $icon['url']; ?>" class="lazyload" alt="<?= $icon['alt']; ?>">
                                <div class="">
									<?= $title; ?>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-sm-9">
                        <div class="practice-area-lists">
							<?php while ( have_rows( 'additional_practice_areas_links', $locationId ) ): the_row();
								$title = get_sub_field( 'title' );
								$url   = get_sub_field( 'url' );

								$sideofGroup[ get_sub_field( 'left_or_right_column' ) ][] = [
									'title' => $title,
									'url'   => $url['url']
								];

								?>

							<?php endwhile; ?>

							<?php $col = isset( $sideofGroup['Right'] ) && count( $sideofGroup['Right'] ) > 0 ? 'col-sm-6' : 'col-sm-12'; ?>
                            <div class="row">
                                <div class="<?= $col; ?>">
                                    <ul>
										<?php foreach ( $sideofGroup['Left'] as $link ): ?>
											<?php printf( '<li><a href="%s">%s</a></li>', $link['url'], $link['title'] ); ?>
										<?php endforeach; ?>
                                    </ul>
                                </div>

								<?php if ( isset( $sideofGroup['Right'] ) ) : ?>
                                    <div class="<?= $col; ?>">
                                        <ul>
											<?php foreach ( $sideofGroup['Right'] as $link ): ?>
												<?php printf( '<li><a href="%s">%s</a></li>', $link['url'], $link['title'] ); ?>
											<?php endforeach; ?>
                                        </ul>
                                    </div>
								<?php endif; ?>

								<?php if ( isset( $sideofGroup['Full'] ) ) : ?>
                                    <div class="col-sm-12">
                                        <ul>
											<?php foreach ( $sideofGroup['Full'] as $link ): ?>
												<?php printf( '<li><a href="%s">%s</a></li>', $link['url'], $link['title'] ); ?>
											<?php endforeach; ?>
                                        </ul>
                                    </div>
								<?php endif; ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
		<?php endwhile; ?>
    </div>
<?php endif; ?>