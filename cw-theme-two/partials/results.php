<?php

    if ( get_field( 'enable_results' ) ):
	    $title = get_field( 'use_default_results' ) == 'yes' ? get_field( 'results_title', 'option' ) : get_field( 'results_title' );
	    $results = get_field( 'use_default_results' ) == 'yes' ? get_field( 'results', 'option' ) : get_field( 'results' );
	    $cta_link = get_field( 'use_default_results' ) == 'yes' ? get_field( 'results_cta_link', 'option' ) : get_field( 'results_cta_link' );
	    $cta_text = get_field( 'use_default_results' ) == 'yes' ? get_field( 'results_cta_text', 'option' ) : get_field( 'results_cta_text' );
	?>

    <section id="results" class="lazyload" data-bg="<?= get_stylesheet_directory_uri();?>/images/bg-wave-2.png">
        <div class="container hidden-md hidden-sm hidden-xs">
            <ul class="result-list">
                <li><?= $title; ?></li>
				<?php foreach ( $results as $result ): ?>
                    <li>
                        <div class="inner-wrap">
                            <h3><?= get_field( 'amount', $result ); ?></h3>
                            <p>
		                        <?php if ( get_field( 'subtitle', $result ) ):
			                        echo get_field( 'subtitle', $result );
		                        endif;
			                        $term = get_the_terms( $result, 'case-result-category' );
			                        echo "<span>".$term[0]->name."</span>";
		                        ?>
                            </p>
                        </div>
                    </li>
				<?php endforeach; ?>
                <li>
                    <a href="<?= $cta_link; ?>">
                        <div><?= $cta_text; ?>
                            <img alt="Right Arrow Button" class="lazyload" data-src="<?php echo get_stylesheet_directory_uri() . '/images/button-arrow-right.png'; ?>"/>
                        </div>
                    </a>
                </li>
            </ul>

        </div>

        <div class="slider-wrapper hidden-lg">
            <div class="container">

                <ul class="result-first">
                    <li><?php echo $title; ?></li>
                </ul>

                <div id="slider-results" class="slider clearfix result-list">
					<?php foreach ( $results as $result ): ?>
<!--                        <li>-->
                            <div class="inner-wrap">
                                <h3><?php echo get_field( 'amount', $result ); ?></h3>
                                <p>
		                            <?php if ( get_field( 'subtitle', $result ) ):
			                            echo get_field( 'subtitle', $result );
                                    endif; 
			                            $term = get_the_terms( $result, 'case-result-category' );
			                            echo "<span>".$term[0]->name."</span>";
		                            ?>
                                </p>
                            </div>
<!--                        </li>-->
					<?php endforeach; ?>
                </div>

                <ul class="result-last">
                    <li>
                        <a href="<?php echo $cta_link; ?>">
                            <div><?php echo $cta_text; ?> <img alt="Right Arrow Button" class="lazyload" data-src="<?php echo get_stylesheet_directory_uri() . '/images/button-arrow-right.png'; ?>"/></div>
                        </a>
                    </li>
                </ul>

            </div>
        </div>

    </section>

<?php endif; ?>
