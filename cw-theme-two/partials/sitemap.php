<ul>
	<?php
	// Add pages you'd like to exclude in the exclude here

	wp_list_pages(
		array(
			'exclude'  => [86073],
			'title_li' => '',
		)
	);

	?>
</ul>

<?php
$team_args = array(
	'post_type'      => 'attorney',
	'posts_per_page' => - 1,
);

$team_query = new WP_Query( $team_args );
if ( $team_query->have_posts() ):
	?>

    <ul>
		<?php while ( $team_query->have_posts() ): $team_query->the_post(); ?>
            <li><a href="<?php the_permalink() ?>"><?php the_title() ?></a></li>
		<?php endwhile ?>
    </ul>

<?php endif ?>

<?php
$loc_args = array(
	'post_type'      => 'wpseo_locations',
	'posts_per_page' => - 1,
);

$loc_query = new WP_Query( $loc_args );
if ( $loc_query->have_posts() ): ?>

    <ul>
		<?php while ( $loc_query->have_posts() ): $loc_query->the_post(); ?>
            <li><a href="<?php the_permalink() ?>"><?php the_title() ?></a></li>
		<?php endwhile ?>
    </ul>

<?php endif ?>