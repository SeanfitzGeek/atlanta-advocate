<?php if ( get_field( 'enable_community') ): ?>
	<?php if ( get_field( 'default_community_content' ) == 'yes' ):
		$background = get_field( 'community_background_image', 'option' );
		$title      = get_field( 'community_title', 'option' );
		$content    = get_field( 'community_content', 'option' );
		$cta        = get_field( 'community_cta', 'option' );
		$cta_link   = get_field( 'community_cta_link', 'option' );
		$cta_text   = get_field( 'community_cta_text', 'option' );
	else:
		$background = get_field( 'community_background_image', 'option' );
		$title      = get_field( 'community_title', 'option' );
		$content    = get_field( 'community_content', 'option' );
		$cta        = get_field( 'community_cta' );
		$cta_link   = get_field( 'community_cta_link', 'option' );
		$cta_text   = get_field( 'community_cta_text', 'option' );
	endif; ?>
    <section id="community" class="with-bg lazyload" data-bg="<?= $background;?>">
<!--        <img src="" data-src="--><?php //echo $background; ?><!--"/>-->
        <div class="container">
            <div class="row">
                <div class="content">
                    <h2><?php echo $title; ?></h2>
					<?php echo $content; ?>

                    <div class="text-left btn-contact">
                        <a href="/our-communities/" aria-label="Learn More about our Communities" class="lazyload" data-bg="<?= get_stylesheet_directory_uri();?>/assets/images/arrow-more.png">Learn More about our Communities</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>