<?php
$locationId = 4611;
$columnCLasses = '';
?>

<?php if ( get_field( 'page_main_section_title', $locationId ) ): ?>
	<h2 class="text-center practice-area-main-title"><?= get_field( 'page_main_section_title', $locationId ); ?></h2>
<?php endif; ?>
	<div class="practice-area-listing">
		<div class="row">
			<?php
			while ( have_rows( 'main_practice_areas', $locationId ) ): the_row();

				$title   = get_sub_field( 'practice_title' );
				$urlObj  = get_sub_field( 'url' );
				$iconObj = get_sub_field( 'practice_icon' );
				// TODO grab alt text for image icons from this img obj
//									$imgAlt = get_post_meta($iconObj->ID, '_wp_attachment_image_alt', true );
				?>
				<div class="<?= $columnCLasses; ?>">
					<div class="practice-listing-container">
						<a href="<?= $urlObj['url']; ?>">
							<div class="img-container">
                                <amp-img src="<?= $iconObj['url']; ?>"
                                         class="lazyload img-responsive text-center"
                                         alt="<?= $iconObj['alt']; ?>" width="120"
                                         height="120" layout="fixed"></amp-img>


							</div>
							<div class="practice-area-listing-title"><?= $title; ?></div>
						</a>
					</div>
				</div>
			<?php endwhile; ?>
		</div>
	</div>

