<?php
/*
  * Template Name: Custom Single Locations Template - developer version
  */

get_header('dark-blue');
$locationId = get_field( 'location_page_id' );
$locationId = $locationId[0];

//var_dump($locationId);
setup_postdata( $locationId );
?>

<section class="content-wrapper">
    <div class="container">
        <div class="locations-heading">
            <h1><?php h1_title(); ?></h1>
            <h2><?= get_field( 'sub_title', $locationId ); ?></h2>
        </div>
        <div class="row">
            <article>
				<?php the_content(); ?>

                <div class="locations-map">
                    <div class="block side-address clearfix">
                        <div class="address-holder">
                            <div class="address">
                                <div class="address-header">
                                    <h4>
                                        <strong>The Millar Law Firm
                                            - <?php echo get_field( '_wpseo_business_phone', $locationId ); ?></strong>
                                        <br>
                                        <strong>
								            <?php echo get_field( '_wpseo_business_address', $locationId ); ?>
								            <?php echo get_field( '_wpseo_business_address_2', $locationId ); ?>
								            <?php echo get_field( '_wpseo_business_city', $locationId ); ?>
                                            , <?php echo get_field( '_wpseo_business_state', $locationId ); ?>
								            <?php echo get_field( '_wpseo_business_zipcode', $locationId ); ?>
                                        </strong>
                                    </h4>
                                </div>
                                <address>
                                    <div class="buttons">
                                        <a target="_blank" href="<?php the_field( 'directions_link', $locationId ); ?>"
                                           class="but-directions lazyload">Directions</a>
                                        <a href="#" class="but-hours">Hours</a>
                                    </div>
                                </address>
                                <div class="popup-hours">
                                    <h5>Hours of Operation</h5>
                                    <ul>
							            <?php for ( $n = 1; $n < 6; $n ++ ): ?>
								            <?php $day = date( 'l', strtotime( 'Sunday +' . $n . ' days' ) ); ?>
                                            <li class="clearfix"><span><?php echo $day ?></span>
                                                <b><?php echo date( 'g:iA', strtotime( get_field( '_wpseo_opening_hours_' . strtolower( $day ) . '_from' ) ) ); ?>
                                                    - <?php echo date( 'g:iA',
											            strtotime( get_field( '_wpseo_opening_hours_' . strtolower( $day ) . '_to' ) ) ); ?></b>
                                            </li>
							            <?php endfor; ?>

                                    </ul>
                                    <p>Scheduled appointments after hours</p>
                                    <div class="popup-arrow"></div>
                                </div>
                            </div>
                            <div class="map-holder">
                                <div data-coords="<?php echo get_field( '_wpseo_coordinates_lat', $locationId ); ?>, <?php echo get_field( '_wpseo_coordinates_long', $locationId ); ?>"
                                     data-marker="1" id="map-3" class="map"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </article>

            <aside>
                <?php get_template_part( 'sidebars/locations' ); ?>
            </aside>
        </div>
    </div>
</section>
<?php get_footer('dark-blue'); ?>