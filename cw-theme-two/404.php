<?php get_header('dark-blue'); ?>
<section class="content-wrapper">
    <div class="container">
        <div class="row">
            <article>
                <h1>Error 404: Page not found</h1>
                <p>Sorry, the page you are looking for cannot be found. Try a search below:</p>
				<?php get_search_form(); ?>
            </article>
            <aside>
				<?php get_template_part( 'sidebars/blog' ); ?>
            </aside>
        </div>
    </div>
</section>
<?php get_footer('dark-blue'); ?>
