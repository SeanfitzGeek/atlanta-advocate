<?php
define( 'JS_VERS', '1.0.364' );
define( 'CSS_VERS', '1.4.2.532' );
define( 'YOUTUBE_API_KEY', 'AIzaSyDShvj96pfuz5JrOYD2EnGyxKESCnL-Mw0' );

require_once __DIR__ . '/includes/class-wp-bootstrap-navwalker.php';
require_once __DIR__ . '/includes/shortcodes/faq-non-accordion.php';
require_once __DIR__ . '/includes/custom-post-legal-terms.php';
require_once __DIR__ . '/includes/custom-post-counties.php';
require_once __DIR__ . '/includes/custom-post-events.php';
require_once __DIR__ . '/includes/custom-post-community.php';

// library for php mobile detection. Used for templates rendering unique content/design for mobile only
require_once __DIR__ . '/includes/libs/Mobile_Detect.php';

// Adds custom image sizes
add_action( 'after_setup_theme', 'afterThemeSetup' );
function afterThemeSetup() {
	add_image_size( 'category-thumb', 195, 130, true );
	add_image_size( 'post-square', 350, 350, true );
	add_image_size( 'post-rectangle', 350, 225, true );
}

// loads up scripts/styles for theme
function theme_scripts() {
	$cssVersion = defined( 'dev_site' ) ? ( new DateTime() )->format( 'h-i-s' ) : CSS_VERS;
	$jsVersion  = defined( 'dev_site' ) ? ( new DateTime() )->format( 'h-i-s' ) : JS_VERS;


	# Deregister local and Load External jQuery
	wp_deregister_script( 'jquery' );
	wp_enqueue_script( 'theme-vendor-js-min', get_stylesheet_directory_uri() . '/dist/built.min.js', array(), $jsVersion, true );

	if ( ! is_page_template( 'page-missed.php' ) && ! is_page_template( 'template-mobile-home.php' ) ) {
		# Combined/minified Css
		wp_enqueue_style( 'theme-main-min', get_stylesheet_directory_uri() . '/dist/built-bs-3-4.min.css', false, $cssVersion );
	}

	# load non critical css not used on home page
	if ( ! is_page_template( 'template-home.php' ) && ! is_page_template( 'template-mobile-home.php' ) && ! is_front_page() ) {
		wp_enqueue_style( 'non-critical-min', get_stylesheet_directory_uri() . '/dist/non-critical.min.css', false, $cssVersion );


		if ( is_page_template( 'template-practice-areas.php' ) || is_page_template( 'template-communities-we-serve.php' ) ) {
			wp_enqueue_script( 'hash-activate', get_stylesheet_directory_uri() . '/assets/js/custom/practice-areas-updated.js', array( 'theme-vendor-js-min' ), $jsVersion, true );
		}
	}

	if ( is_page_template( 'template-about-us.php' ) || is_page_template( 'template-attorney-bio.php' ) ) {
		wp_enqueue_script( 'glide-slider-js', 'https://cdn.jsdelivr.net/npm/@glidejs/glide', array(), $jsVersion, true );
		wp_enqueue_style( 'glide-slider-css', get_stylesheet_directory_uri() . '/css/library/glide.core.min.css', false, $cssVersion );
	}

	if ( is_page_template( 'template-contact-us.php' ) || is_page_template( 'template-location-practice-area.php' ) ) {
		wp_enqueue_script( 'contact-us', get_stylesheet_directory_uri() . '/assets/js/custom/contact-us.js', array( 'theme-vendor-js-min' ), $jsVersion, true );
	}

	// Homepage Template
	if ( is_front_page() ) {

		wp_dequeue_script( 'theme-vendor-js-min' );
		wp_dequeue_style( 'theme-main-bs-3.4-min' );
		wp_dequeue_style( 'theme-main-min' );
		wp_dequeue_script( 'google-recaptcha' );

		wp_enqueue_style( 'home-wide', get_stylesheet_directory_uri() . '/dist/home-wide.min.css', false, $cssVersion );
//		wp_enqueue_script( 'modernizer', get_stylesheet_directory_uri() . '/assets/js/vendor/modernizr-2.8.3.min.js', array(), $jsVersion, true );

		wp_enqueue_script( 'home-wide-js', get_stylesheet_directory_uri() . '/dist/home-wide.min.js', array(), $jsVersion, true );
//		wp_enqueue_script( 'sly-slider', get_stylesheet_directory_uri() . '/assets/js/vendor/sly-compiled.js', array(), $jsVersion, true );
//		wp_enqueue_script( 'glide-slider-js', 'https://cdn.jsdelivr.net/npm/@glidejs/glide', array(), $jsVersion, true );
//		wp_enqueue_style( 'glide-slider-css', get_stylesheet_directory_uri() . '/css/library/glide.core.min.css', false, $cssVersion );
	}


	wp_enqueue_style( 'google-font-playfair', 'https://fonts.googleapis.com/css?family=Playfair+Display:400,700&display=swap', false );

}

function remove_wpseo_jquery() {
	return false;
}

add_filter( 'wpseo_local_load_jquery', 'remove_wpseo_jquery' );
add_action( 'wp_enqueue_scripts', 'theme_scripts' );

/**
 * Disable the emoji's
 */
function disable_emojis() {
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );

	// Remove from TinyMCE
	add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
}

add_action( 'init', 'disable_emojis' );

/**
 * Filter out the tinymce emoji plugin.
 */
function disable_emojis_tinymce( $plugins ) {
	if ( is_array( $plugins ) ) {
		return array_diff( $plugins, array( 'wpemoji' ) );
	} else {
		return array();
	}
}

function unLoadScripts() {
	wp_deregister_script( 'q-a-plus' );
	wp_deregister_script( 'lazysizes' );
	wp_deregister_script( 'lazysizes-unveilhooks' );
	wp_dequeue_script( 'q-a-plus' );
	wp_dequeue_script( 'maps-geocoder' );
	wp_dequeue_script( 'lazysizes' );
	wp_dequeue_script( 'lazysizes-unveilhooks' );

	#todo add this to loader for performance
	#wp_dequeue_script( 'google-recaptcha' );

}

function unloadStyles() {
	wp_deregister_style( 'ssbp-font-awesome' );
	wp_dequeue_style( 'ssbp-font-awesome' );

	wp_deregister_style( 'font-awesome' );
	wp_dequeue_style( 'font-awesome' );

	wp_deregister_style( 'q-a-plus' );
	wp_dequeue_style( 'q-a-plus' );
}

add_filter( 'wp_enqueue_scripts', 'unLoadScripts', 999 );
add_action( 'wp_enqueue_scripts', 'unloadStyles', 999 );


// make sure CF7 scripts and stylesheets arent loading
add_filter( 'wpcf7_load_js', '__return_false' );
add_filter( 'wpcf7_load_css', '__return_false' );

function unloadercssmain() {
	wp_dequeue_style( 'theme-main-min' );
	wp_dequeue_script( 'theme-vendor-js-min' );
}

// H1 title
function h1_title() {
	global $post;
	if ( get_field( 'h1_title' ) ):
		echo get_field( 'h1_title' );
	else:
		the_title();
	endif;
}

function get_id_by_slug( $page_slug ) {
	$page = get_page_by_path( $page_slug );
	if ( $page ) {
		return $page->ID;
	} else {
		return null;
	}
}

function cws_form_output() {
	ob_start();
	?>

    <div class="custom-form form form-wide col-sm-12">

        <form class="clearfix header-form" method="post" action="#">
            <h3>Fill Out the Form Below</h3>

            <div class="row">

                <div class="col-sm-6">
                    <div class="required">
                        <input name="name" type="text" placeholder="First Name"/>
                        <span>required</span>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="required">
                        <input name="enail" type="text" placeholder="Email"/>
                        <span>required</span>
                    </div>
                </div>
                <div class="col-sm-12">
                    <button type="submit" class="but-form">Send to our Attorneys</button>
                </div>

            </div>

        </form>

    </div>

	<?php
	return ob_get_clean();
}

add_shortcode( 'cws_form', 'cws_form_output' );

function getMainContactForm() {
	ob_start();

	include __DIR__ . '/includes/forms/form-redo-design.php';

	return ob_get_clean();
}

add_shortcode( 'get_main_contact_form', 'getMainContactForm' );
add_filter( 'widget_text', 'do_shortcode' );

function cw_wpseo_local_cpt_args( $args_cpt ) {
	$args_cpt['rewrite']['with_front'] = false;

	return $args_cpt;
}

add_filter( 'wpseo_local_cpt_args', 'cw_wpseo_local_cpt_args', 10, 1 );
add_filter( 'image_size_names_choose', 'custom_image_sizes_choose' );

function getPreviousText() {
	ob_start(); ?>
    <div class="former-link">Formerly Known as "Millar & Mixon, LLC"<br>
        <div class="bold">Contact Steve Mixon
            <br>
            Mixon-Law.com <span style="margin-left: 5px;">770-955-0100</span>
        </div>
    </div>
	<?php return ob_get_clean();
}

add_action( 'after_setup_theme', 'registerMenus' );
function registerMenus() {
	register_nav_menu( 'header-dark-blue', 'Main Header Navigation Menu' );
	register_nav_menu( 'footer-menu', 'Main Footer Navigation' );
}

function zap_sid( $l ) {
	return preg_replace( "/id='.*-css'/", "", $l );
}

add_filter( 'style_loader_tag', 'zap_sid' );

// disables notice for search wp
add_filter( 'searchwp_missing_integration_notices', '__return_false' );

/**
 * Advanced Custom Fields Options function
 * Always fetch an Options field value from the default language
 */
function cl_acf_set_language() {
	return acf_get_setting( 'default_language' );
}

function get_global_option( $name ) {
	add_filter( 'acf/settings/current_language', 'cl_acf_set_language', 100 );
	$option = get_field( $name, 'option' );
	remove_filter( 'acf/settings/current_language', 'cl_acf_set_language', 100 );

	return $option;
}

function getGlobalSiteForm( $title, $caption, $notice ) {
	ob_start();
	require __DIR__ . '/includes/forms/global-site-form.php';

	return ob_get_clean();
}

function pm_remove_all_scripts() {
	global $wp_scripts;
	$wp_scripts->queue = array();
}


function pm_remove_all_styles() {
	global $wp_styles;
	$wp_styles->queue = array();
}

function remove_admin_login_header() {
	remove_action( 'wp_head', '_admin_bar_bump_cb' );
}

function remove_independent_publisher_customizer_css() {
	wp_dequeue_style( 'customizer' );
}

add_filter( "template_include", "loadAmpTemplate", 100 );

function removePluginScssCustomStylesAmp() {
	wp_dequeue_style( 'sccss_style' ); //Name of Style ID.
}

function loadAmpTemplate( $template ) {
	$ampEnabled = get_field( 'show_amp_template' );

	if ( ! $ampEnabled ) {
		return $template;
	}

	if ( isset( $_GET['amp'] ) ) {
		add_action( 'get_header', 'remove_admin_login_header' );
		add_action( 'wp_print_scripts', 'pm_remove_all_scripts', 100 );
		add_action( 'wp_print_styles', 'pm_remove_all_styles', 100 );
		add_action( 'wp_print_styles', 'remove_independent_publisher_customizer_css', 100 );

		add_action( 'wp_enqueue_scripts', 'removePluginScssCustomStylesAmp', 999 );

		return locate_template( 'templates/template-practice-area-single-amp.php' );
	}

	$detect = new Mobile_Detect;

	if ( $detect->isMobile() ) {
		if ( isset( $_GET['amp'] ) ) {
			add_action( 'get_header', 'remove_admin_login_header' );
			add_action( 'wp_print_scripts', 'pm_remove_all_scripts', 100 );
			add_action( 'wp_print_styles', 'pm_remove_all_styles', 100 );
			add_action( 'wp_print_styles', 'remove_independent_publisher_customizer_css', 100 );


			return locate_template( 'templates/template-practice-area-single-amp.php' );
		} else {
			global $post;
			wp_redirect( get_permalink( $post->ID ) . '?amp' );
			exit;
		}
	}

	return $template;
}

function getPracticeAreasHtml( $colsize = 'col-sm-4', $showExtended = true, $optionId = null ) {
	if ( $optionId ) {
		$locationId = get_field( 'location_page_id', $optionId );
		$locationId = $locationId[0];
	} else {
		$locationId = get_field( 'location_page_id' );
		$locationId = $locationId[0];

	}

	if ( empty( $locationId ) ) {
		return '';
	}

	ob_start();
	require __DIR__ . '/partials/practice-areas.php';

	return ob_get_clean();
}

function getAmpContent( $content ) {
	$result = preg_replace( '/(<img[^>]+>(?:<\/img>)?)/i', '$1</amp-img>', $content );

	return str_replace( '<img', '<amp-img', $result );
}

/**
 * Replace img tag with amp-img
 *
 * <amp-img src="[src]"
 *   width="[width]"
 *   height="[height]"
 *   layout="contain"
 *   alt="[alt]">
 * </amp-img>
 *
 */
function _ampify_img( $html ) {
	preg_match_all( "#<img(.*?)\\/?>#", $html, $img_matches );
	foreach ( $img_matches[1] as $key => $img_tag ) {
		preg_match_all( '/(alt|src|width|height)=["\'](.*?)["\']/i', $img_tag, $attribute_matches );
		$attributes = array_combine( $attribute_matches[1], $attribute_matches[2] );
		if ( ! array_key_exists( 'width', $attributes ) || ! array_key_exists( 'height', $attributes ) ) {
			if ( array_key_exists( 'src', $attributes ) ) {
				list( $width, $height ) = getimagesize( $attributes['src'] );
				if ( strlen( $width ) === 0 ) {
					unset( $attributes['width'] );

				} else {
					$attributes['width'] = $width;

				}

				if ( strlen( $height ) === 0 ) {
					unset( $attributes['height'] );
				} else {
					$attributes['height'] = $height;
				}
			}
		}
		$amp_tag = '<amp-img ';
		foreach ( $attributes as $attribute => $val ) {
			$amp_tag .= $attribute . '="' . $val . '" ';
		}
		$amp_tag .= 'layout="fill" ';
		$amp_tag .= 'class="contain"';
		$amp_tag .= '>';
		$amp_tag .= '</amp-img>';
		$amp_tag = sprintf( '  <div class="fixed-height-container">%s</div>', $amp_tag );

		$html = str_replace( $img_matches[0][ $key ], $amp_tag, $html );
	}

	return _ampify_iframe( $html );
}

function _ampify_iframe( $html ) {
	preg_match_all( "#<iframe(.*?)\\/?>#", $html, $iframe_match );
	foreach ( $iframe_match[1] as $key => $iframe_tag ) {
		preg_match_all( '/(src|width|height)=["\'](.*?)["\']/i', $iframe_tag, $attribute_matches );
		$attributes = array_combine( $attribute_matches[1], $attribute_matches[2] );
		if ( ! array_key_exists( 'width', $attributes ) || ! array_key_exists( 'height', $attributes ) ) {
			if ( array_key_exists( 'src', $attributes ) ) {
				$attributes['width']  = 700;
				$attributes['height'] = 400;
			}
		}

		if ( array_key_exists( 'allowfullscreen', $attributes ) ) {
			unset( $attributes['allowfullscreen'] );
		}

		$amp_tag = '<amp-iframe ';
		foreach ( $attributes as $attribute => $val ) {
			$amp_tag .= $attribute . '="' . $val . '" ';
		}
		$amp_tag .= 'layout="responsive" ';
		$amp_tag .= 'class="contain" ';
		$amp_tag .= 'sandbox="allow-scripts allow-same-origin allow-presentation" ';
		$amp_tag .= '>';
		$amp_tag .= '</amp-iframe>';
		$amp_tag = sprintf( '  <div class="fixed-height-container">%s</div>', $amp_tag );

		$html = str_replace( $iframe_match[0][ $key ], $amp_tag, $html );
	}

	return $html;
}

function formatSlugForId( $slug ) {
	if ( ctype_alpha( $slug[0] ) === false ) {
		$slug = 'go-' . $slug;
	}

	return $slug;
}

function getEducationSearch() {
	ob_start();

	$form = ob_get_clean();

	return $form;
}

function getBreadcrumbs() {
	$sep = ' > ';
	if ( ! is_front_page() ) {

		// Start the breadcrumb with a link to your homepage
		echo '<div class="breadcrumbs">';
		echo '<a href="';
		echo get_option( 'home' );
		echo '">';
		bloginfo( 'name' );
		echo '</a>' . $sep;

		// Check if the current page is a category, an archive or a single page. If so show the category or archive name.
		if ( is_category() || is_single() ) {
			the_category( 'title_li=' );
		} elseif ( is_archive() || is_single() ) {
			if ( is_day() ) {
				printf( __( '%s', 'text_domain' ), get_the_date() );
			} elseif ( is_month() ) {
				printf( __( '%s', 'text_domain' ), get_the_date( _x( 'F Y', 'monthly archives date format', 'text_domain' ) ) );
			} elseif ( is_year() ) {
				printf( __( '%s', 'text_domain' ), get_the_date( _x( 'Y', 'yearly archives date format', 'text_domain' ) ) );
			} else {
				_e( 'Blog Archives', 'text_domain' );
			}
		}

		// If the current page is a single post, show its title with the separator
		if ( is_single() ) {
			echo $sep;
			the_title();
		}

		// If the current page is a static page, show its title.
		if ( is_page() ) {
			echo the_title();
		}

		// if you have a static page assigned to be you posts list page. It will find the title of the static page and display it. i.e Home >> Blog
		if ( is_home() ) {
			global $post;
			$page_for_posts_id = get_option( 'page_for_posts' );
			if ( $page_for_posts_id ) {
				$post = get_page( $page_for_posts_id );
				setup_postdata( $post );
				the_title();
				rewind_posts();
			}
		}
		echo '</div>';
	}
}


/* TODO combine these two functions h2/h3 anchor into one function */
function anchor_content_h2( $content ) {

	// Pattern that we want to match
	$pattern = '/<h2>(.*?)</h2>/';

	// now run the pattern and callback function on content
	// and process it through a function that replaces the title with an id
	$content = preg_replace_callback( preg_quote( $pattern ), function ( $matches ) {
		$title = $matches[1];
		$slug  = sanitize_title_with_dashes( $title );
		echo $slug;
		echo $title;

		return '<h2 id="' . $slug . '">' . $title . '</h2>';
	}, $content );


	return $content;
}


function anchor_content_h3( $content ) {

	// Pattern that we want to match
	$pattern = '/<h3>(.*?)</h3>/';

	// now run the pattern and callback function on content
	// and process it through a function that replaces the title with an id
	$content = preg_replace_callback( $pattern, function ( $matches ) {
		$title = $matches[1];
		$slug  = sanitize_title_with_dashes( $title );

		return '<h3 id="' . $slug . '">' . $title . '</h3>';
	}, $content );

	return $content;
}

/* Used to add ids to headings for sidebar */
function add_ids_to_header_tags( $content ) {

	$pattern = '#(?P<full_tag><(?P<tag_name>h\d)(?P<tag_extra>[^>]*)>(?P<tag_contents>\s*.*\s*)</h\d>)#i';
	if ( preg_match_all( $pattern, $content, $matches, PREG_SET_ORDER ) ) {
		$find    = array();
		$replace = array();
		foreach ( $matches as $match ) {
			if ( strlen( $match['tag_extra'] ) && false !== stripos( $match['tag_extra'], 'id=' ) ) {
				$match['tag_extra'] = preg_replace( '#\s(id)="[^"]+"#', '', $match['tag_extra'] );

//				continue;
			}
			$find[]  = $match['full_tag'];
			$id      = sanitize_title( $match['tag_contents'] );
			$id_attr = sprintf( ' id="%s"', formatSlugForId( $id ) );

			$replace[] = sprintf( '<%1$s%2$s%3$s>%4$s</%1$s>', $match['tag_name'], $match['tag_extra'], $id_attr, $match['tag_contents'] );
		}
		$content = str_replace( $find, $replace, $content );
	}

	return $content;
}


function generate_navigation( $HTML ) {
	$DOM = new DOMDocument();
	$DOM->loadHTML( $HTML );

	// Iterating through all elements
	$h2IteratorStatus = 0; //0-closed, 1-open
	$h3IteratorStatus = 0; //0-closed, 1-open
	$navigationArray  = [];
	$lastElement      = '';
	foreach ( $DOM->getElementsByTagName( '*' ) as $element ) {

		if ( $element->tagName == 'h2' && strlen( trim( $element->textContent ) ) > 0 ) {
			$sanatizedTitle                           = sanitize_title( $element->textContent );
			$lastElement                              = $sanatizedTitle;
			$navigationArray[ $lastElement ]['title'] = $element->textContent;


			if ( $h3IteratorStatus ) {
				//it's open, need to close
				$h3IteratorStatus = 0;
			}

			if ( $h2IteratorStatus ) {
				//it's open, need to close
				$h2IteratorStatus = 0;
			}

			$h2IteratorStatus = 1;

		} else if ( $element->tagName == 'h3' && strlen( trim( $element->textContent ) ) > 0 ) {
			$h3Element                                                                     = sanitize_title( $element->textContent );
			$navigationArray[ $lastElement === '' ? 'no-h2' : $lastElement ]['children'][] = [
				'slug'  => $h3Element,
				'title' => $element->textContent
			];

			if ( ! $h3IteratorStatus ) {
				$h3IteratorStatus = 1;
			}
		}
	}

	return $navigationArray;
}


//test code for permalnk custom tags
add_action( 'init', 'sub_category_init', 10 );

function sub_category_init() {
	if ( ! taxonomy_exists( 'sub_category' ) ) {
		register_taxonomy( 'sub_category', 'post',
			array(
				'hierarchical'      => false,
				'label'             => __( 'Sub Category Tag' ),
				'taxonomies'        => array( 'post' ),
				'public'            => false,
				'show_ui'           => true,
				'query_var'         => 'sub_category',
				'show_admin_column' => true,
				'show_in_nav_menus' => true,
				'show_tagcloud'     => false,
				'rewrite'           => true,
				'meta_box_cb'       => false,
			) );
	}
}

// for the buil-in post type post
register_taxonomy_for_object_type( 'sub_category', 'post' );


add_filter( 'rewrite_rules_array', 'remove_bare_folder_rule' );
function remove_bare_folder_rule( $rules ) {
	unset( $rules['([^/]+)/?$'] );

	return $rules;
}

// TODO this should be removed after we re-enable spanish translation plugins
add_filter( 'query_vars', 'custom_query_var_home', 10, 1 );
function custom_query_var_home( $vars ) {
	$vars[] = 'lang';

	return $vars;
}

function generateSidebarQuickLinks( $content ) {
	$content = apply_filters( 'the_content', $content );
	$content = str_replace( ']]>', ']]&gt;', $content );
	$content = htmlspecialchars_decode( utf8_decode( htmlentities( $content, ENT_COMPAT, 'utf-8', false ) ) );

	return $content;
}

function get_post_primary_category( $post_id, $term = 'category', $return_all_categories = false ) {
	$return = array();

	if ( class_exists( 'WPSEO_Primary_Term' ) ) {
		// Show Primary category by Yoast if it is enabled & set
		$wpseo_primary_term = new WPSEO_Primary_Term( $term, $post_id );
		$primary_term       = get_term( $wpseo_primary_term->get_primary_term() );

		if ( ! is_wp_error( $primary_term ) ) {
			$return['primary_category'] = $primary_term;
		}
	}

	if ( empty( $return['primary_category'] ) || $return_all_categories ) {
		$categories_list = get_the_terms( $post_id, $term );

		if ( empty( $return['primary_category'] ) && ! empty( $categories_list ) ) {
			$return['primary_category'] = $categories_list[0];  //get the first category
		}
		if ( $return_all_categories ) {
			$return['all_categories'] = array();

			if ( ! empty( $categories_list ) ) {
				foreach ( $categories_list as &$category ) {
					$return['all_categories'][] = $category->term_id;
				}
			}
		}
	}

	return $return;
}