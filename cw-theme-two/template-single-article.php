<?php
/*
  * Template Name: Single Article Template
  */

get_header( 'dark-blue' );
add_filter( 'the_content', 'add_ids_to_header_tags' );


?>
<div class="single-article-template">

	<?php while ( have_posts() ) : the_post(); ?>
		<?php
		$content      = apply_filters( 'the_content', get_the_content() );
		$content      = str_replace( ']]>', ']]&gt;', $content );
		$content      = htmlspecialchars_decode( utf8_decode( htmlentities( $content, ENT_COMPAT, 'utf-8', false ) ) );
		$sidebarArray = generate_navigation( $content );

		ob_start(); ?>

        <div class="quick-link-block">
			<?php
			$i = 0;
			foreach ( $sidebarArray as $h2Slug => $h3Array ) {
				$i ++;

				if ( empty( $h2Slug ) ) {
					continue;
				}

				if ( count( $h3Array['children'] ) === 0 ) :
					printf( '<h3><a href="#%s">%s</a></h3>', $h2Slug, $h3Array['title'] );
				else :
					printf( '<h3><a role="button" data-toggle="collapse"  class="sidebar-toggle"
                                                href="#position-%s" aria-expanded="false">%s <span class="glyphicon glyphicon-chevron-right"></span></a></h3>', $i, $h3Array['title'] );
					?>
                    <div class="collapse" id="position-<?= $i; ?>">
                        <ul>
							<?php foreach ( $h3Array['children'] as $h3 ) :
								if ( empty( $h3 ) ) {
									continue;
								}

								printf( '<li><a href="#%s">%s</a></li>', $h3['slug'], $h3['title'] );
							endforeach; ?>
                        </ul>
                    </div>
				<?php endif; ?>
			<?php } ?>
        </div>

		<?php $sidebar = ob_get_clean(); ?>
        <section class="content-wrapper">
            <div class="container">
	            <?= getBreadcrumbs(); ?>

                <div class="row">
                    <div class="col-md-4 col-lg-3">
                        <div class="article-quick-links ">
                            <div class="sidebar-opener visible-sm visible-xs" data-toggle="collapse" data-target="#blog-sidebar" aria-expanded="false" aria-controls="blog-sidebar">
                                Article Outline
                            </div>
                            <div id="blog-sidebar" class="collapse" aria-expanded="true">
                                <div class="quick-link-block">
									<?php
									$i = 0;
									foreach ( $sidebarArray as $h2Slug => $h3Array ) {
										$i ++;

										if ( empty( $h2Slug ) ) {
											continue;
										}

										if ( count( $h3Array['children'] ) === 0 ) :
											printf( '<h3><a href="#%s">%s</a></h3>', $h2Slug, $h3Array['title'] );
										else :
											printf( '<h3><a role="button" data-toggle="collapse"  class="sidebar-toggle"
                                                href="#position-%s" aria-expanded="false">%s <span class="glyphicon glyphicon-chevron-right"></span></a></h3>', $i, $h3Array['title'] );
											?>
                                            <div class="collapse" id="position-<?= $i; ?>">
                                                <ul>
													<?php foreach ( $h3Array['children'] as $h3 ) :
														if ( empty( $h3 ) ) {
															continue;
														}

														printf( '<li><a href="#%s">%s</a></li>', $h3['slug'], $h3['title'] );
													endforeach; ?>
                                                </ul>
                                            </div>
										<?php endif; ?>
									<?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-8 col-lg-9">
                        <article>
                            <h1><?php the_title(); ?></h1>
                            <div class="article-container">
								<?php the_content(); ?>

                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </section>
	<?php endwhile; ?>
</div>

<?php get_footer( 'dark-blue' ); ?>
