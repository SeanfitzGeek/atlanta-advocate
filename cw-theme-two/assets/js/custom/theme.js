$(document).ready(function () {

    var Gibbons = (function () {

        var sameHeight = function () {


            $('.sameHeight').matchHeight();
            $('#areas-main .col .item header').matchHeight();

            $('#how-help .col .item header label').matchHeight();
            $('#how-help .col .item .content p').matchHeight();

            var slider_verdicts = $("#slider-verdicts");

            if (slider_verdicts.length) {
                slider_verdicts.owlCarousel({
                    singleItem: true,
                    pagination: false,
                    navigation: true,
                });
            }

        };

        var faqs = function () {

            $('.qa-faqs h3 a').on('click', function (event) {
                event.preventDefault();
                $(this).stop().toggleClass('active');
                $(this).parent().toggleClass('open');

            });

        };

        return {
            init: function () {
                sameHeight();
                faqs();
            }
        }

    })();

    Gibbons.init();

});