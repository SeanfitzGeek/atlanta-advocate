$(document).ready(function () {
    function initMap() {
        console.log('init map');
        var selector = jQuery('.map-box');

        jQuery.each(selector, function (k, v) {
            var lat = jQuery(selector[k]).data('lat');
            var long = jQuery(selector[k]).data('long');
            var myLatLng = {lat: parseFloat(lat), lng: parseFloat(long)};

            var map = new google.maps.Map(selector[k], {
                zoom: 14,
                center: myLatLng
            });

            var marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
            });
        });
    }

    jQuery.getScript("https://maps.googleapis.com/maps/api/js?key=AIzaSyDRlV9FItZ3Hqk2OSfJNlXpJGQL3AhDAFQ").done(function (script, textStatus) {
        if (typeof google != "undefined") {
            initMap();
        }
    });

});
    //
// jQuery(window).on('scroll', function () {
//
// });
