$(document).ready(function () {
    var locked = false;

    $('.panel-main').on('shown.bs.collapse', function (t) {
        if ($(t.target).hasClass('practice-listing')) {
            return;
        }

        if (!locked) {

            $(this).addClass('panel-open');
            locked = true;
        }

        locked = false;

        let dropdownId = $(this).attr('id').replace('-opener','');

        let b = document.querySelector('#' + dropdownId);
        let headerHeight = $('.navbar-fixed-top').outerHeight();
            $('html,body').animate({
                scrollTop: $(b).offset().top - headerHeight
            }, 50);
            window.location.hash = dropdownId;

    });

    $('.panel-main').on('hide.bs.collapse', function (t) {
        $(this).removeClass('panel-open');
    });

    $('.data-dropdown-opener-list a.dropdown-opener').click(function (e) {
        e.preventDefault();
        $('.panel-collapse.in').collapse('hide');

        let dropdownId = $(this).attr('data-opener');

        $(dropdownId).collapse('show');

    });

    let hash = window.location.hash;
    if (hash) {
        // console.log('we have a hash: ' + hash);
        $(hash).collapse('show');
        // $(hash).click();
        // var b = document.querySelector(hash + '-opener');
        // window.scrollTo(b.getBoundingClientRect().x, b.getBoundingClientRect().y - 100);
        // Fragment exists
    }
});