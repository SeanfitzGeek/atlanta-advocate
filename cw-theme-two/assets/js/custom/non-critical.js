$(document).ready(function () {
    $('.btn-chat').click(function (e) {
        e.preventDefault();

        $('#fd-chat-tab').click();
    });

    var btn_search = document.getElementById('btn-new-search');
    if (btn_search) {
        btn_search.onclick = function (e) {
            e.preventDefault();
            document.getElementById('search-form').classList.toggle('form-hidden');
        }
    }
    if (typeof jsSocials != "undefined") {

        $("#share").jsSocials({
            shares: [
                {
                    share: "twitter",           // name of share
                    label: "",             // share button text (optional)
                    logo: "/wp-content/themes/cw-theme-two/assets/images/social/twitter.png",
                },
                {
                    share: "linkedin",           // name of share
                    logo: "/wp-content/themes/cw-theme-two/assets/images/social/link.png",
                    label: "",             // share button text (optional)

                },
                {
                    share: "email",           // name of share
                    label: "",             // share button text (optional)
                    logo: "/wp-content/themes/cw-theme-two/assets/images/social/mail.png",
                    to: "",

                },
                {
                    share: "facebook",           // name of share
                    label: "",             // share button text (optional)
                    logo: "/wp-content/themes/cw-theme-two/assets/images/social/facebook.png",
                },
            ],
        });
    }

    var slider_verdicts = $("#slider-verdicts");

    if (slider_verdicts.length) {
        slider_verdicts.owlCarousel({
            singleItem: true,
            pagination: false,
            navigation: true,
        });
    }

    var sliderFeaturedPosts = $("#featured-posts");

    if (sliderFeaturedPosts.length) {
        sliderFeaturedPosts.owlCarousel({
            singleItem: true,
            // pagination: false,
            navigation: true,
        });

        // Custom Navigation Events
        $(".prev").click(function () {
            sliderFeaturedPosts.trigger('owl.prev');
        });
        $(".next").click(function () {
            sliderFeaturedPosts.trigger('owl.next');
        });
    }

    $.fn.isInViewport = function () {
        var elementTop = $(this).offset().top;
        var elementBottom = elementTop + $(this).outerHeight();

        var viewportTop = $(window).scrollTop();
        var viewportBottom = viewportTop + $(window).height();

        return elementBottom > viewportTop && elementTop < viewportBottom;
    };

    var map_home;

    var mapLoaded = false;
    var videosLoaded = false;

    // $(window).on('scroll', function () {
    //     if (mapLoaded === false) {
    //         if ($('.map-home').isInViewport()) {
    //             mapLoaded = true;
    //             $('.map-home').each(function () {
    //                 var map_ = $(this).attr('id');
    //                 mapInitializeHome(map_);
    //             });
    //             $(window).off('scroll');
    //         }
    //     }
    // load gmaps and fire map functions
    // jQuery.getScript("https://maps.googleapis.com/maps/api/js?key=AIzaSyDRlV9FItZ3Hqk2OSfJNlXpJGQL3AhDAFQ", function () {
    //     function mapInitialize(map_) {
    //         var coords_ = $('#' + map_).data('coords');
    //
    //         if (coords_) {
    //             var tmpCoords = coords_.split(',');
    //             var latitude = tmpCoords[0];
    //             var longtitude = tmpCoords[1];
    //
    //             var latlng = new google.maps.LatLng(latitude, longtitude);
    //
    //             var myOptions = {
    //                 zoom: 16,
    //                 center: latlng,
    //                 disableDefaultUI: true,
    //                 mapTypeId: google.maps.MapTypeId.ROADMAP
    //             };
    //
    //             var map = new google.maps.Map(document.getElementById(map_), myOptions);
    //
    //             var marker = new google.maps.Marker({
    //                 position: latlng,
    //                 map: map
    //             });
    //         }
    //     };
    //
    //     function mapInitializeHome(map_) {
    //         var coords_ = $('#' + map_).data('coords');
    //
    //         if (coords_) {
    //             var latitude = coords_[0][0];
    //             var longtitude = coords_[0][1];
    //             var latlng = new google.maps.LatLng(latitude, longtitude);
    //
    //             var myOptions = {
    //                 zoom: 16,
    //                 center: latlng,
    //                 disableDefaultUI: true,
    //                 mapTypeId: google.maps.MapTypeId.ROADMAP
    //             };
    //
    //             map_home = new google.maps.Map(document.getElementById(map_), myOptions);
    //
    //             $.each(coords_, function (index, value) {
    //                 var latitude = value[0];
    //                 var longtitude = value[1];
    //                 var latlng = new google.maps.LatLng(latitude, longtitude);
    //
    //                 var marker = new google.maps.Marker({
    //                     position: latlng,
    //                     icon: '/wp-content/themes/cw-theme-two/images/icon-pointer-alt.png',
    //                     map: map_home
    //                 });
    //             });
    //         }
    //     };
    // });

    // $('.map, .map-hold').each(function () {
    //     var map_ = $(this).attr('id');
    //     mapInitialize(map_);
    // });
    // });

    $(window).on('scroll', function () {
        if (!mapLoaded) {
            if ($('#map-home').isInViewport()) {
                mapLoaded = true;
                // $(window).off('scroll');

                jQuery.getScript("https://maps.googleapis.com/maps/api/js?key=AIzaSyDRlV9FItZ3Hqk2OSfJNlXpJGQL3AhDAFQ").done(function (script, textStatus) {
                    function mapInitialize(map_) {
                        var coords_ = $('#' + map_).data('coords');

                        if (coords_) {
                            var tmpCoords = coords_.split(',');
                            var latitude = tmpCoords[0];
                            var longtitude = tmpCoords[1];

                            var latlng = new google.maps.LatLng(latitude, longtitude);

                            var myOptions = {
                                zoom: 15,
                                center: latlng,
                                disableDefaultUI: true,
                                mapTypeId: google.maps.MapTypeId.ROADMAP
                            };

                            var map = new google.maps.Map(document.getElementById(map_), myOptions);

                            var marker = new google.maps.Marker({
                                position: latlng,
                                map: map
                            });
                        }
                    };

                    function mapInitializeHome(map_) {
                        var coords_ = $('#' + map_).data('coords');

                        if (coords_) {
                            var latitude = coords_[0][0];
                            var longtitude = coords_[0][1];
                            var latlng = new google.maps.LatLng(latitude, longtitude);

                            var myOptions = {
                                zoom: 15,
                                center: latlng,
                                disableDefaultUI: true,
                                mapTypeId: google.maps.MapTypeId.ROADMAP
                            };

                            map_home = new google.maps.Map(document.getElementById(map_), myOptions);

                            $.each(coords_, function (index, value) {
                                var latitude = value[0];
                                var longtitude = value[1];
                                var latlng = new google.maps.LatLng(latitude, longtitude);

                                var marker = new google.maps.Marker({
                                    position: latlng,
                                    icon: '/wp-content/themes/cw-theme-two/images/icon-pointer-alt.png',
                                    map: map_home
                                });
                            });
                        }
                    };

                    $('.map, .map-hold').each(function () {
                        var map_ = $(this).attr('id');
                        mapInitialize(map_);
                    });

                    $('.map-home').each(function () {
                        var map_ = $(this).attr('id');
                        mapInitializeHome(map_);
                    });
                });
            }
        }
    });


    $(".accordion-contacts dt a").click(function (e) {
        var coords_ = $(this).data("coords");

        if (coords_) {
            var tmpCoords = coords_.split(',');
            var latitude = tmpCoords[0];
            var longtitude = tmpCoords[1];
            var latlng = new google.maps.LatLng(latitude, longtitude);
            map_home.setCenter(latlng);
        }
    });

    var slider_results = $("#slider-results");

    slider_results.owlCarousel({
        items: 6,
        itemsDesktop: [1200, 4],
        itemsDesktopSmall: [992, 3],
        itemsMobile: [767, 1],
        pagination: false,
        navigation: true
    });

    var slider_team = $("#slider-team");

    slider_team.owlCarousel({
        items: 3,
        itemsDesktop: [1200, 3],
        itemsDesktopSmall: [992, 2],
        itemsMobile: [767, 1],
        pagination: false,
        navigation: true,
        autoWidth: false
    });

    var slider_awards = $("#slider-awards");

    slider_awards.owlCarousel({
        items: 5,
        itemsDesktop: [1200, 4],
        itemsDesktopSmall: [992, 3],
        itemsMobile: [767, 1],
        pagination: false,
        navigation: true
    });

    var slider_verdicts = $("#slider-verdicts");

    if (slider_awards.length) {
        slider_verdicts.owlCarousel({
            singleItem: true,
            pagination: false,
            navigation: true,
        });
    }

    var attorney_slider = $("#attorney_slider");
    if (attorney_slider.length) {
        attorney_slider.owlCarousel({
            singleItem: true,
            pagination: false,
            navigation: true
        });
    }

    var TIMEOUT = 300;
    // $('body').wrapInner('<div class="page-inner-area"/>');
    // $('body > .page-inner-area').wrapInner('<div id="wrapper"/>');
    // $('<span class="fader"/>').appendTo('.page-inner-area').css('opacity', 0);


    $('.fader').click(function () {
        $('body').removeClass('open-menu');
        $('.fader').css('display', 'none').animate({
            opacity: 0,
            right: 0
        }, TIMEOUT, function () {
            $(this).css('display', 'none')
        });
        $('#wrapper').animate({right: 0}, TIMEOUT);
        $('.single-post').css('display', 'block');
    });


    // $('.no-backgroundsize header, .no-backgroundsize .with-bg').each(function (e) {
    //     var bg_ = $(this).find('>img').attr('src');
    //     $(this).find('>img').hide();
    //     $(this).css({
    //         "filter": "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" + bg_ + "', sizingMethod='scale')",
    //         "-ms-filter": "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" + bg_ + "', sizingMethod='scale')"
    //     });
    // });


    // Single Blog Sidebar fixed Scroll
    var sidebar = $("#blog-sidebar");
    var siteHeader = $('.site-header-dark-blue');
    if (siteHeader.length) {
        var siteFooter = $('.dark-blue-footer');
        $(window).scroll(function () {
            var scroll = $(window).scrollTop();
            if (siteHeader.isInViewport() || scroll < 349) {
                sidebar.removeClass("fixed-sidebar");
            } else if (scroll >= 350) {
                sidebar.addClass('fixed-sidebar');
            }
        });
    }


    $('#post-category-dropdowns').on('show.bs.collapse', function (t) {
        console.log('firing show..');

        $('.category-information-dropdown.panel-collapse').collapse('hide');
        $('.category-information-action').not('.item-active').addClass('item-inactive');
    });

    $('#post-category-dropdowns').on('shown.bs.collapse', function () {
        console.log('firing shown..');
        $('.category-information-action').not('.item-active').collapse('hide');


    });

    $('.category-information-action').click(function (e) {
        $('.category-information-action').removeClass('item-active');
        $('.category-information-action').removeClass('item-inactive');
        $(this).addClass('item-active');
    });


    $('.back-to-top a, .panel-top').click(function () {
        $('.collapse').collapse('hide');
    });

    $('.trigger-on-hover').hover(function () {
        $(this).toggleClass('visible-block');
    }, function () {
        $(this).toggleClass('visible-block');
    });

    if (typeof Glide != "undefined") {
        var glide_legal_team = new Glide('.glide-legal-team', {
            type: 'slider',
            // startAt: 2,
            perView: 4,
            focusAt: 0,
            gap: 0,
            breakpoints: {
                991: {
                    perView: 1
                }
            }
        }).mount();

        var glide_staff = new Glide('.glide-staff', {
            type: 'slider',
            startAt: 0,
            perView: 4,
            focusAt: 0,
            gap: 0,
            breakpoints: {
                991: {
                    perView: 1
                }
            }
        }).mount();


        var glide_testimonials = new Glide('.glide-testimonials', {
            type: 'carousel',
            startAt: 0,
            perView: 3,
            focusAt: 0,
            gap: 0,
            breakpoints: {
                991: {
                    perView: 1
                }
            }
        }).mount();
    }

    var maxHeight = '200px';
    var why_choose_content = $('.why-choose .content').map(function () {
        return $(this).outerHeight();
    }).get();

    maxHeight = Math.max.apply(null, why_choose_content);
    $('.why-choose-banner').css('min-height', maxHeight);

    var quickLinkOffset = $('.add-affix-left');
    var headerHeight = $('.site-header-dark-blue').outerHeight();

    console.log(headerHeight);
    $('#wrapper').css('padding-top', headerHeight);

    if ($('#wpadminbar').length) {
        headerHeight = headerHeight + $('#wpadminbar').outerHeight();
    }

    if ($('.add-affix-left').length) {
        quickLinkOffset = quickLinkOffset.offset();
        quickLinkOffset = quickLinkOffset.top - headerHeight;

        $('.add-affix-left').affix({
            offset: {
                top: function () {
                    // return (950)
                    return (quickLinkOffset)
                },
                bottom: function () {
                    return (this.bottom = $('.footer').outerHeight(true))
                }
            }
        });
    }

    $('.affix-header').affix({
        offset: {
            top: function () {
                // return (950)
                return (headerHeight)
            },
            bottom: function () {
                return (this.bottom = $('.footer').outerHeight(true))
            }
        }
    });


    $('.quick-link-block').each(function (e) {
        $(this).css('top', headerHeight);
    });


    // Set Wrapper Padding due to fixed header
});

jQuery('.btn-user-response').click(function (e) {
    var userQuestion = jQuery('.user-question-response').val();
    jQuery('input[name="your-question"]').val(userQuestion);

    jQuery('.question-input').hide(300, function () {
        jQuery('.question-form-fields').show(200);
    });
});

jQuery('.btn-user-response').click(function (e) {
    var userQuestion = jQuery('.user-question-response').val();
    jQuery('input[name="your-question"]').val(userQuestion);

    jQuery('.question-input').hide(300, function () {
        jQuery('.question-form-fields').show(200);
    });
});

$('body').on('click', '.init-scroll-to', function (e) {
    e.preventDefault();
    var scrollId = $(this).data('scroll-to');
    scrollToId(scrollId);
});

var hash = window.location.hash;

if (hash) {
    var err = 0;

    try {
        scrollToId(hash);
        err = 0;
    } catch (err) {
        console.log('error_loading_hash' + hash);
        err = 1;
    }

    if (err === 1) {

    }
}

function scrollToId(scrollToId) {
    var b = document.querySelector(scrollToId);
    var headerHeight = $('.site-header-dark-blue').outerHeight();
    var headingHeight = $(scrollToId).outerHeight();

    $('html,body').animate({
        scrollTop: $(b).offset().top - headerHeight - headingHeight
    }, 50);
    window.location.hash = scrollToId;
}

function scrollToName(scrollToId) {
    var b = document.getElementsByName(scrollToId)[0];
    var headerHeight = $('.site-header-dark-blue').outerHeight();
    var headingHeight = $(scrollToId).outerHeight();

    if (b) {
        $('html,body').animate({
            scrollTop: $(b).offset().top - headerHeight - headingHeight
        }, 50);
        window.location.hash = scrollToId;
    }
}

function toggleIcon(e) {
    $(e.target)
        .prev('.panel-heading')
        .find('.more-less')
        .toggleClass('glyphicon-plus glyphicon-minus');
}

$('.panel-group').on('hidden.bs.collapse', toggleIcon);
$('.panel-group').on('shown.bs.collapse', toggleIcon);

window.onresize = function () {
    var headerHeight = $('.site-header-dark-blue').outerHeight();
    // if ($('#wpadminbar').length) {
    //     headerHeight = headerHeight + $('#wpadminbar').outerHeight();
    // }
    console.log(headerHeight);
    $('#wrapper').css('padding-top', headerHeight);
};
