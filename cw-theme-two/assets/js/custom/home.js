$(document).ready(function () {
    // Opens Chat from button with .btn-chat class
    $('.btn-chat').click(function (e) {
        e.preventDefault();

        $('#fd-chat-tab').click();
    });
    var $sec = $("section");

// On any of both arrows click
    $(".prev-section a, .next-section a").click(function (e) {
        e.preventDefault();
        // We need to get current element
        // before defining the `.next()` or `.prev()` element to target
        // and get it's `offset().top` into an `y` variable we'll animate to.
        // A current element is always the one which bottom position
        // (relative to the browser top) is higher than 0.
        var y = $sec.filter(function (i, el) {
            return el.getBoundingClientRect().bottom > 0;
        })[$(this).parent().hasClass("next-section") ? "next" : "prev"]("section").offset().top;
        // (Line above:) if the clicked button className was `"next"`,
        // target the the `.next("section")`, else target the `.prev("section")`
        // and retrieve it's `.offset().top`

        $("html, body").stop().animate({scrollTop: y}, 50);

    });

    var scrollTo = 0;
    $('body').on('click', ".scroll-container a", function () {
        var activeItem = $('li.active');
        var selectedItem = $(this).parent();

        var activeIndex = $('li').index(activeItem);
        var selectedIndex = $('li').index(selectedItem);

        scrollTo = -selectedItem.position().left + $('.scroll-container').width() / 2 - selectedItem.width() / 2;

        $('ul').css('transform', 'translateX(' + scrollTo + 'px)');
        activeItem.removeClass('active');
        selectedItem.addClass('active');
    });


    var glide = new Glide('.glide', {
        type: 'carousel',
        startAt: 0,
        perView: 3,
        focusAt: 'center',
        gap: 0,
        breakpoints: {
            1200: {
                perView: 2
            },
            850: {
                perView: 1
            }
        }
    }).mount();

    var glide_practice = new Glide('.glide-practice-areas', {
        type: 'carousel',
        startAt: 0,
        perView: 3,
        focusAt: 'center',
        gap: 0,
        breakpoints: {
            991: {
                perView: 1
            }
        }
    }).mount();

    var glide_legal_team = new Glide('.glide-legal-team', {
        type: 'carousel',
        startAt: 0,
        perView: 3,
        focusAt: 'center',
        gap: 0,
        breakpoints: {
            991: {
                perView: 1
            }
        }
    }).mount();

    jQuery('#videomodal').on('hidden.bs.modal', function () {
        jQuery("#videomodal iframe").attr("src", jQuery("#videomodal iframe").attr("src"));
    });


    $.fn.isInViewport = function () {
        var elementTop = $(this).offset().top;
        var elementBottom = elementTop + $(this).outerHeight();

        var viewportTop = $(window).scrollTop();
        var viewportBottom = viewportTop + $(window).height();

        return elementBottom > viewportTop && elementTop < viewportBottom;
    };

    var map_home;
    var mapLoaded = false;

    $(window).on('scroll', function () {
        if (!mapLoaded) {
            if ($('#map-home').isInViewport()) {
                mapLoaded = true;
                // $(window).off('scroll');

                jQuery.getScript("https://maps.googleapis.com/maps/api/js?key=AIzaSyDRlV9FItZ3Hqk2OSfJNlXpJGQL3AhDAFQ").done(function (script, textStatus) {
                    function mapInitialize(map_) {
                        var coords_ = $('#' + map_).data('coords');

                        if (coords_) {
                            var tmpCoords = coords_.split(',');
                            var latitude = tmpCoords[0];
                            var longtitude = tmpCoords[1];

                            var latlng = new google.maps.LatLng(latitude, longtitude);

                            var myOptions = {
                                zoom: 16,
                                center: latlng,
                                disableDefaultUI: true,
                                mapTypeId: google.maps.MapTypeId.ROADMAP
                            };

                            var map = new google.maps.Map(document.getElementById(map_), myOptions);

                            var marker = new google.maps.Marker({
                                position: latlng,
                                map: map
                            });
                        }
                    };

                    function mapInitializeHome(map_) {
                        var coords_ = $('#' + map_).data('coords');

                        if (coords_) {
                            var latitude = coords_[0][0];
                            var longtitude = coords_[0][1];
                            var latlng = new google.maps.LatLng(latitude, longtitude);

                            var myOptions = {
                                zoom: 16,
                                center: latlng,
                                disableDefaultUI: true,
                                mapTypeId: google.maps.MapTypeId.ROADMAP
                            };

                            map_home = new google.maps.Map(document.getElementById(map_), myOptions);

                            $.each(coords_, function (index, value) {
                                var latitude = value[0];
                                var longtitude = value[1];
                                var latlng = new google.maps.LatLng(latitude, longtitude);

                                var marker = new google.maps.Marker({
                                    position: latlng,
                                    icon: '/wp-content/themes/cw-theme-two/images/icon-pointer-alt.png',
                                    map: map_home
                                });
                            });
                        }
                    };

                    $('.map, .map-hold').each(function () {
                        var map_ = $(this).attr('id');
                        mapInitialize(map_);
                    });

                    $('.map-home').each(function () {
                        var map_ = $(this).attr('id');
                        mapInitializeHome(map_);
                    });
                });
            }
        }
    });


    $('.accordion > dt > a').on('click', function (e) {
        e.preventDefault();
        var parent = $(this).parents("dt");
        if (!parent.hasClass("accordion-opened")) {
            $(".accordion > dt.accordion-opened").removeClass("accordion-opened");
            parent.addClass("accordion-opened");
            var targetId = $(this).attr("href");
            var target = $(targetId);
            var current = $(".accordion > dd.accordion-opened");
            target.addClass("accordion-opened");
            current.removeClass("accordion-opened");
            current.slideUp(400);
            target.slideDown(400);
        }
    });

    $(".accordion-contacts dt a").click(function (e) {
        var coords_ = $(this).data("coords");

        if (coords_) {
            var tmpCoords = coords_.split(',');
            var latitude = tmpCoords[0];
            var longtitude = tmpCoords[1];
            var latlng = new google.maps.LatLng(latitude, longtitude);
            map_home.setCenter(latlng);
        }
    });

    $('.trigger-on-hover').hover(function () {
        $(this).toggleClass('visible-block');
    }, function () {
        $(this).toggleClass('visible-block');
    });

    // start
    var links = jQuery('.link-directory ul li a');

    jQuery(links).click(function (e) {

        e.preventDefault();
        var $this = jQuery(this);
        jQuery(this).addClass('active');
        var link_id = $this.attr('href');
        var position = $this.position();

        setBorderWidth(position, $this);
        console.log(link_id);
        var b = document.querySelector(link_id);
        jQuery('html,body').animate({
            scrollTop: jQuery(b).offset().top
        }, 50);
        window.location.hash = link_id;
    });

    jQuery(links).hover(function () {
        var $this = jQuery(this);
        var position = $this.position();

        setBorderWidth(position, $this);
    });


    function setBorderWidth(pos, element) {
        jQuery('.dynamic-border-bar').css('width', (pos.left + element.outerWidth()) + 'px');
    }

    // end
});



