$(window).on('load', function () {
    $('.but-mobile').on('click', function () {

        var TIMEOUT = 300;
        if (!$('body').hasClass('open-menu')) {
            $('body').addClass('open-menu');
            $('.fader').css('display', 'block').animate({
                opacity: 1,
                right: '80%'
            }, TIMEOUT);
            $('#wrapper').animate({right: '80%'}, TIMEOUT);
            $('.menu-main').animate({'margin-right': 0}, TIMEOUT);
            //$('.single-post').css('display', 'none');
        } else {
            $('body').removeClass('open-menu');
            $('.fader').css('display', 'none').animate({
                opacity: 0,
                right: 0
            }, TIMEOUT, function () {
                $(this).css('display', 'none');
            });
            $('#wrapper').animate({right: 0}, TIMEOUT);
            $('.menu-main').animate({'margin-right': '-80%'}, TIMEOUT);
            $('.single-post').css('display', 'block');
        }
        ;
        return false;
    });

    $('a[href*=\\#]:not([href=\\#]).scroller').on('click', function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top - 30
                }, 1000);
                return false;
            }
        }
    });

    $('.btn-load-more-ajax a').on('click', function () {
        var btn = $(this);
        var contentTarget = btn.parent().data('target');
        var appendTo = btn.parent().data('append-to');
        var loadMore = btn.parent().data('load-more');
        $.get(btn.attr('href'), function (data) {
            $(appendTo).append($(data).find(contentTarget));
            var nextBtn = $(data).find(loadMore).attr('href');
            if (nextBtn) {
                btn.attr('href', nextBtn);
            } else {
                btn.parent().hide();
            }
        });
        return false;
    });


    //////////////////
    // $('header, .with-bg').each(function (e) {
    //     var bgUrl_ = $(this).find('> img').attr('src');
    //         var bg_ = 'url(' + bgUrl_ + ')';
    //         $(this).find('> img').hide();
    //
    //         if ($(this).hasClass('lazyload')) {
    //             $(this).attr('data-bg', bgUrl_);
    //         } else {
    //             $(this).css('background-image', bg_);
    //             console.log('not found');
    //         }
    // });

    //////////////////


    $(".side-address a.but-hours").on("click", function (e) {
        e.preventDefault();
        var parent = $(this).parents(".side-address");
        var container = parent.find(".popup-hours");
        var delta = parent.find(".address-header").innerHeight();
        var height = container.innerHeight();
        var position = $(this).position();
        var top = position.top + delta;
        container.css("top", top - height - 15);
        container.toggleClass("visible");
        var offsetButton = $(this).offset().left;
        var offsetContainer = container.offset().left;
        container.find(".popup-arrow").css("left", (offsetButton - offsetContainer + $(this).innerWidth() / 2) - 10);
    });

    $("#areas .areas-block .opener").on('click', function () {
        e.preventDefault();
        var container = $(this).parent().find("ul");
        container.toggleClass("opened");
    });

    $(".btn-go-url").on('click', function (e) {
        e.preventDefault();
        var parent = $(this).parent(),
            href = parent.find("select option:selected").val();
        window.location = href;
    });

    $('.accordion > dt > a').on('click', function (e) {
        e.preventDefault();
        var parent = $(this).parents("dt");
        if (!parent.hasClass("accordion-opened")) {
            $(".accordion > dt.accordion-opened").removeClass("accordion-opened");
            parent.addClass("accordion-opened");
            var targetId = $(this).attr("href");
            var target = $(targetId);
            var current = $(".accordion > dd.accordion-opened");
            target.addClass("accordion-opened");
            current.removeClass("accordion-opened");
            current.slideUp(400);
            target.slideDown(400);
        }
    });


    $('.qa-faqs h3 a').on('click', function (event) {
        event.preventDefault();
        $(this).stop().toggleClass('active');
        $(this).parent().toggleClass('open');
        $(this).parent().next('.qa-faq-answer').toggle();
    });


});

