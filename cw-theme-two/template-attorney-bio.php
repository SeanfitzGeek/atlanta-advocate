<?php
/*
  * Template Name: Attorney Bio Template
  */

get_header( 'dark-blue' );
$ctaBg      = get_field( 'cta_attorney_bio_section_image' );
$ctaContent = get_field( 'cta_content' ) ?: get_the_title();

$communityBg = get_field( 'in_community_content_image' );
$ctaAwardImg = get_field( 'cta_awards_image' );

$meetImage     = get_field( 'meet_image' );
$meetContent   = get_field( 'meet_content' );
$credsContent  = get_field( 'lawyer_creds_content' );
$credsBg       = get_field( 'lawyer_creds_bg' );
$attorneyVideo = get_field( 'video_section_media' );
?>

    <div class="about-us">

        <section class="main-banner-section lazyload" data-bg="<?= $ctaBg['url']; ?>">

            <div class="cta-main-left">
                <div class="inner-container">
                    <div class="content">
						<?= $ctaContent; ?>
                    </div>
                </div>
            </div>
        </section>

        <section class="meet-the-law legal-team" id="meet-the-law">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4">
						<?= wp_get_attachment_image( $meetImage['ID'], '', false, [ 'class' => 'img-responsive center-block' ] ); ?>
                    </div>

                    <div class="col-sm-8">
						<?= $meetContent; ?>
                    </div>
                </div>
            </div>
        </section>

        <section class="lawyers-trust section-white">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
						<?= get_field( 'lawyer_trust_content' ); ?>
                    </div>

					<?php
					while ( have_rows( 'trust_badges' ) ): the_row();
						$image = get_sub_field( 'trust_badge' );
						$title = get_sub_field( 'badge_title' );
						?>
                        <div class="col-sm-3">
                            <div class="trust-badge">
                                <img data-src="<?= $image['url']; ?>" class="img-responsive lazyload">

                                <div class="badge-title">
                                    <h4><?= $title; ?></h4>
                                </div>
                            </div>
                        </div>

					<?php endwhile; ?>
                </div>
            </div>
        </section>

		<?php if ( strlen( trim( $credsContent ) ) > 0 ): ?>
            <section class="attorney-credientials lazyload" data-bg="<?= $credsBg['url']; ?>">
                <div class="container">
                    <h2 class="color-white text-center" style="font-size: 42px;
    font-weight: 700; color: #FFF;"><?= get_field( 'lawyer_creds_title' ); ?></h2>

                    <div class="creds-inner">

						<?= $credsContent; ?>
                    </div>
                </div>
            </section>
		<?php endif; ?>

		<?php if ( strlen( trim( $attorneyVideo ) ) > 0 ): ?>
            <section class="attorney-video">
                <div class="video-frame">
					<?= $attorneyVideo; ?>
                </div>
            </section>
		<?php endif; ?>
        <section class="legal-team">
            <h3 class="text-center">Meet Your Legal Team</h3>
            <h5 class="text-center">Award Winning Georgia Personal Injury Lawyers</h5>


            <div class="glide-legal-team">
                <div class="glide__track" data-glide-el="track">
                    <ul class="glide__slides">
						<?php
						while ( have_rows( 'meet_the_lawyers_repeater_fields', 4414 ) ): the_row();
							$name  = get_sub_field( 'lawyer_title' );
							$link  = get_sub_field( 'lawyer_link' );
							$image = get_sub_field( 'lawyer_image' );
							?>
                            <li class="glide__slide">

                                <a href="<?= $link['url']; ?>">
                                    <div class="glide-slide-container lazyload"
                                         data-bg="<?= $image['url']; ?>">
                                        <div class="team-info-container">
                                            <div class="team-name"><?= $name; ?></div>
                                        </div>


                                    </div>

                                    <div class="glide-slide-overlay"></div>
                                </a>
                            </li>
						<?php endwhile; ?>
                    </ul>
                </div>
                <div class="glide__arrows" data-glide-el="controls">
                    <button class="glide__arrow prev" data-glide-dir="<"><span
                                class="glyphicon glyphicon-menu-left"></span>
                    </button>
                    <button class="glide__arrow next" data-glide-dir=">"><span
                                class="glyphicon glyphicon-menu-right"></span></button>
                </div>
            </div>

        </section>


        <section class="meet-our-legal-staff legal-team" id="our-staff">
            <h3 class="text-center">Meet Our Legal Staff</h3>
            <div class="glide-staff">
                <div class="glide__track" data-glide-el="track">
                    <ul class="glide__slides">
						<?php
						while ( have_rows( 'meet_the_staff_repeater_fields', 4414 ) ): the_row();
							$name  = get_sub_field( 'staff_title' );
							$link  = get_sub_field( 'staff_link' );
							$image = get_sub_field( 'staff_image' );
							?>
                            <li class="glide__slide">

								<?php if ( strlen( $link['url'] ) ): ?><a href="<?= $link['url']; ?>"><?php endif; ?>
                                    <div class="glide-slide-container lazyload"
                                         data-bg="<?= $image['url']; ?>">
                                        <div class="team-info-container">
                                            <div class="team-name"><?= $name; ?></div>
                                        </div>


                                    </div>

                                    <!--                                    <div class="glide-slide-overlay"></div>-->
									<?php if ( strlen( $link['url'] ) ): ?></a><?php endif; ?>
                            </li>

						<?php endwhile; ?>
                    </ul>

                    <div class="glide__arrows" data-glide-el="controls">
                        <button class="glide__arrow prev" data-glide-dir="<"><span
                                    class="glyphicon glyphicon-menu-left"></span>
                        </button>
                        <button class="glide__arrow next" data-glide-dir=">"><span
                                    class="glyphicon glyphicon-menu-right"></span></button>
                    </div>
                </div>
            </div>
        </section>

    </div>

    <section class="contact-form lazyload" id="main-contact-form"
             data-bg="<?= get_stylesheet_directory_uri(); ?>/assets/images/contact-us/over-city-skyline-atlanta-ga-downtown-dusk-georgia-PF48CFM.jpg">
        <div class="container">
            <div class="form-box-container">
                <h2 class="">Receive a FREE Case Evaluation</h2>
                <p>*All information submitted is secure & confidential.
                    All submissions will be sent immediately to our attorneys and legal staff, who will reach out to
                    you, free of charge, and advise you on the next steps to take.</p>

				<?= do_shortcode( '[contact-form-7 id="104671" title="Contact Us Form"]' ); ?>

            </div>

        </div>
    </section>

<?php get_footer( 'dark-blue' ); ?>