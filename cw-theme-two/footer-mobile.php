<span class="fader" style="opacity: 0;"></span>
<footer class="text-center color-white dark-blue-bg">
    <div class="footer-copyright">
        &copy; The Millar Law Firm - 2019
    </div>
</footer>

</div> <!-- End Wrapper -->
</div> <!-- End page-inner-area -->

<script type="text/javascript">
    //<![CDATA[
    var wpcf7 = {
        "apiSettings": {
            "root": "https:\/\/atlantaadvocate.com\/wp-json\/contact-form-7\/v1",
            "namespace": "contact-form-7\/v1"
        }
    };
    //]]>
</script>
<?php wp_footer(); ?>
<script>
    jQuery(document).ready(function () {
        $.fn.isInViewport = function () {
            var elementTop = $(this).offset().top;
            var elementBottom = elementTop + $(this).outerHeight();

            var viewportTop = $(window).scrollTop();
            var viewportBottom = viewportTop + $(window).height();

            return elementBottom > viewportTop && elementTop < viewportBottom;
        };
        var contactFormLoaded = false;

        $(window).on('scroll', function () {
            if ($('#map-home').isInViewport()) {
                $(window).off('scroll');

                jQuery.getScript("https://maps.googleapis.com/maps/api/js?key=AIzaSyDRlV9FItZ3Hqk2OSfJNlXpJGQL3AhDAFQ").done(function (script, textStatus) {
                    function mapInitializeFullZoom(map_, zoom) {
                        var coords_ = $('#' + map_).data('coords');

                        if (coords_) {
                            var latitude = coords_[0][0];
                            var longtitude = coords_[0][1];
                            var latlng = new google.maps.LatLng(latitude, longtitude);

                            var myOptions = {
                                zoom: zoom,
                                center: latlng,
                                disableDefaultUI: true,
                                mapTypeId: google.maps.MapTypeId.ROADMAP
                            };

                            map_home = new google.maps.Map(document.getElementById(map_), myOptions);
                            $.each(coords_, function (index, value) {
                                var latitude = value[0];
                                var longtitude = value[1];
                                var latlng = new google.maps.LatLng(latitude, longtitude);

                                var marker = new google.maps.Marker({
                                    position: latlng,
                                    icon: '/wp-content/themes/cw-theme-two/images/icon-pointer-alt.png',
                                    map: map_home
                                });
                            });
                        }
                    };

                    var map_ = $('.section-8-mobile .map-home').attr('id');
                    mapInitializeFullZoom(map_, 9);
                });
            }

        });


        $(window).on('scroll', function () {
            if (contactFormLoaded === false && $('.section-4-mobile').isInViewport()) {
                contactFormLoaded = true;
                var script = document.createElement('script');
                var prefix = document.location.protocol;
                script.async = true;
                script.type = 'text/javascript';
                var target = '/wp-content/plugins/contact-form-7/includes/js/scripts.js';
                script.src = target;
                var elem = document.head;
                elem.appendChild(script);
            }
        });


    });
</script>

<script type="text/javascript">
    _linkedin_partner_id = "302513";
    window._linkedin_data_partner_ids = window._linkedin_data_partner_ids || [];
    window._linkedin_data_partner_ids.push(_linkedin_partner_id);
</script>
<script type="text/javascript">
    (function () {
        var s = document.getElementsByTagName("script")[0];
        var b = document.createElement("script");
        b.type = "text/javascript";
        b.async = true;
        b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";
        s.parentNode.insertBefore(b, s);
    })();
</script>
<noscript>
    <img height="1" width="1" style="display:none;" alt=""
         src="https://px.ads.linkedin.com/collect/?pid=302513&fmt=gif"/>
</noscript>

<script type='application/ld+json'>
    {
        "@context": "http://www.schema.org",
        "@type": "LegalService",
        "name": "The Millar Law Firm",
        "url": "https://atlantaadvocate.com/",
        "logo": "https://atlantaadvocate.com/wp-content/uploads/2018/02/MP-Header_LOGO_090718-300-cut.png",
        "image": "https://atlantaadvocate.com/wp-content/uploads/2019/03/Billboard-BANNER_031719-100.jpg",
        "description": "Personal Injury Law Firm located in the Atlanta, Georgia. ",
        "telephone": "+1-770-212-3795",
        "priceRange": "$000 - $10000",
        "address": {
            "@type": "PostalAddress",
            "streetAddress": "2221 Peachtree Road NE Suite P32",
            "addressLocality": "Atlanta ",
            "addressRegion": "Georgia",
            "postalCode": "30309",
            "addressCountry": "United States "
        },
        "contactPoint": {
            "@type": "ContactPoint",
            "telephone": "+1-(770) 212-3795",
            "contactType": "Customer support"
        }
    }
</script>

<!-- Juvo Lead Tracking tag for atlantaadvocate.com -->
<script type="text/javascript">
    (function () {
        var fd = document.createElement('script');
        fd.type = 'text/javascript';
        fd.async = true;
        fd.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.juvoleads.com/tag/926453398.js?v=' + Math.floor(Math.random() * 9999999999);
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(fd, s);
    })();
</script>

</body>
</html>