<?php
/*
  * Template Name: Mobile Home Template
  */

get_header( 'mobile' );
$attorneys = get_field( 'team_attorneys' );
?>
    <!-- Mobile Section #1 -->
    <section class="section-1-mobile">
        <h1 class="color-blue text-center">
            MAIN PRACTICE AREAS
        </h1>

        <div class="practice-area-listing">
            <div class="row">
				<?php
				while ( have_rows( 'main_practice_areas', 4611 ) ): the_row();
					$title   = get_sub_field( 'practice_title' );
					$urlObj  = get_sub_field( 'url' );
					$iconObj = get_sub_field( 'practice_icon' );
					// TODO grab alt text for image icons from this img obj
//									$imgAlt = get_post_meta($iconObj->ID, '_wp_attachment_image_alt', true );
					?>
                    <div class="col-xs-6">
                        <div class="practice-listing-container">
                            <a href="<?= $urlObj['url']; ?>">
                                <div class="img-container">
                                    <img data-src="<?= $iconObj['url']; ?>" alt="<?= $iconObj['alt']; ?>"
                                         class="lazyload img-responsive text-center">
                                </div>
                                <div class="practice-area-listing-title"><?= $title; ?></div>
                            </a>
                        </div>
                    </div>
				<?php endwhile; ?>
            </div>
        </div>

        <p class="text-center marg-top-10">
            <a href="/legal-services/" class="bold">Click here for more Practice Areas</a>
        </p>
    </section>
    <!-- End Mobile Section #1 -->

    <!-- Mobile Section #2 -->
    <section class="section-2-mobile dark-blue-bg">
        <div class="container">
            <h2 class="text-center color-white">MEET YOUR<br>
                ATLANTA ADVOCATES</h2>

            <div class="row">
                <div class="col-sm-12 attorney">
                    <img data-src="<?= get_stylesheet_directory_uri(); ?>/assets/images/Bruce-Millar.jpg"
                         alt="Image of Bruce Millar of Atlanta Advocates"
                         class="img-responsive lazyload">
                    <div class="attorney-info">
                        <span class="attorney-name">Bruce R. Millar, Esq.</span>
                        <span class="attorney-title">Senior Partner, Trial Lawyer</span>
                        <p> Mr. Millar is a graduate of the Emory University School of Law. In over 26 years of practice
                            the Millar Law Firm has recovered more than $100,000,000 for clients. <a
                                    href="https://atlantaadvocate.com/about-us/bruce-millar/">(...read more)</a>
                        </p>
                    </div>
                </div>

                <div class="col-sm-12 attorney">
                    <img data-src="<?= get_stylesheet_directory_uri(); ?>/assets/images/Anthony-Jones.jpg"
                         alt="Image of Anthony Jones of Atlanta Advocates"
                         class="img-responsive lazyload">
                    <div class="attorney-info">
                        <span class="attorney-name">Anthony Jones, II, Esq.</span>
                        <span class="attorney-title">Trial Lawyer</span>
                        <p>
                            Mr. Jones is The Millar Law Firm’s senior litigation Associate attorney. He has tried dozens
                            of personal injury cases to verdict with top results. Anthony brings experience, passion and
                            success to his work. <a href="https://atlantaadvocate.com/about-us/anthony-jones/">(...read
                                more)</a>
                        </p>
                    </div>
                </div>

                <div class="col-sm-12 attorney">
                    <img data-src="<?= get_stylesheet_directory_uri(); ?>/assets/images/Ivory-Roberson.jpg"
                         alt="Image of Ivory Roberson of Atlanta Advocates"
                         class="img-responsive lazyload">
                    <div class="attorney-info">
                        <span class="attorney-name">Ivory Roberson, Esq.</span>
                        <span class="attorney-title">Trial Lawyer</span>
                        <p>
                            Ivory Roverson is a battle tested attorney. She specializes in accident, premises liability
                            and dog bite cases. In addition to trying and settling injury cases, Ivory manages our
                            firm’s case intake.
                            <a href="https://atlantaadvocate.com/about-us/ivory-roberson/">(...read more)</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Mobile Section #2 -->

    <!-- Mobile Section #3 -->
    <section class="section-3-mobile light-blue-bg color-white">
        <div class="container">
            <h2 class="text-center">
                EXPERIENCE & EXCELLENCE
                <br>
                is our mission.
            </h2>
            <div class="">
                <div class="number-block">1</div>
                <p><span class="bold">The Millar Law Firm has one simple goal:</span> to give you fast and helpful legal
                    advice, and get you the best
                    possible settlement or award in your Georgia injury case.</p>
            </div>

            <div class="">
                <div class="number-block">2</div>
                <p>
                    <span class="bold">The Millar Law Firm has been specializing in injury law for almost 30 years.</span>
                    Our entire staff is trained
                    to handle the special concerns you face when injured.</p>
            </div>

            <div class="">
                <div class="number-block">3</div>
                <p><span class="bold">Most of our cases come from repeat referrals</span> from other attorneys, and
                    former satisfied clients. This is
                    a real vote of confidence in our strength and success rate.</p>
            </div>

            <div class="">
                <div class="number-block">4</div>
                <p><span class="bold">Our lawyers focus continuously on improving their legal skills.</span> We invest
                    in the latest technology and
                    continuously update our team’s legal education.</p>
            </div>
        </div>
    </section>
    <!-- End Mobile Section #3 -->

    <section class="section-4-mobile">
        <div class="container">
            <div class="call-cta text-center">
                <a href="tel:770-400-0000" class="btn-red color-white">
                    <strong>CALL NOW</strong><br>
                    FOR A FREE CASE EVALUATION
                </a>
            </div>

			<?php
			$title   = '<strong>OR SUBMIT YOUR INFO</strong><br> to have an experienced attorney call you back.';
			$caption = '*All information submitted is secure & confidential.';
			$notice  = 'All submissions will be quickly reviewed by one of our experienced attorneys, who will reach 
			out to you, free of charge, and advise you on the next steps to take';
			echo getGlobalSiteForm( $title, $caption, $notice ); ?>
        </div>
    </section>

    <section class="section-5-mobile red-bg">
        <div class="container">
            <img data-src="<?= get_stylesheet_directory_uri(); ?>/assets/images/stars.png" style="max-width: 170px;"
                 class="img-responsive center-block lazyload" alt="Review Rating">
            <h2 class="text-center">
                Check Out Our<br>
                Client Reviews & Testimonials
            </h2>

            <div class="testimonial-content">
                <div class="quote-block">
                    <img data-src="<?= get_stylesheet_directory_uri(); ?>/assets/images/quote-icon.png" alt="Quote Icon"
                         class="img-responsive center-block lazyload">
                </div>
                <div class="text-center">
                    <img data-src="<?= get_stylesheet_directory_uri(); ?>/images/cassandra-gilliam.jpg"
                         alt="Atlanta Advocate review from client"
                         class="lazyload review-image">
                </div>
                <p class="text-center">
                    I was completely satisfied with the service I received from the Millar Law Firm. Everyone answered
                    my questions and explained everything thoroughly. My calls were always returned in a timely manner.
                    The attorneys would often rotate calls to me to just to check up and see how I was doing and asked
                    if I need anything.
                    <br>
                    <small><i>- Cassandra G. former client</i></small>
                </p>
                <div class="quote-block">
                    <img data-src="<?= get_stylesheet_directory_uri(); ?>/assets/images/quote-icon.png" alt="Quote Icon"
                         class="img-responsive center-block lazyload">
                </div>
            </div>
        </div>
    </section>

    <section class="section-6-mobile dark-blue-bg">
        <div class="container">
            <h2 class="text-center color-white"><strong>OUR CASE RESULTS</strong>
                <br>
                speak for themselves:</h2>

            <div class="case-result">
                <h4 class="color-red">
                    $25 MILLION
                </h4>
                <p><span>Judgment for a client who was seriously injured in a parking-lot collision.</span></p>
            </div>

            <div class="case-result">
                <h4 class="color-red">
                    $7 MILLION
                </h4>
                <p><span> Verdict against a former police officer who shot a citizen.</span></p>
            </div>

            <div class="case-result">
                <h4 class="color-red">
                    $1.9 MILLION
                </h4>
                <p><span>A structured settlement for the children of a man killed due to negligent security.</span></p>
            </div>
            <div class="text-center">
                <a href="/case-results/" class="bold color-teal"><i>Click here for more Case Results</i></a>
            </div>
        </div>
    </section>

    <section class="section-7-mobile dark-blue-bg">
        <div class="container">
            <h2 class="text-center">
                SERVING THE ENTIRE
                METRO ATLANTA AREA
            </h2>
            <div class="call-cta text-center">
                <a href="tel:770-400-0000" class="btn-red color-white">
                    <span class="bold">CALL NOW</span>
                    <br>
                    FOR A FREE CASE EVALUATION</a>
            </div>
        </div>
    </section>

<?php $args = array(
	'post_type'      => 'wpseo_locations',
	'post_status'    => 'publish',
	'posts_per_page' => - 1,
	'orderby'        => 'ID',
	'order'          => 'ASC'
);
$query      = new WP_Query( $args );
if ( $query->have_posts() ) : ?>
    <section id="contacts" class="map-section section-8-mobile lazyload"
             data-bg="<?= get_stylesheet_directory_uri(); ?>/assets/images/static-atl-map.jpg">
		<?php $coords = array();
		$count        = 0;
		while ( $query->have_posts() ) : $query->the_post();
			$coords[] = array( get_field( '_wpseo_coordinates_lat' ) => get_field( '_wpseo_coordinates_long' ) );
			$count ++; endwhile;

		$output = '';
		foreach ( $coords as $coord ):
			foreach ( $coord as $lat => $long ):
				$output .= '[' . $lat . ',' . $long . '],';
			endforeach;
		endforeach;
		?>
        <div class="map-home" data-coords="[<?php echo rtrim( $output, ',' ); ?>]" id="map-home"></div>
        <div class="container" style="position: absolute;top: 0;left: 0;right: 0;">
            <div class="row">
				<?php while ( $query->have_posts() ) : $query->the_post(); ?>
                    <div class="col-sm-6">
                        <div class="location-link">
                            <a target="_blank" href="<?php echo get_field( 'directions_link' ); ?>"
                               class="but but-direction lazyload"
                               rel="noopener"><?php echo get_field( '_wpseo_business_address' ); ?><br/>
								<?php if ( get_field( '_wpseo_business_address_2' ) ) { ?>
									<?php echo get_field( '_wpseo_business_address_2' ); ?>
                                    <br/>
								<?php } ?>
								<?php echo get_field( '_wpseo_business_city' ); ?>,
								<?php echo get_field( '_wpseo_business_state' ); ?>
								<?php echo get_field( '_wpseo_business_zipcode' ); ?><br/>
                            </a>
                        </div>
                    </div>

				<?php endwhile; ?>
            </div>
        </div>
    </section>
<?php endif; ?>

<?php get_footer( 'mobile' ); ?>