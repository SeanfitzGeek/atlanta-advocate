<?php get_header('dark-blue'); ?>
<section class="content-wrapper">
    <div class="container">
        <div class="row">
            <article>
                <h1><?php h1_title(); ?></h1>
                <p><?php if ( has_post_thumbnail() ): ?>
                        <img src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>" class="thumbnail image left"/>
					<?php endif; ?>
					<?php the_content(); ?></p>

                <div class="block side-address clearfix">
                    <div class="address-holder">
                        <div class="address">
                            <div class="address-header">
                                <h3><?php echo  get_the_title(); ?></h3>
                            </div>
                            <address>
                                <h4><strong>The Millar Law Firm</strong></h4>
                                <p><?php echo get_field( '_wpseo_business_address' ); ?></p>
                                <?php if(get_field( '_wpseo_business_address_2' )) { ?><p><?php echo get_field( '_wpseo_business_address_2' ); ?></p><?php } ?>
                                <p><?php echo get_field( '_wpseo_business_city' ); ?>, <?php echo get_field( '_wpseo_business_state' ); ?> <?php echo get_field( '_wpseo_business_zipcode' ); ?></p>
                                <p><?php echo get_field( '_wpseo_business_phone' ); ?></p>
                                <div class="buttons">
                                    <a target="_blank" href="<?php the_field( 'directions_link' ); ?>" class="but-directions lazyload">Directions</a>
                                    <a href="#" class="but-hours">Hours</a>
                                </div>
                            </address>
                            <div class="popup-hours">
                                <h5>Hours of Operation</h5>
                                <ul>
									<?php for ( $n = 1; $n < 6; $n ++ ): ?>
										<?php $day = date( 'l', strtotime( 'Sunday +' . $n . ' days' ) ); ?>
                                        <li class="clearfix"><span><?php echo $day ?></span> <b><?php echo date( 'g:iA', strtotime( get_field( '_wpseo_opening_hours_' . strtolower( $day ) . '_from' ) ) ); ?> - <?php echo date( 'g:iA',
													strtotime( get_field( '_wpseo_opening_hours_' . strtolower( $day ) . '_to' ) ) ); ?></b></li>
									<?php endfor; ?>

                                </ul>
                                <p>Scheduled appointments after hours</p>
                                <div class="popup-arrow"></div>
                            </div>
                        </div>
                        <div class="map-holder">
                            <div data-coords="<?php echo get_field( '_wpseo_coordinates_lat' ); ?>, <?php echo get_field( '_wpseo_coordinates_long' ); ?>" data-marker="1" id="map-3" class="map"></div>
                        </div>
                    </div>
                </div>
				<?php if ( get_field( 'show_team' ) ): ?>
                    <h2><?php the_field( 'team_title' ); ?></h2>
                    <p><?php the_field( 'team_description' ); ?></p>

                    <div id="meet-location">
                        <ul>
							<?php foreach ( get_field( 'team_members' ) as $team ): ?>
                                <li>
                                    <a href="<?php echo get_the_permalink( $team ); ?>">
                                        <figure>
											<?php echo get_the_post_thumbnail( $team ); ?>
                                            <figcaption><span><?php echo get_the_title( $team ); ?></span> <?php echo get_field( 'job_title', $team ); ?></figcaption>
                                        </figure>
                                    </a>
                                </li>
							<?php endforeach; ?>
                        </ul>
                    </div>
				<?php endif; ?>

                <div>
                    <?php the_field( 'location_bottom_content' ); ?>
                </div>
	            <?= getPracticeAreasHtml(); ?>

            </article>

            <aside>
				<?php get_template_part( 'sidebars/default' ); ?>
            </aside>
        </div>
    </div>
</section>
<?php get_footer('dark-blue'); ?>
