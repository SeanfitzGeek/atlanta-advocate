<div class="map">
    <amp-iframe width="600" height="450"
                sandbox="allow-scripts allow-same-origin"
                layout="responsive"
                src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3325.867404592477!2d-84.3591663!3d33.5308329!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x88f4fa2bad7f8b5d%3A0x183fcde60dd44f74!2sThe%20Millar%20Law%20Firm!5e0!3m2!1sen!2sus!4v1574809149847!5m2!1sen!2sus">
    </amp-iframe>
</div>

<footer class="text-center color-white dark-blue-bg">
    <div class="footer-copyright">
        © The Millar Law Firm - 2019
    </div>
</footer>
</body>
</html>