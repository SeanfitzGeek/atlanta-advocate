<?php get_header('dark-blue'); ?>

<?php get_template_part('includes/breadcrumbs'); ?>

<?php get_template_part('includes/tap-buttons'); ?>

<section id="inner-headline">
	<div class="wrapper">
		<?php if ( is_day() ) : ?>
		<h1>Archive: <span><?php echo  get_the_date( 'l, F j, Y' ); ?></span></h1>                                  
		<?php elseif ( is_month() ) : ?>
		<h1>Archive: <span><?php echo  get_the_date( 'F Y' ); ?></span></h1>    
		<?php elseif ( is_year() ) : ?>
		<h1>Archive: <span><?php echo  get_the_date( 'Y' ); ?></span></h1>
		<?php elseif ( is_category() ) : ?>
		<h1>Archive: <span><?php single_term_title(); ?></span></h1>    
		<?php elseif ( is_search() ) : ?>
		<h1>Search Results for: <span><?php the_search_query(); ?></span></h1>    
		<?php else : ?>
		<h1>Our Blog</h1>    
		<?php endif; ?> 
	</div>
</section>

<section id="body">
	<div class="wrapper">
	
		<div class="content left">
			
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<div class="blog-post">
				<h2><?php h1_title(); ?></h2>
				<?php if ( '' != get_the_post_thumbnail() ) { 
					the_post_thumbnail('inner_photo');
				} ?>
				<?php the_excerpt(); ?>	
			</div>	
			<?php endwhile; endif; ?>
		
			<div class="blog-pag">
				<?php
				global $wp_query;
				$big = 999999999; // need an unlikely integer
				echo paginate_links( array(
				'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
				'format' => '/page/%#%',
				'current' => max( 1, get_query_var('paged') ),
				'total' => $wp_query->max_num_pages, 
				'type' => 'list'
				) );
				?>
			</div>		
		
		</div>
		
		<?php get_sidebar(); ?>
		
	</div>
</section>

<?php get_footer('dark-blue'); ?>