<?php


$args  = array(
	'post_type'      => 'wpseo_locations',
	'post_status'    => 'publish',
	'posts_per_page' => - 1,
	'orderby'        => 'ID',
	'order'          => 'ASC'
);
$query = new WP_Query( $args );

?>

<footer>
    <section class="dark-blue-footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-5">

                    <img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/logo-long-white-2.png"
                         alt="Atlanta Advocates Logo" class="img-responsive center-block">

                    <div class="footer-info">
                        <div class="row">
                            <div class="col-xs-6 col-sm-6">
                                <h5>Information</h5>
                                <ul>
                                    <li><a href="/privacy-policy/">Privacy Policy</a></li>
                                    <li><a href="/disclaimer/">Disclaimer</a></li>
                                    <li><a href="/site-map/">Site Map</a></li>
                                </ul>
                            </div>
                            <div class="col-xs-6 col-sm-6">
                                <h5><a href="/contact-us/" style="    color: #FFF;">Contact Us</a></h5>
                                <ul>
                                    <li><a href="https://atlantaadvocate.com/georgia/jonesboro/">Jonesboro Office</a>
                                    </li>
                                    <li><a href="https://atlantaadvocate.com/georgia/atlanta/">Atlanta Office</a></li>

                                    <li><a href="tel:770-400-0000">Call (770) 400-0000</a></li>
                                </ul>
                            </div>
                            <div class="col-xs-12 col-sm-12" style="margin: 15px 0;">
                                <div class="connect">
                                    <h5>Connect With Us</h5>

                                    <div class="col socials">
                                        <a href="https://www.facebook.com/MillarLawFirm/" class="social"
                                           aria-label="Atlanta Advocate Social media icon" target="blank"><span><i
                                                        class="fa fa-facebook" aria-hidden="true"></i></span></a>
                                        <a href="https://www.instagram.com/millarlawfirm/?hl=en" class="social"
                                           aria-label="Atlanta Advocate Social media icon" target="blank"><span><i
                                                        class="fa fa-instagram" aria-hidden="true"></i></span></a>
                                        <a href="https://twitter.com/MillarLawFirm" class="social"
                                           aria-label="Atlanta Advocate Social media icon" target="blank"><span><i
                                                        class="fa fa-twitter" aria-hidden="true"></i></span></a>
                                        <a href="https://www.linkedin.com/company/millarlawfirm/" class="social"
                                           aria-label="Atlanta Advocate Social media icon" target="blank"><span><i
                                                        class="fa fa-linkedin" aria-hidden="true"></i></span></a>
                                        <a href="https://www.youtube.com/channel/UCNVwrSaRliRBzPotJcqtnfw/featured"
                                           class="social" aria-label="Atlanta Advocate Social media icon"
                                           target="blank"><span><i class="fa fa-youtube" aria-hidden="true"></i></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="col-sm-12 col-md-7">
                    <div class="footer-map-block">
                        <h4 class="color-white text-center">Two Locations to Meet You</h4>


						<?php if ( $query->have_posts() ) : ?>
                            <section id="contacts" class="map-section">
								<?php $coords = array();
								$count        = 0;
								while ( $query->have_posts() ) : $query->the_post();
									$coords[] = array( get_field( '_wpseo_coordinates_lat' ) => get_field( '_wpseo_coordinates_long' ) );
									$count ++; endwhile;

								$output = '';
								foreach ( $coords as $coord ):
									foreach ( $coord as $lat => $long ):
										$output .= '[' . $lat . ',' . $long . '],';
									endforeach;
								endforeach; ?>

                                <div class="map-home" data-coords="[<?php echo rtrim( $output, ',' ); ?>]"
                                     id="map-home"></div>
                                <div class="accordion-contacts">
                                    <dl class="accordion style-blue-1">
										<?php $counter = 0;
										while ( $query->have_posts() ) : $query->the_post(); ?>
                                            <dt class="<?= $counter === 0 ? 'accordion-opened' : ''; ?>">
                                                <a href="#footer-contacts-<?php echo $counter; ?>"
                                                   data-coords="<?php echo get_field( '_wpseo_coordinates_lat' ); ?>, <?php echo get_field( '_wpseo_coordinates_long' ); ?>">
													<?php echo get_the_title(); ?><span
                                                            class="lazyload"></span>
                                                </a>
                                            </dt>
                                            <dd id="footer-contacts-<?php echo $counter; ?>"
                                                class='footer-accordion <?php if ( $counter == 0 ): echo ' accordion-opened'; endif; ?>'
                                                style="display: <?php echo $counter == 0 ? 'block' : 'none'; ?>"
                                                itemscope=""
                                                itemtype="http://schema.org/Attorney">

                                                <address>
                                                    <div itemprop="address" itemscope=""
                                                         itemtype="http://schema.org/PostalAddress">
                                                        <span itemprop="streetAddress"><?php echo get_field( '_wpseo_business_address' ); ?></span><br/>
														<?php if ( get_field( '_wpseo_business_address_2' ) ) { ?>
                                                            <span itemprop="streetAddress"><?php echo get_field( '_wpseo_business_address_2' ); ?></span>
                                                            <br/>
														<?php } ?>
                                                        <span itemprop="addressLocality"><?php echo get_field( '_wpseo_business_city' ); ?></span>,
                                                        <span itemprop="addressRegion"><?php echo get_field( '_wpseo_business_state' ); ?></span>
                                                        <span itemprop="postalCode"><?php echo get_field( '_wpseo_business_zipcode' ); ?></span><br/>
                                                    </div>
                                                    <strong>Phone: <span
                                                                itemprop="telephone"><?php echo get_field( '_wpseo_business_phone' ); ?></span></strong>

													<?php if ( get_field( '_wpseo_business_notes_1' ) ): ?>
<!--                                                        <br/>-->
<!--                                                        <br/>-->
<!--                                                        <span>Mailing Address:<br/>--><?php //echo get_field( '_wpseo_business_notes_1' ); ?><!--<br/>--><?php //echo get_field( '_wpseo_business_notes_2' ); ?><!--</span>-->
													<?php endif; ?>


                                                    </p>
                                                    <div class="text-center">
                                                        <a target="_blank"
                                                           href="<?php echo get_field( 'directions_link' ); ?>"
                                                           class="but but-direction lazyload"
                                                           rel="noopener">Directions</a>
                                                    </div>
                                                </address>
												<?= get_field( 'location_schema_meta_data', get_the_id() ); ?>

                                            </dd>
											<?php $counter ++; endwhile;
										wp_reset_query();
										?>
                                    </dl>
                                </div>

                            </section>
						<?php endif; ?>

                    </div>
                </div>


            </div>
            <div class="footer-copywrite">
                <p>© <?= ( new DateTime() )->format( 'Y' ); ?> The Millar Law Firm, LLC- All Rights Reserved</p>
            </div>
        </div>
</footer>

<?php echo get_field( 'cw_apps_form_js', 'option' ); ?>

<span class="fader" style="opacity: 0;"></span>
</div> <!-- End Wrapper -->
</div> <!-- End page-inner-area -->
<script type="text/javascript">
    //<![CDATA[
    var wpcf7 = {
        "apiSettings": {
            "root": "https:\/\/atlantaadvocate.com\/wp-json\/contact-form-7\/v1",
            "namespace": "contact-form-7\/v1"
        }
    };
    //]]>
</script>
<?php wp_footer(); ?>

<noscript>
    <img height="1" width="1" style="display:none;" alt=""
         src="https://px.ads.linkedin.com/collect/?pid=302513&fmt=gif"/>
</noscript>

<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "LegalService",
        "name": "The Millar Law Firm",
        "areaServed": {
            "@type": "state",
            "name": "Georgia",
            "containedInPlace": "United States"
        },
        "description": "Personal Injury Lawyers",
        "logo": "https://c9j7q2u7.stackpathcdn.com/wp-content/themes/cw-theme-two/assets/images/logo-long-white-2.png",
        "image": "https://atlantaadvocate.com/wp-content/uploads/2019/03/Billboard-BANNER_031719-100.jpg",
        "url": "https://atlantaadvocate.com/",
        "priceRange": "$0-$100000",
        "telephone": "(770) 400-0000",
        "address": [
            {
                "@type": "PostalAddress",
                "streetAddress": "1201 West Peachtree Street #2339",
                "addressLocality": "Atlanta",
                "addressRegion": "GA",
                "addressCountry": "United States",
                "postalCode": "30309"
            },
            {
                "@type": "PostalAddress",
                "streetAddress": "1201 West Peachtree Street #2339",
                "addressLocality": "Atlanta",
                "addressRegion": "GA",
                "addressCountry": "United States",
                "postalCode": "30309"
            }
        ],
        "aggregateRating": {
            "@type": "AggregateRating",
            "ratingValue": "4.8",
            "bestRating": "5",
            "worstRating": "1",
            "ratingCount": "69"
        },
        "reviews": [
            {
                "@type": "Review",
                "author": {
                    "@type": "Person",
                    "name": "Kathie Neyman"
                },
                "datePublished": "2019-04-01",
                "reviewRating": {
                    "@type": "Rating",
                    "ratingValue": "5"
                },
                "reviewBody": "I am very appreciative of the help I received from Millar Law Firm. The staff was so professional and kind from beginning to end. Everyone I encountered was very kind and attentive to my case. I dealt primarily with Joe Baker who always answered any questions in a timely manner and took the time to explain and address any concerns I had. I highly recommend this firm!"
            },
            {
                "@type": "Review",
                "author": {
                    "@type": "Person",
                    "name": "Pamela jJean Bourassa"
                },
                "datePublished": "2019-02-25",
                "reviewRating": {
                    "@type": "Rating",
                    "ratingValue": "5"
                },
                "reviewBody": "These are AWESOME Attorneys! This firm has helped me and my family for years! They helped my mom (RIP MAMA), my son and then me! They are kind, compassionate, understanding and do a great job! THEY WIN!! Joe Baker, you ROCK!!"
            }
        ]
    }
</script>

<script type="text/javascript">
    (function () {
        //	Jump to a FAQ and Open the Related Answer
        function jumpToAndOpenAnswer(aHash) {
            //	assigning `#topFaq` here will bring us back to the index list of questions, when we click back after visiting `aHash`
            location.hash = '#topFaq';
            //	`aHash` needs to include the leading `#` symbol and is the valid value of an id attribute on the page.
            location.hash = aHash;

            var anchor = aHash + ' > .qa-faq-title > .qa-faq-anchor';
            var anchorActState = aHash + ' > .qa-faq-title > .active';
            if (!document.querySelector(anchorActState)) {
                document.querySelector(anchor).click();
            }
        }

        function changeChatHeaderText() {
            var chatTitle = document.getElementsByClassName('juvo-chat-color')[0].getElementsByTagName('span')[0];
            chatTitle.innerHTML = 'Atlanta Advocate Injury Lawyers<small>How Can We Help With Your Case?</small>';
            chatTitle.style.opacity = 1;
        }

        // lazy load tracking script on a timer
        setTimeout(function () {
            _linkedin_partner_id = "302513";
            window._linkedin_data_partner_ids = window._linkedin_data_partner_ids || [];
            window._linkedin_data_partner_ids.push(_linkedin_partner_id);

            (function () {
                var s = document.getElementsByTagName("script")[0];
                var b = document.createElement("script");
                b.type = "text/javascript";
                b.async = true;
                b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";
                s.parentNode.insertBefore(b, s);
            })();

            (function (w, d, s, l, i) {
                w[l] = w[l] || [];
                w[l].push({
                    'gtm.start':
                        new Date().getTime(), event: 'gtm.js'
                });
                var f = d.getElementsByTagName(s)[0],
                    j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
                j.async = true;
                j.src =
                    'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', 'GTM-WF4XL8F');

            // timeout to assist with speed and performance
            // setTimeout(function () {
            // Clixtell Tracking Code
            var script = document.createElement('script');
            var prefix = document.location.protocol;
            script.async = true;
            script.type = 'text/javascript';
            var target = prefix + '//app.clixtell.com/scripts/latest.js';
            script.src = target;
            var elem = document.head;
            elem.appendChild(script);

            //  Twitter universal website tag code
            !function (e, t, n, s, u, a) {
                e.twq || (s = e.twq = function () {
                    s.exe ? s.exe.apply(s, arguments) : s.queue.push(arguments);
                }, s.version = '1.1', s.queue = [], u = t.createElement(n), u.async = !0, u.src = '//static.ads-twitter.com/uwt.js',
                    a = t.getElementsByTagName(n)[0], a.parentNode.insertBefore(u, a))
            }(window, document, 'script');
            // Insert Twitter Pixel ID and Standard Event data below
            twq('init', 'o1axe');
            twq('track', 'PageView');

            // FB Tracking Code
            !function (f, b, e, v, n, t, s) {
                if (f.fbq) return;
                n = f.fbq = function () {
                    n.callMethod ?
                        n.callMethod.apply(n, arguments) : n.queue.push(arguments)
                };
                if (!f._fbq) f._fbq = n;
                n.push = n;
                n.loaded = !0;
                n.version = '2.0';
                n.queue = [];
                t = b.createElement(e);
                t.async = !0;
                t.src = v;
                s = b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t, s)
            }(window,
                document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');

            fbq('init', '320668835423599');
            fbq('set', 'agent', 'tmgoogletagmanager', '320668835423599');
            fbq('track', "PageView");
            //End Facebook Pixel Code

            // chimptastic
            !function (c, h, i, m, p) {
                m = c.createElement(h), p = c.getElementsByTagName(h)[0], m.async = 1, m.src = i, p.parentNode.insertBefore(m, p)
            }(document, "script", "https://chimpstatic.com/mcjs-connected/js/users/8571009818cd0a863463f19af/4bc74922c5c86008c2cad3f18.js");

            jQuery('.add-delayed-lazyload').addClass('lazyload');

            var fd = document.createElement('script');
            fd.type = 'text/javascript';
            fd.async = true;
            fd.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.juvoleads.com/tag/926453398.js?v=' + Math.floor(Math.random() * 9999999999);
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(fd, s);

            if (document.getElementsByClassName('juvo-chat-color')[0] !== undefined)  {
                console.log('Accessed Tag..');
                changeChatHeaderText();

            } else {
                console.log('chat element not dom ready');
                setTimeout(function () {
                    console.log('set timeout to fire chat window change..');
                    changeChatHeaderText();

                }, 1500);
            }
        }, 3500);
    })();
</script>
</body>
</html>