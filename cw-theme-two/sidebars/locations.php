<?php
global $locationId;


?>
<div id="locations-sidebar">

	<?php if ( $map = get_field( 'sidebar_map', $locationId ) ) : ?>
        <div class="map-of-clayton-container" style="text-align: center;">
            <img src="<?= $map['url']; ?>"
                 alt="<?= $map['alt']; ?>" class="center-block img-responsive"/>
        </div>

        <hr>
	<?php endif; ?>
    <div class="video-container">
        <h3><?= get_field( 'sidebar_video_title', $locationId ); ?></h3>
        <div class="video-frame">
			<?= get_field( 'sidebar_video', $locationId ); ?>
        </div>

        <div class="video-content">
			<?= get_field( 'sidebar_video_content', $locationId ); ?>
        </div>
    </div>

    <hr>

    <div class="meet-attorney-slider">
        <h3><?= get_field( 'meet_attorney_title', $locationId ); ?></h3>

        <!-- start -->
        <div id="attorney_slider" class="slider clearfix">
			<?php foreach ( get_field( 'team_members', $locationId ) as $team ): ?>
                <div class="inner-wrap">
                    <a href="<?php echo get_the_permalink( $team ); ?>">
                        <figure>
                            <div class="attorney-img">
								<?php echo get_the_post_thumbnail( $team ); ?>
                            </div>
                            <figcaption>
                                <span><?php echo get_the_title( $team ); ?></span>
                            </figcaption>
                        </figure>
                    </a>
                </div>
			<?php endforeach; ?>
        </div>
    </div>
    <!-- end slider -->
    <hr>
    <div class="practice-areas-sidebar">
        <h3><?= get_field( 'practice_areas_title', $locationId ); ?></h3>
		<?= getpracticeareashtml( 'col-sm-6', false ); ?>
    </div>
</div>