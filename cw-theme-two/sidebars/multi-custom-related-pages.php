<?php
	
	global $post;
	
	$curr_id = $post->ID;	
	
	//	repeater option field
	$rows = get_field('field_5cf9d81504c3e', 'option');
	if( ! $rows ){
		//bail early if there aren't any parent pages defined in ACF options
		exit;
	}
	$rel_pages_title = get_field('field_5cf9d78904c3d', 'option');
	
	//	get flag to see if this sidebar was enabled on the Edit Page screen
	$show_rel_pages = get_field( 'show_rel_pages_sidebar', $curr_id, true );
	if( true == $show_rel_pages ){
		// then show the sidebar on this page
?>
		<div class="custom-related-pages">
			<h3><?php echo $rel_pages_title; ?></h3>
<?php
		function find_pages($row){
			$pages = get_pages(array('post_type' => 'page',
									 'child_of'  => $row['parent_page']->ID,
									 'post_status'    => 'publish',
								));
			return array( $row['parent_page']->ID, $row['parent_page'], 'child_pages' => $pages );
		}
		$rel_page_groups = array_map('find_pages', $rows);

		$my_tree = array();
		
		//	callback for `array_map`
		function find_child_ids($obj){
			return $obj->ID;
		}
		function exclude_children_according_to_shown($obj){
			return ( true == get_field( 'show_rel_pages_sidebar', $obj->ID, true ) );
		}
		foreach ( $rel_page_groups as $parent ) {
			if( $curr_id == $parent[0] || in_array( $curr_id, array_map( 'find_child_ids', $parent['child_pages'] ) ) ){
				//	we found the subtree we want, using the parent's `page-id` a.k.a., `$parent[0]`
				//	now add the parent page, followed by each of its children, to the `my_tree` list
				$children = array_filter( $parent['child_pages'], 'exclude_children_according_to_shown', 0 );
				$my_tree = array_merge( array($parent[1]), $children );
				break;
			}
		}
		
		$parent_id = $my_tree[0]->ID;
		$pages_to_show = array();
		foreach( $my_tree as $page ){
			$css_classes = '';
			if( $curr_id == $page->ID ){
				//	reflexive page link
				$css_classes .= 'reflexive ';
			}
			if( $parent_id == $page->ID ){
				//	the parent page
				$css_classes .= 'custom-related-parent ';
			}
			else{
				//	a child page
				$css_classes .= 'custom-related-child ';
			}
			$css_classes .= 'id-' . $page->ID;
			$pages_to_show[] = array( $css_classes, $page );
		}
?>
			<ul>
<?php	foreach($pages_to_show as $page){
			//	strict equality test for `strpos` is deliberate
			if( false === strpos( $page[0], 'reflexive' ) ){
				//	does not have the css class `reflexive` anywhere in the string variable `css_classes` a.k.a., `$page[0]`
				//	so print an actual link to this page
?>
				<li>
					<a class="<?php echo $page[0]; ?>" href="<?php echo get_page_link( $page[1]->ID ); ?>">
						<?php echo $page[1]->post_title; ?>
					</a>
				</li>
<?php
			}
			else{
				//	has a reflexive css class, so print out a span (not an active link) that indicates in the sidebar which page we are on
?>
				<li>
					<span class="<?php echo $page[0]; ?>">
						<?php echo $page[1]->post_title; ?>
					</span>
				</li>
<?php
			}	
		}
?>
			</ul>
		</div>
<?php
	}
?>