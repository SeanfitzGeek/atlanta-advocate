<?php 
/*
* Template Name: Landing Page
*/

require_once 'includes/Mobile_Detect.php';
$detect = new Mobile_Detect;

get_header(); ?>

<div id="display-area" class="practice-area-page">
	
  <?php
  if ( $detect->isMobile() ) {
       $background = get_field('mobile_banner_image');
  } else {
       $background = get_field('banner_image');
  }
  ?>
  <div id="banner" style="background-image:url(<?php echo $background; ?>);"> 
  	<div class="wrapper">
      <div class="banner-desc clearfix">
      	<?php the_field('banner_text'); ?>
      </div><!-- /banner-desc -->
    </div><!-- /wrapper -->
  </div><!-- /banner -->