
// map tiles
var baseLayer = L.tileLayer('https://api.mapbox.com/styles/v1/{id}/cit3b0oa3004e2xo9prtuw945/tiles/{z}/{x}/{y}?access_token={accessToken}', {
	attribution: '',
	subdomains: 'abcd',
	id: 'cw-jbutrymowicz',
	accessToken: 'pk.eyJ1IjoiY3ctamJ1dHJ5bW93aWN6IiwiYSI6IjFlNmQ3YjNiNDBjY2ZhYTNmMzZmOWEzNjZmMDQ0M2Q3In0.yNkDgCQvB4njkVTRF89T6Q'
}); 


//heatmap.js config
var cfg = {
	// radius should be small ONLY if scaleRadius is true (or small radius is intended)
	// if scaleRadius is false it will be the constant radius used in pixels
	"radius": 0.05,
	"maxOpacity": .4, 
	// scales the radius based on map zoom
	"scaleRadius": true, 
	// if set to false the heatmap uses the global maximum for colorization
	// if activated: uses the data maximum within the current map boundaries 
	//   (there will always be a red spot with useLocalExtremas true)
	"useLocalExtrema": true,
	// which field name in your data represents the latitude - default "lat"
	//latField: 'latitude',
	// which field name in your data represents the longitude - default "lng"
	//lngField: 'longitude',
	// which field name in your data represents the data value - default "value"
	//valueField: 'count'
};

var heatmapLayer = new HeatmapOverlay(cfg);



// set up map and add tile baseLayer & empty heatmapLayer (to be filled later during renderMap())
var map = new L.Map('map', {
	center: new L.LatLng(31.95,-83.032),
	zoom: 7,
	minZoom: 5,
	scrollWheelZoom:false,
	layers: [baseLayer, heatmapLayer]
});

// create empty layer toggle control pane
// we will add layers to it below, after marker groups are created
var toggleLayers = L.control.layers().addTo(map);


// add mobile map options
if (L.Browser.mobile || L.Browser.touch) {
    //map.dragging.disable();  // causes mobile touch swiping/scrolling to not work over the map.
    map.setView([30.5,-84],6);
}




jQuery(document).ready(function() {

   	// render map layers
	renderHeatMap();	
	
	// load smooth scroll on the links at page load
	smoothScroll();
	
	// year switching on link click
	yearSwitching();	
	
	
	// create the back to top button
	jQuery('body').prepend('<a href="#" class="back-to-top">Back to Top</a>');
	
	var amountScrolled = 300;
	
	jQuery(window).scroll(function() {
	    if (jQuery(window).scrollTop() > amountScrolled) {
	        jQuery('a.back-to-top').fadeIn('slow');
	    } else {
	        jQuery('a.back-to-top').fadeOut('slow');
	    }
	});
	
	jQuery('a.back-to-top, a.simple-back-to-top').click(function() {
	    jQuery('html, body').animate({
	        scrollTop: 0
	    }, 700);
	    return false;
	});
	
	jQuery(function() {
	    jQuery('a[href*="#"]:not([href="#"])').click(function() {
	        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
	            var target = jQuery(this.hash);
	            target = target.length ? target : jQuery('[name=' + this.hash.slice(1) + ']');
	            if (target.length) {
	                jQuery('html, body').animate({
	                    scrollTop: target.offset().top
	                }, 1000);
	                return false;
	            }
	        }
	    });
	});	
	
    jQuery('.first-h3').addClass("animate-hidden").viewportChecker({
        classToAdd: 'animate-visible animated fadeInDownBig',
        offset: 100
    });

    jQuery('.second-h3').addClass("animate-hidden").viewportChecker({
        classToAdd: 'animate-visible animated fadeInLeftBig',
        offset: 100
    });

    jQuery('header h1').addClass("animate-hidden").viewportChecker({
        classToAdd: 'animate-visible animated fadeInRightBig',
        offset: 100
    });

    jQuery('.border').addClass("animate-hidden").viewportChecker({
        classToAdd: 'animate-visible animated fadeIn',
        offset: 100
    });

    jQuery('header p').addClass("animate-hidden").viewportChecker({
        classToAdd: 'animate-visible animated fadeInUpBig',
        offset: 100
    });

    jQuery('.statclaim1 li').addClass("animate-hidden").viewportChecker({
        classToAdd: 'animate-visible animated slideInLeft',
        offset: 100
    });

    jQuery('.statclaim3 li').addClass("animate-hidden").viewportChecker({
        classToAdd: 'animate-visible animated slideInRight',
        offset: 100
    });

    jQuery('.btn').addClass("animate-hidden").viewportChecker({
        classToAdd: 'animate-visible animated rubberBand',
        offset: 100
    });

    jQuery('.progressbar-container').addClass("animate-hidden").viewportChecker({
        classToAdd: 'animate-visible animated bounceIn',
        offset: 100
    });

    jQuery('.resource-box').addClass("animate-hidden").viewportChecker({
        classToAdd: 'animate-visible animated fadeIn',
        offset: 100
    });

    // get box count
    var count = 0;
    var checked = 0;

    function countBoxes() {
        count = jQuery("input[type='checkbox']").length;
        //console.log(count);
    }

    countBoxes();
    jQuery(":checkbox").click(countBoxes);

    // count checks

    function countChecked() {
        checked = jQuery("input:checked").length;

        var percentage = parseInt(((checked / count) * 100), 10);
        jQuery(".progressbar-bar").progressbar({
            value: percentage
        });
        jQuery(".progressbar-label").text(percentage + "%");
    }

    countChecked();
    jQuery(":checkbox").click(countChecked);
			

});




// HEATMAP

function renderHeatMap (year){

	
	// get year if link was clicked, otherwise default to 2014
	year = year ? year : "2014";
	

	// grabs data from json file and sends through our filtering/formatting
	function getHeatMapData(callback){
		
		//get json from local file
		jQuery.getJSON( "/wp-content/themes/cw-theme-two/missed/missed-fars/all-ga-crashes-"+year+".json", function( rawData ) {
		    
		    callback(rawData);
	   
		});	
		
	}	

	
    getHeatMapData( function(callback){
		
		var data = callback;
		
		// set up array for data to be used in the heatmap layer group
		var heatmapData = [];
		
		// pushing items into array each by each and then add markers & list items
		for(i=0;i<data.length;i++){	
			
			// heatmap data gathering
			
			var heatmapDataObj = {};
			
		    heatmapDataObj.lat = data[i][0];
		    heatmapDataObj.lng = data[i][1]; 
			heatmapDataObj.value = 15;
			heatmapDataObj.fatals = data[i][2];
		    
		    heatmapData.push(heatmapDataObj);
					
		} // end data loop
						
		// set heatmap layer with the data we gathered above
		var mapData = {
		  max: 15,
		  data: heatmapData
		};   	
	   	
	   	heatmapLayer.setData(mapData);
	   	
	   	// render total fatalities overlay
	   	totalFatalities(year,heatmapData);	   	
	   	
		// render line graph overlay
		renderLineGraph();	   	
	   		
    });   
   

}

// END HEATMAP



// TOTAL FATALITIES

function totalFatalities (year,heatmapData){
	
	year = year;
	heatmapData = heatmapData;	
	
	
	// count the whole state's fatalities & crashes
	var gaTotalCrashes = heatmapData.length;
	var gaFatalitiesCount = 0;
	
	for(var i=0; i < heatmapData.length; i++){
		if(!isNaN(heatmapData[i].fatals)) {
			gaFatalitiesCount += parseInt(heatmapData[i].fatals);
		}
	}
	
	
	// add control pane overlay to display totals
	var totalsOverlayControl = L.Control.extend({
	    options: {
	        position: 'bottomleft'
	    },
	    onAdd: function (map) {
	        // create the control container with a particular class name
	        var container = L.DomUtil.create('div', 'totals-overlay');
	        return container;
	    }
	});
	map.addControl(new totalsOverlayControl());	
	
	
	// add totals to the overlay
	jQuery('.totals-overlay').append('<div class="left-pane"><h4>'+year+'</h4><div class="totals-txt"><p class="total-fatalities"><strong>Total Fatalities: <span>'+gaFatalitiesCount+'</span></strong></p><p class="total-crashes"><strong>Total Crashes: <span>'+gaTotalCrashes+'</span></strong></p></div><span class="totals-disclaimer">(Total Fatalities may be higher than Total Crashes due to multiple cars/victims per crash.)</span></div><div class="right-pane"><div id="yearsGraph"></div></div>');	
			

}

// END TOTAL FATALITIES



// LINE GRAPH

function renderLineGraph (graphSeries) {
	
	
	var data = {
        labels: [2005,2006,2007,2008,2009,2010,2011,2012,2013,2014],
        series: [
	        [1729,1693,1641,1495,1292,1247,1226,1192,1179,1164],
	        [1582,1557,1492,1370,1180,1150,1119,1126,1085,1080]
        ]
    };
     
		
	// create chart
    new Chartist.Line('#yearsGraph', data, {
		fullWidth: true,
	    chartPadding: {
		    top: 0,
		    right: 20,
		    bottom: 0,
		    left: 0
	    },		
        low: 1000, 
        axisY: {
	        scaleMinSpace: 20, 
	        onlyInteger: true
	    },
	    height: 150,	
		classNames: {
		    chart: 'ct-chart-line',
		    label: 'ct-label',
		    labelGroup: 'ct-labels',
		    series: 'ct-series',
		    line: 'ct-line',
		    point: 'ct-point',
		    area: 'ct-area',
		    grid: 'ct-grid',
		    gridGroup: 'ct-grids',
		    vertical: 'ct-vertical',
		    horizontal: 'ct-horizontal',
		    start: 'ct-start',
		    end: 'ct-end'
		},    
        //showArea: true, 
        plugins: [
	    	Chartist.plugins.tooltip()	    	
		]
    });

	
}

// END LINE GRAPH



// ADDITIONAL FUNCTIONS

function smoothScroll () {
	
	/* smooth scrolling down to anchor */
	jQuery('a.scroll').on('click',function (e) {
		e.preventDefault();

	    var target = this.hash,
	    $target = jQuery(target);

	    jQuery('html, body').stop().animate({
	        'scrollTop': $target.offset().top
	    }, 1200, 'swing', function () {
	        window.location.hash = target;
	    });
	});	
	
}

function yearSwitching () {
	
	// update map data & selected classes on click
	jQuery('.year-nav a.year').on('click', function(element){
		
		// get chosen year from this link's text
		var year = jQuery(this).text();
		
		// remove all 'selected' classes and add 'selected' class to the clicked link
	    jQuery('.year-nav a.year').removeClass('selected');
	    jQuery(this).addClass('selected');
		
		// remove all markers & toggle links before rendering new map layers	
	    toggleLayers.removeLayer(heatmapLayer);
	    jQuery('.totals-overlay').remove();
		
		// render new map layer data with selected year
		renderHeatMap(year);	
		
	});		
	
}