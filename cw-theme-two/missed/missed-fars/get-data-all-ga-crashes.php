<?php

//------ ACCIDENT DATA FROM FARS ----------------------------//

//GET ALL ACCIDENT DATA
function get_all_accident_post_data() {

	
	// default no results
	$results = array();

	// DATABASE CREDS
	$username = "dataserv_devfars";
	$password = "w+0VXof=Jp0t";
	$database = "dataserv_FARS";
	$hostname = "104.131.8.157";

	// connection to the database
	if ($db = mysqli_connect($hostname, $username, $password, $database)) {

		$year_min = 2005;
		$year_max = 2005;
		
		$sql = '';

		/*
		accident.`STATE` = 13   (georgia)
		// accident.`ST_CASE`   AS case_id,
		*/
		for($i = $year_min; $i <= $year_max; $i++) {
			$sql .= "
				(
					SELECT
						accident.`LATITUDE`  AS lat,
						accident.`LONGITUD`  AS lng, 
						accident.`FATALS`  	 AS fatals 
					FROM
						FARS_{$i}_accident AS accident
					WHERE
						accident.`STATE` = 13
				)
			";

			if ($i != $year_max) {
				$sql .= " UNION ";
			}
		}

		$query = $db->query($sql);

		if ($query) {
			// get the rows!
			while ( $row = $query->fetch_assoc() ) {
				$results[] = array(		
					$row['lat'],
					$row['lng'], 
					cleanNumbers($row['fatals']) 
				);
			}
    			
		} else {
			$results[] = $db->error;
		}

	}

	return $results;
}//end function get_all_accident_post_data()




function cleanNumbers($value) {
	
	if(is_numeric($value)) {
		return $value;
	}
	
}



function print_out_fars_data() {
	
	$result = "Error: No Results";
	
	$result = json_encode(get_all_accident_post_data());
	
	// put the new encoded json object into a local file
	file_put_contents('/home/miscslientwork/public_html/wp-content/themes/twentyfifteen/millar-mixon/fars/all-ga-crashes-2005.json',$result);		
	
	// and/or echo it out on the page
	echo $result;			
	
}


// uncomment to print and save fars data
print_out_fars_data();