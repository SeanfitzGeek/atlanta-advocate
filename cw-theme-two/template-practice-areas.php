<?php
/* 280 W */
/*
  * Template Name: Practice Areas Updated
  */

get_header( 'dark-blue' );

$practiceAreaList = get_field( 'practice_areas_categories' );
$dropdown         = 0;
$counter          = 0;
$subCatCount      = 0;
$listCounter      = 0;
?>
    <section class="practice-area-main">
        <div class="col-half">
            <div class="practice-area-list data-dropdown-opener-list">
                <ul>
					<?php foreach ( $practiceAreaList as $item ) : ?>

						<?php $title = trim( str_replace('&', 'and', str_replace( ' ', '-', strip_tags( strtolower( $item['practice_area_title'] ) ) ) ));?>
                        <li>
                            <a href="#<?= $title; ?>"
                               class="dropdown-opener"
                               data-opener="#<?= $title;?>"><?= $item['practice_area_title']; ?></a>
                        </li>
					<?php endforeach; ?>
                </ul>
            </div>
        </div>
        <div class="col-half">
            <div class="practice-area-shader"></div>
            <div class="lazyload practice-area-main-background"
                 data-bg="<?= get_field( 'main_practice_area_background' ); ?>">
                <div class="relative">
                    <div class="red-box">
                        <h2>Practice Areas</h2>
                        <div class="content">
                            <p><?= get_field( 'main_call_to_action' ); ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </section>

    <section class="practice-area-dropdowns block-dropdowns">
        <div class="panel-group" id="practice-area-categories-dropdown" role="tablist" aria-multiselectable="true">
			<?php foreach ( $practiceAreaList as $item ): ?>
				<?php $title = trim( str_replace('&', 'and', str_replace( ' ', '-', strip_tags( strtolower( $item['practice_area_title'] ) ) ) ));?>
                <div class="panel panel-default panel-main"
                     id="<?= $title; ?>-opener">

                    <div class="panel-heading panel-heading-main lazyload" role="tab"
                         data-bg="<?= $item['practice_area_title_background']; ?>">


                        <a role="button" data-toggle="collapse" data-parent="#practice-area-categories-dropdown"
                           href="#<?= $title;?>" id="heading-<?= $counter; ?>-opener" aria-expanded="true"
                           class="panel-button collapsed"
                           aria-controls="heading-<?= $counter; ?>">
                            <h2 class="panel-title"><?= $item['practice_area_title']; ?> </h2>

                            <div class="panel-click">Click To Collapse</div>

<!--                            <a href="#" class="panel-top"><span class="glyphicon glyphicon-menu-up"></span></a>-->
                            <a role="button" data-toggle="collapse" data-parent="#counties-dropdown" href="#<?= $title; ?>" class="panel-activate"><span class="glyphicon glyphicon-menu-down"></span></a>

                        </a>


                        <div class="panel-background-shader"></div>

                    </div>
                    <div id="<?= $title;?>" class="panel-collapse collapse" role="tabpanel">
                        <div class="container">
                            <div class="panel-body">
                                <div class="panel-category-description">
									<?= $item['practice_area_category_description']; ?>
                                </div>

                                <div class="row">
									<?php
									$colSize = $item['practice_area_category_video'] === '' ? '12' : '6';
									?>
                                    <div class="col-sm-<?= $colSize; ?>">
                                        <div class="panel-category-video">
											<?= $item['practice_area_category_video']; ?>
                                        </div>
                                    </div>

                                    <div class="col-sm-<?= $colSize; ?>">
                                        <div class="panel-category-sub-category-information">
											<?php foreach ( $item['practice_areas_categories_sub_information'] as $subInformation ) : ?>
                                                <div class="panel-group panel_sub_cat_dropdown"
                                                     id="sub_category_dropdown" role="tablist"
                                                     aria-multiselectable="true">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading" role="tab" id="headingOne">
                                                            <h4 class="panel-title sub_cat_dropdown">


                                                                <?php $link = isset($subInformation['sub_field_link']) && !empty($subInformation['sub_field_link'])  ? true : false;?>
                                                                <a href="<?= $link ? $subInformation['sub_field_link'] : '#sub_cat_counter_' . $subCatCount;?>"
                                                                   class="non-auto-panel"
                                                                    <?= $link ? '' : 'role="button" data-toggle="collapse"';?>
                                                                   data-parent="#sub_category_dropdown">
                                                                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
																	<?= $subInformation['sub_info_title']; ?>
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="sub_cat_counter_<?= $subCatCount ++; ?>"
                                                             class="panel-collapse collapse" role="tabpanel">
                                                            <div class="panel-body">
																<?= $subInformation['sub_info_content']; ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
											<?php endforeach; ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel-category-full-information">
									<?= $item['practice_area_category_full_information']; ?>
                                </div>

                                <div class="panel-category-thumbnails">
                                    <div class="row">
										<?php
                                        $imageCounter = count($item['practice_areas_categories_image_gallery']);
                                        switch($imageCounter) {
                                            case 3 :
	                                            $colSize = 4;
                                                break;
                                            case 2:
                                                $colSize = 6;
                                                break;
                                            case 1:
                                                $colSize = 12;
                                                break;

                                            default:
                                                $colSize = 4;
                                        }
										foreach ( $item['practice_areas_categories_image_gallery'] as $thumbnail ) :
											$thumbnail = $thumbnail['practice_areas_categories_image_gallery_image'];
											?>
                                            <div class="col-xs-<?= $colSize; ?>">
                                                <img src="<?= $thumbnail['sizes']['post-square']; ?>" alt="<?= $thumbnail['alt']; ?>"
                                                     class="img-responsive">
                                            </div>

										<?php endforeach; ?>
                                    </div>
                                </div>
                                <div class="back-to-top">
                                    <a href="#">Back to Top <span class="glyphicon glyphicon-menu-up"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
			<?php endforeach; ?>
        </div>
    </section>

<?php get_footer( 'dark-blue' ); ?>